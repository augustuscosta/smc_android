package br.com.otgmobile.trackteam.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.cloud.rest.GoogleGeolocationCloud;
import br.com.otgmobile.trackteam.database.AgenteDAO;
import br.com.otgmobile.trackteam.database.CercaDAO;
import br.com.otgmobile.trackteam.database.EnqueteDAO;
import br.com.otgmobile.trackteam.database.FilmagemDAO;
import br.com.otgmobile.trackteam.database.FotoDAO;
import br.com.otgmobile.trackteam.database.HistoricoDAO;
import br.com.otgmobile.trackteam.database.MaterialDAO;
import br.com.otgmobile.trackteam.database.MensagemDAO;
import br.com.otgmobile.trackteam.database.OcorrenciaDAO;
import br.com.otgmobile.trackteam.database.PerguntaDAO;
import br.com.otgmobile.trackteam.database.RotaDAO;
import br.com.otgmobile.trackteam.database.VeiculoDAO;
import br.com.otgmobile.trackteam.gps.DLGps;
import br.com.otgmobile.trackteam.gps.DLGpsObserver;
import br.com.otgmobile.trackteam.model.Agente;
import br.com.otgmobile.trackteam.model.Cerca;
import br.com.otgmobile.trackteam.model.ConfiguracaoVisualizaOcorrencia;
import br.com.otgmobile.trackteam.model.GeoPonto;
import br.com.otgmobile.trackteam.model.Historico;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.Rota;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.model.Veiculo;
import br.com.otgmobile.trackteam.service.DaemonManagerService;
import br.com.otgmobile.trackteam.service.PositionService;
import br.com.otgmobile.trackteam.service.SocketService;
import br.com.otgmobile.trackteam.task.SyncCallBack;
import br.com.otgmobile.trackteam.task.SyncTask;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.NavigationUtil;
import br.com.otgmobile.trackteam.util.Session;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;


public class MapViewActivity extends GenericFragmentActivity implements DLGpsObserver ,SyncCallBack, OnMarkerClickListener{

	private static final int USER_INTERACTION_TIME = 15000;
	private GoogleMap map;

	private List<Veiculo> veiculos;
	private List<Agente> agentes;
	private List<Cerca> cercas;
	private List<Rota> rotas;
	private List<Ocorrencia> ocorrencias;
	private Long touchedTime;
	private EditText addressTextView;
	private FrameLayout root;
	private static SyncTask syncTask;
	private static ReverseGeocoderTask reverseGeocodeTask;



	private boolean centerZoom;

	private Ocorrencia selectedOcorrencia;
	private OcorrenciaDAO ocorrenciaDAO;
	private PerguntaDAO perguntaDAO;
	private EnqueteDAO enqueteDAO;
	private HistoricoDAO historicoDAO;
	private CercaDAO cercaDAO;
	private RotaDAO rotaDAO;
	private MensagemDAO mensagemDAO;
	private MaterialDAO materialDAO;

	// Connection  Status
	private Button telemetriaStatus;
	private Button socketStatus;
	private Button bateriaStatus;


	@Override
	protected void onCreate(Bundle bundler) {
		super.onCreate(bundler);
		setContentView(R.layout.map);
		initMap();
		configureActivity();
		startServices();
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu map_menu) {
		MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.map_menu, map_menu);
        return true;
	};
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        /*case R.id.map_menu_new_occurrence:
        	registerNewOcorrencia();
            return true;*/
 
        case R.id.map_menu_occurrences:
        	seePendingOcurrences();
            return true;
 
        case R.id.map_menu_materials:
        	seeAvailableMaterials();
            return true;
 
        case R.id.map_menu_sync:
            syncButtonClick();
            return true;
 
        case R.id.map_menu_chat:
            startChat();
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	private void initMap(){
		SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        map = fm.getMap();
        map.setMyLocationEnabled(true);
        map.setOnMarkerClickListener(this);
	}

	protected void configureActivity() {
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		telemetriaStatus = (Button) findViewById(R.id.telemetriaStatusIcon);
		bateriaStatus = (Button) findViewById(R.id.bateriaStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
		addressTextView = (EditText) findViewById(R.id.addressTextView);
		root = (FrameLayout) findViewById(R.id.root);
		registerForContextMenu(root);
	}


	@Override
	protected void onResume() {
		super.onResume();
		DLGps.addGpsObserver(this, this);
		updateStatusBySharedPreferences();
		centerMap(centerZoom);
		populate(true);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.map_ocorrencia_context_menu, menu);
		menu.setHeaderTitle(selectedOcorrencia.getDescricao());
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.visualizar:
			visualiseOcorrencia(selectedOcorrencia);
			break;
		case R.id.navigate:
			navigateToOcorrencia(selectedOcorrencia);
			break;
		}
		return true;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		touchedTime = AppHelper.getCurrentTime();
		return super.dispatchTouchEvent(ev);
	}



	private void navigateToOcorrencia(Ocorrencia ocorrencia) {
		NavigationUtil.openNavigationFromStopMap(this, ocorrencia.getEndereco().getLatitude(), ocorrencia.getEndereco().getLongitude());
	}

	private void visualiseOcorrencia(Ocorrencia ocorrencia) {
		Intent intent = new Intent(MapViewActivity.this, OcorrenciaPreviewActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivity(intent);
	}

	private void centerMap(final boolean centerZoom){
		Location location = DLGps.getLastLocation(this);
		if(location != null){
			centerMap(getLatLngByLocation(location));
		}else{
			//TODO need something like center map by overlays method
		}
	}

	@Override
	protected void onPause() {
		stopSyncTask();
		stopGeocodeTask();
		DLGps.removeGpsObserver(this, this);
		super.onPause();
	}
	
	private void populate(boolean centerZoom){
		CercaDAO cercaDAO = new CercaDAO(this);
		cercas = cercaDAO.fetchAllParsed();
		RotaDAO rotaDAO = new RotaDAO(this);
		rotas = rotaDAO.fetchAllParsed();
		ocorrencias = ocorrenciaDAO().fetchAllValidadaParsed();
		VeiculoDAO veiculoDAO = new VeiculoDAO(this);
		veiculos = veiculoDAO.fetchAllParsed();
		agentes = new AgenteDAO(this).fetchAllParsed();
		addOverlaysToMap(centerZoom);
	}

	private void addOverlaysToMap(boolean centerZoom){
		cleanMap();
		addCercas();
		addRotas();
		addOcorrencias();
		addVehicles();
		addAgentes();
		centerMap(centerZoom);
	}


	private void cleanMap(){
		map.clear();
	}

	public boolean isUserInteracting(){
		Long currentTime = AppHelper.getCurrentTime();
		if(touchedTime == null){
			return false;
		}else{
			if(currentTime - touchedTime < USER_INTERACTION_TIME){
				return true;
			}else{
				return false;
			}
		}

	}

	private void addCercas(){
		if(cercas == null){
			return;
		}
		for(Cerca cerca : cercas){
			if(cerca.getGeoPonto() != null){
				addCerca(cerca.getGeoPonto());
			}
		}
	}

	private void addRotas(){
		if(rotas == null){
			return;
		}
		for(Rota rota : rotas){
			if(rota.getGeoPonto() != null){
				addRota(rota.getGeoPonto());
			}
		}
	}

	// To draw routes
	private void addCerca(List<GeoPonto> path){
		if(path.isEmpty())
			return;
		List<LatLng> points = new ArrayList<LatLng>();
		for(GeoPonto geoPonto : path){
			LatLng point = geoPonto.getLatLng();
			points.add(point);
		}
		PolygonOptions polygon = getPolygonOptionsForCerca(points);
		map.addPolygon(polygon);
	}

	private PolygonOptions getPolygonOptionsForCerca(List<LatLng> points) {
		PolygonOptions polygon = new PolygonOptions()
	     .addAll(points)
	     .strokeColor(Color.RED);
		return polygon;
	}

	private void addRota(List<GeoPonto> path){
		if(path.isEmpty())
			return;
		List<LatLng> points = new ArrayList<LatLng>();
		for(GeoPonto geoPonto : path){
			points.add(geoPonto.getLatLng());
		}
		map.addPolygon(getPolygonOptionsForRota(points));
	}
	
	private PolygonOptions getPolygonOptionsForRota(List<LatLng> points) {
		PolygonOptions polygon = new PolygonOptions()
	     .addAll(points)
	     .strokeColor(Color.BLACK);
		return polygon;
	}

	private void addVehicles(){
		if(veiculos == null){
			return;
		}
		for(Veiculo vehicle : veiculos){
			if(vehicle.getUltimoHistorico() != null){
				map.addMarker(getVehicleMarker(vehicle));
			}
		}
	}
	
	private MarkerOptions getVehicleMarker(Veiculo vehicle){
		String titulo = "";
		String subTitulo = "";
		if(vehicle.getPlaca() != null)
			titulo = vehicle.getPlaca();
		if(vehicle.getMarca() != null)
			subTitulo = vehicle.getMarca();
		
		MarkerOptions marker = new MarkerOptions()
        .position(vehicle.getUltimoHistorico().getLatLng())
        .title(titulo)
        .snippet(subTitulo)
        .icon(BitmapDescriptorFactory.fromResource(getVeiculosDrawable(vehicle)));
		
		return marker;
	}

	private int getVeiculosDrawable(Veiculo vehicle) {
		if(Session.getVeiculoID(this) == vehicle.getId())
			return R.drawable.meu_veiculo;
		return R.drawable.viatura;
	}

	private void addAgentes() {
		if(agentes == null){
			return;
		}
		for(Agente agente : agentes){
			if(agente.getUltimoHistorico() != null){
				map.addMarker(getAgenteMarker(agente));
			}
		}
	}
	
	private MarkerOptions getAgenteMarker(Agente agente){
		String titulo = "";
		String subTitulo = "";
		if(agente.getNome() != null)
			titulo = agente.getNome();
		if(agente.getMatricula() != null)
			subTitulo = agente.getMatricula();
		
		MarkerOptions marker = new MarkerOptions()
        .position(agente.getUltimoHistorico().getLatLng())
        .title(titulo)
        .snippet(subTitulo)
        .icon(BitmapDescriptorFactory.fromResource(getAgentesDrawable(agente)));
		
		return marker;
	}

	private int getAgentesDrawable(Agente agente) {
		if(Session.getAgenteID(this) == agente.getId()){
			return R.drawable.meu_agente;
		}
		return R.drawable.agente;
	}

	private void addOcorrencias(){
		if(ocorrencias == null || ocorrencias.size() < 1){
			return;
		}
		for(Ocorrencia ocorrencia : ocorrencias){
			if(ocorrencia.getEndereco() != null){
				map.addMarker(getOcorrenciaMarker(ocorrencia));
			}
		}
	}

	private int getOcorrenciaDrawable(Ocorrencia ocorrencia) {
		return getOcorrenciaOveralayByConfig(ocorrencia);
	}

	private int getOcorrenciaOveralayByConfig(Ocorrencia ocorrencia) {
		ConfiguracaoVisualizaOcorrencia config = Session.getConfiguracaoVisualizaOcorrencia(this);
		switch (config) {
		case VISUALIZA_ATENDIMENTO:

			return getOcorrenciaOverLayByAtendimento(ocorrencia);

		case VISUALIZA_EMERGENCIA:

			return getOcorrenciaOverLayByEmergencia(ocorrencia);

		default:
			return getOcorrenciaOverLayByAtendimento(ocorrencia);

		}
	}

	private int getOcorrenciaOverLayByAtendimento(Ocorrencia obj) {
		if((obj.getInicio()!= null && obj.getInicio()>0 )&& (obj.getFim() == null||obj.getFim() == 0)){
			return R.drawable.ocorrencia_amarelo;
		}
		
		if((obj.getInicio()!= null && obj.getInicio()>0 )&& (obj.getFim() != null||obj.getFim() > 0)){
			return R.drawable.ocorrencia_verde;						
		}
		return R.drawable.ocorrencia_vermelho;
	}

	private int getOcorrenciaOverLayByEmergencia(Ocorrencia ocorrencia) {
		if((ocorrencia.getInicio() != null && ocorrencia.getInicio() > 0) && (ocorrencia.getFim() == null||ocorrencia.getFim() == 0))
			return R.drawable.ocorrencia_em_atendimento;

		if(ocorrencia.getNivelEmergencia() != null){
			if("#4edc4e".equals(ocorrencia.getNivelEmergencia().getCor()))
				return R.drawable.ocorrencia_verde;
			if("#ff5353".equals(ocorrencia.getNivelEmergencia().getCor()))
				return R.drawable.ocorrencia_vermelho;
		}
		//#ffde5b
		return R.drawable.ocorrencia_amarelo;
	}

	private MarkerOptions getOcorrenciaMarker(Ocorrencia ocorrencia){
		String titulo = "";
		if(ocorrencia.getCodigo() != null)
			titulo = ocorrencia.getCodigo();
		String descricao = "";
		if(ocorrencia.getDescricao() != null)
			descricao = ocorrencia.getDescricao();
		MarkerOptions marker = new MarkerOptions()
        .position(ocorrencia.getEndereco().getLatLng())
        .title(titulo)
        .snippet(descricao)
        .icon(BitmapDescriptorFactory.fromResource(getOcorrenciaDrawable(ocorrencia)));
		return marker;
	}

	@Override
	protected void gotNewOccurrence(Ocorrencia ocorrencia) {
		super.gotNewOccurrence(ocorrencia);
		populate(true);
	}

	@Override
	protected void gotNewHistory(Historico historico) {
		super.gotNewHistory(historico);
		populate(false);
	}

	public void searchAddressButtonOnClick(View view){
		String addressInput = addressTextView.getText().toString();
		if(!addressIsValid(addressInput)){
			AppHelper.getInstance().presentError(view.getContext(), getResources().getString(R.string.not_valid),
					getResources().getString(R.string.address_not_valid));
			return;
		}
		stopGeocodeTask();
		reverseGeocodeTask = new ReverseGeocoderTask(this);
		reverseGeocodeTask.execute(addressInput);
	}

	private boolean addressIsValid(String addressInput){
		if(addressInput == null || addressInput.length() < 5){
			return false;
		}
		return true;
	}

	protected LatLng getLatLngByLocation(Location location) {
		return new LatLng(location.getLatitude(), location.getLongitude());
	}

	private void centerMap(LatLng latLng) {
		map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        map.animateCamera(CameraUpdateFactory.zoomTo(17));
	}

	/*private void registerNewOcorrencia(){
		Intent intent = new Intent(MapViewActivity.this,OcorrenciaForm.class);
		startActivityForResult(intent, ConstUtil.OCORRENCIA_REQUEST);
	}*/

	private void seeAvailableMaterials(){
		Intent intent = new Intent(MapViewActivity.this,MaterialManagementActivity.class);
		startActivityForResult(intent, ConstUtil.MATERIAL_REQUEST);
	}


	private void seePendingOcurrences() {
		startActivity(new Intent(MapViewActivity.this,OcorrenciaList.class));
	}

	@Override
	public void onLocationChanged(Location location) {
		if(!isUserInteracting())centerMap(getLatLngByLocation(location));
		super.onLocationChanged(location);
	}

	@Override
	public void onStatusChanged(int status) {
		super.onStatusChanged(status);
	}


	public void syncButtonClick(){
		if (AppHelper.getInstance().isOnWifi(this)) {
			confirmSync();
		} else {
			new AlertDialog.Builder(this)
			.setIcon(android.R.drawable.ic_dialog_alert)
			.setTitle(R.string.alert)
			.setMessage(R.string.not_on_wifi)
			.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					confirmSync();  
				}

			})
			.setNegativeButton(R.string.no, null)
			.show();
		}
	}

	private void startChat() {
		startActivity(new Intent(MapViewActivity.this,OperatorsListActivity.class));
	}

	private void startSyncTask() {
		stopSyncTask();
		syncTask = new SyncTask(this);
		syncTask.syncCallBack = this;
		syncTask.execute();
	}

	private void stopSyncTask() {
		if(syncTask != null){
			syncTask.cancel(true);
			syncTask = null;			
		}
	}
	
	private void stopGeocodeTask() {
		if(reverseGeocodeTask != null){
			reverseGeocodeTask.cancel(true);
			reverseGeocodeTask = null;			
		}
	}

	private void confirmSync(){
		new AlertDialog.Builder(this)
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setTitle(R.string.alert)
		.setMessage(R.string.sync_time_sure)
		.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				startSyncTask();   
			}

		})
		.setNegativeButton(R.string.no, null)
		.show();
	}

	private void getQuitConfirmation(){
		new AlertDialog.Builder(this)
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setTitle(R.string.logout)
		.setMessage(R.string.logout_sure)
		.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				closeSession();   
			}

		})
		.setNegativeButton(R.string.no, null)
		.show();
	}

	private void startServices(){
		SocketService.startService(this);
		PositionService.startService(this);
		DaemonManagerService.start(this);
	}

	private void stopServices(){
		SocketService.stopService(this);
		PositionService.stopService(this);
		DaemonManagerService.stop(this);
	}

	private void resetConectionStatus(){
		Session.setSocketConectionStatus(this, SocketConectionStatus.DISCONNECTED.getValue());
	}

	private void closeSession(){
		cleanDataFromSession();
		Session.setToken("", this);
		stopServices();
		resetConectionStatus();
		finish();
	}

	private void cleanDataFromSession() {
		FotoDAO fotoDAO = new FotoDAO(this);
		FilmagemDAO filmDAO = new FilmagemDAO(this);
		List<Ocorrencia> ocorrencias = ocorrenciaDAO().fetchAllParsed();

		for (Ocorrencia ocorrencia : ocorrencias) {
			fotoDAO.deleteFotoFilesFromOcorrencia(ocorrencia.get_id());
			filmDAO.deleteFotoFilesFromOcorrencia(ocorrencia.get_id());
		}
		materialDAO().deleteAll();
		rotaDAO().deleteAll();
		cercaDAO().deleteAll();
		ocorrenciaDAO().deleteAll();
		historicoDAO().deleteAll();
		mensagemDAO().deleteAll();
		perguntaDAO().deleteAll();
		enqueteDAO().deleteAll();
		Session.setLastFixTime((long) 0, this);
	}



	@Override
	public void finishTask(final boolean ok) {
		centerZoom = ok;
		populate(ok);
	}

	@Override
	protected void gotDeleteOccurrence(Ocorrencia ocorrencia) {
		populate(false);
	}
	
	@Override
	public boolean onMarkerClick(Marker marker) {
		Ocorrencia ocorrencia = getOcorrenciaByCodigo(marker.getTitle());
		ocorrenciaSelected(ocorrencia);
		return false;
	}
	
	private Ocorrencia getOcorrenciaByCodigo(String tittle){
		if(ocorrencias != null){
			for(Ocorrencia ocorrencia:ocorrencias){
				if(tittle.equals(ocorrencia.getCodigo()))
					return ocorrencia;
			}
		}
		return null;
	}

	private void ocorrenciaSelected(Ocorrencia selectedOcorrencia) {
		this.selectedOcorrencia = selectedOcorrencia;
		openContextMenu(root);
	}

	//Lazy Instances For Data access objects

	public OcorrenciaDAO ocorrenciaDAO(){
		if(ocorrenciaDAO == null){
			ocorrenciaDAO = new OcorrenciaDAO(this);
		}

		return ocorrenciaDAO;
	}
	
	public EnqueteDAO enqueteDAO(){
		if(enqueteDAO == null){
			enqueteDAO = new EnqueteDAO(this);
		}

		return enqueteDAO;
	}
	
	public PerguntaDAO perguntaDAO(){
		if(perguntaDAO == null){
			perguntaDAO = new PerguntaDAO(this);
		}

		return perguntaDAO;
	}

	private HistoricoDAO historicoDAO() {
		if(historicoDAO == null) {
			historicoDAO = new HistoricoDAO(this);
		}

		return historicoDAO;
	}

	private CercaDAO cercaDAO() {
		if(cercaDAO == null) {
			cercaDAO = new CercaDAO(this);
		}

		return cercaDAO;
	}

	private RotaDAO rotaDAO() {
		if(rotaDAO == null) {
			rotaDAO = new RotaDAO(this);
		}

		return rotaDAO;
	}

	private MensagemDAO mensagemDAO() {
		if(mensagemDAO == null){
			mensagemDAO = new MensagemDAO(this);
		}
		return mensagemDAO;
	}

	private MaterialDAO materialDAO() {
		if(materialDAO == null){
			materialDAO = new MaterialDAO(this);
		}
		return materialDAO;
	}

	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}

	@Override
	public void onBackPressed() {
		getQuitConfirmation();
	}

	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}

	private void updateStatusBySharedPreferences() {
		setSocketStatus();

	}


	protected void setSocketStatus() {
		SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);

		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));

			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}

	@Override
	protected void updateTelemetriaStatusByBroadcast(String status) {
		if(status.equals(getString(R.string.connected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluegreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.blueyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluered));
			return;
		}
	}

	@Override
	protected void updateBatteryStatus(int percent) {
		if(percent <= 30){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_vermelha));
		}else if(percent <= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_amarela));

		}else if(percent >= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_verde));			
		}

	}
	
	private class ReverseGeocoderTask extends AsyncTask<String, Void, String> {
		Context context;
		ProgressDialog mDialog;
		LatLng latLng;
		String searchAddress;
		GoogleGeolocationCloud cloud;

		ReverseGeocoderTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "",
					getString(R.string.processing), true, false);
			mDialog.setCancelable(false);
			cloud = new GoogleGeolocationCloud();
		}

		@Override
		protected void onPreExecute() {
			setProgressBarIndeterminate(true);
		}

		@Override
		public String doInBackground(String... params) {
			searchAddress = params[0];
			return search();
		}

		String search() {
			try {
				latLng = cloud.reverseGeocode(searchAddress, context);
				if(latLng == null){
					return context.getResources().getString(R.string.address_not_found);
				}
			}
			catch (Exception e) {
				return context.getResources().getString(R.string.search_not_possible);
			}
			return null;
		}

		@Override
		public void onPostExecute(String erroMessage) {
			reverseGeocodeTask = null;
			setProgressBarIndeterminate(false);
			mDialog.dismiss();

			if(erroMessage != null){
				AppHelper.getInstance().presentError(context, getResources().getString(R.string.error),erroMessage);
			}else if(latLng != null){
				centerMap(latLng);
			}
		}
	}

}
