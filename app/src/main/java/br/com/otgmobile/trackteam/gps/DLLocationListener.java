package br.com.otgmobile.trackteam.gps;

import java.util.ArrayList;
import java.util.List;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

public class DLLocationListener implements LocationListener {
	
	private List<DLGpsObserver> observers;
	private Location lastLocation;
	private int lastStatus = -1;
	
	public void addObserver(DLGpsObserver observer){
		if ( observers == null ) {
			observers = new ArrayList<DLGpsObserver>();
		}
		
		if ( containsObserver(observer) ) {
			return;
		}
		
		observers.add(observer);
	}
	
	public void removeObserver(DLGpsObserver listener){
		if ( observers == null ) {
			return;
		}
		
		observers.remove(listener);
	}
	
	public boolean isTimeToTurnOff(){
		if ( observers == null || observers.isEmpty() ) {
			return true;
		}
		
		return false;
	}

	private boolean containsObserver(DLGpsObserver listener) {
		return observers != null && !observers.isEmpty() && observers.contains(listener);
	}
	
	@Override
	public void onLocationChanged(Location location) {
		updateLocation(location);
	}

	@Override
	public void onProviderDisabled(String provider) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		updateStatus(status);
	}
	
	public Location getLastLocation(){
		return lastLocation;
	}
	
	public int getLastStatus(){
		return lastStatus;
	}
	
	private synchronized void updateLocation(Location location){
		lastLocation = location;
		if(observers != null){
			for(DLGpsObserver listener : observers){
				listener.onLocationChanged(location);
			}
		}
	}
	
	private synchronized void updateStatus(int status){
		if(observers != null){
			for(DLGpsObserver listener : observers){
				listener.onStatusChanged(status);
			}
		}
	}

}
