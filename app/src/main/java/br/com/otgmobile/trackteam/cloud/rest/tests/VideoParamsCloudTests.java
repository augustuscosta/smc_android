package br.com.otgmobile.trackteam.cloud.rest.tests;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import br.com.otgmobile.trackteam.cloud.rest.VideoParamsCloud;
import br.com.otgmobile.trackteam.util.Session;


public class VideoParamsCloudTests extends AndroidTestCase {

	@Override
	protected void setUp() throws Exception {
		Session.setToken(TestCont.TOKEN, getContext());
		Session.setServer(TestCont.SERVER,getContext());
		super.setUp();
	}
	
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	public void testSendParams() throws Exception{
		VideoParamsCloud cloud = new VideoParamsCloud();
		
		String params = "m=video 5006 RTP/AVP 96 b=RR:0 a=rtpmap:96 H264/90000 " +
				"a=fmtp:96 packetization-mode=1;profile-level-id=42801e;sprop-parameter-sets=Z0KAHpWgKA9E," +
				"aM48gA==; m=audio 5004 RTP/AVP 96 b=AS:128 b=RR:0 a=rtpmap:96 AMR/8000 a=fmtp:96 octet-align=1";
		
		cloud.sendParams(params, getContext());
		
		Assert.assertTrue(cloud.getResponseCode() == 200);
	}
	
	
}
