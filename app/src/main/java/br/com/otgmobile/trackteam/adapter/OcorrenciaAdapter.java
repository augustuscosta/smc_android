package br.com.otgmobile.trackteam.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.row.OcorrenciaRowHolderInfo;
import br.com.otgmobile.trackteam.util.HandleListButton;
import br.com.otgmobile.trackteam.util.SelectListItem;

public class OcorrenciaAdapter extends BaseAdapter {

	private List<Ocorrencia> list;
	private Activity context;
	private HandleListButton handler;

	public OcorrenciaAdapter(List<Ocorrencia> list, Activity context,
			HandleListButton handler) {
		this.list = list;
		this.context = context;
		this.handler = handler;

	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list == null ? 0 : list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		OcorrenciaRowHolderInfo rowInfo;
		
		if (view == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.ocorrencia_row, null);

			rowInfo = new OcorrenciaRowHolderInfo();
			rowInfo.context = context;
			rowInfo.ocorrenciaListAddress = (TextView) row
					.findViewById(R.id.ocurrence_list_address);
			rowInfo.ocorrenciaListId = (TextView) row.findViewById(R.id.id_ocorrencia_list);
			rowInfo.ocorrenciaListHoraProgramada = (TextView) row
					.findViewById(R.id.ocurrence_list_hora_programada);
			rowInfo.ocorenciaListButton = (Button) row
					.findViewById(R.id.ocorrencia_list_button);
			rowInfo.confirmImage = (ImageView) row
					.findViewById(R.id.ocorrencia_done_image);
			
			row.setTag(rowInfo);
		}

		else {
			rowInfo = (OcorrenciaRowHolderInfo) row.getTag();
		}
		rowInfo.ocorenciaListButton.setOnClickListener(new SelectListItem(
				position, handler, row));
		
		Ocorrencia obj = list.get(position);
		rowInfo.drawRow(obj);
		return row;
	}

	
}

