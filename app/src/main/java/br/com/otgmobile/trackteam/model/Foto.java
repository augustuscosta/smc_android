package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

public class Foto implements Serializable{

	private static final long serialVersionUID = -1249058911147813134L;
	
	private Integer _id;

	private Integer id;
	private String imagem;
	private Integer ocorrenciaId;
	private Integer ocorrenciaServerId;
	private Long enviado;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getOcorrenciaId() {
		return ocorrenciaId;
	}
	public void setOcorrenciaId(Integer ocorrenciaId) {
		this.ocorrenciaId = ocorrenciaId;
	}
	
	public Integer get_id() {
		return _id;
	}
	public void set_id(Integer _id) {
		this._id = _id;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public Integer getOcorrenciaServerId() {
		return ocorrenciaServerId;
	}
	public void setOcorrenciaServerId(Integer ocorrenciaServerId) {
		this.ocorrenciaServerId = ocorrenciaServerId;
	}
	public Long getEnviado() {
		return enviado;
	}
	public void setEnviado(Long enviado) {
		this.enviado = enviado;
	}
	
}
