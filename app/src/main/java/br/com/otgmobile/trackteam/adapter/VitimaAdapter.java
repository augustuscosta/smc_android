package br.com.otgmobile.trackteam.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Vitima;
import br.com.otgmobile.trackteam.row.VitimaRowHolderInfo;
import br.com.otgmobile.trackteam.util.HandleListButton;
import br.com.otgmobile.trackteam.util.SelectListItem;

public class VitimaAdapter extends BaseAdapter {
	
	private List<Vitima> list;
	private Activity context;
	private HandleListButton handler; 
	
	public VitimaAdapter(List<Vitima> list, Activity context,HandleListButton handler){
		this.list = list;
		this.context = context;
		this.handler = handler;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list == null ? 0 : list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		VitimaRowHolderInfo rowInfo;

		if (view == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.vitima_row, null);

			rowInfo = new VitimaRowHolderInfo();
			rowInfo.vitimaListDescription = (TextView) row.findViewById(R.id.victim_list_description);
			rowInfo.vitimaListAge = (TextView) row.findViewById(R.id.victim_list_age);
			rowInfo.vitimaListButton = (LinearLayout) row.findViewById(R.id.victim_list_button);
			
			row.setTag(rowInfo);
		} else {
			rowInfo = (VitimaRowHolderInfo) view.getTag();
		}
		rowInfo.vitimaListButton.setOnClickListener(new SelectListItem(position, handler, row));
		Vitima obj = list.get(position);
		rowInfo.drawRow(obj);
		return row;
	}

}
