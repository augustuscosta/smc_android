package br.com.otgmobile.trackteam.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Solicitante;
import br.com.otgmobile.trackteam.row.SolicitanteRowHolderInfo;
import br.com.otgmobile.trackteam.util.HandleListButton;
import br.com.otgmobile.trackteam.util.SelectListItem;

public class SolicitanteAdapter extends BaseAdapter {

	private List<Solicitante> list;
	private Activity context;
	private HandleListButton handler;
	private boolean blockDeleteAndCreate;
	
	
	
	public SolicitanteAdapter(List<Solicitante> list, Activity context,HandleListButton handler, boolean  blockDeleteAndCreate) {
		this.list = list;
		this.context = context;
		this.handler = handler;
		this.blockDeleteAndCreate = blockDeleteAndCreate;
	}

	
	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list == null ? 0 : list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		SolicitanteRowHolderInfo rowInfo;
		Solicitante solicitante = list.get(position);

		if (view == null) {
			rowInfo = new SolicitanteRowHolderInfo();
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.solicitante_row, null);
			rowInfo.solicitanteListName = (TextView) row.findViewById(R.id.solicitante_list_name);
			rowInfo.solicitanteListCpf = (TextView) row.findViewById(R.id.solicitante_list_cpf);
			rowInfo.solicitanteListButton= (LinearLayout) row.findViewById(R.id.solicitante_list_button);
			
			row.setTag(rowInfo);
		} else {
			rowInfo = (SolicitanteRowHolderInfo) view.getTag();
		
		}
		if(!blockDeleteAndCreate){
			rowInfo.solicitanteListButton.setOnClickListener(new SelectListItem(position, handler, row));			
		}else{
			rowInfo.solicitanteListButton.setVisibility(View.GONE);			
		}
		row.setTag(R.id.solicitante_row_container, solicitante);
		rowInfo.drawRow(solicitante);
		return row;
	}
	
	
}
