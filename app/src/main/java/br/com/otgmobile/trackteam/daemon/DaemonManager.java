package br.com.otgmobile.trackteam.daemon;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.content.Context;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.PrecisionUtil;

public class DaemonManager {

	public static final int THREAD_POOL_SIZE = 5;
	private static volatile boolean daemonRunning = false;
	private static ExecutorService threadPool;
	private static DaemonOcorrenciaSender ocorrenciaDaemonSender;
	private static DaemonHistoricoSender historicoDaemonSender;
	private static DaemonImagemRequeridaAndLocalizaMaterialSender imagemRequeridaDaemonSender;
	private static List<DaemonSender> threads = null;
	private static Thread daemon;

	public synchronized static boolean startDaemons(Context context) {
		if (context == null) {
			LogUtil.d("Invalid context(context equals null)...");
			return false;
		}

		if (isRunning()) {
			LogUtil.d("Daemon(DaemonManager) is running");
			return true;
		}
		createDaemonsInstance(context);
		startThread(context);
		return true;
	}


	private static void createDaemonsInstance(Context context) {
		ocorrenciaDaemonSender = new DaemonOcorrenciaSender(context);
		historicoDaemonSender = new DaemonHistoricoSender(context);
		imagemRequeridaDaemonSender = new DaemonImagemRequeridaAndLocalizaMaterialSender(context);
		
		threads = new ArrayList<DaemonSender>();
		threads.add(ocorrenciaDaemonSender);
		threads.add(historicoDaemonSender);
		threads.add(imagemRequeridaDaemonSender);
	}

	private static void startThread(final Context context) {
		threadPool = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
		daemon = new Thread() {

			@Override
			public void run() {
				
				while(isRunning()){
					
					try {
						executeThreads();
						sleep(PrecisionUtil.getDaemonsSleepTime(context));
					} catch (InterruptedException e) {
						LogUtil.e("Error putting thread(DaemonManager) to sleep", e);
					} finally {
						
					}
				}
			}
			
			private void executeThreads() {
				for (DaemonSender daemon : threads) {
					threadPool.execute(daemon);
				}
			}
		};
		daemon.setPriority(Thread.MIN_PRIORITY);
		daemon.start();

		LogUtil.d("DaemonManager has been started");
		daemonRunning = true;
	}
 
	public synchronized static void stopRunner() {
		if (!isRunning()) {
			return;
		}

		LogUtil.d("Stoping DaemonManager...");
		threadPool.shutdown();
		threadPool = null;
		ocorrenciaDaemonSender = null;
		historicoDaemonSender = null;
		imagemRequeridaDaemonSender = null;
		threads = null;

		LogUtil.d("DaemonManager has been stopped");
		daemonRunning = false;
	}

	public synchronized static void stopDaemon(DaemonSender daemon) {
		LogUtil.d("Stoping Daemon(" + daemon.getClass().getSimpleName() + ")...");
		daemon.stop();
		threads.remove(daemon);
	}

	public synchronized static boolean isRunning() {
		return daemonRunning;
	}

}
