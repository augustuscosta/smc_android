package br.com.otgmobile.trackteam.activity;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.adapter.VitimaAdapter;
import br.com.otgmobile.trackteam.database.VitimaDAO;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.model.Vitima;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.HandleListButton;
import br.com.otgmobile.trackteam.util.Session;

public class VitimaList extends GenericListActivity implements HandleListButton{
	
	private List<Vitima> list;
	private Ocorrencia ocorrencia;
	private boolean otherRuns = false;
	private LinearLayout vitimListContainer;
	private Button socketStatus;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_vitimas);
		configureActivity();
		handleIntent();
		fetchData();

	}
	
	@Override
	public void onBackPressed() {
		quitAndPassVictims();
	};
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu vitimas_menu) {
		MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.vitimas_menu, vitimas_menu);
        return true;
	};
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.vitimas_menu_new_victim:
        	registerNewVitima();
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	private void configureActivity() {
		vitimListContainer = (LinearLayout) findViewById(R.id.list_victims_container);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		refresh();
		setSocketStatus();
	}
	
	@Override
	protected void onDestroy() {
		setListAdapter(null);
		AppHelper.unbindDrawables(vitimListContainer);
		super.onDestroy();
	}
	
	private void handleIntent(){
		Intent intent = getIntent();
		ocorrencia = (Ocorrencia)  intent.getExtras().get(ConstUtil.SERIALIZABLE_KEY);		
	}
	
	private void fetchData(){
		list = search();
		if(list!=null && !list.isEmpty()){
			setAdapter();
		}
		else{
			if(!otherRuns){
				otherRuns = true;
				registerNewVitima();
			}
		}
	}
	
	private void setAdapter(){		
			VitimaAdapter adapter = new VitimaAdapter(list, this,this);
			setListAdapter(adapter);					
	}
	
	@Override
	public void positionSelect(int position) {
		delete(list.get(position));
		refresh();
	}
	
	private void refresh() {
	 VitimaDAO dao = new VitimaDAO(this);
	 list = dao.findFromOcorrencia(ocorrencia.get_id());
	 if(list!=null){
		 setAdapter();
	 }
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Vitima obj = list.get(position);
		Intent intent = new Intent(VitimaList.this,VitimaForm.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, obj);
		intent.putExtras(bundle);
		startActivity(intent);
		super.onListItemClick(l, v, position, id);
	}

	
	private void registerNewVitima(){
		Intent intent = new Intent(VitimaList.this,VitimaForm.class);
		Vitima obj = new Vitima();
		obj.setOcorrenciaID(ocorrencia.get_id());
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, obj);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.VITIM_CAD_REQUEST);
	}
	
	private void quitAndPassVictims() {
	ocorrencia.setVitimas(list);
	Intent intent = new Intent();
	Bundle bundle = new Bundle();
	bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
	intent.putExtras(bundle);
	setResult(RESULT_OK,intent);
	finish();
	}

	//DataBase Accesses 
	
	private List<Vitima> search() {
		VitimaDAO dao = new VitimaDAO(this);
		List<Vitima> toReturn = dao.findFromOcorrencia(ocorrencia.get_id());
		return toReturn;
	}
	
	private boolean delete(Vitima obj) {
		VitimaDAO dao = new VitimaDAO(this);
		boolean a = dao.delete(obj.get_id());
		return a;
	}
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}
}
