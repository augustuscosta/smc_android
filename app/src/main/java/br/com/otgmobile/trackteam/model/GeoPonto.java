package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.maps.GeoPoint;

public class GeoPonto implements Serializable {
	
	private static final long serialVersionUID = 6575608162183348325L;

	private Integer _id;
	
	private Integer id;
	
	private Float latitude;
	
	private Float longitude;
	
	private Integer rotaID;
	
	private Integer cercaID;
	
	public GeoPoint getGeopoint(){
		double geoLatitude = getLatitude() * 1E6;
		double geoLongitude = getLongitude() * 1E6;
		return new GeoPoint((int) geoLatitude, (int) geoLongitude);
	}
	
	public LatLng getLatLng(){
		return new LatLng(getLatitude() , getLongitude());
	}

	public Integer getRotaID() {
		return rotaID;
	}

	public void setRotaID(Integer rotaID) {
		this.rotaID = rotaID;
	}

	public Integer getCercaID() {
		return cercaID;
	}

	public void setCercaID(Integer cercaID) {
		this.cercaID = cercaID;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public Integer get_id() {
		return _id;
	}

	public void set_id(Integer _id) {
		this._id = _id;
	}
	
	

}
