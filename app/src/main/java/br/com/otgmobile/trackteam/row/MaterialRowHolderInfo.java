package br.com.otgmobile.trackteam.row;

import android.widget.CheckBox;
import android.widget.TextView;
import br.com.otgmobile.trackteam.model.Material;

public class MaterialRowHolderInfo {
	
	public TextView materialListCode;
	public TextView materialListDescrription;
	public CheckBox materialListCheckBox;
	
	public void drawRow(Material obj){
		if(obj == null){
			return;
		}
		if(obj.getCodigo()!=null){
			materialListCode.setText(obj.getCodigo());
		}
		else{
			materialListCode.setText("");
		}
		if(obj.getDescricao()!=null){
			materialListDescrription.setText(obj.getDescricao());
		}
		else{
			materialListDescrription.setText(obj.getDescricao());
		}
		
	}

	public boolean isChecked() {
		if ( !isExistCheckbox() ) {
			return false;
		}
		
		return materialListCheckBox.isChecked();
	}

	private boolean isExistCheckbox() {
		return materialListCheckBox != null;
	}
	
	public void setChecked(final boolean value) {
		materialListCheckBox.setChecked(value);
	}
	
}
