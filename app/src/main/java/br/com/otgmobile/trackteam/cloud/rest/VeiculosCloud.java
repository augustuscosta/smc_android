package br.com.otgmobile.trackteam.cloud.rest;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import br.com.otgmobile.trackteam.database.CercaDAO;
import br.com.otgmobile.trackteam.database.RotaDAO;
import br.com.otgmobile.trackteam.database.VeiculoDAO;
import br.com.otgmobile.trackteam.model.Cerca;
import br.com.otgmobile.trackteam.model.Rota;
import br.com.otgmobile.trackteam.model.Veiculo;
import br.com.otgmobile.trackteam.util.Session;

import com.google.gson.Gson;


public class VeiculosCloud extends RestClient {
	
	private final String URL = "comunicacao/veiculo";
	
	private final String URL_CERCA = "comunicacao/veiculo/cerca";
	
	private final String URL_ROTA = "comunicacao/veiculo/rota";

	private final String ROOT_OBJECT = "result";
	
	public List<Veiculo> list(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return getListFromResponse();
	}
	
	public Veiculo find(Integer id,Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("id", id.toString());
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return getObjectFromResponse();
	}
	
	public Cerca findCerca(Veiculo veiculo,Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("id", veiculo.getId().toString());
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL_CERCA);
		execute(RequestMethod.GET);
		return getCercaFromResponse();
	}
	
	public Rota findRota(Veiculo veiculo,Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("id", veiculo.getId().toString());
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL_ROTA);
		execute(RequestMethod.GET);
		return getRotaFromResponse();
	}
	
	public Boolean sync(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return storeResponse(context);
	}
	
	private Boolean storeResponse(Context context) throws Exception {
		VeiculoDAO dao = new VeiculoDAO(context);
		try {
			JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
			if (jsonArray != null) {
				Veiculo obj;
				Gson gson = new Gson();
				dao.deleteAll();
				for (int i = 0; i < jsonArray.length(); i++) {
					obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),Veiculo.class);
					dao.save(obj);
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
		}
		return true;
	}
	
	public Boolean syncCerca(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL_CERCA);
		execute(RequestMethod.GET);
		return storeCercaResponse(context);
	}
	
	private Boolean storeCercaResponse(Context context) throws Exception {
		CercaDAO dao = new CercaDAO(context);
		try {
			JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
			if (jsonArray != null) {
				Cerca obj;
				Gson gson = new Gson();
				dao.deleteAll();
				for (int i = 0; i < jsonArray.length(); i++) {
					obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),Cerca.class);
					dao.save(obj);
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
		}
		return true;
	}
	
	public Boolean syncRota(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL_ROTA);
		execute(RequestMethod.GET);
		return storeRotaResponse(context);
	}
	
	private Boolean storeRotaResponse(Context context) throws Exception {
		RotaDAO dao = new RotaDAO(context);
		try {
			JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
			if (jsonArray != null) {
				Rota obj;
				Gson gson = new Gson();
				dao.deleteAll();
				for (int i = 0; i < jsonArray.length(); i++) {
					obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),Rota.class);
					dao.save(obj);
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
		}
		return true;
	}
	

	
	private Veiculo getObjectFromResponse() throws JSONException {
		JSONObject jsonObject = getJsonObjectFromResponse(ROOT_OBJECT);
		if (jsonObject != null) {
			Veiculo toReturn;
			Gson gson = new Gson();
			toReturn = gson.fromJson(jsonObject.toString(),Veiculo.class);
			return toReturn;
		}
		return null;
	}
	
	private Cerca getCercaFromResponse() throws JSONException {
		JSONObject jsonObject = getJsonObjectFromResponse(ROOT_OBJECT);
		if (jsonObject != null) {
			Cerca toReturn;
			Gson gson = new Gson();
			toReturn = gson.fromJson(jsonObject.toString(),Cerca.class);
			return toReturn;
		}
		return null;
	}
	
	private Rota getRotaFromResponse() throws JSONException {
		JSONObject jsonObject = getJsonObjectFromResponse(ROOT_OBJECT);
		if (jsonObject != null) {
			Rota toReturn;
			Gson gson = new Gson();
			toReturn = gson.fromJson(jsonObject.toString(),Rota.class);
			return toReturn;
		}
		return null;
	}

	private List<Veiculo> getListFromResponse() throws JSONException {
		JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
		if (jsonArray != null) {
			Veiculo obj;
			Gson gson = new Gson();
			List<Veiculo> toReturn = new ArrayList<Veiculo>();
			for (int i = 0; i < jsonArray.length(); i++) {
				
				obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),Veiculo.class);
				toReturn.add(obj);
			}
			return toReturn;
		}
		return null;

	}
}
