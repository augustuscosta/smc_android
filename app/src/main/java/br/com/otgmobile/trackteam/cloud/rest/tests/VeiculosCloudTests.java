package br.com.otgmobile.trackteam.cloud.rest.tests;

import java.util.List;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import br.com.otgmobile.trackteam.cloud.rest.VeiculosCloud;
import br.com.otgmobile.trackteam.model.Cerca;
import br.com.otgmobile.trackteam.model.Rota;
import br.com.otgmobile.trackteam.model.Veiculo;
import br.com.otgmobile.trackteam.util.Session;

public class VeiculosCloudTests extends AndroidTestCase {

	@Override
	protected void setUp() throws Exception {
		Session.setToken(TestCont.TOKEN, getContext());
		Session.setServer(TestCont.SERVER,getContext());
		super.setUp();
	}
	
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	public void testListVeiculos() throws Exception{
		VeiculosCloud cloud = new VeiculosCloud();
		List<Veiculo> response = cloud.list(getContext());
		Assert.assertNotNull(response);
		Assert.assertTrue(response.size() > 0);
	}
	
	public void testFindVeiculo() throws Exception{
		VeiculosCloud cloud = new VeiculosCloud();
		Veiculo response = cloud.find(7,getContext());
		Assert.assertNotNull(response);
		Assert.assertTrue(response.getId() == 7);
	}
	
	public void testFindCercaVeiculo() throws Exception{
		VeiculosCloud cloud = new VeiculosCloud();
		Veiculo veiculo = new Veiculo();
		veiculo.setId(7);
		Cerca response = cloud.findCerca(veiculo,getContext());
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getGeoPonto());
	}
	
	public void testRotaVeiculo() throws Exception{
		VeiculosCloud cloud = new VeiculosCloud();
		Veiculo veiculo = new Veiculo();
		veiculo.setId(7);
		Rota response = cloud.findRota(veiculo,getContext());
		Assert.assertNotNull(response);
		Assert.assertNotNull(response.getGeoPonto());
	}
	
	public void testSync() throws Exception{
		VeiculosCloud cloud = new VeiculosCloud();
		Assert.assertTrue(cloud.sync(getContext()));
	}
	
	public void testSyncCerca() throws Exception{
		VeiculosCloud cloud = new VeiculosCloud();
		Assert.assertTrue(cloud.syncCerca(getContext()));
	}
	
	public void testSyncRota() throws Exception{
		VeiculosCloud cloud = new VeiculosCloud();
		Assert.assertTrue(cloud.syncCerca(getContext()));
	}
	
}
