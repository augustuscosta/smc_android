package br.com.otgmobile.trackteam.row;

import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.trackteam.model.Solicitante;

public class SolicitanteRowHolderInfo {
	public TextView solicitanteListName;
	public TextView solicitanteListCpf;
	public LinearLayout	solicitanteListButton;
	
	public void drawRow(Solicitante obj){
		if(obj == null){
			return;
		}
		
		if(obj.getNome()!=null){
			solicitanteListName.setText(obj.getNome());
		}
		else{
			solicitanteListName.setText(obj.getNome());
		}
		
		if(obj.getCpf()!=null){
			solicitanteListCpf.setText(obj.getCpf());
		}
		else{
			solicitanteListName.setText("");
		}
		
	}
	

}
