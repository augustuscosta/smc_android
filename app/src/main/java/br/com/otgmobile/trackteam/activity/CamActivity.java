package br.com.otgmobile.trackteam.activity;

import java.io.IOException;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.text.Html;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.widget.TextView;
import br.com.otgmobile.trackteam.cam.CamPreferences;
import br.com.otgmobile.trackteam.cam.CameraStreamer;
import br.com.otgmobile.trackteam.cam.TestH264;
import br.com.otgmobile.trackteam.cam.librtp.SessionDescriptor;

import br.com.otgmobile.trackteam.R;


/*
 * Main activity
 * It will also test H.264 support on the phone
 */

public class CamActivity extends GenericActivity {

    
    static final public String LOG_TAG = "CAM_SERVICE";
    
    private SurfaceView camera;
    private TextView console;
    
    private PowerManager.WakeLock wl;
    
    private int resX, resY, fps, br;
    private static CameraStreamer streamer = new CameraStreamer();
    
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.surface);
        
        console = (TextView)findViewById(R.id.console);
        
        camera = (SurfaceView)findViewById(R.id.smallcameraview);
        camera.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        
        resX = CamPreferences.getResX(this);
       	resY = CamPreferences.getResY(this);
       	fps = CamPreferences.getFps(this);
       	br = CamPreferences.getBr(this);
        
        
        Callback shcb = new SurfaceHolder.Callback() {

    		@Override
			public void surfaceChanged(SurfaceHolder holder, int format,
					int width, int height) {

			}

    		@Override
			public void surfaceCreated(SurfaceHolder holder) {

			}

    		@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
    			stopStreaming();
			}
    		
    	};
    	
    	camera.getHolder().addCallback(shcb);
    	
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "SpydroidWakeLock");
    	
    }
    
    public void onResume() {
    	super.onResume();
    	
    	log("<b>Iniciando</b>");
    	log("Qualidade: "+String.valueOf(resX)+"x"+String.valueOf(resY)+", "+String.valueOf(fps)+"fps");
    	
//    	if (CamPreferences.isSupported(this)) {
//    		// Phone has already been tested
//    		log("Phone supported !");
//    	    log("Upload /sdcard/spydroid.sdp on your computer");
//    	    log("And open it with vlc");
//    	    new Handler().post(new Runnable() { 
//			    public void run() { toggleStreaming(); } 
//			  });
//    		return;
//    	}
    	
    	// Test H.264 Support
		TestH264.RunTest(this.getCacheDir(),camera.getHolder(), resX, resY, fps, new TestH264.Callback() {
			
			@Override
			public void onStart() {
				log("<b>Testando H.264</b>");
			}
			
			// Called if H.264 isn't supported with chosen quality settings
			public void onError(String error) {
				log(error);
				log("Algo saiu errado, a aplicação pode não rodar corretamente");
			}
			
			// Called if H.264 is supported with chosen quality settings
			public void onSuccess(String result) {

				String[] params = result.split(":");
				
				log("Enviando...");
			    
			    SessionDescriptor sd = new SessionDescriptor();
			    sd.addH264Track(params[0], params[2], params[1]);
			    sd.addAMRNBTrack();
			    try {
					sd.saveToFile("/sdcard/camconf.sdp");
					CamPreferences.setCamParams(sd.toString(), CamActivity.this);
					CamPreferences.setSupport(true, CamActivity.this);
					// Store H264 parameters
		    	    CamPreferences.setProfile(params[0], CamActivity.this);
		    	    CamPreferences.setPps(params[1], CamActivity.this);
		    	    CamPreferences.setSps(params[2], CamActivity.this);
				} catch (IOException e) {
					log("Não foi possível gerar o arquivo de configuração");
				}
				
				new Handler().post(new Runnable() { 
    			    public void run() { toggleStreaming(); } 
    			  });
				
			}
			
		});

    }
    
    private void toggleStreaming() {
    	
    	if (streamer.isStreaming())
    		stopStreaming();
    	else
    		startStreaming();

    }
    
    private void startStreaming() {
    	
    	if (streamer.isStreaming()) return;
    	
		try {
			streamer.setup(CamPreferences.getServer(this),CamPreferences.getPort(this), resX, resY, fps, br, camera.getHolder());
		} catch (IOException e) {
			log(e.getMessage());
			return;
		}

		streamer.start();
		wl.acquire();
		
    }
    
    private void stopStreaming() {
    	
    	if (!streamer.isStreaming()) return;
    	
		streamer.stop();
		wl.release();
		finish();
		
    }
    
    @Override
    public void onBackPressed() {
    	stopStreaming();
    }

    
    
    public void log(String s) {
    	console.append(Html.fromHtml(s+"<br />"));
    }
    
    protected void gotCamRequest() {
		//Do nothing 
	}
	
	protected void gotCloseCamRequest() {
		stopStreaming();
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(int status) {
		// TODO Auto-generated method stub
		
	}
    
    
}