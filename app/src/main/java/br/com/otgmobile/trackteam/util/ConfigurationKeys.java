package br.com.otgmobile.trackteam.util;

public enum ConfigurationKeys {

	CONFIGURATION_PASSWORD("CONFIGURATION_PASSWORD"),
	PRECISION("PRECISION"),
	SPY("SPY");

	private String text;

	ConfigurationKeys(String text) {
		this.text = text;
	}
	
	public String getText(){
		return text;
	}
}
