package br.com.otgmobile.trackteam.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.customwidgets.CpfEditText;
import br.com.otgmobile.trackteam.database.VitimaDAO;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.model.Vitima;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.Session;

public class VitimaForm extends GenericActivity {
	private Vitima obj;
	private EditText nomeText;
	private EditText rgText;
	private CpfEditText cpfText;
	private EditText vitimaDescription;
	private EditText vitimaAge;
	private LinearLayout vitima_form_Container;
	private Button socketStatus;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vitima);
		getSearchData();
		configureActivity();
		setData();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}
	
	
	@Override
	protected void onDestroy() {
		AppHelper.unbindDrawables(vitima_form_Container);
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	};
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};

	private void registerVitima() {
		if(vitimaAge.getText().toString().length() > 0)
			obj.setIdade(Integer.parseInt(vitimaAge.getText().toString()));
		obj.setDescricao(vitimaDescription.getText().toString());
		obj.setNome(nomeText.getText().toString());
		obj.setCpf(cpfText.getText().toString());
		obj.setRg(rgText.getText().toString());
		VitimaDAO dao = new VitimaDAO(this);
		if(obj.get_id()==null){
			dao.save(obj);
		}
		else{
			dao.update(obj);
		}
		finish();
	}

	private void getSearchData() {
		Intent intent = getIntent();
		if(intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)){
			Bundle b = getIntent().getExtras();
			obj = (Vitima) b.getSerializable(ConstUtil.SERIALIZABLE_KEY);			
		}
	}

	private void configureActivity() {
		nomeText 	  = (EditText) findViewById(R.id.vitima_name);
		rgText 	  = (EditText) findViewById(R.id.vitima_rg);
		cpfText  	  = (CpfEditText) findViewById(R.id.vitima_cpf);
		vitimaDescription = (EditText) findViewById(R.id.victim_description);
		vitimaAge = (EditText) findViewById(R.id.victim_age);
		vitima_form_Container = (LinearLayout) findViewById(R.id.vitima_form_container);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);

	}

	public void confirmButtonOnClick(View view) {
		registerVitima();
	}

	private void setData() {
		if (obj != null) {
			if (obj.getDescricao() != null) {
				vitimaDescription.setText(obj.getDescricao());
			}
			if (obj.getIdade() != null) {
				vitimaAge.setText(Integer.toString(obj.getIdade()));
			}
			if (obj.getNome() != null) {
				nomeText.setText(obj.getNome());
			}
			if (obj.getRg() != null) {
				rgText.setText(obj.getRg());
			}
			if (obj.getCpf() != null) {
				cpfText.setText(obj.getCpf());
			}
		}
	}
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		super.onLocationChanged(location);
		
	}

	@Override
	public void onStatusChanged(int status) {
		super.onStatusChanged(status);
	}

}
