package br.com.otgmobile.trackteam.activity;

import java.util.Date;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.adapter.OcorrenciaAdapter;
import br.com.otgmobile.trackteam.database.OcorrenciaDAO;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.HandleListButton;
import br.com.otgmobile.trackteam.util.Session;

public class OcorrenciaList extends GenericListActivity implements
HandleListButton {

	private LinearLayout listOcorrenciaContainer;
	private List<Ocorrencia> list;
	private OcorrenciaAdapter adapter;
	private Button socketStatus;
	private Button bateriaStatus;
	private Button telemetriaStatus;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_ocorrencias);
		configureActivity();

	}
	private void configureActivity() {
		listOcorrenciaContainer = (LinearLayout) findViewById(R.id.list_ocorrencias_container);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		bateriaStatus = (Button) findViewById(R.id.bateriaStatusIcon);
		telemetriaStatus = (Button) findViewById(R.id.telemetriaStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}
	@Override
	protected void onResume() {
		super.onResume();
		refresh();
		setSocketStatus();
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};

	@Override
	protected void onPause() {
		System.gc();
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		setListAdapter(null);
		AppHelper.unbindDrawables(listOcorrenciaContainer);
		super.onDestroy();
	}

	private void getDataFromDataBase() {
		OcorrenciaDAO dao = new OcorrenciaDAO(this);
		list = dao.fetchAllValidadaParsed();
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Ocorrencia ocorrencia = list.get(position);
		Intent intent = new Intent(OcorrenciaList.this, OcorrenciaPreviewActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivity(intent);
	}

	private void setAdapter(List<Ocorrencia> list) {
		adapter = new OcorrenciaAdapter(list, this, this);
		setListAdapter(adapter);
	}

	@Override
	public void positionSelect(int position) {
		setAtendimento(list.get(position));

	}

	private void setAtendimento(Ocorrencia ocorrencia) {
		if(!isInCourse(ocorrencia)){
			atenderOuFinalizar(ocorrencia);
		}
		else{
			AppHelper.getInstance().presentError(this, 
					getResources().getString(R.string.error), 
					getResources().getString(R.string.em_atedimento));
		}
	}

	private void atenderOuFinalizar(Ocorrencia ocorrencia) {
		OcorrenciaDAO dao = new OcorrenciaDAO(this);
		if (ocorrencia.getInicio() == null ||ocorrencia.getInicio() == 0) {
			ocorrencia.setInicio(new Date().getTime());
			ocorrencia.setEnviado(false);
		} else {
			if (ocorrencia.getFim() == null||ocorrencia.getFim() == 0) {
				ocorrencia.setFim(new Date().getTime());
				ocorrencia.setEnviado(false);
			}
		}
		dao.save(ocorrencia);
		refresh();
	}

	private boolean isInCourse(Ocorrencia ocorrencia){
		if(ocorrencia == null){
			return false;
		}
		if(ocorrencia.getInicio()!=null&& ocorrencia.getInicio()>0){
			return false;
		}
		for(Ocorrencia obj :list){
			if((obj.getInicio()!= null && obj.getInicio()>0 )&& (obj.getFim() == null||obj.getFim() == 0)){
				if(!obj.equals(ocorrencia))
					return true;
			}
		}
		return false;
	}

	private void refresh() {
		getDataFromDataBase();
		setAdapter(list);
	}

	public void returnButtonOnClick(View view){
		finish();
	}

	@Override
	protected void gotDeleteOccurrence(Ocorrencia ocorrencia) {
		refresh();
	}

	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}

	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}

	protected void setSocketStatus() {
		SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);

		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));

			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}


	@Override
	protected void updateBatteryStatus(int percent) {
		if(percent <= 30){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_vermelha));
		}else if(percent <= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_amarela));

		}else if(percent >= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_verde));			
		}

	}

	@Override
	protected void updateTelemetriaStatusByBroadcast(String status) {
		if(status.equals(getString(R.string.connected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluegreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.blueyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluered));
			return;
		}
	}

}
