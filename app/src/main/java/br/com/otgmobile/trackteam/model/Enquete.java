package br.com.otgmobile.trackteam.model;

import java.io.Serializable;
import java.util.List;

public class Enquete implements Serializable {

	private static final long serialVersionUID = 6631304233650322382L;

	private Integer _id;
	private Integer id;
	private String nome;	
	private String descricao;	
	private List<Pergunta> perguntas;
	
	public Integer get_id() {
		return _id;
	}
	public void set_id(Integer _id) {
		this._id = _id;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public List<Pergunta> getPerguntas() {
		return perguntas;
	}
	public void setPerguntas(List<Pergunta> perguntas) {
		this.perguntas = perguntas;
	}

}
