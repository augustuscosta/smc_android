package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.Veiculo;

public class VeiculoDAO {
	// Database fields
	public static final String _ID = "_id";
	public static final String ID = "id";
	public static final String PLACA = "placa";
	public static final String MODELO = "modelo";
	public static final String RENAVAN = "renavan";
	public static final String ANO = "ano";
	public static final String MARCA = "marca";
	public static final String COR = "cor";
	public static final String LOCALID = "local_id";
	public static final String CERCAID = "cerca_id";

	private static final String DATABASE_TABLE = "veiculo";
	private SQLiteDatabase database;
	private Context context;

	public VeiculoDAO(Context context) {
		database = DBHelper.getDatabase(context);
		this.context = context;
	}

	public long save(Veiculo obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = update(obj);
		}

		return inserted;
	}
	
	private boolean isNew(Veiculo obj) {
		if(find(obj.getId()) == null)
			return true;
		return false;
	}

	public long create(Veiculo obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	public long update(Veiculo obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, ID + " = ?",
				new String[] { obj.getId().toString() });
	}

	public boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, ID + "=" + id, null) > 0;
	}

	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE, null, null) > 0;
	}

	private Cursor findCursor(int codigo) {
		return database.query(DATABASE_TABLE, new String[] { _ID,ID, PLACA,
				MODELO, RENAVAN, ANO, MARCA, COR, LOCALID,CERCAID },
				ID + " = ?", new String[] { codigo + "" }, null, null, null);
	}

	public Veiculo find(int id) {
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}

	public Cursor fetchAll() {
		return database.query(DATABASE_TABLE, new String[] { _ID,ID, PLACA,
				MODELO, RENAVAN, ANO, MARCA, COR, LOCALID,CERCAID }, null, null,
				null, null, null);
	}

	public List<Veiculo> fetchAllParsed() {
		Cursor cursor = fetchAll();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}

	private List<Veiculo> parse(Cursor cursor) {
		List<Veiculo> toReturn = null;
		if (cursor != null) {
			toReturn = new ArrayList<Veiculo>();
			Veiculo obj;
			HistoricoDAO historicoDAO = new HistoricoDAO(context);
			if (cursor.moveToFirst()) {
				while (!cursor.isAfterLast()) {
					obj = parseObject(cursor);
					obj.setUltimoHistorico(historicoDAO.findByVehicle(obj));
					toReturn.add(obj);
					cursor.moveToNext();
				}
			}
		}

		return toReturn;
	}

	private Veiculo parseObject(Cursor cursor) {
		Veiculo obj = null;
		obj = new Veiculo();
		obj.set_id(cursor.getInt(0));
		obj.setId(cursor.getInt(1));
		obj.setPlaca(cursor.getString(2));
		obj.setModelo(cursor.getString(3));
		obj.setRenavan(cursor.getString(4));
		obj.setAno(cursor.getString(5));
		obj.setMarca(cursor.getString(6));
		obj.setCor(cursor.getString(7));
		obj.setLocalID(cursor.getInt(8));
		obj.setCercaID(cursor.getInt(9));
		return obj;
	}

	private ContentValues createContentValues(Veiculo obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(PLACA, obj.getPlaca());
		values.put(MODELO, obj.getModelo());
		values.put(RENAVAN, obj.getRenavan());
		values.put(ANO, obj.getAno());
		values.put(MARCA, obj.getMarca());
		values.put(COR, obj.getCor());
		values.put(LOCALID, obj.getLocalID());
		values.put(CERCAID, obj.getCercaID());
		return values;
	}

	private ContentValues createUpdateContentValues(Veiculo obj) {
		ContentValues values = new ContentValues();
		values.put(PLACA, obj.getPlaca());
		values.put(MODELO, obj.getModelo());
		values.put(RENAVAN, obj.getRenavan());
		values.put(ANO, obj.getAno());
		values.put(MARCA, obj.getMarca());
		values.put(COR, obj.getCor());
		values.put(LOCALID, obj.getLocalID());
		values.put(CERCAID, obj.getCercaID());
		return values;
	}
}
