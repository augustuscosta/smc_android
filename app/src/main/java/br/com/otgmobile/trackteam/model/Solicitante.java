package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

public class Solicitante implements Serializable {

	private static final long serialVersionUID = -5456728015517389821L;

	private Integer _id;
	
	private Integer id;
	
	private String cpf;
	
	private String nome;
	
	private String telefone1;
	
	private String telefone2;
	
	private String rg;
	
	private Integer ocorrenciaId;
	


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public Integer get_id() {
		return _id;
	}

	public void set_id(Integer _id) {
		this._id = _id;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public Integer getOcorrenciaId() {
		return ocorrenciaId;
	}

	public void setOcorrenciaId(Integer ocorrenciaId) {
		this.ocorrenciaId = ocorrenciaId;
	}


	
		
	
}
