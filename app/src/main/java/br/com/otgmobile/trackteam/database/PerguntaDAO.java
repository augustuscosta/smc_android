package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import br.com.otgmobile.trackteam.model.Enquete;
import br.com.otgmobile.trackteam.model.Pergunta;

public class PerguntaDAO implements BaseColumns{

	public static final String ID = "id";
	public static final String PERGUNTA = "pergunta";
	public static final String TIPO_ENTRADA = "tipo_entrada";
	public static final String ENQUETE_ID = "enquete_id";

	private static final String[] FIELDS = {_ID,ID,PERGUNTA,TIPO_ENTRADA,ENQUETE_ID};
	private static final String DATABASE_TABLE = "pergunta";
	private SQLiteDatabase database;
	private Context context;
	
	public PerguntaDAO(Context context) {
		this.context = context;
		database = DBHelper.getDatabase(context);
	}
	
	public long save(Pergunta obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = update(obj);
		}

		return inserted;
	}
	
	private boolean isNew(Pergunta obj) {
		if(find(obj.getId()) == null)
			return true;
		return false;
	}
	
	public long create(Pergunta obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	public long update(Pergunta obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, ID + " = " +obj.getId(),null);
	}

	public boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, ID + "=" + id, null) > 0;
	}
	
	public boolean deleteByEnquete(Integer enqueteId) {
		return database.delete(DATABASE_TABLE, ENQUETE_ID + "=" + enqueteId, null) > 0;
	}
	
	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE,null,null) > 0;
	}

	public Cursor findCursor(final Integer codigo){
		return database.query(DATABASE_TABLE, FIELDS, ID+ " = ?", new String[] { codigo + "" }, null, null, null);
	}

	public Pergunta find(final int id) {
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}
	public Cursor fetchAll() {
		return database.query(DATABASE_TABLE, FIELDS, null, null, null, null, null);
	}

	public List<Pergunta> fetchAllParsed() {
		Cursor cursor = fetchAll();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	public Cursor fetchAll(Enquete enquete) {
		return database.query(DATABASE_TABLE, FIELDS, ENQUETE_ID+ " = ?", new String[] { enquete.getId() + "" }, null, null, null);
	}

	public List<Pergunta> fetchAllParsed(Enquete enquete) {
		Cursor cursor = fetchAll(enquete);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	private List<Pergunta> parse(Cursor cursor) {
		List<Pergunta> toReturn = null;
		if (cursor != null) {
			toReturn = new ArrayList<Pergunta>();
			Pergunta obj;
			if (cursor.moveToFirst()) {
				while (!cursor.isAfterLast()) {
					obj = parseObject(cursor);
					toReturn.add(obj);
					cursor.moveToNext();
				}
			}
		}

		return toReturn;
	}
	
	private Pergunta parseObject(Cursor cursor) {
		Pergunta obj = null;
		obj = new Pergunta();
		obj.set_id(cursor.getInt(0));
		obj.setId(cursor.getInt(1));
		obj.setPergunta(cursor.getString(2));
		obj.setTipoEntrada(cursor.getString(3));
		return obj;
	}
	
	private ContentValues createContentValues(Pergunta obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(PERGUNTA, obj.getPergunta());
		values.put(TIPO_ENTRADA, obj.getTipoEntrada());
		values.put(ENQUETE_ID, obj.getEnqueteId());
		return values;
	}

	private ContentValues createUpdateContentValues(Pergunta obj) {
		ContentValues values = new ContentValues();
		values.put(PERGUNTA, obj.getPergunta());
		values.put(TIPO_ENTRADA, obj.getTipoEntrada());
		values.put(ENQUETE_ID, obj.getEnqueteId());
		return values;
	}
}
