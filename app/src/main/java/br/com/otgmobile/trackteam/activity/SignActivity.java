package br.com.otgmobile.trackteam.activity;

import java.io.ByteArrayOutputStream;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import br.com.otgmobile.trackteam.R;


public class SignActivity extends GenericActivity implements OnTouchListener {
	
	ImageView imageView;
	Bitmap bitmap;
	Paint paint;
	Canvas canvas;

	private int lastX = -1;
	private int lastY = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign);
		
		configureActivity();
		createCanvas();
	}
//
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	}
	
	private void configureActivity(){
		imageView = (ImageView) findViewById(R.id.sign_image_view);
	}
	
	private void createCanvas(){
		Display curretDisplay = getWindowManager().getDefaultDisplay();
		float dw = curretDisplay.getWidth();
		float dh = curretDisplay.getHeight();
		bitmap = Bitmap.createBitmap((int)dw, (int)dh, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(bitmap);
		canvas.drawColor(Color.WHITE);
		paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setStrokeWidth(3);
		imageView.setImageBitmap(bitmap);
		imageView.setOnTouchListener(this);
	}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sign, menu);
		return true;
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.sign_confirm:
        	saveSign();
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	private void saveSign(){
		Intent returnIntent = new Intent();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		 returnIntent.putExtra("sign",byteArray);
		 setResult(RESULT_OK,returnIntent);     
		 finish();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int X = (int) event.getX();
		int Y = (int) event.getY();
		
		switch (event.getAction()) {
		
			case MotionEvent.ACTION_UP:
				this.lastX = -1;
				break;

			case MotionEvent.ACTION_DOWN:
				if (this.lastX != -1) {
					if ((int) event.getX() != this.lastX) {
						canvas.drawLine(this.lastX, this.lastY, X, Y,
							paint);
					}
				}
				this.lastX = (int) event.getX();
				this.lastY = (int) event.getY();
				break;

			case MotionEvent.ACTION_MOVE:

				if (this.lastX != -1) {
					canvas.drawLine(this.lastX, this.lastY, X, Y,
							paint);
				}

				this.lastX = (int) event.getX();
				this.lastY = (int) event.getY();
				break;
			}
		imageView.invalidate();
		return true;
	}

}
