package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.Resposta;

public class RespostaDAO {
	
	public static final String _ID = "_id";
	public static final String RESPOSTA = "resposta";
	public static final String PERGUNTAID = "pergunta_id";
	public static final String OCORRENCIAID = "ocorrencia_id";
	
	private static String[] FIELDS = {_ID, RESPOSTA, PERGUNTAID, OCORRENCIAID};
	
	private static final String DATABASE_TABLE = "resposta";
	private SQLiteDatabase database;
	
	private Context context;
	
	public RespostaDAO(Context context){
		database = DBHelper.getDatabase(context);
		this.context = context;
	}
	
	public long save(Resposta obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = find(obj.getId()).getId(); 
			update(obj);
		}
		return inserted;
	}
	private boolean isNew(Resposta obj) {
		return true;
	}
	
	private long create(Resposta obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}
	
	private long update(Resposta obj) {
		ContentValues values = createUpdateContentValues(obj); 
		return database.update(DATABASE_TABLE, values, _ID+"=?", new String[]{obj.getId().toString()});
	}
	public boolean delete(final Resposta obj){
		String whereClause = _ID + " = " +obj.getId().toString();
		return database.delete(DATABASE_TABLE, whereClause , null) > 0;
	}
	
	public Resposta find(int codigo){
		Cursor cursor = findCursor(codigo);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}
			
			return parseObject(cursor);
		} finally {
			cursor.close();
		}
		
	}
	
	private Cursor findCursor(int codigo){
		return database.query(DATABASE_TABLE, FIELDS, _ID+" = ?",new String[]{codigo+""}, null, null, null);
	}
	
	public Cursor fetchAll() {
		return database.query(DATABASE_TABLE, FIELDS, null, null,
				null, null, null);
	}
	
	public List<Resposta> fetchAllParsed(){
		Cursor cursor = fetchAll();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
		
	}
	
	private List<Resposta> parse(Cursor cursor) {
		List<Resposta> toReturn = new ArrayList<Resposta>();
		if(cursor !=null){
			if(cursor.moveToFirst()){
				Resposta obj;
				while(!cursor.isAfterLast()){
					obj = parseObject(cursor);
					toReturn.add(obj);
					cursor.moveToNext();
				}
			}
		}
		return toReturn;
	}
	
	private Resposta parseObject(Cursor cursor) {
		Resposta obj = null;
		obj = new Resposta();
		obj.setId(DatabaseUtil.returnNullValueForInt(cursor,_ID));
		obj.setResposta(cursor.getString(1));
		obj.setPergunta_id(cursor.getInt(2));
		obj.setOcorrencia_id(cursor.getInt(3));
		return obj;
	}
	
	private ContentValues createContentValues(Resposta obj) {
		ContentValues values = new ContentValues();
		values.put(RESPOSTA, obj.getResposta());
		values.put(PERGUNTAID, obj.getPergunta_id());
		values.put(OCORRENCIAID, obj.getOcorrencia_id());
		return values;
	}
	
	private ContentValues createUpdateContentValues(Resposta obj) {
		ContentValues values = new ContentValues();
		values.put(RESPOSTA, obj.getResposta());
		values.put(PERGUNTAID, obj.getPergunta_id());
		values.put(OCORRENCIAID, obj.getOcorrencia_id());
		return values;
	}

}
