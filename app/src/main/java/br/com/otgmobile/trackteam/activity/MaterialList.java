package br.com.otgmobile.trackteam.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.adapter.MaterialAdapter;
import br.com.otgmobile.trackteam.database.MaterialDAO;
import br.com.otgmobile.trackteam.model.Material;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.Session;

public class MaterialList extends GenericListActivity {
	
	private List<Material> list;
	private boolean toOcorrencia = false;
	private MaterialAdapter adapter ;
	private Ocorrencia ocorrencia;
	private boolean forRequest;
	private LinearLayout materialListContainer;
	private Button socketStatus;

	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_materiais);
		configureActivity();
		handleIntent();
		search();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}
	
	@Override
	protected void onDestroy() {
		setListAdapter(null);
		AppHelper.unbindDrawables(materialListContainer);
		super.onDestroy();
	}

	@Override
	public void onBackPressed(){
		finishMaterialList();
		super.onBackPressed();
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (toOcorrencia) return true ;
		else return false;
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu material_list_menu) {
		MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.material_list_menu, material_list_menu);
        return true;
	};
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.material_list_menu_capture_qrcode:
        	captureQRCode();
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	private void captureQRCode(){
		try {

		    Intent intent = new Intent("com.google.zxing.client.android.SCAN");
		    intent.putExtra("SCAN_MODE", "QR_CODE_MODE"); // "PRODUCT_MODE for bar codes

		    startActivityForResult(intent, 0);

		} catch (Exception e) {

		    Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
		    Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
		    startActivity(marketIntent);

		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {           
	    super.onActivityResult(requestCode, resultCode, data);
	    if (requestCode == 0) {

	        if (resultCode == RESULT_OK) {
	            String contents = data.getStringExtra("SCAN_RESULT");
	            
	            for(Material material : list){
	            	if(material.getCodigo().equals(contents)){
	            		List<Material> mList;
	            		mList = new ArrayList<Material>();
	            		mList.add(material);
	            		adapter.setCheckedItems(mList);
	            	}
	            }
	        }
	        if(resultCode == RESULT_CANCELED){
	            //handle cancel
	        }
	    }
	}
	
	private void configureActivity() {
		materialListContainer = (LinearLayout) findViewById(R.id.list_materiais_container);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}	

	public void handleIntent(){
		Intent intent = getIntent();
		if(intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)){
			ocorrencia = (Ocorrencia)  intent.getExtras().get(ConstUtil.SERIALIZABLE_KEY);
			toOcorrencia = true;
			forRequest = intent.getBooleanExtra(ConstUtil.FOR_MATERIAL_REQUEST, false);
		}
	}
	
	public void search() {
		if(!forRequest){
			MaterialDAO dao = new MaterialDAO(this);
			list = dao.fetchAVailableParsed();
			if(list!=null){
				setAdapter();
			}			
		}else{
			list = ocorrencia.getMateriais();
			if(list!=null){
				setAdapter();
			}			
		}
	}
	

	private void setAdapter(){
		adapter = new MaterialAdapter(list, this,toOcorrencia);
		if(ocorrencia!= null && ocorrencia.getMateriais()!=null){
			adapter.setCheckedItems(ocorrencia.getMateriais());
		}
		setListAdapter(adapter);
	}
	
	public void checkButtonOnClick(View view){
		adapter.checkAll();
	}
	
	public void unCheckButtonOnClick(View view){
		adapter.uncheckAll();
	}
	
	public  void returnButtonOnClick(View view){
		finishMaterialList();
	}
	
	private void finishMaterialList(){
		if(toOcorrencia){
			quitAndPassMaterials();
			return;
		}
		finish();
	}
	
	private void quitAndPassMaterials() {
		if(ocorrencia.getMateriais() == null || !ocorrencia.getMateriais().equals(adapter.getCheckedItems())){
			ocorrencia.setEnviado(false);
		}
		ocorrencia.setMateriais(adapter.getCheckedItems());
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		setResult(RESULT_OK,intent);
		finish();
	}
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}

}
