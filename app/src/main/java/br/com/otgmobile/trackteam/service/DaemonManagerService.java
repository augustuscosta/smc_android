package br.com.otgmobile.trackteam.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import br.com.otgmobile.trackteam.daemon.DaemonManager;

public class DaemonManagerService extends Service {

	public static final String DAEMON_MANAGER = "DAEMON_MANAGER_INTENT";

	public static void start(Context context) {
		context.startService(new Intent(DAEMON_MANAGER));
	}

	public static void stop(Context context) {
		context.stopService(new Intent(DAEMON_MANAGER));
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (!DaemonManager.startDaemons(getApplicationContext())) {
			stopSelf();
		}

		return super.onStartCommand(intent, flags, startId);
	}

	
	@Override
	public void onDestroy() {
		DaemonManager.stopRunner();
		super.onDestroy();
	}

	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}