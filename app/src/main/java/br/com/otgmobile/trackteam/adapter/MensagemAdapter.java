package br.com.otgmobile.trackteam.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Mensagem;
import br.com.otgmobile.trackteam.row.MensagemRowHolderInfo;

public class MensagemAdapter extends BaseAdapter {

	private Activity context;
	private List<Mensagem> list;

	public MensagemAdapter(Activity context, List<Mensagem> list) {
		this.context = context;
		this.list = list;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list == null ? 0 :list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		MensagemRowHolderInfo rowInfo;

		if (view == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.mensgem_row, null);

			rowInfo = new MensagemRowHolderInfo();

			rowInfo.messageRowContainer = (LinearLayout) row.findViewById(R.id.menssageRowContainer);
			rowInfo.messageOwner 		= (TextView) row.findViewById(R.id.messageOwner);
			rowInfo.messageContent 		= (TextView) row.findViewById(R.id.messageContent);
			rowInfo.messageDate 		= (TextView) row.findViewById(R.id.messageDate);
			rowInfo.mensagemUnsentFlag 	= (Button) row.findViewById(R.id.messageUnsentFlag);
			
			row.setTag(rowInfo);
		} else {
			rowInfo = (MensagemRowHolderInfo) row.getTag();	
		}

		Mensagem obj = list.get(position);
		rowInfo.drawRow(obj, context);
		return row;
	}

}
