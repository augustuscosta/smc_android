package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import br.com.otgmobile.trackteam.model.Filmagem;

public class FilmagemDAO implements BaseColumns{

	// Database fields

	public static final String ID = "id";
	public static final String FILMAGEM = "filmagem";
	public static final String OCORRENCIA_ID = "ocorrencia_id";
	public static final String OCORRENCIA_SERVER_ID = "ocorrencia_server_id";
	public static final String ENVIADO = "enviado";

	private static final String[] FIELDS = {_ID,ID,FILMAGEM,OCORRENCIA_ID,OCORRENCIA_SERVER_ID,ENVIADO}; 
	private static final String DATABASE_TABLE = "filmagem_ocorrencia";
	private SQLiteDatabase database;
	private Context context;

	public FilmagemDAO(Context context) {
		this.context = context;
		database = DBHelper.getDatabase(context);
	}

	public long save(Filmagem obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = update(obj);
		}

		return inserted;
	}

	private boolean isNew(Filmagem obj) {
		if(obj.get_id() == null || find(obj.get_id()) == null)
			return true;
		return false;
	}

	public long create(Filmagem obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	public long update(Filmagem obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, _ID + " = " +obj.get_id(),null);
	}

	public boolean delete(Integer id) {
		Filmagem obj = find(id);
		context.getContentResolver().delete(Uri.parse(obj.getFilmagem()), null, null);
		return database.delete(DATABASE_TABLE, _ID + "=" + id, null) > 0;
	}

	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE, null, null) > 0;
	}

	private Cursor findCursor(int codigo) {
		return database.query(DATABASE_TABLE, FIELDS, _ID + " = ?", new String[] { codigo + "" }, null,
				null, null);
	}

	private Cursor findFromOcorrenciaCursor(int codigo) {
		return database.query(DATABASE_TABLE, FIELDS, OCORRENCIA_ID + " = ?", new String[] { codigo + "" }, null,
				null, null);
	}
	
	private Cursor findCursorFromOcorrencianotSent(int codigo) {
		String whereCaluse = OCORRENCIA_ID +" = "+ codigo +" AND "+ ENVIADO +" is null" ;
		return database.query(DATABASE_TABLE, FIELDS,whereCaluse, null, null, null, null);
	}

	public Filmagem find(int id) {
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}

	
	public void deleteFotoFilesFromOcorrencia(int codigo){
		List<Filmagem> filmagens = findFromOcorrencia(codigo);
		if(filmagens!=null && !filmagens.isEmpty()){
			for(Filmagem filmagem:filmagens){
				delete(filmagem.get_id());
			}
		}
	}
	
	public List<Filmagem> findFromOcorrencia(int id) {
		Cursor cursor = findFromOcorrenciaCursor(id);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	public List<Filmagem> findFromOcorrenciaNotSent(int id) {
		Cursor cursor = findCursorFromOcorrencianotSent(id);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}

	public Cursor fetchAll() {
		return database.query(DATABASE_TABLE, FIELDS, null, null, null, null, null);
	}

	public List<Filmagem> fetchAllParsed() {
		Cursor cursor = fetchAll();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}

	private List<Filmagem> parse(Cursor cursor) {
		List<Filmagem> toReturn = null;
		if (cursor != null) {
			toReturn = new ArrayList<Filmagem>();
			Filmagem obj;
			if (cursor.moveToFirst()) {
				while (!cursor.isAfterLast()) {
					obj = parseObject(cursor);
					toReturn.add(obj);
					cursor.moveToNext();
				}
			}
		}

		return toReturn;
	}

	private Filmagem parseObject(Cursor cursor) {
		Filmagem obj = new Filmagem();
		obj.set_id(cursor.getInt(0));
		obj.setId(cursor.getInt(1));
		obj.setFilmagem(cursor.getString(2));
		obj.setOcorrenciaId(DatabaseUtil.returnNullValueForInt(cursor, OCORRENCIA_ID));
		obj.setOcorrenciaServerId(DatabaseUtil.returnNullValueForInt(cursor, OCORRENCIA_SERVER_ID));
		obj.setEnviado(DatabaseUtil.returnNullValueForLong(cursor, ENVIADO));
		return obj;
	}

	private ContentValues createContentValues(Filmagem obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(FILMAGEM, obj.getFilmagem());
		values.put(OCORRENCIA_ID, obj.getOcorrenciaId());
		values.put(OCORRENCIA_SERVER_ID, obj.getOcorrenciaServerId());
		values.put(ENVIADO, obj.getEnviado());
		return values;
	}

	private ContentValues createUpdateContentValues(Filmagem obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(FILMAGEM, obj.getFilmagem());
		values.put(OCORRENCIA_ID, obj.getOcorrenciaId());
		values.put(OCORRENCIA_SERVER_ID, obj.getOcorrenciaServerId());
		values.put(ENVIADO, obj.getEnviado());
		return values;
	}

}
