package br.com.otgmobile.trackteam.task;

public interface SyncCallBack {

	public void finishTask(boolean ok);
	
}
