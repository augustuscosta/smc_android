package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

public class Vitima implements Serializable {

	private static final long serialVersionUID = 1577637245579738044L;

	private Integer _id;

	private Integer id;

	private String cpf;

	private String nome;
	
	private String rg;

	private String descricao;

	private Integer idade;

	private Integer ocorrenciaID;

	private Ocorrencia ocorrencia;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public Integer getOcorrenciaID() {
		return ocorrenciaID;
	}

	public void setOcorrenciaID(Integer ocorrenciaID) {
		this.ocorrenciaID = ocorrenciaID;
	}

	public Ocorrencia getOcorrencia() {
		return ocorrencia;
	}

	public void setOcorrencia(Ocorrencia ocorrencia) {
		this.ocorrencia = ocorrencia;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public Integer get_id() {
		return _id;
	}

	public void set_id(Integer _id) {
		this._id = _id;
	}


}
