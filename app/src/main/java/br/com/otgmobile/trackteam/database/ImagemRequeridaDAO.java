package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import br.com.otgmobile.trackteam.model.ImagemRequerida;

public class ImagemRequeridaDAO implements BaseColumns{

	// Database fields
	public static final String ID = "id";
	public static final String IMAGE = "image";
	
	
	private static final String[] FIELDS = {_ID,ID,IMAGE};
	private static final String DATABASE_TABLE = "imagem_requerida";
	private SQLiteDatabase database;
	private Context context;

	public ImagemRequeridaDAO(Context context) {
		this.context = context;
		database = DBHelper.getDatabase(context);
	}

	public long save(ImagemRequerida obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = update(obj);
		}

		return inserted;
	}

	private boolean isNew(ImagemRequerida obj) {
		if(obj.get_id() == null || find(obj.get_id()) == null)
			return true;
		return false;
	}

	public long create(ImagemRequerida obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	public long update(ImagemRequerida obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, _ID + " = " +obj.get_id(),null);
	}

	private boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, _ID + "=" + id, null) > 0;
	}
	
	public boolean delete(ImagemRequerida foto) {
		boolean toReturn = delete(foto.get_id());
		if(toReturn){
			deleteImagemRequeridaFile(foto);
		}
		return toReturn;
	}
	
	public void deleteImagemRequeridaFile(ImagemRequerida foto){
		context.deleteFile(foto.getImagem());
		delete(foto.get_id());
	}

	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE, null, null) > 0;
	}

	private Cursor findCursor(int codigo) {
		return database.query(DATABASE_TABLE,FIELDS, _ID + " = ?", new String[] { codigo + "" }, null,
				null, null);
	}

	public ImagemRequerida find(int id) {
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}

	
	public Cursor fetchAll() {
		return database.query(DATABASE_TABLE, new String[] { _ID, IMAGE}, null, null, null, null, null);
	}

	public List<ImagemRequerida> fetchAllParsed() {
		Cursor cursor = fetchAll();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}

	private List<ImagemRequerida> parse(Cursor cursor) {
		List<ImagemRequerida> toReturn = null;
		if (cursor != null) {
			toReturn = new ArrayList<ImagemRequerida>();
			ImagemRequerida obj;
			if (cursor.moveToFirst()) {
				while (!cursor.isAfterLast()) {
					obj = parseObject(cursor);
					toReturn.add(obj);
					cursor.moveToNext();
				}
			}
		}

		return toReturn;
	}

	private ImagemRequerida parseObject(Cursor cursor) {
		ImagemRequerida obj = null;
		obj = new ImagemRequerida();
		obj.set_id(cursor.getInt(0));
		obj.setImagem(cursor.getString(1));
		return obj;
	}

	private ContentValues createContentValues(ImagemRequerida obj) {
		ContentValues values = new ContentValues();
		values.put(IMAGE, obj.getImagem());
		return values;
	}

	private ContentValues createUpdateContentValues(ImagemRequerida obj) {
		ContentValues values = new ContentValues();
		values.put(IMAGE, obj.getImagem());
		return values;
	}
}
