package br.com.otgmobile.trackteam.activity;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.ConfiguracaoVisualizaOcorrencia;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.Session;

public class SettingsActivity extends GenericActivity {

	private Button btnNativeSettings;
	private Button socketStatus;
	private LinearLayout settingsContainer;

	
	public static boolean open(Activity activity) {
		if ( activity == null ) {
			return false;
		}
		
		activity.startActivityForResult(new Intent(activity, SettingsActivity.class), ConstUtil.REQUEST_CODE_SETTINGS);
		return true;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		loadComponentsFromView();
		setComponentListeners();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}

	private void loadComponentsFromView() {
		btnNativeSettings = (Button) findViewById(R.id.btnNativeSettings);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
		settingsContainer = (LinearLayout) findViewById(R.id.settingsContainer);
	}
	
	private void setComponentListeners() {
		btnNativeSettings.setOnClickListener(onClickNativeSettings);
	}
	
	private OnClickListener onClickNativeSettings = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			startActivity(new Intent(Settings.ACTION_SETTINGS));
		}
	};
	
	public void configComms(View view){
		Intent intent = new Intent(SettingsActivity.this,MainActivity.class);
		startActivityForResult(intent, ConstUtil.COMM_SETTINGS_REQUEST);
	}
	
	public void configOcorrenciaView(View view){
		openTypeChooser();
	}
	
	/*public void returnButtonOnClick(View view){
		finish();
	}*/
	
	//Selecao de visualizaco de ocorrencia
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.visualiza_ocorrencia_menu, menu);
		
	}
	
	private void openTypeChooser(){
		registerForContextMenu( settingsContainer ); 
		openContextMenu( settingsContainer );
	}
	
	@Override 
	public boolean onContextItemSelected(MenuItem item) {  
		switch (item.getItemId()) {
		case R.id.visualizaAtendimento:
			setVisualizacaoType(0);
			return true;
		case R.id.visualizaEmergencia:
			setVisualizacaoType(1);
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}
	
	private void setVisualizacaoType(int i) {
		Session.setConfiguracaoVisualizaOcorrencia(ConfiguracaoVisualizaOcorrencia.fromValue(i), this);
	}

	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}

	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}

	protected void setSocketStatus() {
		SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);

		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));

			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}

	@Override
    public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    	return true;
    };
	
	@Override
	public void onLocationChanged(Location location) {
	super.onLocationChanged(location);
		
	}

	@Override
	public void onStatusChanged(int status) {
		super.onStatusChanged(status);
		
	}
	
}
