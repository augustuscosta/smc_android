package br.com.otgmobile.trackteam.maps.overlay;

import java.util.ArrayList;

import android.graphics.drawable.Drawable;
import br.com.otgmobile.trackteam.model.Ocorrencia;

import com.google.android.maps.ItemizedOverlay;

public class OcorrenciaOverlay extends ItemizedOverlay<OcorrenciaOverlayItem> {
	private ArrayList<OcorrenciaOverlayItem> mOverlays = new ArrayList<OcorrenciaOverlayItem>();
	private OcorrenciaOverlayCallback callback;
	
	public OcorrenciaOverlay(Drawable defaultMarker, OcorrenciaOverlayCallback callback) {
		super(boundCenterBottom(defaultMarker));
		this.callback = callback;
	}
	
	public OcorrenciaOverlay(OcorrenciaOverlayCallback callback) {
		super(null);
		this.callback = callback;
	}
	
	public void addOverlay(OcorrenciaOverlayItem overlay) {
	    mOverlays.add(overlay);
	    populate();
	}

	@Override
	protected OcorrenciaOverlayItem createItem(int i) {
		return mOverlays.get(i);
	}

	@Override
	public int size() {
		return mOverlays.size();
	}
	
	@Override
	protected boolean onTap(int index) {
		Ocorrencia ocorrencia = mOverlays.get(index).getOcorrencia();
		callback.ocorrenciaSelected(ocorrencia);
		return super.onTap(index);
	}


}
