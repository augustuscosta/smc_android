package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import br.com.otgmobile.trackteam.model.Endereco;


public class EnderecoDAO implements BaseColumns {

	// Database fields
	public static final String ID = "id";
	public static final String LATITUDE ="latitude";
	public static final String LONGITUDE ="longitude";
	public static final String ENDGEOREF ="end_georef";
	public static final String BAIRRO ="bairro";
	public static final String LOGRADOURO ="logradouro";
	public static final String NUMERO ="numero";
	public static final String[] COLUMNS = new String[] { _ID,ID,LATITUDE,LONGITUDE,ENDGEOREF,BAIRRO,LOGRADOURO,NUMERO };

	private static final String DATABASE_TABLE = "endereco";
	private SQLiteDatabase database;

	public EnderecoDAO(Context context){
		database = DBHelper.getDatabase(context);
	}

	public long save(Endereco obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			update(obj);
			inserted = find(obj.getId()).get_id(); 
		}

		return inserted;
	}

	private boolean isNew(Endereco obj) {
		if((obj.getId()) == null){
			return true;
		}
		if(find(obj.getId())==null){
			return true;
		}

		return false;
	}

	public long create(Endereco obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	public long update(Endereco obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, ID+ " = ?",new String[] { obj.getId().toString()});
	}

	public boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, ID + "=" + id, null) > 0;
	}

	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE,null,null) > 0;
	}

	public Cursor findCursor(final Integer codigo){
		return database.query(DATABASE_TABLE, COLUMNS, ID+ " = ?", new String[] { codigo + "" }, null, null, null);
	}

	public Cursor findLocalCursor(final Integer codigo){
		return database.query(DATABASE_TABLE, COLUMNS, _ID+ " = ?", new String[] { codigo + "" }, null, null, null);
	}

	public Endereco find(final int id) {
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}

	public Endereco findLocal(final int id) {
		Cursor cursor = findLocalCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}

	public List<Endereco> fetchAllParsed(){
		Cursor cursor = findCursor(null);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}

	private List<Endereco> parse(Cursor cursor){
		List<Endereco> toReturn = new ArrayList<Endereco>();
		if(cursor.moveToFirst()){
			while(!cursor.isAfterLast()){
				toReturn.add(parseObject(cursor));
				cursor.moveToNext();
			}
		}

		return toReturn; 
	}

	private Endereco parseObject(Cursor cursor){
		Endereco obj = new Endereco();
		obj.set_id(cursor.getInt(0));
		obj.setId(cursor.getInt(1));
		obj.setLatitude(cursor.getFloat(2));
		obj.setLongitude(cursor.getFloat(3));
		obj.setEndGeoref(cursor.getString(4));
		obj.setBairro(cursor.getString(5));
		obj.setLogradouro(cursor.getString(6));
		obj.setNumero(cursor.getString(7));
		return obj;
	}

	private ContentValues createContentValues(Endereco obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(LATITUDE, obj.getLatitude());
		values.put(LONGITUDE, obj.getLongitude());
		values.put(ENDGEOREF, obj.getEndGeoref());
		values.put(BAIRRO, obj.getBairro());
		values.put(LOGRADOURO, obj.getLogradouro());
		values.put(NUMERO, obj.getNumero());
		return values;
	}

	private ContentValues createUpdateContentValues(Endereco obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(LATITUDE, obj.getLatitude());
		values.put(LONGITUDE, obj.getLongitude());
		values.put(ENDGEOREF, obj.getEndGeoref());
		values.put(BAIRRO, obj.getBairro());
		values.put(LOGRADOURO, obj.getLogradouro());
		values.put(NUMERO, obj.getNumero());
		return values;
	}

}
