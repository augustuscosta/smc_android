package br.com.otgmobile.trackteam.model;

import java.io.Serializable;
import java.util.List;

public class LocalizaMaterial implements Serializable {

	
	private static final long serialVersionUID = -8815750333513035822L;
	
	private Integer _id;
	private Integer id;
	private String Descricao;
	private Integer enderecoId;
	private Endereco endereco;
	private List<Material> materiais;
	
	public Integer get_id() {
		return _id;
	}
	
	public void set_id(Integer _id) {
		this._id = _id;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDescricao() {
		return Descricao;
	}
	
	public void setDescricao(String descricao) {
		Descricao = descricao;
	}
	
	public Integer getEnderecoId() {
		return enderecoId;
	}

	public void setEnderecoId(Integer enderecoId) {
		this.enderecoId = enderecoId;
	}

	public Endereco getEndereco() {
		return endereco;
	}
	
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	public List<Material> getMateriais() {
		return materiais;
	}
	
	public void setMateriais(List<Material> materiais) {
		this.materiais = materiais;
	}

}
