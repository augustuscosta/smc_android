package br.com.otgmobile.trackteam.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Comentario;
import br.com.otgmobile.trackteam.row.ComentarioRowHolderInfo;
import br.com.otgmobile.trackteam.util.HandleListButton;
import br.com.otgmobile.trackteam.util.SelectListItem;

public class ComentarioAdapter extends BaseAdapter {
	
	List<Comentario> list;
	Activity context;
	HandleListButton handler;
	
	public ComentarioAdapter(List<Comentario> list, Activity context, HandleListButton handler){
		this.list = list;
		this.context = context;
		this.handler = handler;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list == null ? 0 :list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		ComentarioRowHolderInfo rowInfo;
		if(view == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.comentario_row, null);
			
			rowInfo = new ComentarioRowHolderInfo();
			rowInfo.comentario = (TextView) row.findViewById(R.id.comment);
			rowInfo.comentarioButton = (LinearLayout) row.findViewById(R.id.comentario_list_button);
			row.setTag(rowInfo);
		}
		else{
			rowInfo = (ComentarioRowHolderInfo) view.getTag();
		}
		
		rowInfo.comentarioButton.setOnClickListener(new SelectListItem(position, handler, row));
		Comentario obj = list.get(position);
		rowInfo.drawRow(obj);
		return row;
	}

}
