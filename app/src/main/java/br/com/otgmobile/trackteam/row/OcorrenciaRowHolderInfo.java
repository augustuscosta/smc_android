package br.com.otgmobile.trackteam.row;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.util.AppHelper;



public class OcorrenciaRowHolderInfo {
	
	public TextView ocorrenciaListAddress;
	public TextView ocorrenciaListId;
	public TextView ocorrenciaListHoraProgramada;
	public Button ocorenciaListButton;
	public ImageView confirmImage;
	public Activity context;
	private Integer color;
	
	public void drawRow(Ocorrencia obj){		
		if(obj == null){
			return;
		}
		
		if(obj.isEnviado()){
			color = Color.WHITE;
		}else{
			color = Color.RED;
		}
		
		if(obj.getEndereco()!=null && obj.getEndereco().getEndGeoref()!=null){
			ocorrenciaListAddress.setText(obj.getEndereco().getEndGeoref());
			ocorrenciaListAddress.setTextColor(color);
		}
		
		else{
			ocorrenciaListAddress.setText("");
		}
		
		if(obj.getCodigo() != null){
			ocorrenciaListId.setText(obj.getCodigo().toString());
			ocorrenciaListId.setTextColor(color);
		}
		
		else{
			ocorrenciaListId.setText("");
		}
		
		if(obj.getInicioProgramado()!=null &&obj.getInicioProgramado()>0){
			ocorrenciaListHoraProgramada.setText(AppHelper.getInstance().formateDate(context, obj.getInicioProgramado()));
			ocorrenciaListHoraProgramada.setTextColor(color);
		}else{
			ocorrenciaListHoraProgramada.setText("");
		}
		
		setRowIcons(obj);
		
	}
	
	private void setRowIcons(Ocorrencia obj){
		if ((obj.getInicio() == null||obj.getInicio() == 0)) {
			ocorenciaListButton.setText(context.getResources().getString(R.string.answer));
			ocorenciaListButton
					.setCompoundDrawablesWithIntrinsicBounds(
							null,
							context.getResources().getDrawable(
									R.drawable.atender_button_background),
							null,
							null);
			confirmImage.setVisibility(View.GONE);
			ocorenciaListButton.setVisibility(View.VISIBLE);
		} else {
			if ((obj.getFim() == null||obj.getFim() == 0)) {
				ocorenciaListButton.setText(context.getResources().getString(R.string.finish));
				ocorenciaListButton.setCompoundDrawablesWithIntrinsicBounds(null,context.getResources().getDrawable(
						R.drawable.finalizar_button_background),
						null,null);
				confirmImage.setVisibility(View.GONE);
				ocorenciaListButton.setVisibility(View.VISIBLE);
			} else {
				ocorenciaListButton.setVisibility(View.GONE);
				confirmImage.setVisibility(View.VISIBLE);
			}

			
		}
	}

}
