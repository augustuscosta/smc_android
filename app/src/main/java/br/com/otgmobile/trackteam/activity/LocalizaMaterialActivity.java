package br.com.otgmobile.trackteam.activity;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.cloud.rest.LocalizaMaterialCloud;
import br.com.otgmobile.trackteam.database.LocalizaMaterialDAO;
import br.com.otgmobile.trackteam.gps.DLGps;
import br.com.otgmobile.trackteam.gps.DLGpsObserver;
import br.com.otgmobile.trackteam.model.Endereco;
import br.com.otgmobile.trackteam.model.LocalizaMaterial;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

public class LocalizaMaterialActivity extends GenericActivity implements DLGpsObserver {

	// Activity's dynamic buttons
	private EditText resumeText;
	private EditText addressText;	
	private Location location;
	private LocalizaMaterial localizaMaterial;
	private Geocoder coder = null;
	private LocalizaMaterialDAO localizaMaterialDAO;
	private static GeocoderTask geocoderTask;
	private static SaveTask saveTask;
	private Button socketStatus;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.localiza_material);
		configureActivity();
		localizaMaterial =  new LocalizaMaterial();
		DLGps.addGpsObserver(this, this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}

	@Override
	protected void onPause() {
		System.gc();
		super.onPause();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	};

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu localiza_material_menu) {
		MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.localiza_material_menu, localiza_material_menu);
        return true;
	};
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.localiza_material_material_select:
        		getMaterials();
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	private void configureActivity() {
		// Setting the Views form the XML
		addressText = (EditText) findViewById(R.id.address_edit_text);
		resumeText = (EditText) findViewById(R.id.description_edit_text);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}

	public void selectedLocalizationButtonOnClick(View view) {
		getLocaltionWithMap();
	}

	public void geocodeButtonOnClick(View view) {
		runGeocodingTask();
	}

	@Override
	protected void onDestroy() {
		DLGps.removeGpsObserver(this, this);
		super.onDestroy();
	}

	// Starting and finishing the AsyncTasks

	private void runGeocodingTask() {
		finishTask();
		geocoderTask = new GeocoderTask(this);
		geocoderTask.execute();
	}

	private void finishTask() {
		if (geocoderTask != null) {
			geocoderTask.cancel(true);
			geocoderTask = null;
		}
	}



	// metods to simplify the code
	/**
	 * Converte as lista de emergencia, status e naturezas em String[].
	 */

	public String returnValue(String value) {

		if (value == null) {
			value = "";
		}

		return value;
	}

	// metods for the button's Clicks

	public void confirmButtonOnClick(View view) {
		saveLocalizacaoTask();
	}

	private void getLocaltionWithMap() {
		Intent intent = new Intent(LocalizaMaterialActivity.this, SelectLocationMap.class);
		startActivityForResult(intent, ConstUtil.MAP_SELECTION_REQUEST);

	}


	private void getMaterials() {
		Intent intent = new Intent(LocalizaMaterialActivity.this, MaterialList.class);
		Bundle bundle = new Bundle();
		Ocorrencia ocorrencia = new Ocorrencia();
		ocorrencia.setMateriais(localizaMaterial.getMateriais());
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.MATERIAL_REQUEST);
	}


	// Save Localização

	private void saveLocalizacaoTask() {
		if(validateOcorrencia()){
			localizaMaterial.setDescricao(resumeText.getText().toString());
			saveTask = new SaveTask(this);
			saveTask.execute();
		}else{
			AppHelper.getInstance().presentError(this, getString(R.string.ERROR_MISSINGINFO_TITLE), getString(R.string.preencher_dados_localiza_matterial));
		}
	}

	// Activity's Results
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {

		if (requestCode == ConstUtil.MATERIAL_REQUEST) {
			if (resultCode == RESULT_OK) {
				Ocorrencia ocorrencia;
				ocorrencia = (Ocorrencia) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY);
				localizaMaterial.setMateriais(ocorrencia.getMateriais());

			}
		}

		if (requestCode == ConstUtil.MAP_SELECTION_REQUEST) {
			if (resultCode == RESULT_OK) {
				Endereco obj = (Endereco) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY);
				if (obj != null) {
					localizaMaterial.setEndereco(obj);
					addressText
					.setText(localizaMaterial.getEndereco().getEndGeoref());
				}
			}
		}

	}

	// Async Tasks

	private class GeocoderTask extends AsyncTask<String, Void, String> {
		Context context;
		Address address;
		double latitude;
		double longitude;
		ProgressDialog mDialog;

		GeocoderTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "",
					getString(R.string.processing), true, false);
			mDialog.setCancelable(false);

		}

		@Override
		protected void onPreExecute() {
			getWindow()
			.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			setProgressBarIndeterminate(true);
		}

		@Override
		public String doInBackground(String... params) {
			return search();
		}

		String search() {
			if (location == null) {
				location = DLGps.getLastLocation(context);
				if (location == null) {
					return context.getResources().getString(
							R.string.error_location);

				}
			}
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			try {
				coder = new Geocoder(context);
				List<Address> foundAdresses = coder.getFromLocation(latitude,
						longitude, 1);
				if (foundAdresses != null && foundAdresses.size() > 0) {
					address = foundAdresses.get(0);

				} else {
					return context.getResources().getString(
							R.string.address_not_found);
				}
			} catch (Exception e) {
				return context.getResources().getString(
						R.string.search_not_possible);
			}

			return null;
		}

		@Override
		public void onPostExecute(String erroMessage) {
			getWindow().clearFlags(
					WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			geocoderTask = null;
			setProgressBarIndeterminate(false);
			mDialog.dismiss();

			if (erroMessage != null) {
				AppHelper.getInstance().presentError(context,
						getResources().getString(R.string.error), erroMessage);
			} else if (address != null) {
				addressText.setText(AppHelper.getInstance()
						.getAddressLineFromGoogleAddress(address));
				Endereco obj = new Endereco();
				obj.setEndGeoref(addressText.getText().toString());
				obj.setLatitude((float) latitude);
				obj.setLongitude((float) longitude);
				setEndereco(obj);
			}
		}
	}


	private class SaveTask extends AsyncTask<Void, Void, Void> {
		Context context;
		ProgressDialog mDialog;
		LocalizaMaterialCloud cloud = new LocalizaMaterialCloud();
		SaveTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "",
					getString(R.string.processing), true, false);
			mDialog.setCancelable(false);

		}

		@Override
		protected void onPreExecute() {
			getWindow()
			.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			setProgressBarIndeterminate(true);
		}

		@Override
		public Void doInBackground(Void... params) {
			save();
			return null;
		}

		void save() {
			try {
				localizaMaterialDAO().save(localizaMaterial);
				boolean a = cloud.sendMaterialLocation(context, localizaMaterial);				 

				if(a){
					localizaMaterialDAO().delete(localizaMaterial);
				}
			} catch (Throwable e) {
				LogUtil.e("erro ao criar atendimento",e);
			} 

		}

		@Override
		public void onPostExecute(Void result) {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			mDialog.cancel();
			saveTask = null;
			finish();
		}
	}

	private void setEndereco(Endereco endereco) {
		localizaMaterial.setEndereco(endereco);
	}

	@Override
	public void onLocationChanged(Location location) {
		this.location = location;
		super.onLocationChanged(location);
	}

	@Override
	public void onStatusChanged(int status) {
		super.onStatusChanged(status);
	}

	private boolean validateOcorrencia() {
		return (localizaMaterial.getEndereco() != null 
				&& resumeText.getText().length() > 0 && localizaMaterial.getMateriais() != null && !localizaMaterial.getMateriais().isEmpty());
	}

	private LocalizaMaterialDAO localizaMaterialDAO() {
		if(localizaMaterialDAO == null) {
			localizaMaterialDAO = new LocalizaMaterialDAO(this);
		}

		return localizaMaterialDAO;
	}

	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}

	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}

	protected void setSocketStatus() {
		SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);

		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));

			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}

}
