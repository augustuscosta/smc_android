package br.com.otgmobile.trackteam.util;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

public final class IntentUtil {
	public static boolean isIntentAvailable(Context context, Intent intent) {
		if (context == null || intent == null) {
			LogUtil.d("Does not exist Context or Intent, to check if exist Intent Listener");
			return false;
		}

		List<ResolveInfo> resolveInfo = context.getPackageManager()
				.queryIntentActivities(intent, PackageManager.GET_ACTIVITIES);
		if (resolveInfo == null || resolveInfo.isEmpty()) {
			LogUtil.d("Does not exist Intent(Listener) in S.O");
			return false;
		}

		return true;
	}

}
