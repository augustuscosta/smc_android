package br.com.otgmobile.trackteam.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.model.VeiculoEnvolvido;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.Session;



public class VeiculoDetail extends GenericActivity {

	private VeiculoEnvolvido veiculoE;
	private TextView placa;
	private TextView marca;
	private TextView ano;
	private TextView modelo;
	private Button socketStatus;
	private Button bateriaStatus;
	private Button telemetriaStatus;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vehicle);
		configureActivity();
		handleIntent();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}
	
	private void configureActivity() {
		placa  = (TextView) findViewById(R.id.license_name);
		marca  = (TextView) findViewById(R.id.brand_name);
		ano    = (TextView) findViewById(R.id.year_name);
		modelo = (TextView) findViewById(R.id.model_name);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		bateriaStatus = (Button) findViewById(R.id.bateriaStatusIcon);
		telemetriaStatus = (Button) findViewById(R.id.telemetriaStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}

	private void handleIntent() {
		Intent intent = getIntent();
		if(intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)){
			veiculoE = (VeiculoEnvolvido) intent.getExtras().get(ConstUtil.SERIALIZABLE_KEY);
			setVehicleDetailstoView();
		}
	}
	
	

	private void setVehicleDetailstoView() {
		if(veiculoE == null){
			return;
		}
		if(veiculoE.getPlaca()!=null){
			placa.setText(veiculoE.getPlaca());
		}
		else{
			placa.setText("");
		}
		
		if(veiculoE.getMarca()!=null){
			marca.setText(veiculoE.getMarca());
		}
		else{
			marca.setText("");
		}
		
		if(veiculoE.getModelo()!=null){
			modelo.setText(veiculoE.getModelo());
		}
		else{
			modelo.setText("");
		}
		
		if(veiculoE.getAno()!=null){
			ano.setText(veiculoE.getAno());
		}
		else{
			ano.setText("");
		}
	}
	
	
	public void returnButtonOnClick(View view){
		finish();
	}

	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}
	
	
	@Override
	protected void updateBatteryStatus(int percent) {
		if(percent <= 30){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_vermelha));
		}else if(percent <= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_amarela));
			
		}else if(percent >= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_verde));			
		}
		
	}
	
	@Override
	protected void updateTelemetriaStatusByBroadcast(String status) {
		if(status.equals(getString(R.string.connected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluegreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.blueyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluered));
			return;
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		super.onLocationChanged(location);
		
	}

	@Override
	public void onStatusChanged(int status) {
		super.onStatusChanged(status);
		
	}
	
}
