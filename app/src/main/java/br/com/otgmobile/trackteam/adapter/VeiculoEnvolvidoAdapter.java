package br.com.otgmobile.trackteam.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.VeiculoEnvolvido;
import br.com.otgmobile.trackteam.row.VeiculoEnvolvidoRowHolderInfo;

public class VeiculoEnvolvidoAdapter extends BaseAdapter {

	private List<VeiculoEnvolvido> list;
	private Activity context;
	private List<VeiculoEnvolvido> checkedList;

	public VeiculoEnvolvidoAdapter(List<VeiculoEnvolvido> list, Activity context){
		this.context = context;
		this.list = list;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list == null ? 0 : list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int positon, View view, ViewGroup parent) {
		View row = view;
		VeiculoEnvolvidoRowHolderInfo rowInfo;
		VeiculoEnvolvido obj = list.get(positon);

		if(row == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.veiculo_envolvido_row, null);

			rowInfo = new VeiculoEnvolvidoRowHolderInfo();
			rowInfo.involvedVehicleLicense = (TextView) row.findViewById(R.id.involved_vehicle_license);
			rowInfo.involvedVehicleBrand   = (TextView) row.findViewById(R.id.involved_vehicle_brand);
			row.setTag(rowInfo);
		}else{
			rowInfo = (VeiculoEnvolvidoRowHolderInfo) row.getTag();
		}

		row.setTag(R.layout.list_vehicles,obj);
		rowInfo.drawRow(obj);
		return row;
	}


	public List<VeiculoEnvolvido> getCheckedItems(){
		return checkedList;
	}



}
