package br.com.otgmobile.trackteam.bluetooth;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import br.com.otgmobile.trackteam.util.LogUtil;

/**
 * 
 * @author pierrediderot@gmail.com
 * @since 14/03/2012
 * @version $Revision:  $ <br>
 *          $Date:  $ <br> 
 *          $Author:  $
 */
public abstract class BluetoothConnector {

	public static final UUID UUID_RFCOMM_GENERICO = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	
	/**
	 * This interface defines callback to the discovery procedure. 
	 */
	public interface OnDiscoveryListener {
		public void onDiscoveryStarted();
		public void onDiscoveryFinished();
		public void onDiscoveryError(String error);
		public void onDeviceFound(String name, String address);
	}

	/** Holds when to use NKD components or native API. */  
	static boolean sExpectNDK = false;

	/** Initialize static data. */ 
	static {
		try { 
			Class.forName("android.bluetooth.BluetoothAdapter"); 
		} catch (ClassNotFoundException e) {
			sExpectNDK = true;
			LogUtil.e("Erro API not contais class android.bluetooth.BluetoothAdapter.", e);
		}
	}

	public static BluetoothConnector getConnector(Context context) throws IOException {		
		if ( sExpectNDK ) {
			return null;			
		} else {
			return new APIConnector(context);
		}		
	}

	public abstract void startDiscovery(OnDiscoveryListener listener) throws IOException;		
	public abstract void connect(String address) throws IOException;
	public abstract void close() throws IOException;
	public abstract InputStream getInputStream()throws IOException;
	public abstract OutputStream getOutputStream() throws IOException;
	
}

final class APIConnector extends BluetoothConnector  {

	private BluetoothAdapter mAdapter;
	private BluetoothSocket mSocket;
	private Context mContext;
	private OnDiscoveryListener mListener;

	final private BroadcastReceiver mReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();

			if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_STARTED)) {
				mListener.onDiscoveryStarted();

			} else if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {
				mListener.onDiscoveryFinished();
				mContext.unregisterReceiver(this);

			} else if (action.equals(BluetoothDevice.ACTION_FOUND)) {
				BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				mListener.onDeviceFound(device.getName(), device.getAddress());				
			}
		}				
	};

	public APIConnector(Context context) throws IOException {
		super();
		mContext = context;
		mAdapter = BluetoothAdapter.getDefaultAdapter();
		if ( mAdapter == null ) {
			throw new IOException("Bluetooth is not supported on this hardware platform.");
		}				
	}

	public void startDiscovery(OnDiscoveryListener listener) throws IOException {
		if ( listener == null ) {
			throw new NullPointerException("The listener is a null");
		} else if ( !mAdapter.isEnabled() ) {
			throw new IOException("Bluetooth is not enabled");
		}

		mAdapter.cancelDiscovery();
		mListener = listener;
		mContext.registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED));
		mContext.registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED));
		mContext.registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));	

		if ( !mAdapter.startDiscovery() ) {
			mContext.unregisterReceiver(mReceiver);			
		}
	}

	@Override
	public void close() throws IOException {
		if ( mSocket != null ) {
			getOutputStream().close();
			getInputStream().close();
			mSocket.close();
			mSocket = null;
		}
	}

	@Override
	public void connect(final String address) throws IOException {
		final BluetoothDevice device = mAdapter.getRemoteDevice(address);		
		mAdapter.cancelDiscovery();
		mSocket = device.createRfcommSocketToServiceRecord(UUID_RFCOMM_GENERICO);			
		mSocket.connect();				
	}

	@Override
	public InputStream getInputStream() throws IOException {
		if (mSocket == null) {
			throw new IOException("The stream is not connected");
		}
		return mSocket.getInputStream();
	}

	@Override
	public OutputStream getOutputStream() throws IOException {
		if (mSocket == null) {
			throw new IOException("The stream is not connected");
		}
		return mSocket.getOutputStream();
	}		
}
