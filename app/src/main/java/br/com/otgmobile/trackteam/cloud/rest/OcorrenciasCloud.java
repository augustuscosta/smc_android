
package br.com.otgmobile.trackteam.cloud.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import br.com.otgmobile.trackteam.database.ComentarioDAO;
import br.com.otgmobile.trackteam.database.OcorrenciaDAO;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

import com.google.gson.Gson;

public class OcorrenciasCloud extends RestClient {

	private final String URL = "comunicacao/ocorrencia";
	private OcorrenciaDAO dao;
	private final String ROOT_OBJECT = "result";
	private ComentarioDAO cdao;

	public List<Ocorrencia> list(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return getListFromResponse();
	}
	
	public Ocorrencia find(Integer id,Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("id", id.toString());
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return getObjectFromResponse();
	}
	
	public Ocorrencia merge(Ocorrencia ocorrencia,Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		Gson gson = new Gson();
		String ocorrenciaJson = gson.toJson(ocorrencia);
		addParam("ocorrencia", ocorrenciaJson);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.PUT);
		return getObjectFromResponse();
	}
	
	public Boolean sync(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return storeResponse(context);
	}
	
	private Boolean storeResponse(Context context) throws Exception {
		try {
			JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
			if (jsonArray != null) {
				Ocorrencia obj;
				Gson gson = new Gson();
				ocorrenciaDAO(context).deleteAll();
				comentarioDAO(context).deleteAll();
				for (int i = 0; i < jsonArray.length(); i++) {
					obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),Ocorrencia.class);
					ocorrenciaDAO(context).save(obj);
				}
			}
		} catch (Exception e) {
			LogUtil.e("erro ao pegar resopnse do sincronismo das ocorrencias", e);
			throw e;
		} finally {
		}
		return true;
	}

	private Ocorrencia getObjectFromResponse() throws JSONException {
		JSONObject jsonObject = getJsonObjectFromResponse(ROOT_OBJECT);
		if (jsonObject != null) {
			Ocorrencia toReturn;
			Gson gson = new Gson();
			toReturn = gson.fromJson(jsonObject.toString(),Ocorrencia.class);
			return toReturn;
		}
		return null;
	}
	

	private List<Ocorrencia> getListFromResponse() throws JSONException {
		JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
		if (jsonArray != null) {
			Ocorrencia obj;
			Gson gson = new Gson();
			List<Ocorrencia> toReturn = new ArrayList<Ocorrencia>();
			for (int i = 0; i < jsonArray.length(); i++) {
				obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),Ocorrencia.class);				
				toReturn.add(obj);
			}
			return toReturn;
		}
		return null;

	}
	
	public synchronized List<Integer> update(final Context context, final List<Ocorrencia> objs) {
		if ( objs == null ) return Collections.emptyList(); 

		List<Integer> sentIds = new ArrayList<Integer>();
		FotosCloud fotoCloud = new FotosCloud();
		SignCloud signCloud = new SignCloud();
		FilmagensCloud filmCloud = new FilmagensCloud();
		for (Ocorrencia obj : objs) {
			try {
				Ocorrencia fromServer = merge(obj, context);
				
				if(fromServer != null) fromServer.set_id(obj.get_id());
				
				if ( HttpStatus.SC_OK == getResponseCode() ) {
					if(obj.getId() == null && fromServer != null){
						obj.setId(fromServer.getId());
						obj.setCodigo(fromServer.getCodigo());
						obj.setValidada(fromServer.isValidada());
						ocorrenciaDAO(context).save(obj);
					}
					fotoCloud.uploadAllFotosFromOcorrencia(context, obj);
					signCloud.uploadAllSignsFromOcorrencia(context, obj);
					filmCloud.uploadAllFilmagens(context, obj);
					sentIds.add(obj.get_id());
				}
			} catch (Throwable e) {
				LogUtil.e("Erro ao tentar enviar ocorrencia	 pra cloud...", e);
			}
		}
		
		return sentIds;
	}
	
	private OcorrenciaDAO ocorrenciaDAO(Context context) {
		if ( dao == null ) {
			dao = new OcorrenciaDAO(context);
		}
		
		return dao;
	}
	
	private ComentarioDAO comentarioDAO(Context context) {
		if ( cdao == null ) {
			cdao = new ComentarioDAO(context);
		}
		
		return cdao;
	}
	
	public void sendQueue(Context context) {
		try {
			List<Ocorrencia> pendings = ocorrenciaDAO(context).retrieveAllPending();
			if ( pendings.isEmpty() ) {
				LogUtil.d("Nao existe ocorrencia pendente...");
				return;
			}
			
			List<Integer> sentIds = update(context, pendings);
			ocorrenciaDAO(context).updateSentStatus(sentIds, true);
		} catch (Throwable e) {
			LogUtil.e("Erro ao tentar processar ocorrencias pendentes", e);
		} finally {
			
		}
		
	}

}
