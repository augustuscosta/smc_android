package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.maps.GeoPoint;
import com.google.gson.annotations.SerializedName;

public class Historico implements Serializable {

	private static final long serialVersionUID = 6766549998526281287L;
	
	private Integer _id;
	private Integer id;
	private Long dataHora;
	private Float latitude;
	private Float longitude;
	private String temperatura;
	private String velocidade;
	private String tensaoBateria;
	private String rpm;
	private boolean enviado;
	@SerializedName("veiculo_id")
	private Integer veiculoID;
	@SerializedName("agente_id")
	private Integer agenteID;
	
	public GeoPoint getGeopoint(){
		double geoLatitude = getLatitude() * 1E6;
		double geoLongitude = getLongitude() * 1E6;
		return new GeoPoint((int) geoLatitude, (int) geoLongitude);
	}
	
	public LatLng getLatLng(){
		return new LatLng(getLatitude(), getLongitude());
	}
	
	public Integer get_id() {
		return _id;
	}

	public void set_id(Integer _id) {
		this._id = _id;
	}

	
	public boolean isEnviado() {
		return enviado;
	}

	public void setEnviado(boolean enviado) {
		this.enviado = enviado;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getDataHora() {
		return dataHora;
	}

	public void setDataHora(Long dataHora) {
		this.dataHora = dataHora;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}

	public String getVelocidade() {
		return velocidade;
	}

	public void setVelocidade(String velocidade) {
		this.velocidade = velocidade;
	}

	public String getTensaoBateria() {
		return tensaoBateria;
	}

	public void setTensaoBateria(String tensaoBateria) {
		this.tensaoBateria = tensaoBateria;
	}

	public String getRpm() {
		return rpm;
	}

	public void setRpm(String rpm) {
		this.rpm = rpm;
	}

	public Integer getVeiculoID() {
		return veiculoID;
	}

	public void setVeiculoID(Integer veiculoID) {
		this.veiculoID = veiculoID;
	}

	public Integer getAgenteID() {
		return agenteID;
	}

	public void setAgenteID(Integer agenteID) {
		this.agenteID = agenteID;
	}
	
	
	
}
