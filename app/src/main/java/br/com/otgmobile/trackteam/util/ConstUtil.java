package br.com.otgmobile.trackteam.util;

import br.com.otgmobile.trackteam.R;

public class ConstUtil {

	public static final int INVALID_TOKEN_CODE = 31425;
	public static final int INVALID_DEVICE_CODE = 31426;
	public static final String CONECTION_STATUS ="CONECTION_STATUS";
	public static final String TELEMETRIA_CONECTION_STATUS ="TELEMETRIA_CONECTION_STATUS";
	public static final String WRONG_USER_OR_PASSWORD = "WRONG_USER_OR_PASSWORD";
	public static final String INVALID_TOKEN = "INVALID_TOKEN";
	public static final String TOKEN = "token";
	public static final String INVALID_DEVICE = "INVALID_DEVICE";
	public static final String SERIALIZABLE_KEY = "SERIALIZABLE_KEY";
	public static final String ID_KEY = "ID_KEY";
	public static final String STRING_KEY = "STRING_KEY";
	public static final String PATH = "PATH";
	public static final String FOTO = "foto";
	public static final String SIGN = "sign";
	public static final String FILMAGEM = "filmagem";
	public static final String FOR_ATENDIMENTO = "filmagem";
	public static final String TEXTO = "texto";
	public static final String SIMNAO = "sim_nao";
	public static final String DATA = "data";
	public static final String DATAHORA = "data_hora";
	public static final String NUMERICO = "numero";
	public static final String BLOCK_DELETE_AND_CREATE = "block_delete_and_create";
	public static int LINES =110;
	public static final int MAP_SELECTION_REQUEST= 2231;
	public static final int SIGN_REQUEST = 2230;
	public static final int CAMERA_PIC_REQUEST = 2232;
	public static final int ADDRESS_SELECTED = 2233;
	public static final String ADDRESS_SELECTED_MESSAGE = "ADDRESS_SELECTED";
	public static final int MATERIAL_REQUEST = 2234;
	public static final int VITIM_REQUEST = 2235;
	public static final int VITIM_CAD_REQUEST = 2236;
	public static final int CALLER_REQUEST = 2237;
	public static final int FILM_REQUEST = 2238;
	public static final int OCORRENCIA_REQUEST = 2239;
	public static final int VEHICLE_REQUEST = 2240;
	public static final int VEHICLE_CAD_REQUEST = 2247;
	public static final int VEHICLE_EDIT_REQUEST = 2248;
	public static final int FOTO_REQUEST = 2241;
	public static final int FOOTAGE_REQUEST = 2242;
	public static final String SIGN_TYPE = "SIGN_TYPE";
	public static final String TALAO = "com.fotosensores.talaoeletronico.LOGIN";
	public static final int SECURITY_REQUEST = 2243;
	public static final int COMMENT_REQUEST = 2244;
	public static final int COMM_SETTINGS_REQUEST = 2245;
	public static final int STOP_NAVIGATION = 2246;
	public static final int MATERIAL_CHECKLIST = 2247;
	public static final int MATERIAL_ON_STREET = 2248;
	public static final int ENQUETE_REQUEST = 2249;
	public static final String LIST_POSITION ="list_position";
	public static final String FOR_MATERIAL_REQUEST ="request_materiais";
	
	public static final String OCORRENCIATAG = "ocorrencia";
	public static final String FOTOTAG = "foto";
	public static final String SIGNTAG = "sign";
	public static final String VIDEOTAG = "video";
	
	public static final int REQUEST_CODE_SETTINGS = R.layout.settings;
	public static final String LOCALIZA_MATERIAL = "localizaMaterial";
	public static final String RESPOSTA = "respostaJson";
	
	public static final char[] CAPITAL_LETTERS = {'A', 'B', 'C', 'D', 'E', 'F',
		'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
		'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
	
	public static final char[] NUMBERS = { '0', '1', '2', '3', '4', '5', '6', '7',
		'8', '9'};
}
