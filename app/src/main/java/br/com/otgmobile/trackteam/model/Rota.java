package br.com.otgmobile.trackteam.model;

import java.io.Serializable;
import java.util.List;

public class Rota implements Serializable {

	private static final long serialVersionUID = 1084953320670900761L;

	private Integer _id;

	private Integer id;

	private String descricao;

	private Long horaInicio;

	private Long horaFim;

	private List<GeoPonto> geoPonto;

	private Integer veiculoID;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Long horaInicio) {
		this.horaInicio = horaInicio;
	}

	public Long getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(Long horaFim) {
		this.horaFim = horaFim;
	}

	public List<GeoPonto> getGeoPonto() {
		return geoPonto;
	}

	public void setGeoPonto(List<GeoPonto> geoPonto) {
		this.geoPonto = geoPonto;
	}

	public Integer getVeiculoID() {
		return veiculoID;
	}

	public void setVeiculoID(Integer veiculoID) {
		this.veiculoID = veiculoID;
	}

	public Integer get_id() {
		return _id;
	}

	public void set_id(Integer _id) {
		this._id = _id;
	}

}
