package br.com.otgmobile.trackteam.cloud.rest;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

public class GoogleGeolocationCloud extends RestClient {

	private String GEOCODE_URL = "http://maps.google.com/maps/api/geocode/json?";
	private String ADDRESS_PARAMETER = "address";
	private String LAT_LNG_PARAMETER = "latlng";
	private String SENSOR_PARAMETER = "sensor";

	public LatLng reverseGeocode(String address, Context context) throws Exception {
		cleanParams();
		String geocoderUrl = GEOCODE_URL + ADDRESS_PARAMETER + "=" + address.replaceAll(" ", "+") + "&" + SENSOR_PARAMETER + "=" + "false"; 
		setUrl(geocoderUrl);
		execute(RequestMethod.GET);
		return getLatLngFromResponse();
	}

	private LatLng getLatLngFromResponse() throws Exception {
		JSONObject jsonObject = getJsonObjectFromResponse();
		Double longitude = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
				.getJSONObject("geometry").getJSONObject("location")
				.getDouble("lng");

		Double latitude = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
				.getJSONObject("geometry").getJSONObject("location")
				.getDouble("lat");
		return new LatLng(latitude, longitude);
	}
	
	public String geocode(LatLng latLng) throws Exception{
		cleanParams();
		String geocoderUrl = GEOCODE_URL + LAT_LNG_PARAMETER + "=" + getStringForLatLng(latLng) + "&" + SENSOR_PARAMETER + "=" + "false";
		setUrl(geocoderUrl);
		execute(RequestMethod.GET);
		return getStringAddressFromResponse();
	}
	
	private String getStringAddressFromResponse() throws Exception{
		JSONObject jsonObject = getJsonObjectFromResponse();
		return ((JSONArray) jsonObject.get("results")).getJSONObject(0).getString("formatted_address");
	}

	private String getStringForLatLng(LatLng latLng){
		return latLng.latitude + "," + latLng.longitude;
	}
	
	
}