package br.com.otgmobile.trackteam.activity;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.cloud.rest.GoogleGeolocationCloud;
import br.com.otgmobile.trackteam.cloud.rest.OcorrenciasCloud;
import br.com.otgmobile.trackteam.database.NaturezaDAO;
import br.com.otgmobile.trackteam.database.NivelEmergenciaDAO;
import br.com.otgmobile.trackteam.database.OcorrenciaDAO;
import br.com.otgmobile.trackteam.database.StatusDAO;
import br.com.otgmobile.trackteam.gps.DLGps;
import br.com.otgmobile.trackteam.gps.DLGpsObserver;
import br.com.otgmobile.trackteam.model.Endereco;
import br.com.otgmobile.trackteam.model.Estado;
import br.com.otgmobile.trackteam.model.Natureza;
import br.com.otgmobile.trackteam.model.NivelEmergencia;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

import com.google.android.gms.maps.model.LatLng;

public class OcorrenciaForm extends GenericActivity implements DLGpsObserver {

	// Activity's dynamic buttons
	private Spinner nivelEmergenciaSpinner;
	private Spinner statusSpinner;
	private Spinner naturezaSpinner;     
	private EditText descriptionText;
	private EditText resumeText;
	private EditText addressText;
	private Location location;
	private Ocorrencia ocorrencia;
	private List<NivelEmergencia> niveisEmergencias;
	private List<Estado> listStatus;
	private List<Natureza> naturezas;
	private String[] emergenciaArrayProvider;
	private String[] statusArrayProvider;
	private String[] naturezaArrayProvider;
	private OcorrenciaDAO ocorrenciaDAO;
	private LinearLayout ocorrenciaViewGroup;
	private static GeocoderTask geocoderTask;
	private static SaveTask saveTask;
	private Button socketStatus;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ocorrencia);
		configureActivity();
		fillDataOnSpinners();
		initializeSpinners();
		if (!handleIntent()) {
			getInitialData();
		}
		DLGps.addGpsObserver(this, this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}
	
	@Override
	protected void onPause() {
		System.gc();
		super.onPause();
	}
	

	private void configureActivity() {
		// Setting the Views form the XML
		descriptionText = (EditText) findViewById(R.id.description_edit_text);
		addressText = (EditText) findViewById(R.id.address_edit_text);
		resumeText = (EditText) findViewById(R.id.resume_edit_text);
		naturezaSpinner = (Spinner) findViewById(R.id.naturezaSpinner);
		statusSpinner = (Spinner) findViewById(R.id.statusSpinner);
		nivelEmergenciaSpinner = (Spinner) findViewById(R.id.nivelEmergenciaSpinner);
		ocorrenciaViewGroup = (LinearLayout) findViewById(R.id.ocorrencia_background);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu ocorrencias_menu) {
		MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.ocorrencias_menu, ocorrencias_menu);
        return true;
	};
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.ocorrencias_menu_photos:
        	takePicture();
            return true;
 
        case R.id.ocorrencias_menu_video:
        	takeFootage();
            return true;
 
        case R.id.ocorrencias_menu_materials:
        	getMaterials();
            return true;
 
        case R.id.ocorrencias_menu_caller:
            getCallers();
            return true;
 
       /* case R.id.ocorrencias_menu_vitim:
            getVictims();
            return true;*/
            
        case R.id.ocorrencias_menu_vehicle:
            getVehicles();
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	@Override
	public void onBackPressed() {
		cancelOcorrencia();
		super.onBackPressed();
	}

	private boolean handleIntent() {
		Intent intent = getIntent();
		if (intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)) {
			Bundle bundle = intent.getExtras();
			ocorrencia = (Ocorrencia) bundle
					.getSerializable(ConstUtil.SERIALIZABLE_KEY);
			fillFormWithData();
			return true;
		}
		return false;
	}

	private void fillFormWithData() {
		if (ocorrencia.getDescricao() != null) {
			descriptionText.setText(ocorrencia.getDescricao());
		}
		if (ocorrencia.getResumo() != null) {
			resumeText.setText(ocorrencia.getResumo());
		}
		if (ocorrencia.getEndereco() != null
				&& ocorrencia.getEndereco().getEndGeoref() != null) {
			addressText.setText(ocorrencia.getEndereco().getEndGeoref());
		}
		if(ocorrencia.getNaturezaObj()!= null && naturezas!=null){
			for(int i = 0;i<naturezas.size();i++){
				if(ocorrencia.getNaturezaObj() == naturezas.get(i)){
					naturezaSpinner.setSelection(i, true);
				}
			}
		}
		if(ocorrencia.getEstadoObj()!= null && listStatus!=null){
			for(int i = 0;i<listStatus.size();i++){
				if(ocorrencia.getEstadoObj() == listStatus.get(i)){
					statusSpinner.setSelection(i);		
				}
			}

		}
		if(ocorrencia.getNivelEmergencia()!=null && niveisEmergencias!=null){
			for(int i = 0;i<niveisEmergencias.size();i++){
				if(ocorrencia.getNivelEmergencia() == niveisEmergencias.get(i)){
					nivelEmergenciaSpinner.setSelection(i);
				}
			}
		}
	}

	@Override
	protected void onDestroy() {
		AppHelper.unbindDrawables(ocorrenciaViewGroup);
		DLGps.removeGpsObserver(this, this);
		super.onDestroy();
	}



	// Starting and finishing the AsyncTasks

	private void runGeocodingTask() {
		finishTask();
		geocoderTask = new GeocoderTask(this);
		geocoderTask.execute();
	}

	private void finishTask() {
		if (geocoderTask != null) {
			geocoderTask.cancel(true);
			geocoderTask = null;
		}
	}

	private void getInitialData() {
		getDataFromDataBase();
	}

	// show count on icons

	

	// Spiner initializations

	private void initializeSpinners() {
		if (niveisEmergencias != null && !niveisEmergencias.isEmpty()) {
			initializeNiveisEmergenciatoSpinner();
		}
		if (listStatus != null && !listStatus.isEmpty()) {
			initializeStatusSpinner();
		}
		if (naturezas != null && !naturezas.isEmpty()) {
			initializeNaturezasSpinner();
		}
	}

	private void getDataFromDataBase() {
		ocorrencia = new Ocorrencia();
		long _id = ocorrenciaDAO().create(ocorrencia);
		ocorrencia.set_id((int) _id);
		ocorrencia.setEnviado(true);
		fillDataOnSpinners();
	}

	private void fillDataOnSpinners() {
		NivelEmergenciaDAO nDao = new NivelEmergenciaDAO(this);
		niveisEmergencias = nDao.fetchAllParsed();
		StatusDAO sDao = new StatusDAO(this);
		listStatus = sDao.fetchAllParsed();
		NaturezaDAO ndao = new NaturezaDAO(this);
		naturezas = ndao.fetchAllParsed();
	}

	// metods to simplify the code
	/**
	 * Converte as lista de emergencia, status e naturezas em String[].
	 */
	private void initializeStatusSpinner() {

		statusArrayProvider = new String[listStatus.size()];
		for (int i = 0; i < listStatus.size(); i++) {
			statusArrayProvider[i] = returnValue(listStatus.get(i).getValor());
		}
		
		if (statusArrayProvider.length > 0) {
			setStatusSpinners();
		}
	}

	private void initializeNaturezasSpinner() {

		naturezaArrayProvider = new String[naturezas.size()];
		for (int i = 0; i < naturezas.size(); i++) {
			naturezaArrayProvider[i] = returnValue(naturezas.get(i).getValor());
		}
		if (naturezaArrayProvider.length > 0) {
			setNaurezaSpinners();
		}
	}

	private void initializeNiveisEmergenciatoSpinner() {

		emergenciaArrayProvider = new String[niveisEmergencias.size()];
		for (int i = 0; i < niveisEmergencias.size(); i++) {
			emergenciaArrayProvider[i] = returnValue(niveisEmergencias.get(i)
					.getDescricao());
		}
		
		if (emergenciaArrayProvider.length > 0) {
			setNivelEmergenciaSpinner();
		}
	}

	private void setNivelEmergenciaSpinner() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				OcorrenciaForm.this, R.layout.spinner_target,
				emergenciaArrayProvider);
		adapter.setDropDownViewResource(R.layout.spinner_target);
		nivelEmergenciaSpinner.setAdapter(adapter);
		nivelEmergenciaSpinner
		.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> adpterView,
					View view, int position, long l) {
				if (niveisEmergencias == null
						|| niveisEmergencias.isEmpty()) {
					return;
				}

				setNivelEmergencia(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				return;
			}
		});
	}

	private void setStatusSpinners() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				OcorrenciaForm.this, R.layout.spinner_target,
				statusArrayProvider);
		adapter.setDropDownViewResource(R.layout.spinner_target);
		statusSpinner.setAdapter(adapter);
		statusSpinner
		.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> adapterView,
					View view, int position, long l) {
				if (listStatus == null || listStatus.isEmpty()) {
					return;
				}

				setStatus(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				return;
			}
		});
	}

	private void setNaurezaSpinners() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				OcorrenciaForm.this, R.layout.spinner_target,
				naturezaArrayProvider);
		adapter.setDropDownViewResource(R.layout.spinner_target);
		naturezaSpinner.setAdapter(adapter);
		naturezaSpinner
		.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> adapterView,
					View view, int position, long l) {
				if (naturezas == null || naturezas.isEmpty()) {
					return;
				}

				setNatureza(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				return;
			}

		});

	}

	public String returnValue(String value) {

		if (value == null) {
			value = "";
		}

		return value;
	}

	private void setStatus(int position) {
		ocorrencia.setEstado(listStatus.get(position).getId());
		ocorrencia.setEstadoObj(listStatus.get(position));
	}

	private void setNatureza(int position) {
		ocorrencia.setNatureza(naturezas.get(position).getId());
		ocorrencia.setNaturezaObj(naturezas.get(position));
	}

	private void setNivelEmergencia(int position) {
		ocorrencia
		.setNivelEmergenciaID(niveisEmergencias.get(position).getId());
		ocorrencia.setNivelEmergencia(niveisEmergencias.get(position));
	}

	// metods for the button's Clicks

	

	public void geocodeButtonOnClick(View view) {
		runGeocodingTask();
	}

	public void selectedLocalizationButtonOnClick(View view) {
		getLocaltionWithMap();
	}

	public void confirmButtonOnClick(View view) {
		saveOcorrenciaTask();
	}

	// Get Lists and objects for Ocorrencia

	private void getVehicles() {
		Intent intent = new Intent(OcorrenciaForm.this,
				VeiculoEnvolvidoCadList.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.VEHICLE_REQUEST);

	}

	private void getLocaltionWithMap() {
		Intent intent = new Intent(OcorrenciaForm.this, SelectLocationMap.class);
		startActivityForResult(intent, ConstUtil.MAP_SELECTION_REQUEST);

	}

	/* private void getVictims() {
		Intent intent = new Intent(OcorrenciaForm.this, VitimaList.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.VITIM_REQUEST);
	} */

	private void getMaterials() {
		Intent intent = new Intent(OcorrenciaForm.this, MaterialList.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.MATERIAL_REQUEST);
	}

	private void getCallers() {
		Intent intent = new Intent(OcorrenciaForm.this, SolicitanteList.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.CALLER_REQUEST);
	}

	// SaveOcorrencia

	private void saveOcorrenciaTask() {
		if(validateOcorrencia()){
		setResumoText();
		ocorrencia.setDescricao(descriptionText.getText().toString());
		ocorrencia.setEnviado(false);
		saveTask = new SaveTask(this);
		saveTask.execute();
		}else{
			AppHelper.getInstance().presentError(this, getString(R.string.ERROR_MISSINGINFO_TITLE), getString(R.string.preencher_dados));
		}
	}

	protected void setResumoText() {
		if(resumeText.getText().length() < 1){
			ocorrencia.setResumo(naturezaSpinner.getSelectedItem().toString());
		}else{
			ocorrencia.setResumo(resumeText.getText().toString());			
		}
	}

	private void cancelOcorrencia() {
		OcorrenciaDAO dao = new OcorrenciaDAO(this);
		dao.delete(ocorrencia.get_id());

	}

	// Request Access to the device's camera
	private void takePicture() {
		Intent intent = new Intent(OcorrenciaForm.this, FotoListActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.FOTO_REQUEST);
	}

	private void takeFootage() {
		Intent intent = new Intent(OcorrenciaForm.this,
				FilmagemListActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.FOOTAGE_REQUEST);
	}

	// Activity's Results
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		if (requestCode == ConstUtil.FOTO_REQUEST) {
			if (resultCode == RESULT_OK) {
				ocorrencia = (Ocorrencia) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY);
			}
		}

		if (requestCode == ConstUtil.FOOTAGE_REQUEST) {
			if (resultCode == RESULT_OK) {
				ocorrencia = (Ocorrencia) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY);
			}
		}

		if (requestCode == ConstUtil.MATERIAL_REQUEST) {
			if (resultCode == RESULT_OK) {
				ocorrencia = (Ocorrencia) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY);
			}
		}

		if (requestCode == ConstUtil.VITIM_REQUEST) {
			if (resultCode == RESULT_OK) {
				ocorrencia = (Ocorrencia) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY);
			}
		}
		if (requestCode == ConstUtil.CALLER_REQUEST) {
			if (resultCode == RESULT_OK) {
				ocorrencia = (Ocorrencia) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY);
			}
		}
		if (requestCode == ConstUtil.MAP_SELECTION_REQUEST) {
			if (resultCode == RESULT_OK) {
				Endereco obj = (Endereco) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY);
				if (obj != null) {
					ocorrencia.setEndereco(obj);
					addressText
					.setText(ocorrencia.getEndereco().getEndGeoref());
				}
			}
		}

		if (requestCode == ConstUtil.VEHICLE_REQUEST) {
			if (resultCode == RESULT_OK) {
				ocorrencia = (Ocorrencia) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY);
			}
		}
	}

	// Async Tasks

	private class GeocoderTask extends AsyncTask<String, Void, String> {
		Context context;
		double latitude;
		double longitude;
		ProgressDialog mDialog;
		GoogleGeolocationCloud cloud;
		String address;

		GeocoderTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "",
					getString(R.string.processing), true, false);
			mDialog.setCancelable(false);
			cloud = new GoogleGeolocationCloud();
		}

		@Override
		protected void onPreExecute() {
			getWindow()
			.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			setProgressBarIndeterminate(true);
		}

		@Override
		public String doInBackground(String... params) {
			return search();
		}

		String search() {
			if (location == null) {
				location = DLGps.getLastLocation(context);
				if (location == null) {
					return context.getResources().getString(
							R.string.error_location);

				}
			}
			latitude = location.getLatitude();
			longitude = location.getLongitude();
			try {
				String foundAddress = cloud.geocode(new LatLng(latitude, longitude));
				if(foundAddress != null && foundAddress.length() > 0){
					address = foundAddress;
					return null;
				}else{
					return context.getResources().getString(R.string.address_not_found);
				}
			}
			catch (Exception e) {
				return context.getResources().getString(R.string.search_not_possible);
			}
		}

		@Override
		public void onPostExecute(String erroMessage) {
			getWindow().clearFlags(
					WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			geocoderTask = null;
			setProgressBarIndeterminate(false);
			mDialog.dismiss();

			if (erroMessage != null) {
				AppHelper.getInstance().presentError(context,
						getResources().getString(R.string.error), erroMessage);
			} else if (address != null) {
				addressText.setText(address);
				Endereco obj = new Endereco();
				obj.setEndGeoref(addressText.getText().toString());
				obj.setLatitude((float) latitude);
				obj.setLongitude((float) longitude);
				setEndereco(obj);
			}
		}
	}


	private class SaveTask extends AsyncTask<Void, Void, Void> {
		Context context;
		ProgressDialog mDialog;
		OcorrenciasCloud cloud = new OcorrenciasCloud();
		SaveTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "",
					getString(R.string.processing), true, false);
			mDialog.setCancelable(false);

		}

		@Override
		protected void onPreExecute() {
			getWindow()
			.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			setProgressBarIndeterminate(true);
		}

		@Override
		public Void doInBackground(Void... params) {
			save();
			return null;
		}

		void save() {
			try {
				Ocorrencia ocorrenciaSent = cloud.merge(ocorrencia, context);
				
				if(ocorrenciaSent != null) {
					ocorrencia.setId(ocorrenciaSent.getId());
					ocorrencia.setValidada(ocorrenciaSent.isValidada());
					ocorrenciaDAO().updateInternal(ocorrencia);
				}
				
			} catch (Exception e) {
				LogUtil.e("erro ao criar atendimento",e);
			} 

		}

		@Override
		public void onPostExecute(Void result) {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			saveTask = null;
			ocorrencia.setEnviado(false);
			ocorrenciaDAO().updateInternal(ocorrencia);
			setProgressBarIndeterminate(false);
			mDialog.dismiss();
			finish();
		}
	}

	private void setEndereco(Endereco endereco) {
		ocorrencia.setEndereco(endereco);
	}

	@Override
	public void onLocationChanged(Location location) {
		this.location = location;
		super.onLocationChanged(location);
	}

	@Override
	public void onStatusChanged(int status) {
		super.onStatusChanged(status);
		
	}
	
	public boolean validateOcorrencia() {
		return (ocorrencia.getEndereco() != null 
				&& ocorrencia.getNivelEmergencia() != null 
				&& ocorrencia.getNatureza() != null
				&& ocorrencia.getEstado() != null
				&& descriptionText.getText().length() > 0);
	}
	
	private OcorrenciaDAO ocorrenciaDAO(){
		if(ocorrenciaDAO == null){
			ocorrenciaDAO = new OcorrenciaDAO(this);
		}
		
		return ocorrenciaDAO;
	}
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}
}
