package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

public class ImagemRequerida implements Serializable{

	private static final long serialVersionUID = -1630291192556594467L;

	private Integer _id;
	private String imagem;
	
	public Integer get_id() {
		return _id;
	}
	public void set_id(Integer _id) {
		this._id = _id;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	
}
