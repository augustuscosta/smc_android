package br.com.otgmobile.trackteam.maps.overlay;

import java.util.List;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

public class CercaOverlay extends Overlay {
	
	private List<GeoPoint> points;
	private Paint mPaint;
	private Path path;
	private Projection projection;
	
	private GeoPoint lastGeoPoint;
	
	public CercaOverlay(List<GeoPoint> points, Projection projection){
		this.points = points;
		this.projection = projection;
	}

	public void draw(Canvas canvas, MapView mapv, boolean shadow){
        super.draw(canvas, mapv, shadow);
        mPaint = new Paint();
        mPaint.setDither(true);
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(8);
        mPaint.setAlpha(60);
        path = new Path();
        Point p1;
        Point p2;
        for(GeoPoint geoPoint : points){
        	if(lastGeoPoint != null){
        		p1 = new Point();
        		p2 = new Point();
        		projection.toPixels(lastGeoPoint, p1);
                projection.toPixels(geoPoint, p2);
                path.moveTo(p2.x, p2.y);
                path.lineTo(p1.x,p1.y);
        	}
        	lastGeoPoint = geoPoint;
        }
        canvas.drawPath(path, mPaint);
	}
}
