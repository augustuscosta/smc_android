package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Agente implements Serializable{
	

	private static final long serialVersionUID = -5100415976025890594L;

	private Integer _id;
	private Integer id;
	private String nome;
	private String cpf;
	private String matricula;
	private String login;
	private String descricao;
	@SerializedName("veiculo_id")
	private Integer veiculoID;
	private String token;
	
	private Historico ultimoHistorico;
	
	public Integer get_id() {
		return _id;
	}
	public void set_id(Integer _id) {
		this._id = _id;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Integer getVeiculoID() {
		return veiculoID;
	}
	public void setVeiculoID(Integer veiculoID) {
		this.veiculoID = veiculoID;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Historico getUltimoHistorico() {
		return ultimoHistorico;
	}
	public void setUltimoHistorico(Historico ultimoHistorico) {
		this.ultimoHistorico = ultimoHistorico;
	}
	
	
	
}
