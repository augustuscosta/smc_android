package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

public class Local implements Serializable {


	private static final long serialVersionUID = 2380352491516360617L;
	
	private Integer _id;
	
	private Integer id;
	
	private String municipio;
	
	private String uf;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public Integer get_id() {
		return _id;
	}

	public void set_id(Integer _id) {
		this._id = _id;
	}
	
}
