package br.com.otgmobile.trackteam.activity;


import java.util.Date;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import br.com.otgmobile.trackteam.adapter.ComentarioAdapter;
import br.com.otgmobile.trackteam.database.ComentarioDAO;
import br.com.otgmobile.trackteam.model.Comentario;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.HandleListButton;
import br.com.otgmobile.trackteam.util.Session;

import br.com.otgmobile.trackteam.R;

public class ComentarioListActivity extends GenericListActivity implements HandleListButton{

	private List<Comentario> list;
	private Ocorrencia ocorrencia;
	private ComentarioAdapter adapter;
	private EditText comentarioEditText;
	private Button socketStatus;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comentario_list);
		configureActivity();
		handleIntent();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
	
	private void configureActivity() {
		comentarioEditText  = (EditText) findViewById(R.id.coment_edit_text);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
		
	}

	private void handleIntent() {
		Intent intent = getIntent();
		if (intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)) {
			ocorrencia = (Ocorrencia) intent.getExtras().get(
					ConstUtil.SERIALIZABLE_KEY);		
			refresh();
		}
	}

	private void refresh() {
		fetchDataFromDataBase();
		setAdapter();
	}
	
	private void fetchDataFromDataBase() {
		ComentarioDAO dao = new ComentarioDAO(this);
		list = dao.fetchAllParsedFromOcorrencia(ocorrencia.getId());
		
	}

	private void setAdapter() {
		if(list!=null){
			adapter = new ComentarioAdapter(list,this,this);
			setListAdapter(adapter);
		}
	}
	
	public void addComentario(View view){
		if(comentarioEditText.getText().length() >0){
			addComentariosAndRefreshView();	
		}else{
			AppHelper.getInstance().presentError(this, getResources().getString(R.string.error), 
					getResources().getString(R.string.sem_comentario));
		}
	}

	private void addComentariosAndRefreshView() {
		Comentario comment = new Comentario();
		comment.setDescricao(comentarioEditText.getText().toString());
		comment.setOcorrenciaId(ocorrencia.getId());
		comment.setData(new Date().getTime());
		ComentarioDAO dao = new ComentarioDAO(this);
		dao.create(comment);
		comentarioEditText.setText("");
		ocorrencia.setEnviado(false);
		refresh();
	}

	@Override
	public void positionSelect(int position) {
		deleteComentario(list.get(position));
	}

	private void deleteComentario(Comentario comentario) {
		ComentarioDAO dao = new ComentarioDAO(this);
		dao.deleteInternal(comentario.get_id());
		ocorrencia.setEnviado(false);
		refresh();
	}
	
	public void returnButtonOnClick(View view){
		finishAndPassComents();
	}

	private void finishAndPassComents() {
		ocorrencia.setComentarios(list);
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		setResult(RESULT_OK,intent);
		finish();
	}
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}

	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}

	protected void setSocketStatus() {
		SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);

		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));

			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}
}
