package br.com.otgmobile.trackteam.model;

public enum TelemetriaConectionStatus {
	
	DISCONNECTED(0),
	CONNECTING(1),
	CONNECTED(2);
	
	private int value;
	
	private TelemetriaConectionStatus(int value) {
		this.value = value;
	}

	public static TelemetriaConectionStatus fromValue(int value){
		for (TelemetriaConectionStatus telemetriaConectionStatus : TelemetriaConectionStatus.values()) {
			if(telemetriaConectionStatus.getValue() == value) return telemetriaConectionStatus;
		}
		
		return null;
	}

	public int getValue() {
		return value;
	}
}
