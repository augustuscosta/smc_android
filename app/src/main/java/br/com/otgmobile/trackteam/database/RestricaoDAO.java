package br.com.otgmobile.trackteam.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import br.com.otgmobile.trackteam.model.Restricao;

public class RestricaoDAO implements BaseColumns {
	
	/**Created by Bruno Ramos Dias
	 * at On the Go mobile
	 * 04/07/2010
	 */

	private static String ID = "id";
	private static String NOME = "nome";
	private static String COR = "cor";
	private static String DESCRICAO = "descricao";
	private static String TIPO = "tipo";
	
	private static String[] FIELDS = {_ID,ID,NOME,COR,DESCRICAO,TIPO};
	
	private static String DATABASE_TABLE ="restricao";
	
	private SQLiteDatabase database;
	
	public RestricaoDAO(Context context) {
		database = DBHelper.getDatabase(context);
	}
	
	public long save(Restricao obj) {
		long saved = -1;
		
		if(obj == null) return saved;
		
		if(obj.getId() == null) return saved;
		
		if (find(obj.getId()) == null) {
			saved = create(obj);
		}else{
			saved = update(obj);
		}
		
		return saved;
	}

	private long create(Restricao obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	private long update(Restricao obj) {
		ContentValues values = createContentValues(obj);
		return database.update(DATABASE_TABLE, values, ID+ " = ?",new String[] { obj.getId().toString()});
	}
	public boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, ID + "=" + id, null) > 0;
	}
	
	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE,null,null) > 0;
	}
	
	private Cursor findCursor(int codigo){
		 return database.query(DATABASE_TABLE, FIELDS, ID+ " = ?", new String[] { codigo + "" }, null, null, null);
	}

	public Restricao find(Integer id) {
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}
	
	private Restricao parseObject(Cursor cursor) {
		Restricao restricao = new Restricao();
		restricao.set_id(cursor.getInt(0));
		restricao.setId(cursor.getInt(1));
		restricao.setNome(cursor.getString(2));
		restricao.setCor(cursor.getString(3));
		restricao.setDescricao(cursor.getString(4));
		restricao.setTipo(cursor.getString(5));
		return restricao;
	}

	private ContentValues createContentValues(Restricao obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(NOME, obj.getNome());
		values.put(COR, obj.getCor());
		values.put(DESCRICAO, obj.getDescricao());
		values.put(TIPO, obj.getTipo());
		return values;
	}

}
