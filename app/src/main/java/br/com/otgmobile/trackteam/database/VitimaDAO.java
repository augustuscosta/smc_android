package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.Vitima;

public class VitimaDAO {

	// Database fields
		public static final String _ID = "_id";
		public static final String ID = "id";
		public static final String IDADE ="idade";
		public static final String DESCRICAO ="descricao";
		public static final String CPF ="cpf";
		public static final String NOME ="nome";
		public static final String RG ="rg";
		public static final String OCORRENCIAID ="ocorrencia_id";
		private static final String[] FIELDS =  { _ID,ID,IDADE,DESCRICAO,NOME,CPF,RG,OCORRENCIAID};

		
		
		private static final String DATABASE_TABLE = "vitima";
		private SQLiteDatabase database;
		
		public VitimaDAO(Context context){
			database = DBHelper.getDatabase(context);
		}
		
		public long save(Vitima obj){
			long inserted = 0;
			if(isNew(obj)){
				inserted = create(obj);
			}else{
				inserted = update(obj);
			}

			return inserted;
		}

		private boolean isNew(Vitima obj) {
			if(obj.get_id() == null || find(obj.get_id()) == null)
				return true;
			return false;
		}
		
		public long create(Vitima obj) {
			ContentValues initialValues = createContentValues(obj); 
			return database.insert(DATABASE_TABLE, null, initialValues);
		}
		
		public long update(Vitima obj) {
			ContentValues values = createUpdateContentValues(obj);
			return database.update(DATABASE_TABLE, values, _ID+ " = ?",new String[] { obj.get_id().toString()});
		}
		
		public boolean delete(Integer id) {
			return database.delete(DATABASE_TABLE, _ID + "=" + id, null) > 0;
		}
		
		public boolean deleteAll() {
			return database.delete(DATABASE_TABLE,null,null) > 0;
		}
			
		private Cursor findCursor(int codigo){
			return database.query(DATABASE_TABLE, FIELDS, _ID+ " = ?", new String[] { codigo + "" }, null, null, null);
		}
		
		private Cursor findFromOcorrenciaCursor(int codigo){
			return database.query(DATABASE_TABLE, FIELDS, OCORRENCIAID+ " = ?", new String[] { codigo + "" }, null, null, null);
		}
		
		public List<Vitima> findFromOcorrencia(int codigo){
			Cursor cursor = findFromOcorrenciaCursor(codigo);
			try {
				return parse(cursor);
			} finally {
				cursor.close();
			}
		}
		
		public Vitima find(int id){
			Cursor cursor = findCursor(id);
			try {
				cursor.moveToFirst();
				if ( cursor.getCount() == 0 ) {
					return null;
				}

				return parseObject(cursor);
			} finally {
				cursor.close();
			}
		}
		
		
		public Cursor fetchAll() {
			return database.query(DATABASE_TABLE, FIELDS, null, null,
					null, null, null);
		}
		
		public List<Vitima> fetchAllParsed(){
			Cursor cursor = fetchAll();
			try {
				return parse(cursor);
			} finally {
				cursor.close();
			}
		}
		
		private List<Vitima> parse(Cursor cursor){
			List<Vitima>  toReturn= null;
			if(cursor != null){
				toReturn = new ArrayList<Vitima>();
				Vitima obj =null;
				if(cursor.moveToFirst()){
					while(!cursor.isAfterLast()){
						obj = parseObject(cursor);
						toReturn.add(obj);
						cursor.moveToNext();
					}
				}
			}
			return toReturn; 
		}
		
		
		private Vitima parseObject(Cursor cursor) {
			Vitima obj = null;
			obj = new Vitima();
			obj.set_id(cursor.getInt(0));
			obj.setId(DatabaseUtil.returnNullValueForInt(cursor, ID));
			obj.setIdade(cursor.getInt(2));
			obj.setDescricao(cursor.getString(3));
			obj.setNome(cursor.getString(4));
			obj.setCpf(cursor.getString(5));
			obj.setRg(cursor.getString(6));
			obj.setOcorrenciaID(cursor.getInt(7));
			return obj;
		}
		
		private ContentValues createContentValues(Vitima obj) {
			ContentValues values = new ContentValues();
			values.put(ID, obj.getId());
			values.put(IDADE, obj.getIdade());
			values.put(DESCRICAO, obj.getDescricao());
			values.put(OCORRENCIAID, obj.getOcorrenciaID());
			values.put(NOME, obj.getNome());
			values.put(CPF, obj.getCpf());
			values.put(RG, obj.getRg());
			return values;
		}
		
		private ContentValues createUpdateContentValues(Vitima obj) {
			ContentValues values = new ContentValues();
			values.put(ID, obj.getId());
			values.put(IDADE, obj.getIdade());
			values.put(DESCRICAO, obj.getDescricao());
			values.put(OCORRENCIAID, obj.getOcorrenciaID());
			values.put(NOME, obj.getNome());
			values.put(CPF, obj.getCpf());
			values.put(RG, obj.getRg());
			return values;
		}
}
