package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import br.com.otgmobile.trackteam.model.Enquete;
import br.com.otgmobile.trackteam.model.Pergunta;

public class EnqueteDAO implements BaseColumns {

	public static final String ID = "id";
	public static final String NOME = "nome";
	public static final String DESCRICAO = "descricao";
	
	private static String[] FIELDS = {_ID,ID,NOME, DESCRICAO};
	
	private static final String DATABASE_TABLE = "enquete";
	private SQLiteDatabase database;
	
	private Context context;
	private PerguntaDAO perguntaDAO;
	
	public EnqueteDAO(Context context){
		database = DBHelper.getDatabase(context);
		this.context = context;
	}
	
	public long save(Enquete obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = find(obj.getId()).getId(); 
			update(obj);
		}
		
		if(inserted > 0){
			new PerguntaDAO(context).deleteByEnquete(obj.getId());
			if(obj.getPerguntas() != null){
				for(Pergunta pergunta:obj.getPerguntas()){
					if(pergunta !=null){
						pergunta.setEnqueteId(obj.getId());
						perguntaDAO().save(pergunta);
					}
				}
			}
			
		}

		return inserted;
	}
	
	private boolean isNew(Enquete obj) {
		if(find(obj.getId()) == null)
			return true;
		return false;
	}

	private long create(Enquete obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}
	

	private long update(Enquete obj) {
		ContentValues values = createUpdateContentValues(obj); 
		return database.update(DATABASE_TABLE, values, ID+"=?", new String[]{obj.getId().toString()});
	}
	
	public boolean delete(String id){
		return database.delete(DATABASE_TABLE, ID+"=" +id, null)>0 ;
	}
	
	public boolean deleteAll(){
		return database.delete(DATABASE_TABLE, null, null)>0;
	}

	
	private Cursor findCursor(int codigo){
		return database.query(DATABASE_TABLE, FIELDS, ID+" = ?",new String[]{codigo+""}, null, null, null);
	}
	
	public Cursor fetchAll() {
		return database.query(DATABASE_TABLE, FIELDS, null, null,
				null, null, null);
	}
	
	private Cursor searchCursor(String parameter){
		String wereClausule = NOME + " like " + "'%" + parameter + "%'"
				+ " OR " + DESCRICAO + " like " + "'%" + parameter + "%'";
		return database.query(DATABASE_TABLE,FIELDS, wereClausule,null, null, null, null);
	}
	
	public List<Enquete> search(String parameter){
		Cursor cursor = searchCursor(parameter);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	
	public Enquete find(int codigo){
		Cursor cursor = findCursor(codigo);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}
			
			return parseObject(cursor);
		} finally {
			cursor.close();
		}
		
	}

	public List<Enquete> fetchAllParsed(){
		Cursor cursor = fetchAll();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
		
	}
	
	
	private List<Enquete> parse(Cursor cursor) {
		List<Enquete> toReturn = new ArrayList<Enquete>();
		if(cursor !=null){
			if(cursor.moveToFirst()){
				Enquete obj;
				while(!cursor.isAfterLast()){
					obj = parseObject(cursor);
					toReturn.add(obj);
					cursor.moveToNext();
				}
			}
		}
		return toReturn;
	}

	private Enquete parseObject(Cursor cursor) {
		Enquete obj = null;
		obj = new Enquete();
		obj.set_id(cursor.getInt(0));
		obj.setId(DatabaseUtil.returnNullValueForInt(cursor,ID));
		obj.setNome(cursor.getString(2));
		obj.setDescricao(cursor.getString(3));
		obj.setPerguntas(perguntaDAO().fetchAllParsed(obj));
		return obj;
	}
	
	private ContentValues createContentValues(Enquete obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(NOME, obj.getNome());
		values.put(DESCRICAO, obj.getDescricao());
		return values;
	}

	private ContentValues createUpdateContentValues(Enquete obj) {
		ContentValues values = new ContentValues();
		values.put(NOME, obj.getNome());
		values.put(DESCRICAO, obj.getDescricao());
		return values;
	}
	
	private PerguntaDAO perguntaDAO(){
		if(perguntaDAO == null){
			perguntaDAO = new PerguntaDAO(context);
		}
		return perguntaDAO;
	}
}
