package br.com.otgmobile.trackteam.cloud.rest.tests;

import java.util.List;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import br.com.otgmobile.trackteam.cloud.rest.NaturezaCloud;
import br.com.otgmobile.trackteam.model.Natureza;
import br.com.otgmobile.trackteam.util.Session;

public class NaturezaCloudTests extends AndroidTestCase {

	@Override
	protected void setUp() throws Exception {
		Session.setToken(TestCont.TOKEN, getContext());
		Session.setServer(TestCont.SERVER,getContext());
		super.setUp();
	}
	
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	public void testListNatureza() throws Exception{
		NaturezaCloud cloud = new NaturezaCloud();
		List<Natureza> response = cloud.list(getContext());
		Assert.assertNotNull(response);
		Assert.assertTrue(response.size() > 0);
	}
	
	public void testSync() throws Exception{
		NaturezaCloud cloud = new NaturezaCloud();
		Assert.assertTrue(cloud.sync(getContext()));
	}
	
}
