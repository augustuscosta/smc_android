package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

public class Resposta implements Serializable {

	private static final long serialVersionUID = -3908801973560180099L;
	
	private Integer id;
	private String resposta;
	private Integer pergunta_id;
	private Integer ocorrencia_id;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getResposta() {
		return resposta;
	}
	public void setResposta(String resposta) {
		this.resposta = resposta;
	}
	public Integer getPergunta_id() {
		return pergunta_id;
	}
	public void setPergunta_id(Integer pergunta_id) {
		this.pergunta_id = pergunta_id;
	}
	public Integer getOcorrencia_id() {
		return ocorrencia_id;
	}
	public void setOcorrencia_id(Integer ocorrencia_id) {
		this.ocorrencia_id = ocorrencia_id;
	}
}
