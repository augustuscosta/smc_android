package br.com.otgmobile.trackteam.model;

import android.database.Cursor;

public class LocalizaMaterialMaterial {
	
	public LocalizaMaterialMaterial(Cursor cursor){
		this.localizaMaterialId = cursor.getInt(1);
		this.materialId = cursor.getInt(2);
	}
	
	private Integer localizaMaterialId;
	private Integer materialId;
	
	public Integer getLocalizaMaterialId() {
		return localizaMaterialId;
	}
	
	public void setLocalizaMaterialId(Integer localizaMaterialId) {
		this.localizaMaterialId = localizaMaterialId;
	}
	
	public Integer getMaterialId() {
		return materialId;
	}
	
	public void setMaterialId(Integer materialId) {
		this.materialId = materialId;
	}
	
	


}
