package br.com.otgmobile.trackteam.activity;

import java.util.List;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import br.com.otgmobile.trackteam.adapter.MensagemAdapter;
import br.com.otgmobile.trackteam.database.MensagemDAO;
import br.com.otgmobile.trackteam.model.Conexao;
import br.com.otgmobile.trackteam.model.Mensagem;
import br.com.otgmobile.trackteam.model.Operador;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.service.SocketService;
import br.com.otgmobile.trackteam.service.SocketService.ChatListener;
import br.com.otgmobile.trackteam.service.SocketService.IChatService;
import br.com.otgmobile.trackteam.service.SocketService.SocketIOBinder;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

import br.com.otgmobile.trackteam.R;

public class ChatActivity extends GenericListActivity {


	private IChatService mChatService;
	private String mOperator;
	private EditText chatEditText;
	private List<Mensagem> mensagens;
	private MensagemDAO mensagemDAO;
	private Button socketStatus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat);
		configureActivity();
		mOperator = getIntent().getStringExtra("token");
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	};
	
	private void refresh(){
		getMessagesfromDatabase();
		setAdapter();
	}

	private void setAdapter() {
		if(mensagens == null || mensagens.isEmpty()){
			return;
		}else{
			MensagemAdapter adapter = new MensagemAdapter(this, mensagens);
			setListAdapter(adapter);
			setSelection(mensagens.size()-1);
		}
	}

	private void getMessagesfromDatabase(){
		mensagens = mensagemDAO().findMensagens(mOperator);
	}

	@Override
	protected void gotNewChatMessage() {
		refresh();
	}

	private void configureActivity() {
		chatEditText = (EditText) findViewById(R.id.chatEditText);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}

	public void onClickSendMessage(View view) {
		final Mensagem mensagem = new Mensagem();
		mensagem.setFrom(Session.getToken(this));
		mensagem.setTo(mOperator);
		mensagem.setMensagem(chatEditText.getText().toString());
		mensagem.setDataMensagem(AppHelper.getCurrentTime());
		mChatService.sendMessage(mensagem);
		refresh();
		chatEditText.setText("");
	}

	public void connect() {
		final Conexao connection = new Conexao();
		connection.setFrom(Session.getToken(this));
		connection.setTo(mOperator);
		mChatService.connect(connection);
	}

	@Override
	protected void onResume() {
		super.onResume();
		bindService(new Intent(SocketService.SOCKET_SERVICE_INTENT), serviceConn, BIND_AUTO_CREATE);
		setSocketStatus();
	}

	@Override
	protected void onPause() {
		super.onPause();
		unbindService(serviceConn);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};

	final private ServiceConnection serviceConn = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mChatService.removeListener(chatListener);
			mChatService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder binder) {
			mChatService = ((SocketIOBinder) binder).getChatInterface();
			mChatService.addListener(chatListener);
			connect();
			refresh();
		}
	};

	final private ChatListener chatListener = new ChatListener() {

		@Override
		public void onRetrieveOperators(final List<Operador> operators) {

		}

		@Override
		public void onConnected() {
			Toast.makeText(ChatActivity.this, "Conectado com o chat", Toast.LENGTH_SHORT).show();
			LogUtil.d("Connected with chat");
		}
	};
	

	private MensagemDAO mensagemDAO(){
		if(mensagemDAO == null){
			mensagemDAO = new MensagemDAO(this);
		}
		
		return mensagemDAO;
	}
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			refresh();
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}

}
