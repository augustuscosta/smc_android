package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.Configuration;
import br.com.otgmobile.trackteam.util.ConfigurationKeys;
import br.com.otgmobile.trackteam.util.ImageRequestType;

public class ConfigurationDAO {

	// Database fields
	public static final String KEY = "key";
	public static final String VALUE = "value";

	private static final String DATABASE_TABLE = "configuration";
	private SQLiteDatabase database;

	// Default values
	private static final String DEFAULT_CONFIGURATION_PASSWORD = "00000";
	private static final int    DEFAULT_PRECISION = 130;
	
	public ConfigurationDAO(Context context) {
		database = DBHelper.getDatabase(context);
	}

	public long save(Configuration obj){
		if(obj.getChave() == null)
			return -1;
		
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = update(obj);
		}

		return inserted;
	}

	private boolean isNew(Configuration obj) {
		if(find(obj.getChave()) == null)
			return true;
		return false;
	}

	public long create(Configuration obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	public long update(Configuration obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, KEY + " = ?",
				new String[] { obj.getChave().toString() });
	}

	public boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, KEY + "=" + id, null) > 0;
	}

	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE, null, null) > 0;
	}

	private Cursor findCursor(String key) {
		return database.query(DATABASE_TABLE, new String[] { KEY, VALUE }, KEY
				+ " = ?", new String[] { key + "" }, null, null, null);
	}

	public Configuration find(String key) {
		Cursor cursor = findCursor(key);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}
			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}

	public Cursor fetchAll() {
		return database.query(DATABASE_TABLE, new String[] { KEY, VALUE },
				null, null, null, null, null);
	}

	public List<Configuration> fetchAllParsed() {
		Cursor cursor = findCursor(null);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}

	private List<Configuration> parse(Cursor cursor) {
		List<Configuration> toReturn = null;
		if (cursor != null) {
			toReturn = new ArrayList<Configuration>();
			Configuration obj;
			if (cursor.moveToFirst()) {
				while (!cursor.isAfterLast()) {
					obj = parseObject(cursor);
					toReturn.add(obj);
					cursor.moveToNext();
				}
			}
		}

		return toReturn;
	}

	private Configuration parseObject(Cursor cursor) {
		Configuration obj = null;
		obj = new Configuration();
		obj.setChave(cursor.getString(0));
		obj.setValor(cursor.getString(1));

		return obj;
	}

	private ContentValues createContentValues(Configuration obj) {
		ContentValues values = new ContentValues();
		values.put(KEY, obj.getChave());
		values.put(VALUE, obj.getValor());
		return values;
	}

	private ContentValues createUpdateContentValues(Configuration obj) {
		ContentValues values = new ContentValues();
		values.put(VALUE, obj.getValor());
		return values;
	}
	
	public String getConfigurationPassword(){
		String toReturn = DEFAULT_CONFIGURATION_PASSWORD;
		Configuration configuration = find(ConfigurationKeys.CONFIGURATION_PASSWORD.getText());
		if(configuration != null && configuration.getValor() != null){
			toReturn = configuration.getValor();
		}
		return toReturn;
	}
	
	public int getPrecision(){
		int toReturn = DEFAULT_PRECISION;
		Configuration configuration = find(ConfigurationKeys.PRECISION.getText());
		if(configuration != null && configuration.getValor() != null){
			toReturn = Integer.parseInt(configuration.getValor());
		}
		return toReturn;
	}
	
	public ImageRequestType getCamRequest(){
		ImageRequestType toReturn = ImageRequestType.PHOTO;
		Configuration configuration = find(ConfigurationKeys.SPY.getText());
		if(configuration != null && configuration.getValor() != null){
			if("VIDEO".equals(configuration.getValor())){
				toReturn = ImageRequestType.VIDEO;
			}
		}
		return toReturn;
	}

}
