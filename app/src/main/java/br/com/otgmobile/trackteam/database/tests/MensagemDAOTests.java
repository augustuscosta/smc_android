package br.com.otgmobile.trackteam.database.tests;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import br.com.otgmobile.trackteam.database.DBHelper;
import br.com.otgmobile.trackteam.database.MensagemDAO;
import br.com.otgmobile.trackteam.model.Mensagem;

public class MensagemDAOTests extends AndroidTestCase{
	
	@Override
	protected void setUp() throws Exception {
		DBHelper.getDatabase(getContext());
	}

	@Override
	protected void tearDown() throws Exception {

	}
	
	public void testMensagens(){
		MensagemDAO dao = new MensagemDAO(getContext());
		dao.deleteAll();
		List<Mensagem> list= new ArrayList<Mensagem>();
		Assert.assertNotNull(list);
		Assert.assertTrue(list.isEmpty());
		Mensagem obj;
		for (int i = 0; i < 100; i++) {
			obj = new Mensagem();
			obj.setFrom(Integer.toString(i));
			obj.setTo(Integer.toString(i));
			obj.setMensagem(Integer.toString(i));
			dao.create(obj);
		}
		list = dao.fetchAllParsed();
		Assert.assertTrue(!list.isEmpty());
		list = dao.findMensagens("1");;
		Assert.assertTrue(!list.isEmpty());
	}

}
