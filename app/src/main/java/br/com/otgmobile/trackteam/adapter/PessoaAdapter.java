package br.com.otgmobile.trackteam.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Pessoa;
import br.com.otgmobile.trackteam.row.PessoaRowHolderInfo;


public class PessoaAdapter extends BaseAdapter {
	
	private List<Pessoa> list;
	private Activity context;
	
	public PessoaAdapter(Activity context, List<Pessoa> list) {
		this.list = list;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		PessoaRowHolderInfo rowInfo;
		Pessoa obj = list.get(position);
		
		if(row == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.pessoa_row, null);
			rowInfo = new PessoaRowHolderInfo();
			rowInfo.nomeText = (TextView) row.findViewById(R.id.pessoa_list_name);
			rowInfo.cpfText = (TextView) row.findViewById(R.id.pessoa_list_cpf);
			rowInfo.rgText = (TextView)row.findViewById(R.id.pessoa_list_rg);
			
			row.setTag(rowInfo);
		}else{
			rowInfo = (PessoaRowHolderInfo) row.getTag();
		}
		rowInfo.drawRow(obj);
		return row;
	}

}
