package br.com.otgmobile.trackteam.database;

import android.database.Cursor;

public class DatabaseUtil {

	public static Integer returnNullValueForInt(Cursor cursor,String column){		
		int index = cursor.getColumnIndexOrThrow(column);
		Integer x = null;
		if (!cursor.isNull(index)){
			x = cursor.getInt(index);
		}
		return x;
	}
	
	public static Long returnNullValueForLong(Cursor cursor,String column){		
		int index = cursor.getColumnIndexOrThrow(column);
		Long x = null;
		if (!cursor.isNull(index)){
			x = cursor.getLong(index);
		}
		return x;
	}
	
	public static String concatValuesToUseInCondition(Object[] values) {
		if (values == null || values.length == 0) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		for (Object value : values) {
			if (value == null) {
				continue;
			}

			sb.append(value.toString()).append(",");
		}

		return sb.deleteCharAt(sb.length() - 1).toString();
	}
}
