package br.com.otgmobile.trackteam.daemon;

import android.content.Context;
import br.com.otgmobile.trackteam.cloud.rest.HistoricosCloud;

public class DaemonHistoricoSender extends DaemonSender {

	private HistoricosCloud cloud;
	private Context context;

	public DaemonHistoricoSender(Context context) {
		this.cloud = new HistoricosCloud();
		this.context = context;
	}

	@Override
	protected void process() {
		cloud.sendQueue(context);
	}
}
