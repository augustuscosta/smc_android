package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import br.com.otgmobile.trackteam.model.LocalizaMaterial;
import br.com.otgmobile.trackteam.model.LocalizaMaterialMaterial;
import br.com.otgmobile.trackteam.model.Material;

public class LocalizaMaterialDAO implements BaseColumns{
	
	public static final String ID = "id";
	public static final String DESCRICAO = "descricao";
	public static final String ENDERECOID = "endereco_id";
	public static final String LOCALIZA_MATERIALID = "localiza_material_id";
	public static final String MATERIALID = "material_id";
	
	public static final String DATABASE_TABLE="localiza_material";
	public static final String DATABASE_TABLE_REFERENCE_MATERIAIS="localiza_material_material";
	
	public static final String[] COLUMNS = {_ID,ID,DESCRICAO,ENDERECOID};
	public static final String[] REFERENCE_COLUMNS = {_ID,LOCALIZA_MATERIALID,MATERIALID};
	
	private Context context;
	private MaterialDAO materialDAO;
	private SQLiteDatabase database;
	private EnderecoDAO enderecoDAO;
	
	public LocalizaMaterialDAO(final Context context){
		this.context = context;
		database = DBHelper.getDatabase(context);
	}
	
	public long save(final LocalizaMaterial obj){
		long inserted = -1;
		
		if(isNewObj(obj)){
			inserted = create(obj);
			obj.set_id((int)inserted);
		}else{
			inserted = update(obj);			
		}
		
		if(inserted > 0){
			createMaterialReference(obj);
			obj.setEnderecoId((int) saveEndereco(obj));
		}
		
		return inserted;
	}
	
	
	private long saveEndereco(final LocalizaMaterial obj) {
		if (obj.getEndereco() == null) {
			return  -1;
		}

		// TODO Um dia no futuro quando tivermos uma conversao dos dados que vem
		// do server poderemos retirar essa linha.
		return enderecoDAO().save(obj.getEndereco());
	}

	private void createMaterialReference(final LocalizaMaterial obj) {
		if(obj.getMateriais() == null || obj.getMateriais().isEmpty()){
			return;
		}
		deleteMaterialReference(obj);
		for (Material material : obj.getMateriais()) {
			material.setDisponivel(false);
			materialDAO().save(material);
			createMaterialReference(obj, material);
		}
		
	}
	
	public Cursor fetchAll() {
		return database.query(DATABASE_TABLE, COLUMNS, null, null, null, null, null);
	}
	
	public Cursor fetchReference(final int value) {
		String whereClause = LOCALIZA_MATERIALID + " = " + value;
		return database.query(DATABASE_TABLE_REFERENCE_MATERIAIS, REFERENCE_COLUMNS, whereClause, null, null, null, null);
	}

	
	
	private boolean deleteMaterialReference(final LocalizaMaterial obj) {
		String whereClause = LOCALIZA_MATERIALID + " = " +obj.get_id().toString();
		return database.delete(DATABASE_TABLE_REFERENCE_MATERIAIS, whereClause , null) > 0;
	}
	
	public boolean delete(final LocalizaMaterial obj) {
		String whereClause = _ID + " = " +obj.get_id().toString();
		return database.delete(DATABASE_TABLE, whereClause , null) > 0;
	}

	private long createMaterialReference(final LocalizaMaterial obj, final Material material) {
		ContentValues initialValues = createReferenceValues(obj, material);
		return database.insert(DATABASE_TABLE_REFERENCE_MATERIAIS, null, initialValues);
	}
	private long create(final LocalizaMaterial obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}
	
	private long update(final LocalizaMaterial obj) {
		ContentValues values = createContentValues(obj);
		return database.update(DATABASE_TABLE, values, _ID + " = ?",new String[] { obj.get_id().toString() });
	}

	private boolean isNewObj(final LocalizaMaterial obj) {
		if(obj.get_id() != null){
			return false;
		}
		return true;
	}
	
	private List<Material> retriveReferencedMaterials(final int value){
		List<Material> materiais = new ArrayList<Material>();
		Cursor cursor = fetchReference(value);
		try{
			
		if(cursor.moveToFirst()){
			LocalizaMaterialMaterial local = null;
			while(!cursor.isAfterLast()){
				local = new LocalizaMaterialMaterial(cursor);
				materiais.add(materialDAO().find(local.getMaterialId()));
			}
		}
		
		return materiais;
		}finally{
			cursor.close();
		}
	}
	
	public List<LocalizaMaterial> fetchAllParsed() {
		List<LocalizaMaterial>	localizaMAteriais = new ArrayList<LocalizaMaterial>();
		Cursor cursor = fetchAll();
		try{
			
			if(cursor.moveToFirst()){
				localizaMAteriais.add(parse(cursor));
			}
			return localizaMAteriais;
		}finally{
			cursor.close();
		}
	}

	private LocalizaMaterial parse(final Cursor cursor) {
		LocalizaMaterial localizaMaterial = new LocalizaMaterial();
		localizaMaterial.set_id(cursor.getInt(0));
		localizaMaterial.setId(cursor.getInt(1));
		localizaMaterial.setDescricao(cursor.getString(2));
		localizaMaterial.setEndereco(enderecoDAO().find(cursor.getInt(3)));
		localizaMaterial.setMateriais(retriveReferencedMaterials(cursor.getInt(0)));
		return localizaMaterial;
	}

	private ContentValues createContentValues(final LocalizaMaterial obj){
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(DESCRICAO, obj.getDescricao());
		values.put(ENDERECOID, obj.getEnderecoId());
		return values;
	}
	
	private ContentValues createReferenceValues(final LocalizaMaterial obj, final  Material material){
		ContentValues values = new ContentValues();
		values.put(LOCALIZA_MATERIALID, obj.get_id());
		values.put(MATERIALID, material.get_id());
		return values;
	}
	
	private MaterialDAO materialDAO(){
		if(materialDAO == null){
			materialDAO = new MaterialDAO(context);
		}
		
		return materialDAO;
	}
	
	private EnderecoDAO enderecoDAO() {
		if (enderecoDAO == null) {
			enderecoDAO = new EnderecoDAO(context);
		}

		return enderecoDAO;
	}

}
