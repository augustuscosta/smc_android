package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

public class Veiculo implements Serializable {

	private static final long serialVersionUID = -2011610417860654206L;

	private Integer _id;
	
	private Integer id;
	
	private String placa;
	
	private String modelo;
	
	private String renavan;
	
	private String ano;
	
	private String marca;
	
	private String cor;
	
	private Local local;
	
	private Integer localID;
	
	private Integer cercaID;
	
	private Historico ultimoHistorico;
	
	
	public Historico getUltimoHistorico() {
		return ultimoHistorico;
	}

	public void setUltimoHistorico(Historico ultimoHistorico) {
		this.ultimoHistorico = ultimoHistorico;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getRenavan() {
		return renavan;
	}

	public void setRenavan(String renavan) {
		this.renavan = renavan;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public Local getLocal() {
		return local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}


	public Integer get_id() {
		return _id;
	}

	public void set_id(Integer _id) {
		this._id = _id;
	}

	public Integer getLocalID() {
		return localID;
	}

	public void setLocalID(Integer localID) {
		this.localID = localID;
	}

	public Integer getCercaID() {
		return cercaID;
	}

	public void setCercaID(Integer cercaID) {
		this.cercaID = cercaID;
	}
	
	
}
