package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.Natureza;

public class NaturezaDAO {
	// Database fields
	public static final String _ID = "_id";
	public static final String ID = "id";
	public static final String VALOR = "valor";
	public static final String[] COLUMNS = new String[] { _ID,ID,VALOR};

	private static final String DATABASE_TABLE = "natureza";
	private SQLiteDatabase database;

	public NaturezaDAO(Context context){
		database = DBHelper.getDatabase(context);
	}

	public long save(Natureza obj){
		if(obj.getValor() == null)
			return -1;
		
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = update(obj);
		}

		return inserted;
	}

	private boolean isNew(Natureza obj) {
		if(find(obj.getId()) == null)
			return true;
		return false;
	}

	public long create(Natureza obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	public long update(Natureza obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, ID + " = ?", new String[] { obj.getId().toString()});
	}

	public boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, ID + "=" + id, null) > 0;
	}

	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE,null,null) > 0;
	}

	public Cursor findCursor(final Integer codigo) {
		// Se codigo(id) for igual a nulo clasula where nao ira existir retornando todos.
		String whereClasule = codigo == null ? null : ID + " = " + codigo;
		return database.query(DATABASE_TABLE, COLUMNS, whereClasule, null, null, null, null);
	}

	public Natureza find(int id) {
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}
			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}

	public List<Natureza> fetchAllParsed(){
		Cursor cursor = findCursor(null);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}

	private List<Natureza> parse(Cursor cursor){
		List<Natureza>  toReturn = new ArrayList<Natureza>();
		if(cursor.moveToFirst()){
			while(!cursor.isAfterLast()){
				toReturn.add(parseObject(cursor));
				cursor.moveToNext();
			}
		}
		
		return toReturn;
	}

	private Natureza parseObject(Cursor cursor){
		Natureza obj = new Natureza();
		obj.set_id(cursor.getInt(0));
		obj.setId(cursor.getInt(1));
		obj.setValor(cursor.getString(2));
		return obj;
	}

	private ContentValues createContentValues(Natureza obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(VALOR, obj.getValor());
		return values;
	}

	private ContentValues createUpdateContentValues(Natureza obj) {
		ContentValues values = new ContentValues();
		values.put(VALOR, obj.getValor());
		return values;
	}
}
