package br.com.otgmobile.trackteam.activity;

import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Pessoa;
import br.com.otgmobile.trackteam.model.Restricao;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.Session;

public class PessoaDetail extends GenericActivity{
	
	private TextView nomeText;
	private TextView cpfText;
	private TextView rgText;
	private TextView passaportText;
	private TextView descricaoText;
	private TextView restiricaoText;
	private Pessoa obj;
	private Button socketStatus;
	private Button bateriaStatus;
	private Button telemetriaStatus;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pessoa);
		configureActivity();
		handleIntent();
		setDataOnView();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}

	private void configureActivity() {
		nomeText = (TextView) findViewById(R.id.pessoa_name);
		cpfText	 = (TextView) findViewById(R.id.pessoa_cpf);
		rgText	 = (TextView) findViewById(R.id.pessoa_rg);
		passaportText = (TextView) findViewById(R.id.pessoa_passaport);
		descricaoText = (TextView) findViewById(R.id.pessoa_desciption);
		restiricaoText = (TextView) findViewById(R.id.pessoa_resticiton);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		bateriaStatus = (Button) findViewById(R.id.bateriaStatusIcon);
		telemetriaStatus = (Button) findViewById(R.id.telemetriaStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}

	private void handleIntent() {
		Bundle b = getIntent().getExtras();
		obj = (Pessoa) b.getSerializable(ConstUtil.SERIALIZABLE_KEY);		
	}

	private void setDataOnView() {
		if(obj == null){
			return;
		}
		Restricao restricao = obj.getRestricao();
		
		if(obj.getNome() != null){
			nomeText.setText(obj.getNome());
		}
		
		if(obj.getCpf() != null){
			cpfText.setText(obj.getCpf());
		}
		if(obj.getRg() != null){
			rgText.setText(obj.getRg());
		}
		if(obj.getPassaporte() != null){
			passaportText.setText(obj.getPassaporte());
		}
		
		if(obj.getDescricao() != null) {
			descricaoText.setText(obj.getDescricao());
		}
		if(restricao != null && restricao.getDescricao() != null){
			restiricaoText.setText(restricao.getDescricao());
		}
	}
	
	
	public void returnButtonOnClick(View view) {
		finish();
	}
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}
	
	@Override
	protected void updateBatteryStatus(int percent) {
		if(percent <= 30){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_vermelha));
		}else if(percent <= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_amarela));
			
		}else if(percent >= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_verde));			
		}
		
	}
	
	@Override
	protected void updateTelemetriaStatusByBroadcast(String status) {
		if(status.equals(getString(R.string.connected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluegreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.blueyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluered));
			return;
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		super.onLocationChanged(location);
		
	}

	@Override
	public void onStatusChanged(int status) {
		super.onStatusChanged(status);
		
	}

}
