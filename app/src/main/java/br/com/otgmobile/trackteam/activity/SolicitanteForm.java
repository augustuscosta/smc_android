package br.com.otgmobile.trackteam.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.customwidgets.CpfEditText;
import br.com.otgmobile.trackteam.customwidgets.PhoneEditText;
import br.com.otgmobile.trackteam.database.SolicitanteDAO;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.model.Solicitante;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.Session;

public class SolicitanteForm extends GenericActivity{
	private EditText nomeText;
	private EditText rgText;
	private CpfEditText cpfText;
	private PhoneEditText telefone1Text;
	private PhoneEditText telefone2Text;
	private Solicitante solicitante;
	private Button confirmButton;
	private boolean blockDeleteAndCreate;
	private LinearLayout solicitanteContainer;
	private Button socketStatus;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.solicitante);
		configureActivity();
		handleIntent();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}

	@Override
	protected void onDestroy() {
		AppHelper.unbindDrawables(solicitanteContainer);
		super.onDestroy();
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};

	@Override
	public void onBackPressed(){
		finish();
		super.onBackPressed();
	}
	
	private void handleIntent() {
		Intent intent = getIntent();
		if(intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)){
			Bundle bundle = intent.getExtras();
			solicitante = (Solicitante) bundle.getSerializable(ConstUtil.SERIALIZABLE_KEY);
			blockDeleteAndCreate = intent.getBooleanExtra(ConstUtil.BLOCK_DELETE_AND_CREATE, false);
			fillFormWithData();
			blockEditTextsAndRemoveConfirmButton();
		}
		else{
			solicitante = new Solicitante();
		}
	}

	private void blockEditTextsAndRemoveConfirmButton() {
		if(blockDeleteAndCreate){
			nomeText.setEnabled(false);
			cpfText.setEnabled(false);
			telefone1Text.setEnabled(false);
			telefone2Text.setEnabled(false);
			confirmButton.setVisibility(View.GONE);
		}

	}

	private void fillFormWithData() {
		if(solicitante.getNome()!=null){
			nomeText.setText(solicitante.getNome());			
		}
		if(solicitante.getCpf()!=null){
			cpfText.setText(solicitante.getCpf());
		}
		if(solicitante.getTelefone1()!=null){
			telefone1Text.setText(solicitante.getTelefone1());			
		}
		if(solicitante.getTelefone2()!=null){
			telefone2Text.setText(solicitante.getTelefone2());			
		}
		if(solicitante.getRg()!=null){
			rgText.setText(solicitante.getRg());			
		}

	}

	private void configureActivity(){
		nomeText 	  = (EditText) findViewById(R.id.solicitante_name);
		rgText 	  = (EditText) findViewById(R.id.solicitante_rg);
		cpfText  	  = (CpfEditText) findViewById(R.id.solicitante_cpf);
		telefone1Text = (PhoneEditText) findViewById(R.id.solicitante_phone1);
		telefone2Text =  (PhoneEditText) findViewById(R.id.solicitante_phone2);
		confirmButton = (Button) findViewById(R.id.confirmButton);
		solicitanteContainer = (LinearLayout) findViewById(R.id.solicitante_form_container);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}

	private void registerSolicitante(){

		solicitante.setNome(nomeText.getText().toString());
		solicitante.setRg(rgText.getText().toString());
		solicitante.setCpf(cpfText.getCleanText());
		solicitante.setTelefone1(telefone1Text.getCleanText());
		solicitante.setTelefone2(telefone2Text.getCleanText());
		SolicitanteDAO dao = new SolicitanteDAO(this);
		if(solicitante.get_id()==null){
			dao.create(solicitante);
		}
		else{
			dao.update(solicitante);
		}
	}

	public void confirmButtonOnClick(View view){
		registerSolicitante();
		finish();
	}

	
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}
	

	@Override
	public void onLocationChanged(Location location) {
		super.onLocationChanged(location);
		
	}

	@Override
	public void onStatusChanged(int status) {
		super.onStatusChanged(status);
	}
}
