package br.com.otgmobile.trackteam.activity;

import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;

import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.database.RespostaDAO;
import br.com.otgmobile.trackteam.model.Pergunta;
import br.com.otgmobile.trackteam.model.Resposta;
import br.com.otgmobile.trackteam.util.ConstUtil;

public class RespostaActivity extends Activity {
	
	LinearLayout dataHora;
	
	Pergunta pergunta;
	Integer ocorrenciaId;
	EditText respostaEditText;
	DatePicker respostaData1;
	DatePicker respostaDataHora;
	TimePicker respostaHora;
	EditText respostaNumerica;
	RadioGroup respostaSimNao;
	RadioButton respostaSim;
	RadioButton respostaNao;
	String resposta;
	TipoEntrada tipo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_resposta);
		respostaEditText = (EditText) findViewById(R.id.resposta);
		respostaData1 = (DatePicker)findViewById(R.id.respostaData);
		respostaNumerica = (EditText)findViewById(R.id.respostaNumerica);
		respostaSimNao = (RadioGroup)findViewById(R.id.respostaSimNao);
		respostaSim = (RadioButton)findViewById(R.id.respostaSim);
		respostaNao = (RadioButton)findViewById(R.id.respostaNao);
		dataHora = (LinearLayout)findViewById(R.id.linearLayoutDataHora);
		respostaDataHora = (DatePicker)findViewById(R.id.respostaData2);
		respostaHora = (TimePicker)findViewById(R.id.respostaHora);
		resposta = "";
		handleIntent();
		prepareActivity();
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu resposta_menu) {
		MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.resposta_menu, resposta_menu);
        return true;
	};
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.resposta_menu_enviar:
        	sendButtonClick();
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	private void handleIntent(){
		Intent intent = getIntent();
		if (intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)) {
			pergunta = (Pergunta) intent.getExtras().get(ConstUtil.SERIALIZABLE_KEY);
		}
		if (intent.hasExtra(ConstUtil.ID_KEY)) {
			ocorrenciaId = (Integer) intent.getExtras().get(ConstUtil.ID_KEY);
		}
		
	}
	
	private void prepareActivity(){
		if (pergunta.getTipoEntrada().equalsIgnoreCase(ConstUtil.TEXTO)) {
			respostaEditText.setVisibility(1);
			tipo = TipoEntrada.TEXTO;
		}
		if (pergunta.getTipoEntrada().equalsIgnoreCase(ConstUtil.DATA)) {
			respostaData1.setVisibility(1);
			tipo = TipoEntrada.DATA;
		}
		if (pergunta.getTipoEntrada().equalsIgnoreCase(ConstUtil.NUMERICO)) {
			respostaNumerica.setVisibility(1);
			tipo = TipoEntrada.NUMERICO;
		}
		if (pergunta.getTipoEntrada().equalsIgnoreCase(ConstUtil.SIMNAO)) {
			respostaSimNao.setVisibility(1);
			tipo = TipoEntrada.SIMNAO;
		}
		if (pergunta.getTipoEntrada().equalsIgnoreCase(ConstUtil.DATAHORA)) {
			dataHora.setVisibility(1);
			tipo = TipoEntrada.DATAHORA;
		}
		if(tipo == null){
			respostaEditText.setVisibility(1);
			tipo = TipoEntrada.TEXTO;
		}
	}
	
	private void sendButtonClick() {
		switch (tipo) {
		case TEXTO:
			resposta = respostaEditText.getText().toString();
			System.out.println(resposta);
			break;

		case DATA:
			int day = respostaData1.getDayOfMonth();
		    int month = respostaData1.getMonth();
		    int year =  respostaData1.getYear();
		    
		    Calendar calendar = Calendar.getInstance();
		    calendar.set(year, month, day);
		    Date data = calendar.getTime();
		    resposta = data.toString();
		    break;
		    
		case NUMERICO:
			
			resposta = respostaNumerica.getText().toString();
			break;
			
		case SIMNAO:
			
			if(respostaSimNao.getCheckedRadioButtonId() == respostaSim.getId()){
				resposta = respostaSim.getText().toString();
			}
			else {
				resposta = respostaNao.getText().toString();
			}
			break;
			
		case DATAHORA:
		    Date data2 = new Date();
		    data2.setDate(respostaDataHora.getDayOfMonth());
		    data2.setMonth(respostaDataHora.getMonth());
		    data2.setYear(respostaDataHora.getYear());
		    data2.setHours(respostaHora.getCurrentHour());
		    data2.setMinutes(respostaHora.getCurrentMinute());
		    resposta = data2.toString();
		    
			break;
		    
		default:
			resposta = respostaEditText.getText().toString();
			System.out.println(resposta);
			break;
		}
		
		//System.out.println(resposta);
		if (resposta.equalsIgnoreCase("")) {
			return;
		}
		Resposta r = new Resposta();
		r.setOcorrencia_id(ocorrenciaId);
		r.setPergunta_id(pergunta.getId());
		r.setResposta(resposta);
		
		RespostaDAO rDAO = new RespostaDAO(RespostaActivity.this);
		rDAO.save(r);
		finish();
	}
	
	public enum TipoEntrada {
		   TEXTO,
		   NUMERICO,
		   DATA,
		   DATAHORA,
		   SIMNAO
		}
}
