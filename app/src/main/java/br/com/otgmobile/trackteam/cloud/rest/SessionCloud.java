
package br.com.otgmobile.trackteam.cloud.rest;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import br.com.otgmobile.trackteam.model.Agente;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.Session;

import com.google.gson.Gson;

public class SessionCloud extends RestClient {

	private final String URL = "comunicacao/session";
	private final String ROOT_OBJECT = "result";
	
	public void login(String login, String senha,Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		addParam("login", login);
		addParam("senha", senha);
		addParam("dispositivo", Session.getDeviceId(context));
		addParam("versao", AppHelper.getInstance().getAppVersion(context));
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.POST);
		if(responseCode == 200)
			saveSession(getObjectFromResponse(),context);
	}
	

	private void saveSession(Agente agente,Context context) {
		if(agente == null)
			return;
		Session.setToken(agente.getToken(), context);
		Session.setVeiculoID(agente.getVeiculoID(), context);
		Session.setAgenteID(agente.getId(), context);
	}


	private Agente getObjectFromResponse() throws JSONException {
		JSONObject jsonObject = getJsonObjectFromResponse(ROOT_OBJECT);
		if (jsonObject != null) {
			Agente toReturn;
			Gson gson = new Gson();
			toReturn = gson.fromJson(jsonObject.toString(),Agente.class);
			return toReturn;
		}
		return null;
	}
	
	
}
