package br.com.otgmobile.trackteam.model;


public enum ZoomType {

	Geral(0),
	Veiculo(1),
	OcorrenciaPresente(2);

	private int value;

	private ZoomType(final int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}

	public static ZoomType fromValue(final int value) {
		for (ZoomType zoomType : ZoomType.values()) {
			if (value == zoomType.getValue()) {
				return zoomType;
			}
		}

		return null;
	}

}
