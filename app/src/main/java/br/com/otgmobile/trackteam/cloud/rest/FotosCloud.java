package br.com.otgmobile.trackteam.cloud.rest;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;

import android.content.Context;
import br.com.otgmobile.trackteam.database.FotoDAO;
import br.com.otgmobile.trackteam.model.Foto;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

public class FotosCloud extends RestClient {
	
	private final static String UPLOAD_URL = "comunicacao/ocorrencia/fotos";
	private FotoDAO fotoDAO;
	
	
	public void uploadFoto(Context context, Foto foto) throws Throwable {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		setUrl(addSlashIfNeeded(url) + UPLOAD_URL);
		execute(context, foto, token);
	}

	private void execute(Context context, Foto foto, String token) throws Throwable {
		HttpPost request = new HttpPost(url);
		MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		entity.addPart(ConstUtil.OCORRENCIATAG, new StringBody((foto.getOcorrenciaServerId().toString())));
		entity.addPart(ConstUtil.TOKEN, new StringBody((token)));
		entity.addPart(ConstUtil.FOTO, new ByteArrayBody(encodeFotoTobyteArray(foto,context), ConstUtil.FOTO));
		request.setEntity(entity);
		executeRequest(request, url);
	}

	private byte[] encodeFotoTobyteArray(Foto foto, Context context) throws Throwable {
		InputStream is = context.openFileInput(foto.getImagem());
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try{
			byte[] b = new byte[is.available()];
			int bytesRead = 0;
			while ((bytesRead = is.read(b)) != -1) {
				bos.write(b, 0, bytesRead);
			}
			return bos.toByteArray();			
		}finally{
			bos.close();
			is.close();
		}
	}
	
	public void uploadAllFotosFromOcorrencia(Context context, Ocorrencia ocorrencia) throws Throwable{
		if(ocorrencia.getId() == null){
			LogUtil.i("Não foi possivel fazer o upload das fotos ocorrencia ainda não sincronizada");
			return;
		}
		
		List<Foto> fotos = fotoDAO(context).findFromOcorrenciaNotSent(ocorrencia.get_id());
		
		if(fotos == null || fotos.isEmpty()){
			LogUtil.i("sem filmagens para fazer o upload");
			return;
		}
		
		for (Foto foto : fotos) {
				if(foto.getOcorrenciaServerId() == null || foto.getOcorrenciaServerId() == 0 ){
				LogUtil.i("fazendo o upload da Foto "+foto.get_id().toString()+" do atendimento: "+ocorrencia.getId());
				foto.setOcorrenciaServerId(ocorrencia.getId());
				uploadFoto(context, foto);
				}
			if ( HttpStatus.SC_OK == getResponseCode() ) {
				LogUtil.i("upload com sucesso da Foto "+foto.get_id().toString()+" do atendimento: "+ocorrencia.getId());
				foto.setEnviado(AppHelper.getCurrentTime());
				fotoDAO(context).save(foto);
			}
		}
	}
	
	private FotoDAO fotoDAO(Context context){
		if(fotoDAO == null){
			fotoDAO = new FotoDAO(context);
		}
		return fotoDAO;
	}

}
