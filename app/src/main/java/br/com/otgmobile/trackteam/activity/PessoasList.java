package br.com.otgmobile.trackteam.activity;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.adapter.PessoaAdapter;
import br.com.otgmobile.trackteam.cloud.rest.PessoaCloud;
import br.com.otgmobile.trackteam.database.PessoaDAO;
import br.com.otgmobile.trackteam.model.Pessoa;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

public class PessoasList extends GenericListActivity {
	
	private EditText pesquisaText;
	private List<Pessoa> list;
	private OnlineConsultationTask onlineConsultationTask;
	private PessoaCloud pessoaCloud;
	private PessoaDAO pessoaDAO;
	private PessoaAdapter adapter;
	private LinearLayout listPessoasContainer;
	private Button socketStatus;
	private Button bateriaStatus;
	private Button telemetriaStatus;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_pessoas);
		configureActivity();
	}

	private void configureActivity() {
		pesquisaText = (EditText) findViewById(R.id.pessoa_search);
		listPessoasContainer = (LinearLayout) findViewById(R.id.list_pessoas_container);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		bateriaStatus = (Button) findViewById(R.id.bateriaStatusIcon);
		telemetriaStatus = (Button) findViewById(R.id.telemetriaStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		viewDetail(list.get(position));
		super.onListItemClick(l, v, position, id);
	}
	
	@Override
	protected void onDestroy() {
		setListAdapter(null);
		AppHelper.unbindDrawables(listPessoasContainer);
		super.onDestroy();
	}
	
	private void viewDetail(Pessoa pessoa) {
		Intent intent = new Intent(PessoasList.this,PessoaDetail.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, pessoa);
		intent.putExtras(bundle);
		startActivity(intent);
		
	}

	public void returnButtonOnClick(View view) {
		finish();
	}
	
	public void searchButtonOnClick(View view) {
		runOnlineConsult(pesquisaText.getText().toString());
	}
	
	private void runOnlineConsult(String search) {
		onlineConsultationTask = new OnlineConsultationTask(this, search);
		onlineConsultationTask.execute();
	}

	private class OnlineConsultationTask extends AsyncTask<Void, Void, List<Pessoa>>{
		ProgressDialog mDialog;
		Context context;
		String  search;
		
		OnlineConsultationTask(Context context,String  search){
		this.context = context;
		this.search = search;
		}
		
		List<Pessoa> searchPessoa() throws Exception{
			return pessoaCloud().search(context, search);
		}
		
		@Override
		protected void onPreExecute() {
			getWindow()
			.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			mDialog = ProgressDialog.show(context, "",
					getString(R.string.processing), true, false);
			mDialog.setCancelable(false);
			setProgressBarIndeterminate(true);
		}

		@Override
		protected List<Pessoa> doInBackground(Void... params) {
			try {
				return searchPessoa();
			} catch (Exception e) {
				LogUtil.e("erro ao pesquisar pessoa", e);
				return null;
			}
		}
		
		@Override
		protected void onPostExecute(List<Pessoa> result) {
			if(result == null || result.isEmpty()) {
				list = pessoaDAO().search(search);
			}else{
				list = result;
				for(Pessoa pessoa : list){
					pessoaDAO().save(pessoa);
				}
				
			}
			
			mDialog.cancel();
			setAdapter();
		}
	}
	
	private void setAdapter() {
		if(list == null || list.isEmpty()) {
			Toast.makeText(PessoasList.this, getString(R.string.sem_registros), Toast.LENGTH_LONG).show();
		}else{
			adapter = new PessoaAdapter(this, list);
			setListAdapter(adapter);
		}
	}
	
	private PessoaCloud pessoaCloud() {
		if(pessoaCloud == null) {
			pessoaCloud = new PessoaCloud();
		}
		
		return pessoaCloud;
	}

	private PessoaDAO pessoaDAO() {
		if(pessoaDAO == null) {
			pessoaDAO = new PessoaDAO(this);
		}
		
		return pessoaDAO;
	}
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}
	
	@Override
	protected void updateBatteryStatus(int percent) {
		if(percent <= 30){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_vermelha));
		}else if(percent <= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_amarela));
			
		}else if(percent >= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_verde));			
		}
		
	}
	
	@Override
	protected void updateTelemetriaStatusByBroadcast(String status) {
		if(status.equals(getString(R.string.connected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluegreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.blueyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluered));
			return;
		}
	}
}
