package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.Agente;

public class AgenteDAO {
	
	// Database fields
	public static final String _ID = "_id";
	public static final String ID = "id";
	public static final String NOME ="nome";
	public static final String CPF ="cpf";
	public static final String MATRICULA ="matricula";
	public static final String LOGIN ="login";
	public static final String DESCRICAO ="descricao";
	public static final String VEICULOID ="veiculo_id";
	public static final String[] COLUMNS = new String[] { _ID,ID,NOME,CPF,MATRICULA,LOGIN,DESCRICAO,VEICULOID};
	
	
	private static final String DATABASE_TABLE = "agente";
	private SQLiteDatabase database;
	private Context context;
	
	public AgenteDAO(Context context){
		database = DBHelper.getDatabase(context);
		this.context = context;
	}
	
	public long save(Agente obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = update(obj);
		}

		return inserted;
	}

	private boolean isNew(Agente obj) {
		if(find(obj.getId()) == null)
			return true;
		return false;
	}
	
	public long create(Agente obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}
	
	public long update(Agente obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, ID+ " = ?",new String[] { obj.getId().toString()});
	}
	
	public boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, ID + "=" + id, null) > 0;
	}
	
	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE,null,null) > 0;
	}
		
	private Cursor findCursor(int codigo){
		 return database.query(DATABASE_TABLE, COLUMNS, ID+ " = ?", new String[] { codigo + "" }, null, null, null);
	}
	
	public Agente find(int id){
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}
	
	
	public Cursor fetchAll() {
		
		Cursor cursor = null;
		cursor = database.query(DATABASE_TABLE, COLUMNS, null, null,
				null, null, null);
		
		
		return cursor;
	}
	
	public List<Agente> fetchAllParsed(){
		Cursor cursor = fetchAll();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	private List<Agente> parse(Cursor cursor){
		List<Agente>  toReturn= null;
		if(cursor != null){
			toReturn = new ArrayList<Agente>();
			Agente obj;
			HistoricoDAO historicoDAO = new HistoricoDAO(context);
			if(cursor.moveToFirst()){
				while(!cursor.isAfterLast()){
					obj = parseObject(cursor);
					obj.setUltimoHistorico(historicoDAO.findByAgent(obj));
					toReturn.add(obj);
					cursor.moveToNext();
				}
			}
		}
		
		
		return toReturn; 
	}
	
	private Agente parseObject(Cursor cursor){
		Agente obj = null;
		obj = new Agente();
		obj.set_id(cursor.getInt(0));
		obj.setId(cursor.getInt(1));
		obj.setNome(cursor.getString(2));
		obj.setCpf(cursor.getString(3));
		obj.setMatricula(cursor.getString(4));
		obj.setLogin(cursor.getString(5));
		obj.setDescricao(cursor.getString(6));
		obj.setVeiculoID(DatabaseUtil.returnNullValueForInt(cursor, VEICULOID));
		
		return obj;
	}
	
	private ContentValues createContentValues(Agente obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(NOME, obj.getNome());
		values.put(CPF, obj.getCpf());
		values.put(MATRICULA, obj.getMatricula());
		values.put(LOGIN, obj.getLogin());
		values.put(DESCRICAO, obj.getDescricao());
		values.put(VEICULOID, obj.getVeiculoID());
		return values;
	}
	
	private ContentValues createUpdateContentValues(Agente obj) {
		ContentValues values = new ContentValues();
		values.put(NOME, obj.getNome());
		values.put(CPF, obj.getCpf());
		values.put(MATRICULA, obj.getMatricula());
		values.put(LOGIN, obj.getLogin());
		values.put(DESCRICAO, obj.getDescricao());
		values.put(VEICULOID, obj.getVeiculoID());
		return values;
	}

}
