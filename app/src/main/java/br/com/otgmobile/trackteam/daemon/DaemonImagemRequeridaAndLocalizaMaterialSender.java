package br.com.otgmobile.trackteam.daemon;

import br.com.otgmobile.trackteam.cloud.rest.ImagemRequeridaCloud;
import br.com.otgmobile.trackteam.cloud.rest.LocalizaMaterialCloud;
import br.com.otgmobile.trackteam.cloud.rest.RespostaCloud;
import android.content.Context;

public class DaemonImagemRequeridaAndLocalizaMaterialSender extends DaemonSender {

	private ImagemRequeridaCloud cloud;
	private Context context;
	private LocalizaMaterialCloud lcloud;
	private RespostaCloud rcloud;

	public DaemonImagemRequeridaAndLocalizaMaterialSender(Context context) {
		this.cloud = new ImagemRequeridaCloud();
		this.lcloud = new LocalizaMaterialCloud();
		this.rcloud = new RespostaCloud();
		this.context = context;
	}

	@Override
	protected void process() {
		cloud.sendQueue(context);
		lcloud.sendAllUnsent(context);
		rcloud.sendAllUnsent(context);
	}
}
