package br.com.otgmobile.trackteam.cloud.rest;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import br.com.otgmobile.trackteam.database.VeiculoEnvolvidoDAO;
import br.com.otgmobile.trackteam.model.VeiculoEnvolvido;
import br.com.otgmobile.trackteam.util.Session;

import com.google.gson.Gson;


public class VeiculosEnvolvidosCloud extends RestClient {
	
	private final String URL = "comunicacao/veiculoEnvolvido";

	private final String ROOT_OBJECT = "result";
	
	public List<VeiculoEnvolvido> list(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return getListFromResponse();
	}
	
	public List<VeiculoEnvolvido> search(Context context, String searchParam) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		addParam("query", searchParam);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return getListFromResponse();
	}
	
	public Boolean sync(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return storeResponse(context);
	}
	
	private Boolean storeResponse(Context context) throws Exception {
		VeiculoEnvolvidoDAO dao = new VeiculoEnvolvidoDAO(context);
		try {
			JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
			if (jsonArray != null) {
				VeiculoEnvolvido obj;
				Gson gson = new Gson();
				dao.deleteAll();
				for (int i = 0; i < jsonArray.length(); i++) {
					obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),VeiculoEnvolvido.class);
					dao.save(obj);
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
		}
		return true;
	}
	
	private List<VeiculoEnvolvido> getListFromResponse() throws JSONException {
		JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
		if (jsonArray != null) {
			VeiculoEnvolvido obj;
			Gson gson = new Gson();
			List<VeiculoEnvolvido> toReturn = new ArrayList<VeiculoEnvolvido>();
			for (int i = 0; i < jsonArray.length(); i++) {
				
				obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),VeiculoEnvolvido.class);
				toReturn.add(obj);
			}
			
			return toReturn;
		}
		
		return null;
	}
	
}
