package br.com.otgmobile.trackteam.cloud.rest.tests;

import java.util.List;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import br.com.otgmobile.trackteam.cloud.rest.HistoricosCloud;
import br.com.otgmobile.trackteam.model.Historico;
import br.com.otgmobile.trackteam.util.Session;


public class HistoricoCloudTests extends AndroidTestCase {

	@Override
	protected void setUp() throws Exception {
		Session.setToken(TestCont.TOKEN, getContext());
		Session.setServer(TestCont.SERVER,getContext());
		super.setUp();
	}
	
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	public void testCreateHistorico() throws Exception{
		HistoricosCloud cloud = new HistoricosCloud();
		
		Historico historico = new Historico();
		historico.setLatitude(0.F);
		historico.setLongitude(0.F);
		historico.setVeiculoID(176);
		historico.setVelocidade("34");
		cloud.create(historico, getContext());
		
		Assert.assertNotNull(cloud.getResponseCode());
		Assert.assertTrue(cloud.getResponseCode() == 200);
	}
	
	public void testListHistorico() throws Exception{
		HistoricosCloud cloud = new HistoricosCloud();
		List<Historico> response = cloud.list(getContext());
		Assert.assertNotNull(response);
		Assert.assertTrue(response.size() > 0);
	}
	
	public void testSync() throws Exception{
		HistoricosCloud cloud = new HistoricosCloud();
		Assert.assertTrue(cloud.sync(getContext()));
	}
	
	
}
