package br.com.otgmobile.trackteam.activity;

import java.util.List;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewDebug.CapturedViewProperty;
import android.widget.Button;
import android.widget.LinearLayout;
import br.com.otgmobile.trackteam.adapter.FilmagemAdapter;
import br.com.otgmobile.trackteam.database.FilmagemDAO;
import br.com.otgmobile.trackteam.model.Filmagem;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.Session;
import br.com.otgmobile.trackteam.R;

public class FilmagemListActivity extends GenericListActivity  {
	private Ocorrencia ocorrencia;
	private List<Filmagem> list;
	private FilmagemAdapter adapter;
	private LinearLayout filmagemListContainer;
	private Button socketStatus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_filmagens);
		configureActivity();
		handleIntent();
		fetchData();
	}

	private void configureActivity() {
		filmagemListContainer = (LinearLayout) findViewById(R.id.list_filmagens_container);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}
	
	@Override
	protected void onDestroy() {
		setListAdapter(null);
		AppHelper.unbindDrawables(filmagemListContainer);
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed(){
		finishSelection();
		super.onBackPressed();
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu films_menu) {
		MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.films_menu, films_menu);
        return true;
	};
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.films_menu_film:
        	film();
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	private void fetchData() {
		if (ocorrencia != null) {
			FilmagemDAO dao = new FilmagemDAO(this);
			list = dao.findFromOcorrencia(ocorrencia.get_id());
			if (list == null || list.isEmpty()){
				film();
			}
			else{
				setAdapter(list);				
			}
		}

	}

	private void setAdapter(List<Filmagem> list) {
			adapter = new FilmagemAdapter(list, this);
			setListAdapter(adapter);
	}

	private void handleIntent() {
		Intent intent = getIntent();
		if (intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)) {
			ocorrencia = (Ocorrencia) intent.getExtras().get(
					ConstUtil.SERIALIZABLE_KEY);
		}
	}
	
	public  void returnButtonOnClick(View view){
		finishSelection();
	}


	private void film() {
		Intent cameraIntent = new Intent(
				android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
		cameraIntent.putExtra(android.provider.MediaStore.EXTRA_VIDEO_QUALITY,0);
		startActivityForResult(cameraIntent, ConstUtil.FILM_REQUEST);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		if (requestCode == ConstUtil.FILM_REQUEST) {
			if (resultCode == RESULT_OK) {
				ocorrencia.setEnviado(false);
				Uri vid = intent.getData();
				saveVideoToOcorrencia(vid.toString());
			}
		}
	}

	private void saveVideoToOcorrencia(String uri) {
		saveVideoPath(uri);
		refresh();

	}

	private void saveVideoPath(String videoTag) {
		Filmagem film = new Filmagem();
		film.setOcorrenciaId(ocorrencia.get_id());
		film.setFilmagem(videoTag);
		FilmagemDAO dao = new FilmagemDAO(this);
		dao.save(film);

	}

	private void finishSelection() {
		ocorrencia.setFilmagens(list);
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		setResult(RESULT_OK, intent);
		finish();
	}

	private void refresh() {
		FilmagemDAO dao = new FilmagemDAO(this);
		list = dao.findFromOcorrencia(ocorrencia.get_id());
		if(list!=null)
			setAdapter(list);
	}
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}

}
