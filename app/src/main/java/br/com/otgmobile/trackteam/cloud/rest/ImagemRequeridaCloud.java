package br.com.otgmobile.trackteam.cloud.rest;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;

import android.content.Context;
import br.com.otgmobile.trackteam.database.ImagemRequeridaDAO;
import br.com.otgmobile.trackteam.model.ImagemRequerida;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

public class ImagemRequeridaCloud extends RestClient {
	
	private final static String UPLOAD_URL = "device/camera/foto";
	private ImagemRequeridaDAO imagemRequeridaDAO;
	
	
	public void uploadImagemRequerida(Context context, ImagemRequerida foto) throws Throwable {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		setUrl(addSlashIfNeeded(url) + UPLOAD_URL);
		execute(context, foto, token);
	}
	
	public void uploadImagemRequerida(Context context, byte[] foto) throws Throwable {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		setUrl(addSlashIfNeeded(url) + UPLOAD_URL);
		execute(context, foto, token);
	}
	
	private void execute(Context context, byte[] foto, String token) throws Throwable {
		HttpPost request = new HttpPost(url);
		MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		entity.addPart(ConstUtil.TOKEN, new StringBody((token)));
		entity.addPart(ConstUtil.FOTO, new ByteArrayBody(foto, ConstUtil.FOTO));
		request.setEntity(entity);
		executeRequest(request, url);
	}

	private void execute(Context context, ImagemRequerida foto, String token) throws Throwable {
		HttpPost request = new HttpPost(url);
		MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		entity.addPart(ConstUtil.TOKEN, new StringBody((token)));
		entity.addPart(ConstUtil.FOTO, new ByteArrayBody(encodeImagemRequeridaTobyteArray(foto,context), ConstUtil.FOTO));
		request.setEntity(entity);
		executeRequest(request, url);
	}

	private byte[] encodeImagemRequeridaTobyteArray(ImagemRequerida foto, Context context) throws Throwable {
		InputStream is = context.openFileInput(foto.getImagem());
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try{
			byte[] b = new byte[is.available()];
			int bytesRead = 0;
			while ((bytesRead = is.read(b)) != -1) {
				bos.write(b, 0, bytesRead);
			}
			return bos.toByteArray();			
		}finally{
			bos.close();
			is.close();
		}
	}
	
	public void sendQueue(Context context){
		
		List<ImagemRequerida> fotos = imagemRequeridaDAO(context).fetchAllParsed();
		
		if(fotos == null || fotos.isEmpty()){
			LogUtil.i("--------------sem imagens  requeridas para fazer o upload------------------");
			return;
		}
		
		for (ImagemRequerida foto : fotos) {
			try {
				uploadImagemRequerida(context, foto);
				LogUtil.i("--------------fazendo o upload de imagem requerida "+foto.get_id()+"------------------");
				
			} catch (Throwable e) {
				LogUtil.e("Erro ao fazer o upload das imagens requeridas",e);
				return;
			}
			if ( HttpStatus.SC_OK == getResponseCode() ){
				LogUtil.i("--------------upload de imagem requerida "+foto.get_id()+" executado com sucesso------------------");
				imagemRequeridaDAO(context).delete(foto);
			}
		}
	}
	
	private ImagemRequeridaDAO imagemRequeridaDAO(Context context){
		if(imagemRequeridaDAO == null){
			imagemRequeridaDAO = new ImagemRequeridaDAO(context);
		}
		return imagemRequeridaDAO;
	}

}
