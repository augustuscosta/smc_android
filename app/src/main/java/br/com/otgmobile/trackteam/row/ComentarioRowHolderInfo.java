package br.com.otgmobile.trackteam.row;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.trackteam.model.Comentario;

public class ComentarioRowHolderInfo {
	
	public TextView comentario;
	public LinearLayout comentarioButton;
	
	public void drawRow (Comentario obj){
		if(obj == null){
			return;
		}
		
		if(obj.getDescricao()!=null){
			comentario.setText(obj.getDescricao());
		}
		else{
			comentario.setText("");
		}
		if(obj.getId()!=null){
			comentarioButton.setVisibility(View.INVISIBLE);
		}
		else{
			comentarioButton.setVisibility(View.VISIBLE);
		}
		
	}

}
