package br.com.otgmobile.trackteam.util;

import android.content.Context;
import br.com.otgmobile.trackteam.database.ConfigurationDAO;

public class PrecisionUtil {

	private static final int METER_CONSTANT = 10;
	private static final int MIN_GPS_PRECISION = 100;
	private static final int ONE_SECOND_IN_MILLS = 1000;
	private static final int MAX_PRECISION = 200;
	private static ConfigurationDAO configurationDAO;
	
	public static long getMinTimeGps(final Context context){
		return getDefaultGpsTime(context);
	}
	
	public static long getMinDistanceGps(final Context context){
		return getDefaultGpsDistance(context);
	}
	
	public static int getBluetoothReconnectionTime(final Context context){
		return getDefaultReconnectionTime(context);
	}
	
	public static int getSocketReconnectionTime(final Context context){
		return getDefaultReconnectionTime(context);
	}
	
	public static int getDaemonsSleepTime(final Context context){
		return getDefaultDaemonsTime(context);
	}
	
	
	private static ConfigurationDAO getConfigurationDAO(final Context context){
		if(configurationDAO == null)
			configurationDAO = new ConfigurationDAO(context);
		return configurationDAO;
	}
	
	private static int getDefaultReconnectionTime(final Context context){
		return getDefaultTime(context);
	}
	
	private static int getDefaultDaemonsTime(final Context context){
		return getDefaultTime(context);
	}
	
	private static int getDefaultTime(final Context context){
		int precision = getConfigurationDAO(context).getPrecision();
		if(precision < MAX_PRECISION){
			precision = MAX_PRECISION - precision;
			return ONE_SECOND_IN_MILLS * precision;
		}
		return ONE_SECOND_IN_MILLS ;
	}
	
	private static long getDefaultGpsTime(final Context context){
		final int precision = getConfigurationDAO(context).getPrecision();
		if(precision < MAX_PRECISION){
			return (MAX_PRECISION - precision) * ONE_SECOND_IN_MILLS/2;			
		}else{
			return ONE_SECOND_IN_MILLS;
		}
	}
	
	private static long getDefaultGpsDistance(final Context context){
		final int precision = getConfigurationDAO(context).getPrecision();
		if(precision < MAX_PRECISION){
			return (MAX_PRECISION - precision) * METER_CONSTANT;
		}else{
			return MIN_GPS_PRECISION;
		}
	}
	
}
