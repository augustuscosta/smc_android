 package br.com.otgmobile.trackteam.cloud.rest;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import br.com.otgmobile.trackteam.database.MaterialDAO;
import br.com.otgmobile.trackteam.model.Material;
import br.com.otgmobile.trackteam.util.Session;

import com.google.gson.Gson;

public class MaterialCloud extends RestClient {
	
	private final String URL = "comunicacao/materiais";
	private final String URL2 = "comunicacao/materiais/disponiveis";

	private final String ROOT_OBJECT = "result";
	
	public List<Material> list(final Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return getListFromResponse();
	}
	
	public List<Material> search(final Context context, final String pesquisa) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		addParam("pesquisa", pesquisa);		
		setUrl(addSlashIfNeeded(url) + URL2);
		execute(RequestMethod.GET);
		return getListFromResponse();
	}
	
	public Boolean sendCheckList(final List<Material> list, final Context context) throws Throwable {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		Gson gson = new Gson();
		String json = gson.toJson(list);
		addParam("token", token);
		addParam("checkList", json );
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.POST);
		return storeResponse(context);
	}
	

	public Boolean sync(final Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return storeResponse(context);
	}
	
	private Boolean storeResponse(final Context context) throws Exception {
		MaterialDAO dao = new MaterialDAO(context);
		try {
			JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
			if (jsonArray != null) {
				Material obj;
				Gson gson = new Gson();
				dao.deleteAll();
				for (int i = 0; i < jsonArray.length(); i++) {
					obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),Material.class);
					obj.setDisponivel(true);
					dao.save(obj);
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
		}
		return true;
	}
	
	private List<Material> getListFromResponse() throws JSONException {
		JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
		if (jsonArray != null) {
			Material obj;
			Gson gson = new Gson();
			List<Material> toReturn = new ArrayList<Material>();
			for (int i = 0; i < jsonArray.length(); i++) {
				
				obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),Material.class);
				toReturn.add(obj);
			}
			return toReturn;
		}
		return null;

	}

}
