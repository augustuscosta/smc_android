package br.com.otgmobile.trackteam.row;

import android.widget.TextView;
import br.com.otgmobile.trackteam.model.Pessoa;

public class PessoaRowHolderInfo {
	
	public TextView nomeText;
	public TextView cpfText;
	public TextView rgText;

	public void drawRow(Pessoa obj) {
		 if(obj == null) {
			 return;
		 }
		 
		 if(obj.getNome() != null) {
			 nomeText.setText(obj.getNome());
		 }else{
			 nomeText.setText("");			 
		 }
		 
		 if(obj.getCpf() != null) {
			 cpfText.setText(obj.getCpf());
		 }else{
			 cpfText.setText("");
		 }
		 
		 if(obj.getRg() != null) {
			 rgText.setText(obj.getRg());
		 }else{
			 rgText.setText("");
			 
		 }
	}
}
