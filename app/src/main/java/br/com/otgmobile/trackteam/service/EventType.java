package br.com.otgmobile.trackteam.service;

import android.util.Log;

public enum EventType {
	
	ABRIR_CAMERA("camera/open"),
	FECHAR_CAMERA("camera/close"),
	MODIFICA_HISTORICO("historico/change"),

	NOVA_OCORRENCIA("ocorrencia/add"),
	MODIFICA_OCORRENCIA("ocorrencia/change"),
	EXCLUI_OCORRENCIA("ocorrencia/delete"),
	
	NOVO_VEICULO("veiculo/add"),
	MODIFICA_VEICULO("veiculo/change"),
	EXCLUI_VEICULO("veiculo/delete"),
	
	NOVO_AGENTE("agente/add"),
	MODIFICA_AGENTE("agente/change"),
	
	ATUALIZA_SESSAO("sessao/token"),
	
	CONECTA_CHAT("chat/conectar"),
	DESCONECTA_CHAT("chat/desconectar"),
	LISTA_OPERADORES("chat/operadores"),
	ENVIAR_MENSAGEM("chat/mensagem/enviar"),
	RECEBER_MENSAGEM("chat/mensagem/receber")
	
	;

	private String value;

	private EventType(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}

	public static EventType toSocketRequestType(String value) {
		final EventType[] requestTypes = EventType.values();
		for (EventType type : requestTypes) {
			if ( type.getValue().equalsIgnoreCase(value) ) {
				return type;
			}
		}
		
		Log.d(EventType.class.getSimpleName(), "Does not exist request type = " + value);
		return null;
	}
	
}
