package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import br.com.otgmobile.trackteam.model.Pessoa;
import br.com.otgmobile.trackteam.model.Restricao;

public class PessoaDAO implements BaseColumns{
	
	/**Created by Bruno Ramos Dias
	 * at On the Go mobile
	 * 04/07/2010
	 */

	private static String ID = "id";
	private static String NOME = "nome";
	private static String CPF = "cpf";
	private static String RG = "rg";
	private static String PASSAPORTE = "passaporte";
	private static String DESCRICAO = "descricao";
	private static String RESTRICAO_ID = "restricao_id";
	
	private static String[] FIELDS = {_ID,ID,NOME,CPF,RG,PASSAPORTE,DESCRICAO,RESTRICAO_ID};

	
	private static String DATABASE_TABLE = "pessoa";
	private SQLiteDatabase database;
	private Context context;
	private RestricaoDAO restricaoDAO;

	public PessoaDAO(Context context) {
		database = DBHelper.getDatabase(context);
		this.context = context;
	}
	
	public long save(Pessoa obj) {
		long saved = -1;
		if(obj.getId() == null) return saved;
		
		if (find(obj.getId()) == null) {
			saved = create(obj);
		}else{
			saved = update(obj);
		}
		
		if(saved >0 ) {
			createRestricaoReference(obj.getRestricao());
		}
		
		return saved;
	}
	
	private void createRestricaoReference(Restricao obj) {
		restricaoDAO().save(obj);
		
	}
	
	
	public long create(Pessoa obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}


	public long update(Pessoa obj) {
		ContentValues values = createContentValues(obj);
		return database.update(DATABASE_TABLE, values, ID+ " = ?",new String[] { obj.getId().toString()});
	}
	
	public boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, ID + "=" + id, null) > 0;
	}
	
	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE,null,null) > 0;
	}
	
	private Cursor findCursor(int codigo){
		 return database.query(DATABASE_TABLE, FIELDS, ID+ " = ?", new String[] { codigo + "" }, null, null, null);
	}
	
	private Cursor searchCursor(String parameter){
		String wereclausule = ID + " like " + "'%" + parameter + "%'"
				+ " OR " + NOME + " like " + "'%" + parameter + "%'" + " OR "+
				CPF+" like " + "'%" + parameter + "%'"
				+ " OR " + RG + " like " + "'%" + parameter + "%'" 
				+ " OR " + PASSAPORTE + " like " + "'%" + parameter + "%'" ;
		return database.query(DATABASE_TABLE, FIELDS, wereclausule , null, null, null, null);
	}
	
	private Cursor fecthAllCursor(){
		return database.query(DATABASE_TABLE, FIELDS, null , null, null, null, null);
	}
	
	public Pessoa find(int id) {
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}
	
	public List<Pessoa> search(String parameter) {
		List<Pessoa> pessoas = new ArrayList<Pessoa>();
		Cursor cursor = searchCursor(parameter);
		try{
			cursor.moveToFirst();
			if( cursor.getCount() == 0) {
				while(!cursor.isAfterLast()) {
					pessoas.add(parseObject(cursor));
					cursor.moveToNext();
				}
				
				return pessoas;
			}
			
			return null;
		}finally{
			cursor.close();
		}
	}
	
	public List<Pessoa> fetchAllpParsed() {
		List<Pessoa> pessoas = new ArrayList<Pessoa>();
		Cursor cursor = fecthAllCursor();
		try{
			cursor.moveToFirst();
			if( cursor.getCount() == 0) {
				while(!cursor.isAfterLast()) {
					pessoas.add(parseObject(cursor));
					cursor.moveToNext();
				}
				
				return pessoas;
			}
			
			return null;
		}finally{
			cursor.close();
		}
	}
		
	private Pessoa parseObject(Cursor cursor) {
		Pessoa pessoa = new Pessoa();
		pessoa.set_id(cursor.getInt(0));
		pessoa.setId(cursor.getInt(1));
		pessoa.setNome(cursor.getString(2));
		pessoa.setCpf(cursor.getString(3));
		pessoa.setRg(cursor.getString(4));
		pessoa.setPassaporte(cursor.getString(5));
		pessoa.setDescricao(cursor.getString(6));
		pessoa.setRestricaoId(cursor.getInt(7));
		pessoa.setRestricao(restricaoDAO().find(cursor.getInt(7)));
		return pessoa;
	}

	private ContentValues createContentValues(Pessoa obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(NOME, obj.getNome());
		values.put(CPF, obj.getCpf());
		values.put(RG, obj.getRg());
		values.put(PASSAPORTE, obj.getPassaporte());
		values.put(DESCRICAO, obj.getDescricao());
		values.put(RESTRICAO_ID, obj.getRestricaoId());
		return values;
	}
	
	private RestricaoDAO restricaoDAO() {
		if(restricaoDAO == null) {
			restricaoDAO = new RestricaoDAO(context);
		}
		
		return restricaoDAO;
	}

}
