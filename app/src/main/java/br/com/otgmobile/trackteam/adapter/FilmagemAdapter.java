package br.com.otgmobile.trackteam.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Filmagem;
import br.com.otgmobile.trackteam.row.FilmagemRowHolderInfo;
import br.com.otgmobile.trackteam.util.HandleListButton;

public class FilmagemAdapter extends BaseAdapter{

	List<Filmagem> list;
	Activity context;
	HandleListButton handler;
	
	public FilmagemAdapter(List<Filmagem> list,Activity context){
		this.list = list;
		this.context = context;
	}
	
	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list == null ? 0 :list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		FilmagemRowHolderInfo  rowInfo;
		
		if(view == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.filmagem_row, null);
			
			rowInfo = new FilmagemRowHolderInfo();
			rowInfo.context = context;
			rowInfo.filmagemListImage = (ImageView) row.findViewById(R.id.filmagem_list_tumbnail);
			
			
			row.setTag(rowInfo);
		}else{
			rowInfo = (FilmagemRowHolderInfo) view.getTag();
		}
		Filmagem obj = list.get(position);
		rowInfo.drawRow(obj, context);			
		
		return row;
	}

}
