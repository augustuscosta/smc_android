package br.com.otgmobile.trackteam.maps.overlay;

import br.com.otgmobile.trackteam.model.Ocorrencia;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class OcorrenciaOverlayItem extends OverlayItem {
	
	private Ocorrencia ocorrencia;

	public OcorrenciaOverlayItem(GeoPoint point, String title, String snippet, Ocorrencia ocorrencia) {
		super(point, title, snippet);
		this.ocorrencia = ocorrencia;
	}
	
	public Ocorrencia getOcorrencia(){
		return ocorrencia;
	}

}
