package br.com.otgmobile.trackteam.application;

import android.app.Application;
import android.content.Context;
import br.com.otgmobile.trackteam.util.LogUtil;

public class RootApplication extends Application {

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		LogUtil.initLog(base, null);
	}
	
}
