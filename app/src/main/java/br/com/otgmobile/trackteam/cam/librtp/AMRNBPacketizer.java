package br.com.otgmobile.trackteam.cam.librtp;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.SocketException;

import android.util.Log;

/*
 *   RFC 3267
 *   
 *   AMR Streaming over RTP
 *   
 *   
 *   Must be fed with an InputStream containing raw amr nb
 *   Stream must begin with a 6 bytes long header: "#!AMR\n", it will be skipped
 *   
 */

public class AMRNBPacketizer extends AbstractPacketizer {
	
	static final public String LOG_TAG = "CAM";
	
	private long ts = 0;
	
	private final int amrhl = 6; // Header length
	private final int amrps = 32;   // Packet size
	
	public AMRNBPacketizer(InputStream fis, InetAddress dest, int port) throws SocketException {
		super(fis, dest, port);
	}

	public void run() {
	
		// Skip raw amr header
		fill(rtphl,amrhl);
		
		buffer[rtphl] = (byte) 0xF0;
		rsock.markAllPackets();
		
		while (running) {
			
			fill(rtphl+1,amrps);
			
			// RFC 3267 Page 14: 
			// "For AMR, the sampling frequency is 8 kHz, corresponding to
			// 160 encoded speech samples per frame from each channel."
			rsock.updateTimestamp(ts); ts+=160;
			
			rsock.send(rtphl+amrps+1);
			
		}
		
	}

	
	private int fill(int offset,int length) {
		
		int sum = 0, len;
		
		while (sum<length) {
			try { 
				len = fis.read(buffer, offset+sum, length-sum);
				if (len<0) {
					Log.e(LOG_TAG,"Read error");
				}
				else sum+=len;
			} catch (IOException e) {
				stopStreaming();
				return sum;
			}
		}
		
		return sum;
			
	}
	
	
}
