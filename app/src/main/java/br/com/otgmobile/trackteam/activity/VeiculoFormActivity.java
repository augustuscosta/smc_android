package br.com.otgmobile.trackteam.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.customwidgets.PlacaEditText;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.model.VeiculoEnvolvido;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.Session;

public class VeiculoFormActivity extends GenericActivity {

	private VeiculoEnvolvido obj;
	private PlacaEditText placaText;
	private EditText marcaText;
	private EditText chassiText;
	private EditText modeloText;
	private EditText anoText;
	private Integer listPosition;
	private boolean forEdition;
	private Button socketStatus;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.veiculo_form);
		configureActivity();
		handleIntent();
	}
	
	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		finish();
	};
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};

	private void configureActivity() {
		placaText 	= (PlacaEditText) findViewById(R.id.license_name);
		chassiText = (EditText) findViewById(R.id.chassi_name);
		marcaText 	= (EditText) findViewById(R.id.brand_name);
		modeloText	= (EditText) findViewById(R.id.model_name);
		anoText   	= (EditText) findViewById(R.id.year_name);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}

	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}

	private void handleIntent() {
		Intent intent = getIntent();
		if(intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)){
			Bundle b = getIntent().getExtras();
			obj = (VeiculoEnvolvido) b.getSerializable(ConstUtil.SERIALIZABLE_KEY);
			listPosition = b.getInt(ConstUtil.LIST_POSITION);
			fillDataOnForm();
			forEdition = true;
		}
	}

	private void fillDataOnForm() {
		if(obj == null){
			return;
		}

		if(obj.getPlaca() != null){
			placaText.setText(obj.getPlaca());
		}

		if(obj.getChassi() != null){
			chassiText.setText(obj.getChassi());
		}

		if(obj.getMarca() != null){
			marcaText.setText(obj.getMarca());
		}

		if(obj.getModelo() != null){
			modeloText.setText(obj.getModelo());
		}

		if(obj.getAno()!= null){
			anoText.setText(obj.getAno());
		}

	}

	public void confirmButtonOnClick (View view){
		registerOrEditVeiculo();
	}


	private void registerOrEditVeiculo() {
		if(obj == null){
			obj = new VeiculoEnvolvido();
		}

		setFieldsToObject();

		if(validatePlaca()){
			if(forEdition){
				editAndFinish();
			}
			else{
				createAndFinish();
			}			
		}else{
			AppHelper.getInstance().presentError(this, getString(R.string.error), getString(R.string.invalid_placa));
		}
		
	}

	private void setFieldsToObject() {
		obj.setPlaca(placaText.getText().toString());
		obj.setChassi(chassiText.getText().toString());
		obj.setMarca(marcaText.getText().toString());
		obj.setModelo(modeloText.getText().toString());
		obj.setAno(anoText.getText().toString());
	}

	private void createAndFinish() {
		Intent intent = new Intent();
		Bundle b = new Bundle();
		b.putSerializable(ConstUtil.SERIALIZABLE_KEY, obj);
		intent.putExtras(b);
		setResult(RESULT_OK,intent);
		finish();
	}

	private void editAndFinish() {
		Intent intent = new Intent();
		Bundle b = new Bundle();
		b.putSerializable(ConstUtil.SERIALIZABLE_KEY, obj);
		b.putInt(ConstUtil.LIST_POSITION, listPosition);
		intent.putExtras(b);
		setResult(RESULT_OK,intent);
		finish();
	}
	
	private Boolean validatePlaca(){
		String placa = placaText.getCleanText();
		
		if(placa.length() < 7){
			return false;
		}
		String part1 = placa.substring(0,3);
		String part2 = placa.substring(3, 7);
		
		for(char c : part1.toCharArray()){
			for(char a : ConstUtil.NUMBERS){
				if(c == a){
					return false;
				}
			}
		}
		
		for(char c : part2.toCharArray()){
			for(char a : ConstUtil.CAPITAL_LETTERS){
				if(c == a){
					return false;
				}
			}
		}
		
		return true;
		
	}

	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}

	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}

	protected void setSocketStatus() {
		SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);

		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));

			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		super.onLocationChanged(location);
	}

	@Override
	public void onStatusChanged(int status) {
		super.onStatusChanged(status);
		
	}

}
