package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.Solicitante;

public class SolicitanteDAO {
	
	
	// Database fields
		public static final String _ID = "_id";
		public static final String ID = "id";
		public static final String CPF ="cpf";
		public static final String NOME ="nome";
		public static final String TELEFONE1 ="telefone1";
		public static final String TELEFONE2 ="telefone2";
		public static final String RG ="rg";
		public static final String OCORRENCIA_ID ="ocorrencia_id";
		private static final String[] COLUMNS =  { _ID,ID,CPF,NOME,TELEFONE1,TELEFONE2,RG,OCORRENCIA_ID};

		
		
		private static final String DATABASE_TABLE = "solicitante";
		private SQLiteDatabase database;
		
		public SolicitanteDAO(final Context context){
			database = DBHelper.getDatabase(context);
		}
		
		
		public long save(final Solicitante obj){
			long toReturn = 0;
			if(isNew(obj)){
				toReturn = create(obj);
			}else{
				toReturn = update(obj);				
			}
			return toReturn;
		}
		
		private boolean isNew(final Solicitante obj) {
			if((obj.get_id()) != null){
				return false;
			}
			
			if(findByCPF(obj.getCpf()) != null){
				return false;				
			}
			
			if(find(obj.getId()) != null){
				return false;				
			}
			return true;
		}


		public long create(final Solicitante obj) {
			ContentValues initialValues = createContentValues(obj);
			return database.insert(DATABASE_TABLE, null, initialValues);
		}
		
		public long update(final Solicitante obj) {
			if(obj.get_id() == null) 
			return updateByCPf(obj);
			ContentValues values = createUpdateContentValues(obj);
			return database.update(DATABASE_TABLE, values, _ID+ " = ?",new String[] { obj.get_id().toString()});
		}
		
		private long updateByCPf(final Solicitante obj) {
			if(obj == null || obj.getCpf() == null) return -1;
			ContentValues values = createUpdateContentValues(obj);
			return database.update(DATABASE_TABLE, values, _ID+ " = ?",new String[] { obj.getCpf().toString()});
		}


		public boolean delete(Integer id) {
			return database.delete(DATABASE_TABLE, _ID + "=" + id, null) > 0;
		}
		
		public boolean deleteAll() {
			return database.delete(DATABASE_TABLE,null,null) > 0;
		}
			
		private Cursor findCursor(int codigo){
			return database.query(DATABASE_TABLE, COLUMNS, ID+ " = ?", new String[] { codigo + "" }, null, null, null);
		}
		
		private Cursor findbyCpfCursor(String cpf){
			return database.query(DATABASE_TABLE, COLUMNS, CPF+ " = ?", new String[] { cpf + "" }, null, null, null);
		}
		
		private Cursor findbyInternalCursor(int codigo){
			return database.query(DATABASE_TABLE, COLUMNS, _ID+ " = ?", new String[] { codigo + "" }, null, null, null);
		}
		
		private Cursor findOcorrenciaCursor(int codigo){
			return database.query(DATABASE_TABLE, COLUMNS, OCORRENCIA_ID+ " = ?", new String[] { codigo + "" }, null, null, null);
		}
		
		public Solicitante find(int id){
			Cursor cursor = findCursor(id);
			try{
				if(cursor.moveToFirst()){
					return parseObject(cursor);				
				}
				return null;
			}finally{
				cursor.close();				
			}
		}
		
		public Solicitante findByCPF(String cpf){
			Cursor cursor = findbyCpfCursor(cpf);
			try{
				if(cursor.moveToFirst()){
					return parseObject(cursor);				
				}
				return null;
			}finally{
				cursor.close();
			}
		}
		
		public Solicitante findInternal(int id){
			Cursor cursor = findbyInternalCursor(id);
			try{
				if(cursor.moveToFirst()){
					return parseObject(cursor);				
				}
				return null;
			}finally{
				cursor.close();
			}
		}
		
		
		public Cursor fetchAll() {
			return database.query(DATABASE_TABLE, COLUMNS, null, null,
					null, null, null);
		}
		
		public List<Solicitante> fetchAllParsed(){
			Cursor cursor = fetchAll();
			if(cursor == null){
				return null;
			}
			List<Solicitante> toReturn = parse(cursor);
			cursor.close();
			return toReturn;
		}
		
		public List<Solicitante> fetchAllFromOcorrenciaParsed(int id){
			Cursor cursor = findOcorrenciaCursor(id);
			if(cursor == null){
				return null;
			}
			List<Solicitante> toReturn = parse(cursor);
			cursor.close();
			return toReturn;
		}
		
		private Integer returnNullValue(Cursor cursor){
			int index = cursor.getColumnIndexOrThrow(ID);
			Integer x = null;
			if (!cursor.isNull(index)){
				x = cursor.getInt(index);
			}
		return x;
		}
		
		private List<Solicitante> parse(Cursor cursor){
			List<Solicitante>  toReturn= null;
			if(cursor != null){
				toReturn = new ArrayList<Solicitante>();
				Solicitante obj;
				if(cursor.moveToFirst()){
					while(!cursor.isAfterLast()){
						obj = new Solicitante();
						obj.set_id(cursor.getInt(0));
						obj.setId(returnNullValue(cursor));
						obj.setCpf(cursor.getString(2));
						obj.setNome(cursor.getString(3));
						obj.setTelefone1(cursor.getString(4));
						obj.setTelefone2(cursor.getString(5));
						obj.setRg(cursor.getString(6));
						obj.setOcorrenciaId(cursor.getInt(7));
						toReturn.add(obj);
						cursor.moveToNext();
					}
				}
			}
			
			
			return toReturn; 
		}
		
		private Solicitante parseObject(Cursor cursor){
			if(cursor == null){
				return null;
			}
			Solicitante obj = null;
			if(cursor.moveToFirst()){
				obj = new Solicitante();
				obj.set_id(cursor.getInt(0));
				obj.setId(returnNullValue(cursor));
				obj.setCpf(cursor.getString(2));
				obj.setNome(cursor.getString(3));
				obj.setTelefone1(cursor.getString(4));
				obj.setTelefone2(cursor.getString(5));
				obj.setRg(cursor.getString(6));
				obj.setOcorrenciaId(cursor.getInt(7));
			}
			
			return obj;
		}
		
		private ContentValues createContentValues(Solicitante obj) {
			ContentValues values = new ContentValues();
			values.put(ID, obj.getId());
			values.put(CPF, obj.getCpf());
			values.put(NOME, obj.getNome());
			values.put(TELEFONE1, obj.getTelefone1());
			values.put(TELEFONE2, obj.getTelefone2());
			values.put(RG, obj.getRg());
			values.put(OCORRENCIA_ID, obj.getOcorrenciaId());
			return values;
		}
		
		private ContentValues createUpdateContentValues(Solicitante obj) {
			ContentValues values = new ContentValues();
			values.put(ID, obj.getId());
			values.put(CPF, obj.getCpf());
			values.put(NOME, obj.getNome());
			values.put(TELEFONE1, obj.getTelefone1());
			values.put(TELEFONE2, obj.getTelefone2());
			values.put(RG, obj.getRg());
			values.put(OCORRENCIA_ID, obj.getOcorrenciaId());
			return values;
		}

}
