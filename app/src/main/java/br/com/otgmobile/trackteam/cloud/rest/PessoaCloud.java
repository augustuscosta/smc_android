package br.com.otgmobile.trackteam.cloud.rest;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import br.com.otgmobile.trackteam.model.Pessoa;
import br.com.otgmobile.trackteam.util.Session;

import com.google.gson.Gson;

public class PessoaCloud extends RestClient {
	
	private final String URL = "restricoes/consulta/pessoa";

	private final String ROOT_OBJECT = "result";
	
	public List<Pessoa> list(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return getListFromResponse();
	}
	
	public List<Pessoa> search(Context context, String searchParam) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		addParam("query", searchParam);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return getListFromResponse();
	}

	private List<Pessoa> getListFromResponse() throws JSONException {
			JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
			if (jsonArray != null) {
				Pessoa obj;
				Gson gson = new Gson();
				List<Pessoa> toReturn = new ArrayList<Pessoa>();
				for (int i = 0; i < jsonArray.length(); i++) {
					
					obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),Pessoa.class);
					toReturn.add(obj);
				}
				
				return toReturn;
			}
			
			return null;
		}

}
