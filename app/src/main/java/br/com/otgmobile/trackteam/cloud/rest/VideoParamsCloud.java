package br.com.otgmobile.trackteam.cloud.rest;

import android.content.Context;
import br.com.otgmobile.trackteam.util.Session;

public class VideoParamsCloud extends RestClient {

	private final String URL = "comunicacao/video/params";

	public void sendParams(String params,Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("params", params);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.POST);
	}
}
