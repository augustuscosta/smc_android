package br.com.otgmobile.trackteam.daemon;

import br.com.otgmobile.trackteam.cloud.rest.OcorrenciasCloud;
import android.content.Context;

public class DaemonOcorrenciaSender extends DaemonSender {

	private OcorrenciasCloud cloud;
	private Context context;

	public DaemonOcorrenciaSender(Context context) {
		this.cloud = new OcorrenciasCloud();
		this.context = context;
	}

	@Override
	protected void process() {
		cloud.sendQueue(context);
	}
}
