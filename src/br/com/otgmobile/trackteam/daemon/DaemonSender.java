package br.com.otgmobile.trackteam.daemon;

public abstract class DaemonSender implements Runnable {

	public boolean isRunning;
	
	{
		isRunning = true;
	}

	protected abstract void process();

	@Override
	public void run() {
		if (!isRunning) {
			return;
		}
		
		process();
	}
	
	public void stop() {
		isRunning = false;
	}
	
}
