package br.com.otgmobile.trackteam.util;

import java.net.URISyntaxException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public final class NavigationUtil {

	private static final String WAZE_NAVIGATION_INTENT_DEFAULT_LOCATION = "waze://?ll=0,0&z=10";
	private static final String WAZE_NAVIGATION_INTENT = "waze://?ll=";
	private static final String GOOGLE_NAVIGATION_INTENT_DEFAULT_LOCATION = "google.navigation:q=0,0";
	public static final String GOOGLE_NAVIGATION_INTENT = "google.navigation";

	public static void openNavigationFromStopMap(final Activity activity, final Float latitude, final Float longitude) {
		if ( wazeNavigationIntentIsAvailable(activity) ) {
			openWazeNavigationFromStopMap(activity, latitude, longitude);
			
		} else if ( googleNavigationIntentIsAvailable(activity) ) {
			openGoogleNavigationFromStopMap(activity, latitude, longitude);
		}
	}
	
	public static void openWazeNavigationFromStopMap(final Activity activity, final Float latitude, final Float longitude) {
		StringBuilder params = new StringBuilder(WAZE_NAVIGATION_INTENT);
		if ( latitude != null && longitude != null ) {
			params.append(latitude.floatValue());
			params.append(",");
			params.append(longitude.floatValue());
			params.append("&z=10&navigate=YES");
		}
		
		Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(params.toString()));
		if (!IntentUtil.isIntentAvailable(activity, i)) {
			return;
		}
		
		activity.startActivityForResult(i, ConstUtil.STOP_NAVIGATION);
	}
	
	public static void openGoogleNavigationFromStopMap(final Activity activity, final Float latitude, final Float longitude) {
		StringBuilder params = new StringBuilder();
		if (latitude != null && longitude != null) {
			params.append(":q=");
			params.append(latitude.floatValue());
			params.append(",");
			params.append(longitude.floatValue());
		}

		Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_NAVIGATION_INTENT + params.toString()));
		if (!IntentUtil.isIntentAvailable(activity, i)) {
			return;
		}
		
		activity.startActivityForResult(i, ConstUtil.STOP_NAVIGATION);
	}
	
	public static boolean googleNavigationIntentIsAvailable(final Context context) {
		if (context == null) {
			return false;
		}
		
		try {
			return IntentUtil.isIntentAvailable(context, Intent.parseUri(GOOGLE_NAVIGATION_INTENT_DEFAULT_LOCATION, 0));
		} catch (URISyntaxException e) {
			return false;
		}
	}
	
	public static boolean wazeNavigationIntentIsAvailable(final Context context) {
		if (context == null) {
			return false;
		}
		
		try {
			return IntentUtil.isIntentAvailable(context, Intent.parseUri(WAZE_NAVIGATION_INTENT_DEFAULT_LOCATION, 0));
		} catch (URISyntaxException e) {
			return false;
		}
	}
	
}
