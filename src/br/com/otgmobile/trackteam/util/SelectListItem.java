package br.com.otgmobile.trackteam.util;


import android.view.View;

public class SelectListItem implements 
android.view.View.OnClickListener {
	int position;
	HandleListButton handler;
	View v;
	
	public SelectListItem(int position,HandleListButton handler,View v){
		this.position = position;
		this.handler = handler;
		this.v = v;
	}
	

	@Override
	public void onClick(View v) {
		handler.positionSelect(position);
		
	}


}


