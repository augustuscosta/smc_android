package br.com.otgmobile.trackteam.util;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;

public class FileUtil {

	public byte[] convertBitmapToArray(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		return byteArray;
	}

	public void writeBitmapToFile(byte[] byteArray, String fotoTag,Context context)
			throws FileNotFoundException, IOException {
		FileOutputStream fos = context.openFileOutput(fotoTag,
				Context.MODE_PRIVATE);
		fos.write(byteArray);
		fos.close();
	}
}
