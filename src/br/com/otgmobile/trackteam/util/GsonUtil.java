package br.com.otgmobile.trackteam.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.google.gson.Gson;

public class GsonUtil {

	private static Gson gson;

	static {
		gson = new Gson();
	}

	public static <T> List<T> fromGson(final String rootObjectName, Class<T> clazz, final JSONObject... jsonObjects) {
		if (jsonObjects == null || jsonObjects.length == 0)
			return Collections.emptyList();

		final JSONArray jsonArray;
		List<T> toReturn = new ArrayList<T>();
		
		try {
			jsonArray = jsonObjects[0].getJSONArray(rootObjectName);

			if (jsonArray == null)
				return null;

			for (int i = 0; i < jsonArray.length(); i++) {
				toReturn.add(gson.fromJson(jsonArray.getJSONObject(i).toString(), clazz));
			}
		} catch (JSONException e) {
			LogUtil.e("Erro convertendo lista json", e);
			return null;
		}

		return toReturn;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object fromGson(final String rootObjectName,
			final Class clazz, final JSONObject jsonObject) {
		try {
			if (!jsonObject.has(rootObjectName)) {
				Log.w("GSON-Util", "Does not exist object name = "
						+ rootObjectName);
				return null;
			}

			return gson.fromJson(jsonObject.getJSONObject(rootObjectName)
					.toString(), clazz);
		} catch (Throwable e) {
			Log.e("GSON-Util", "Erro when trying convert(" + rootObjectName
					+ ") to - " + clazz.getSimpleName());
		}

		return null;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static Object fromGson(final Class clazz, final String jsonObject) {
		try {
			return gson.fromJson(jsonObject, clazz);
		} catch (Throwable e) {
			Log.e("GSON-Util", "Erro when trying convert to - " + clazz.getSimpleName());
		}

		return null;
	}

}
