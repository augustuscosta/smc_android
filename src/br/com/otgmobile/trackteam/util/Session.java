package br.com.otgmobile.trackteam.util;

import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import br.com.otgmobile.trackteam.model.ConfiguracaoVisualizaOcorrencia;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.model.ZoomType;

public class Session {

	public static final String PREFS = "SESSION_PREFS_SMC";
	public static final String SERVER_PREFS_KEY = "SERVER_PREFS_KEY_SMC";
	public static final String SOCKET_SERVER_PREFS_KEY = "SOCKET_SERVER_PREFS_KEY_SMC";
	public static final String TOKEN_PREFS_KEY = "REST_TOKEN_PREFS_KEY_SMC";
	public static final String LAST_SYNC = "LAST_SYNC_SMC";
	public static final String VEICULO_ID = "VEICULO_ID_SMC";
	public static final String AGENTE_ID = "AGENTE_ID_SMC";
	public static final String MAP_ZOOM_TYPE = "MAP_ZOOM_TYPE_SMC";
	public static final String SOCKET_CONECTION_STATUS = "SOCKET_CONNECTION_STATUS_SMC";
	public static final String LAST_FIX_GPS_TIME = "LAST_FIX_GPS_TIME_SMC";
	public static final String CONFIGURACAO_VISUALIZA_OCORRENCIA = "CONFIGURACAO_VISUALIZA_OCORRENCIA_SMC";

	private static SharedPreferences settings;

	private static SharedPreferences getSharedPreferencesInstance(
			Context context) {
		if (settings == null) {
			settings = context
					.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
		}
		return settings;
	}

	public static String getDeviceId(Context context) {
		TelephonyManager TelephonyMgr = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		String szImei = TelephonyMgr.getDeviceId();
		return szImei;
	}

	public static void setServer(String server, Context context) {
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(SERVER_PREFS_KEY, server);
		editor.commit();
	}

	public static String getServer(Context context) {
		return getSharedPreferencesInstance(context).getString(
				SERVER_PREFS_KEY,
				"http://200.17.34.29:8080/smc");
	}

	public static String getToken(Context context) {
		return getSharedPreferencesInstance(context).getString(TOKEN_PREFS_KEY,
				null);
	}

	public static void setToken(String token, Context context) {
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(TOKEN_PREFS_KEY, token);
		editor.commit();
	}

	public static void setSocketServer(String server, Context context) {
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(SOCKET_SERVER_PREFS_KEY, server);
		editor.commit();
	}

	public static String getSocketServer(Context context) {
		return getSharedPreferencesInstance(context).getString(
				SOCKET_SERVER_PREFS_KEY, "http://200.17.34.29:9090");
	}

	public static void setLastSync(Date date, Context context) {
		Long value = 0L;
		if (date != null)
			value = date.getTime();
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(LAST_SYNC, value);
		editor.commit();
	}

	public static Date getLastSync(Context context) {
		Date toReturn = null;
		Long value = getSharedPreferencesInstance(context)
				.getLong(LAST_SYNC, 0);
		if (value != 0)
			toReturn = new Date(value);
		return toReturn;
	}

	public static void setAgenteID(Integer agenteID, Context context) {
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(AGENTE_ID, agenteID);
		editor.commit();
	}

	public static Integer getAgenteID(Context context) {
		Integer toReturn = getSharedPreferencesInstance(context).getInt(
				AGENTE_ID, -1);
		if (toReturn == -1)
			return null;
		return toReturn;
	}

	public static void setVeiculoID(Integer veiculoID, Context context) {
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(VEICULO_ID, veiculoID);
		editor.commit();
	}

	public static Integer getVeiculoID(Context context) {
		Integer toReturn = getSharedPreferencesInstance(context).getInt(
				VEICULO_ID, -1);
		if (toReturn == -1)
			return null;
		return toReturn;
	}

	public static void setZoomType(final Context context, final int type) {
		SharedPreferences.Editor editor = getSharedPreferencesInstance(context)
				.edit();
		editor.putInt(MAP_ZOOM_TYPE, type);
		editor.commit();
	}

	public static ZoomType getZoomType(Context context) {
		final SharedPreferences sharedPref = getSharedPreferencesInstance(context);
		final int config = sharedPref.getInt(MAP_ZOOM_TYPE, 0);

		return ZoomType.fromValue(config);
	}

	public static void setSocketConectionStatus(Context context,
			final int status) {
		final SharedPreferences.Editor editor = getSharedPreferencesInstance(
				context).edit();
		editor.putInt(SOCKET_CONECTION_STATUS, status);
		editor.commit();
	}

	public static SocketConectionStatus getSocketConectionStatus(Context context) {
		final SharedPreferences sharedPref = getSharedPreferencesInstance(context);
		final int status = sharedPref.getInt(SOCKET_CONECTION_STATUS, 0);

		return SocketConectionStatus.fromValue(status);
	}

	public static void setLastFixTime(Long gpsFixTime, Context context) {
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putLong(LAST_FIX_GPS_TIME, gpsFixTime);
		editor.commit();
	}

	public static long getLastFixTime(Context context) {
		Long value = getSharedPreferencesInstance(context).getLong(
				LAST_FIX_GPS_TIME, 0);
		return value;
	}

	public static void setConfiguracaoVisualizaOcorrencia(
			ConfiguracaoVisualizaOcorrencia configuracao, Context context) {
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(CONFIGURACAO_VISUALIZA_OCORRENCIA,
				configuracao.getValue());
		editor.commit();
	}

	public static ConfiguracaoVisualizaOcorrencia getConfiguracaoVisualizaOcorrencia(
			Context context) {
		int value = getSharedPreferencesInstance(context).getInt(
				CONFIGURACAO_VISUALIZA_OCORRENCIA, 1);
		return ConfiguracaoVisualizaOcorrencia.fromValue(value);
	}

}
