package br.com.otgmobile.trackteam.util;


public enum BroadCastReceiverDictionary {

	NOVA_OCORRENCIA("newOccurrence"),
	MODIFICA_OCORRENCIA("updateOccurrence"),
	EXCLUI_OCORRENCIA("deleteOccurrence"),
	
	NOVO_VEICULO("newVehicle"),
	MODIFICA_VEICULO("updateVehicle"),
	EXCLUI_VEICULO("deleteVehicle"),
	
	NOVO_AGENTE("newAgent"),
	MODIFICA_AGENTE("updateAgent"),
	MODIFICA_HISTORICO("updateHistory"), 
	ABRIR_CAMERA("openCam"),
	FECHAR_CAMERA("closeCam"),
	RECEBER_MENSAGEM("chat/mensagem/receber"),
	EXIBIR_CONEXAO("statusConexao"),
	EXIBIR_CONEXAO_TELEMETRIA("statusConexaoTelemetria")
	
	;
	
	private String value;

	private BroadCastReceiverDictionary(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}
	
}
