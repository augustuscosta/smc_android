package br.com.otgmobile.trackteam.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.activity.OcorrenciaList;
import br.com.otgmobile.trackteam.model.Ocorrencia;

public class NotificationUtil {
	
	public static final int NOTIFICATION_ID = 1;

	
	public static void createNewOcurenceNotification( Ocorrencia ocorrencia, Context context){
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		CharSequence contentTitle = context.getString(R.string.app_name);
		CharSequence contentText = getNewOcurrenceDescription(ocorrencia, context);
		Notification notification = new Notification(R.drawable.icone, contentText, System.currentTimeMillis());
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, getNewOcurrenceIntent(ocorrencia, context), 0);
		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
		long[] vibrate = {0,100,200,300};
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.vibrate = vibrate;
		notification.defaults |= Notification.DEFAULT_LIGHTS;
		notification.ledARGB = 0xff00ff00;
		notification.ledOnMS = 300;
		notification.ledOffMS = 1000;
		notification.flags |= Notification.FLAG_SHOW_LIGHTS;
		mNotificationManager.notify(NOTIFICATION_ID, notification);
	}
	
	private static Intent getNewOcurrenceIntent(Ocorrencia ocorrencia, Context context){
		Intent intent = new Intent(context, OcorrenciaList.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		return intent;
	}
	
	private static String getNewOcurrenceDescription(Ocorrencia ocorrencia, Context context){
		return context.getString(R.string.new_occurrence) + ": " + ocorrencia.getDescricao();
	}
	
	public static void removeNotification(Context context){
		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(NOTIFICATION_ID);
	}
	

}
