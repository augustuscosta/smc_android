package br.com.otgmobile.trackteam.cloud.rest;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import br.com.otgmobile.trackteam.database.AgenteDAO;
import br.com.otgmobile.trackteam.model.Agente;
import br.com.otgmobile.trackteam.util.Session;

import com.google.gson.Gson;

public class AgentesCloud extends RestClient {

	private final String URL = "comunicacao/agente";

	private final String ROOT_OBJECT = "result";

	public List<Agente> list(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return getListFromResponse();
	}

	public Boolean sync(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return storeResponse(context);
	}

	private Boolean storeResponse(Context context) throws Exception {
		AgenteDAO dao = new AgenteDAO(context);
		try {
			JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
			if (jsonArray != null) {
				Agente obj;
				Gson gson = new Gson();
				dao.deleteAll();
				for (int i = 0; i < jsonArray.length(); i++) {
					obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),
							Agente.class);
					dao.save(obj);
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
		}
		return true;
	}

	private List<Agente> getListFromResponse() throws JSONException {
		JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
		if (jsonArray != null) {
			Agente obj;
			Gson gson = new Gson();
			List<Agente> toReturn = new ArrayList<Agente>();
			for (int i = 0; i < jsonArray.length(); i++) {

				obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),
						Agente.class);
				toReturn.add(obj);
			}
			return toReturn;
		}
		return null;

	}
}
