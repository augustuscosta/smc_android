package br.com.otgmobile.trackteam.cloud.rest;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;

import android.content.Context;
import br.com.otgmobile.trackteam.database.SignDAO;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.Sign;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

public class SignCloud extends RestClient {
	
	private final static String UPLOAD_URL = "comunicacao/ocorrencia/assinaturas";
	private SignDAO signDAO;
	
	
	public void uploadSign(Context context, Sign sign) throws Throwable {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		setUrl(addSlashIfNeeded(url) + UPLOAD_URL);
		execute(context, sign, token);
	}

	private void execute(Context context, Sign sign, String token) throws Throwable {
		HttpPost request = new HttpPost(url);
		MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		entity.addPart(ConstUtil.OCORRENCIATAG, new StringBody((sign.getOcorrenciaServerId().toString())));
		entity.addPart(ConstUtil.TOKEN, new StringBody((token)));
		entity.addPart(ConstUtil.SIGN, new ByteArrayBody(encodeSignTobyteArray(sign,context), ConstUtil.SIGN));
		request.setEntity(entity);
		executeRequest(request, url);
	}

	private byte[] encodeSignTobyteArray(Sign sign, Context context) throws Throwable {
		InputStream is = context.openFileInput(sign.getImagem());
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try{
			byte[] b = new byte[is.available()];
			int bytesRead = 0;
			while ((bytesRead = is.read(b)) != -1) {
				bos.write(b, 0, bytesRead);
			}
			return bos.toByteArray();			
		}finally{
			bos.close();
			is.close();
		}
	}
	
	public void uploadAllSignsFromOcorrencia(Context context, Ocorrencia ocorrencia) throws Throwable{
		if(ocorrencia.getId() == null){
			LogUtil.i("Não foi possivel fazer o upload das assinaturas ocorrencia ainda não sincronizada");
			return;
		}
		
		List<Sign> signs = signDAO(context).findFromOcorrenciaNotSent(ocorrencia.get_id());
		
		if(signs == null || signs.isEmpty()){
			LogUtil.i("sem assinaturas para fazer o upload");
			return;
		}
		
		for (Sign sign : signs) {
				if(sign.getOcorrenciaServerId() == null || sign.getOcorrenciaServerId() == 0 ){
				LogUtil.i("fazendo o upload da Assinatura "+sign.get_id().toString()+" do atendimento: "+ocorrencia.getId());
				sign.setOcorrenciaServerId(ocorrencia.getId());
				uploadSign(context, sign);
				}
			if ( HttpStatus.SC_OK == getResponseCode() ) {
				LogUtil.i("upload com sucesso da Assinatura "+sign.get_id().toString()+" do atendimento: "+ocorrencia.getId());
				sign.setEnviado(AppHelper.getCurrentTime());
				signDAO(context).save(sign);
			}
		}
	}
	
	private SignDAO signDAO(Context context){
		if(signDAO == null){
			signDAO = new SignDAO(context);
		}
		return signDAO;
	}

}
