package br.com.otgmobile.trackteam.cloud.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import br.com.otgmobile.trackteam.database.HistoricoDAO;
import br.com.otgmobile.trackteam.model.Historico;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

import com.google.gson.Gson;


public class HistoricosCloud extends RestClient {

	private final String URL = "comunicacao/historicos";
	private HistoricoDAO dao;
	private final String ROOT_OBJECT = "result";

	public synchronized void create(Historico historico,Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		Gson gson = new Gson();
		String historicoJson = gson.toJson(historico);
		addParam("historico", historicoJson);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.POST);
	}
	
	public synchronized void createList(List<Historico> historicos,Context context) throws Exception {
		LogUtil.i("---------------enviando lista de "+ historicos.size()+" historicos para o servidor--------------" );
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		Gson gson = new Gson();
		String historicoJson = gson.toJson(historicos);
		addParam("historico", historicoJson);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.POST);
	}
	
	public List<Historico> list(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return getListFromResponse();
	}
	
	private List<Historico> getListFromResponse() throws JSONException {
		JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
		if (jsonArray != null) {
			Historico obj;
			Gson gson = new Gson();
			List<Historico> toReturn = new ArrayList<Historico>();
			for (int i = 0; i < jsonArray.length(); i++) {
				
				obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),Historico.class);
				toReturn.add(obj);
			}
			return toReturn;
		}
		return null;

	}
	
	public Boolean sync(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return storeResponse(context);
	}
	
	private Boolean storeResponse(Context context) throws Exception {
		try {
			JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
			if (jsonArray != null) {
				Historico obj;
				Gson gson = new Gson();
				historicoDAO(context).deleteAll();
				for (int i = 0; i < jsonArray.length(); i++) {
					obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),Historico.class);
					obj.setEnviado(true);
					if(obj.getVeiculoID() == null || obj.getVeiculoID() == 0){
						obj.setVeiculoID(Integer.parseInt(Session.getToken(context)));
					}
					historicoDAO(context).save(obj);
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
		}
		return true;
	}
	
	public synchronized List<Integer> create(final Context context, final List<Historico> historicos) {
		if ( historicos == null ) return Collections.emptyList(); 

		List<Integer> sentIds = new ArrayList<Integer>(); // Armazenar os ids enviados.
		
		for (Historico historico : historicos) {
			try {
				create(historico, context);
				if ( HttpStatus.SC_OK == getResponseCode() ) {
					sentIds.add(historico.get_id());
				}
			} catch (Throwable e) {
				LogUtil.e("Erro ao tentar enviar historico pra cloud...", e);
			}
		}
		
		return sentIds;
	}
	
	public synchronized List<Integer> createList(final Context context, final List<Historico> historicos) {
		if ( historicos == null ) return Collections.emptyList(); 
		
		List<Integer> sentIds = new ArrayList<Integer>(); // Armazenar os ids enviados.
		try {
			createList(historicos, context);
			if ( HttpStatus.SC_OK == getResponseCode() ) {
				for (Historico historico : historicos) {
					sentIds.add(historico.get_id());
				}
				LogUtil.i("----------------------Enviados "+historicos.size()+"historicos pra cloud--------------------");				
			}
		} catch (Throwable e) {
			LogUtil.e("Erro ao tentar enviar historicos pra cloud...", e);
		}
		
		return sentIds;
	}
	
	private HistoricoDAO historicoDAO(Context context) {
		if ( dao == null ) {
			dao = new HistoricoDAO(context);
		}
		
		return dao;
	}
	
	public synchronized void sendQueue(Context context) {
		List<Historico> historicosPendings = historicoDAO(context).retrieveAllPending();
		try{
			if ( historicosPendings.isEmpty()){
				LogUtil.d("---------------------------Nao existe historico pendente...--------------------");
				return;
			}
			
			List<Integer> sentIds = createList(context, historicosPendings);
			historicoDAO(context).delete(sentIds, true);
		} catch (Throwable e) {
			LogUtil.e("--------------Erro ao tentar processar "+historicosPendings.size()+" historicos pendentes-------------", e);
		} 
	}
	
}
