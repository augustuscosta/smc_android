package br.com.otgmobile.trackteam.cloud.rest;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import br.com.otgmobile.trackteam.database.StatusDAO;
import br.com.otgmobile.trackteam.model.Estado;
import br.com.otgmobile.trackteam.util.Session;

import com.google.gson.Gson;

public class StatusCloud extends RestClient {
	
	private final String URL = "comunicacao/ocorrencia/status";

	private final String ROOT_OBJECT = "result";
	
	public List<Estado> list(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return getListFromResponse();
	}
	
	public Boolean sync(Context context) throws Exception {
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		addParam("token", token);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.GET);
		return storeResponse(context);
	}
	
	private Boolean storeResponse(Context context) throws Exception {
		StatusDAO dao = new StatusDAO(context);
		try {
			JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
			if (jsonArray != null) {
				Estado obj;
				Gson gson = new Gson();
				dao.deleteAll();
				for (int i = 0; i < jsonArray.length(); i++) {
					obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),Estado.class);
					dao.save(obj);
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
		}
		return true;
	}
	
	private List<Estado> getListFromResponse() throws JSONException {
		JSONArray jsonArray = getJsonObjectArrayFromResponse(ROOT_OBJECT);
		if (jsonArray != null) {
			Estado obj;
			Gson gson = new Gson();
			List<Estado> toReturn = new ArrayList<Estado>();
			for (int i = 0; i < jsonArray.length(); i++) {
				
				obj = gson.fromJson(jsonArray.getJSONObject(i).toString(),Estado.class);
				toReturn.add(obj);
			}
			return toReturn;
		}
		return null;

	}

}
