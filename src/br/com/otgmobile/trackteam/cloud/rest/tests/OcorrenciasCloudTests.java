package br.com.otgmobile.trackteam.cloud.rest.tests;

import java.util.Date;
import java.util.List;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import br.com.otgmobile.trackteam.cloud.rest.OcorrenciasCloud;
import br.com.otgmobile.trackteam.model.Endereco;
import br.com.otgmobile.trackteam.model.NivelEmergencia;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.util.Session;

public class OcorrenciasCloudTests extends AndroidTestCase {

	@Override
	protected void setUp() throws Exception {
		Session.setToken(TestCont.TOKEN, getContext());
		Session.setServer(TestCont.SERVER,getContext());
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	public void testListOcorrencias() throws Exception{
		OcorrenciasCloud cloud = new OcorrenciasCloud();
		List<Ocorrencia> response = cloud.list(getContext());
		Assert.assertNotNull(response);
		Assert.assertTrue(response.size() > 0);
	}
	
	public void testFindOcorrencia() throws Exception{
		OcorrenciasCloud cloud = new OcorrenciasCloud();
		Ocorrencia response = cloud.find(1821,getContext());
		Assert.assertNotNull(response);
		Assert.assertTrue(response.getId() == 1821);
	}
	
	public void testUpdateOcorrencia() throws Exception{
		OcorrenciasCloud cloud = new OcorrenciasCloud();
		Ocorrencia response = cloud.find(1821,getContext());
		response.setResumo("update from android test");
		NivelEmergencia nivel = new NivelEmergencia();
		nivel.setId(1);
		nivel.setCor("#FFDDDD");
		nivel.setDescricao("Normal");
		response.setNivelEmergencia(nivel);
		Assert.assertNotNull(response);
		Assert.assertTrue(response.getId() == 1821);
		cloud.merge(response, getContext());
		Assert.assertNotNull(cloud.getResponseCode());
		Assert.assertTrue(cloud.getResponseCode() == 200);
	}
	
	public void testCreateOcorrencia() throws Exception{
		OcorrenciasCloud cloud = new OcorrenciasCloud();
		
		Ocorrencia obj = new Ocorrencia();
		obj.setDescricao("Ocorrencia teste");
		obj.setHoraChegada(new Date().getTime());
		obj.setHoraSaida(new Date().getTime());
		obj.setResumo("Teste");
		
		
		Endereco endereco = new Endereco();
		endereco.setBairro("bairro");
		endereco.setEndGeoref("endereco GeoRef");
		endereco.setLatitude(0.F);
		endereco.setLongitude(0.F);
		endereco.setLogradouro("logradouro");
		endereco.setNumero("120");
		
		obj.setEndereco(endereco);
		
		NivelEmergencia nivel = new NivelEmergencia();
		nivel.setId(1);
		nivel.setCor("#FFDDDD");
		nivel.setDescricao("Normal");
		
		obj.setEstado(7);
		obj.setNatureza(11);
		
		obj.setNivelEmergencia(nivel);
		
		obj = cloud.merge(obj, getContext());
		
		Assert.assertNotNull(obj);
		Assert.assertNotNull(obj.getId());
		Assert.assertTrue(cloud.getResponseCode() == 200);
	}
	
	public void testSync() throws Exception{
		OcorrenciasCloud cloud = new OcorrenciasCloud();
		Assert.assertTrue(cloud.sync(getContext()));
	}
}
