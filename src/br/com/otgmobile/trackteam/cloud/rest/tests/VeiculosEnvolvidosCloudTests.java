package br.com.otgmobile.trackteam.cloud.rest.tests;

import java.util.List;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import br.com.otgmobile.trackteam.cloud.rest.VeiculosEnvolvidosCloud;
import br.com.otgmobile.trackteam.model.VeiculoEnvolvido;
import br.com.otgmobile.trackteam.util.Session;

public class VeiculosEnvolvidosCloudTests extends AndroidTestCase {

	@Override
	protected void setUp() throws Exception {
		Session.setToken(TestCont.TOKEN, getContext());
		Session.setServer(TestCont.SERVER,getContext());
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testSync() throws Exception{
		VeiculosEnvolvidosCloud cloud = new VeiculosEnvolvidosCloud();
		Assert.assertTrue(cloud.sync(getContext()));
	}
	
	public void testListVeiculosEnvolvidos() throws Exception{
		VeiculosEnvolvidosCloud cloud = new VeiculosEnvolvidosCloud();
		List<VeiculoEnvolvido> response = cloud.list(getContext());
		Assert.assertNotNull(response);
		Assert.assertTrue(response.size() > 0);
	}
	
}
