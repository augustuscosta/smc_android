package br.com.otgmobile.trackteam.cloud.rest.tests;

import junit.framework.Assert;

import org.apache.http.HttpStatus;

import android.test.AndroidTestCase;
import br.com.otgmobile.trackteam.cloud.rest.FilmagensCloud;
import br.com.otgmobile.trackteam.model.Filmagem;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.util.Session;

public class FilmagensCloudTest extends AndroidTestCase {
	
	private FilmagensCloud filmagensCloud;

	@Override
	protected void setUp() throws Exception {
		Session.setToken(TestCont.TOKEN, getContext());
		Session.setServer(TestCont.SERVER,getContext());
		super.setUp();
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	protected void testSendFilmagem() throws Throwable{
		Filmagem filmagem = new Filmagem();
		Ocorrencia ocorrencia = new Ocorrencia();
		ocorrencia.setId(1);
		filmagem.setFilmagem("");
		filmagem.setOcorrenciaId(1);
		filmagem.setOcorrenciaServerId(1);
		filmagensCloud().uploadFilmagem(getContext(), filmagem);
		Assert.assertTrue(HttpStatus.SC_OK == filmagensCloud().getResponseCode());
	}
	
	private FilmagensCloud filmagensCloud() {
		if(filmagensCloud == null){
			filmagensCloud = new FilmagensCloud();
		}
		return filmagensCloud;
	}
	
}
