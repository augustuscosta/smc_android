package br.com.otgmobile.trackteam.cloud.rest.tests;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;

import junit.framework.Assert;

import org.apache.http.HttpStatus;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.test.AndroidTestCase;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.cloud.rest.FotosCloud;
import br.com.otgmobile.trackteam.model.Foto;
import br.com.otgmobile.trackteam.util.Session;

public class FotosCloudTest extends AndroidTestCase {

	private static final String TESTPIC = "testpic";
	private FotosCloud fotosCloud;

	@Override
	protected void setUp() throws Exception {
		Session.setToken(TestCont.TOKEN, getContext());
		Session.setServer(TestCont.SERVER,getContext());
		super.setUp();
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	protected void testSendFoto() throws Throwable {
		Foto foto = new Foto();
		foto.setOcorrenciaServerId(1);
		foto.setImagem(getImageToTest());
		fotosCloud().uploadFoto(getContext(), foto);
		Assert.assertTrue(HttpStatus.SC_OK == fotosCloud().getResponseCode());
	}

	private String getImageToTest() throws Throwable {
		Drawable drawable = getContext().getResources().getDrawable(R.drawable.bg_clean);
		BitmapDrawable d = (BitmapDrawable) drawable;
		Bitmap bitmap = d.getBitmap();
		byte[] bytes;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.PNG, 100, bos);
		bytes = bos.toByteArray();
		FileOutputStream fos = getContext().openFileOutput(TESTPIC, Context.MODE_PRIVATE);
		try{
			fos.write(bytes);
			return TESTPIC;
		}finally {
			fos.close();
		}
	}

	private FotosCloud fotosCloud() {
		if(fotosCloud == null){
			fotosCloud = new FotosCloud();
		}
		return fotosCloud;
	}

}
