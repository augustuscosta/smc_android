package br.com.otgmobile.trackteam.cloud.rest;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;

import android.content.Context;
import android.net.Uri;
import br.com.otgmobile.trackteam.database.FilmagemDAO;
import br.com.otgmobile.trackteam.model.Filmagem;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

public class FilmagensCloud extends RestClient{

	private final static String UPLOAD_URL = "comunicacao/ocorrencia/filmagens";
	private FilmagemDAO filmagemDAO;

	public void uploadFilmagem(Context context, Filmagem filmagem) throws Throwable{
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		setUrl(addSlashIfNeeded(url) + UPLOAD_URL);
		execute(context, filmagem, token);
	}

	private void execute(Context context, Filmagem filmagem, String token) throws Throwable {
		HttpPost request = new HttpPost(url);
		MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		entity.addPart(ConstUtil.OCORRENCIATAG, new StringBody((filmagem.getOcorrenciaServerId().toString())));
		entity.addPart(ConstUtil.TOKEN, new StringBody((token)));
		entity.addPart(ConstUtil.FILMAGEM, new ByteArrayBody(encodeFilmagemTobyteArray(filmagem,context), ConstUtil.FILMAGEM));
		request.setEntity(entity);
		executeRequest(request, url);
	}

	private byte[] encodeFilmagemTobyteArray(Filmagem filmagem, Context context) throws Throwable {
		Uri uri = Uri.parse(filmagem.getFilmagem());
		InputStream is = context.getContentResolver().openInputStream(uri);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		int size = is.available();
		
		try{
			byte[] b = new byte[size];
			int bytesRead = 0;
			while ((bytesRead = is.read(b)) != -1) {
				bos.write(b, 0, bytesRead);
			}
			
			return	 bos.toByteArray();
		}finally{
			bos.close();
			is.close();
		}
	}

	public void uploadAllFilmagens(Context context, Ocorrencia ocorrencia) throws Throwable {
		if(ocorrencia.getId() == null) {
			LogUtil.i("Não foi possivel fazer o upload das filmagens ocorrencia ainda não sincronizada");
			return;
		}

		List<Filmagem> filmagens = filmagemDAO(context).findFromOcorrencia(ocorrencia.get_id());
		
		if(filmagens == null || filmagens.isEmpty()){
			LogUtil.i("sem filmagens para fazer o upload");
			return;
		}

		for (Filmagem filmagem : filmagens) {
			if(filmagem.getOcorrenciaServerId() == null || filmagem.getOcorrenciaServerId() == 0){
				LogUtil.i("fazendo o upload das Filmagem "+filmagem.get_id()+" do atendimento: "+ocorrencia.getId());
				filmagem.setOcorrenciaServerId(ocorrencia.getId());
				uploadFilmagem(context, filmagem);
			}
			if(HttpStatus.SC_OK == getResponseCode()){
				LogUtil.i("upload com sucesso da filmagem"+filmagem.getId()+"do atendimento"+ocorrencia.getId());
				filmagem.setEnviado(AppHelper.getCurrentTime());
				filmagemDAO(context).update(filmagem);
			}
		}
	}

	private FilmagemDAO filmagemDAO(Context context) {
		if(filmagemDAO == null) {
			filmagemDAO = new FilmagemDAO(context);
		}
		return filmagemDAO;
	}
}
