package br.com.otgmobile.trackteam.cloud.rest;

import java.util.List;

import org.apache.http.HttpStatus;

import android.content.Context;
import br.com.otgmobile.trackteam.database.RespostaDAO;
import br.com.otgmobile.trackteam.model.Resposta;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

import com.google.gson.Gson;

public class RespostaCloud extends RestClient{
	
	private final String URL ="comunicacao/resposta";
	private RespostaDAO respostaDAO;
	
	public boolean sendRespostas(final Context context,final Resposta obj) throws Throwable{
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		final Gson gson = new Gson();
		String json = gson.toJson(obj, Resposta.class);
		addParam(ConstUtil.TOKEN, token);
		addParam(ConstUtil.RESPOSTA, json);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.POST);
		return getResponseCode() == HttpStatus.SC_OK;
	}
	
	public void sendAllUnsent(final Context context) {
		List<Resposta> respostas= respostaDAO(context).fetchAllParsed();
		if(respostas == null || respostas.isEmpty()){
			LogUtil.i("------------não foram deixadas novas respostas--------------");
			return;
		}
		
		for(Resposta resposta : respostas){
			try{
				if(sendRespostas(context, resposta)){
					LogUtil.i("------------enviando respostas--------------");
					respostaDAO(context).delete(resposta);
				}				
			}catch (Throwable e) {
				LogUtil.e("erro ao enviar respostas",e);
			}
		}
		
	}
	
	private RespostaDAO respostaDAO(Context context) {
		if(respostaDAO == null){
			respostaDAO = new RespostaDAO(context);
		}
		
		return respostaDAO;
	}

}
