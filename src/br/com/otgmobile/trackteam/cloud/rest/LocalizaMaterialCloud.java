package br.com.otgmobile.trackteam.cloud.rest;

import java.util.List;

import org.apache.http.HttpStatus;

import android.content.Context;
import br.com.otgmobile.trackteam.database.LocalizaMaterialDAO;
import br.com.otgmobile.trackteam.model.LocalizaMaterial;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

import com.google.gson.Gson;

public class LocalizaMaterialCloud extends RestClient {
	
	private final String URL ="comunicacao/localizamaterial";
	private LocalizaMaterialDAO localizaMaterialDAO;
	
	public boolean sendMaterialLocation(final Context context,final LocalizaMaterial obj) throws Throwable{
		cleanParams();
		String url = Session.getServer(context);
		String token = Session.getToken(context);
		final Gson gson = new Gson();
		String json = gson.toJson(obj, LocalizaMaterial.class);
		addParam(ConstUtil.TOKEN, token);
		addParam(ConstUtil.LOCALIZA_MATERIAL, json);
		setUrl(addSlashIfNeeded(url) + URL);
		execute(RequestMethod.POST);
		return getResponseCode() == HttpStatus.SC_OK;
	}
	
	public void sendAllUnsent(final Context context) {
		List<LocalizaMaterial> localizaMateriais= localizaMaterialDAO(context).fetchAllParsed();
		if(localizaMateriais == null || localizaMateriais.isEmpty()){
			LogUtil.i("------------não foram deixados novos materiais em campo--------------");
			return;
		}
		
		for(LocalizaMaterial localizaMaterial : localizaMateriais){
			try{
				if(sendMaterialLocation(context, localizaMaterial)){
					LogUtil.i("------------enviando materiais em campo--------------");
					localizaMaterialDAO(context).delete(localizaMaterial);
				}				
			}catch (Throwable e) {
				LogUtil.e("erro ao enviar materiais",e);
			}
		}
		
	}

	private LocalizaMaterialDAO localizaMaterialDAO(Context context) {
		if(localizaMaterialDAO == null){
			localizaMaterialDAO = new LocalizaMaterialDAO(context);
		}
		
		return localizaMaterialDAO;
	}

}
