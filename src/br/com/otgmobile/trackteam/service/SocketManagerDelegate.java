package br.com.otgmobile.trackteam.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import br.com.otgmobile.trackteam.database.AgenteDAO;
import br.com.otgmobile.trackteam.database.HistoricoDAO;
import br.com.otgmobile.trackteam.database.MensagemDAO;
import br.com.otgmobile.trackteam.database.OcorrenciaDAO;
import br.com.otgmobile.trackteam.database.VeiculoDAO;
import br.com.otgmobile.trackteam.model.Agente;
import br.com.otgmobile.trackteam.model.Historico;
import br.com.otgmobile.trackteam.model.Mensagem;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.Operador;
import br.com.otgmobile.trackteam.model.Veiculo;
import br.com.otgmobile.trackteam.service.SocketService.ChatListener;
import br.com.otgmobile.trackteam.util.BroadCastReceiverDictionary;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.GsonUtil;
import br.com.otgmobile.trackteam.util.LogUtil;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

public class SocketManagerDelegate {
	
	private static final String ROOT_JSON_OBJECT = "result";
	private final List<ChatListener> chatListeners = new ArrayList<ChatListener>(); // TODO Ha melhor maneira seria disparar broadCast.
	
	private Context context;
	private OcorrenciaDAO ocorrenciaDAO;
	private VeiculoDAO veiculoDAO;
	private AgenteDAO agenteDAO;
	private HistoricoDAO historicoDAO;
	private MensagemDAO mensagemDAO;
	private Socket socket;
	
	public SocketManagerDelegate(Context context) {
		this.context = context;
	}
	
	public void setSocket(Socket socket){
		this.socket = socket;
		prepareEventListener();
	}
	
	private void prepareEventListener(){
		
		socket.on(EventType.NOVA_OCORRENCIA.getValue(), new Emitter.Listener() {

		  @Override
		  public void call(Object... args) {
			  JSONObject object;
			try {
				object = new JSONObject(args[0].toString());
				novaOcorrencia(object);
			} catch (JSONException e) {
				LogUtil.e("SOCKET IO ERROR " + EventType.NOVA_OCORRENCIA.getValue(), e);
			}
		  }

	    });
		
		socket.on(EventType.MODIFICA_OCORRENCIA.getValue(), new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				  JSONObject object;
				try {
					object = new JSONObject(args[0].toString());
					modificaOcorrencia(object);
				} catch (JSONException e) {
					LogUtil.e("SOCKET IO ERROR " + EventType.MODIFICA_OCORRENCIA.getValue(), e);
				}
			  }

		    });
		
		socket.on(EventType.EXCLUI_OCORRENCIA.getValue(), new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				  JSONObject object;
				try {
					object = new JSONObject(args[0].toString());
					excluiOcorrencia(object);
				} catch (JSONException e) {
					LogUtil.e("SOCKET IO ERROR " + EventType.EXCLUI_OCORRENCIA.getValue(), e);
				}
			  }

		    });
		
		socket.on(EventType.NOVO_VEICULO.getValue(), new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				  JSONObject object;
				try {
					object = new JSONObject(args[0].toString());
					novoVeiculo(object);
				} catch (JSONException e) {
					LogUtil.e("SOCKET IO ERROR " + EventType.NOVO_VEICULO.getValue(), e);
				}
			  }

		    });
		
		socket.on(EventType.MODIFICA_VEICULO.getValue(), new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				  JSONObject object;
				try {
					object = new JSONObject(args[0].toString());
					modificaVeiculo(object);
				} catch (JSONException e) {
					LogUtil.e("SOCKET IO ERROR " + EventType.MODIFICA_VEICULO.getValue(), e);
				}
			  }

		    });
		
		
		socket.on(EventType.EXCLUI_VEICULO.getValue(), new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				  JSONObject object;
				try {
					object = new JSONObject(args[0].toString());
					excluiVeiculo(object);
				} catch (JSONException e) {
					LogUtil.e("SOCKET IO ERROR " + EventType.EXCLUI_VEICULO.getValue(), e);
				}
			  }

		    });
		
		socket.on(EventType.NOVO_AGENTE.getValue(), new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				  JSONObject object;
				try {
					object = new JSONObject(args[0].toString());
					novoAgente(object);
				} catch (JSONException e) {
					LogUtil.e("SOCKET IO ERROR " + EventType.NOVO_AGENTE.getValue(), e);
				}
			  }

		    });
		
		socket.on(EventType.MODIFICA_AGENTE.getValue(), new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				  JSONObject object;
				try {
					object = new JSONObject(args[0].toString());
					modificaAgente(object);
				} catch (JSONException e) {
					LogUtil.e("SOCKET IO ERROR " + EventType.MODIFICA_AGENTE.getValue(), e);
				}
			  }

		    });
		
		
		socket.on(EventType.MODIFICA_HISTORICO.getValue(), new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				JSONObject object;
				try {
					object = new JSONObject(args[0].toString());
					modificaHistorico(object);
				} catch (JSONException e) {
					LogUtil.e("SOCKET IO ERROR " + EventType.MODIFICA_HISTORICO.getValue(), e);
				}
			  }

		    });
		
		socket.on(EventType.ABRIR_CAMERA.getValue(), new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				 abrirCamera();
			  }

		    });
		
		socket.on(EventType.FECHAR_CAMERA.getValue(), new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				 fecharCamera();
			  }

		    });
		
		socket.on(EventType.CONECTA_CHAT.getValue(), new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				 connectChat();
			  }

		    });
		socket.on(EventType.CONECTA_CHAT.getValue(), new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				 connectChat();
			  }

		    });
		
		socket.on(EventType.LISTA_OPERADORES.getValue(), new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
					try {
						JSONObject object = new JSONObject(args[0].toString());
						JSONArray array = object.getJSONArray(ROOT_JSON_OBJECT);
						List<Operador> operadores = new ArrayList<Operador>();
						for(int i = 0 ; i < array.length(); i++){
							Operador operador = toOperador(array.get(i).toString());
							operadores.add(operador);
						}
						listaOperadores(operadores);
					} catch (JSONException e) {
						LogUtil.e("SOCKET IO ERROR " + EventType.LISTA_OPERADORES.getValue(), e);
					}
			  }

		    });
		
		socket.on(EventType.RECEBER_MENSAGEM.getValue(), new Emitter.Listener() {

			  @Override
			  public void call(Object... args) {
				  JSONObject object;
					try {
						object = new JSONObject(args[0].toString());
						receberMensagem(object);
					} catch (JSONException e) {
						LogUtil.e("SOCKET IO ERROR " + EventType.RECEBER_MENSAGEM.getValue(), e);
					}
			  }

		    });
	}
	
	private void novaOcorrencia(JSONObject data){
		String action = null;
		Intent intent = new Intent();
		Ocorrencia ocorrencia;
		
		ocorrencia = toOcorrencia(data);
		addSerializableToIntent(intent, ocorrencia);
		ocorrencia.setEnviado(true);
		ocorrenciaDAO().save(ocorrencia);
		action = BroadCastReceiverDictionary.NOVA_OCORRENCIA.getValue();
		
		intent.setAction(action);
		context.sendBroadcast(intent);
	}
	
	private void modificaOcorrencia(JSONObject data){
		String action = null;
		Intent intent = new Intent();
		Ocorrencia ocorrencia;
		
		ocorrencia = toOcorrencia(data);
		addSerializableToIntent(intent, ocorrencia);
		ocorrencia.setEnviado(true);
		ocorrenciaDAO().comentarioDAO().deleteFromOcorrencia(ocorrencia.getId());
		ocorrenciaDAO().save(ocorrencia);
		action = BroadCastReceiverDictionary.MODIFICA_OCORRENCIA.getValue();
		
		intent.setAction(action);
		context.sendBroadcast(intent);
	}
	
	private void excluiOcorrencia(JSONObject data){
		String action = null;
		Intent intent = new Intent();
		Ocorrencia ocorrencia;
		
		ocorrencia = toOcorrencia(data);
		addSerializableToIntent(intent, ocorrencia);
		Ocorrencia forDelete = ocorrenciaDAO().find(ocorrencia.getId());
		if(forDelete != null){
			ocorrenciaDAO().deleteAndAllObjects(forDelete);				
		}
		action = BroadCastReceiverDictionary.EXCLUI_OCORRENCIA.getValue();
		
		intent.setAction(action);
		context.sendBroadcast(intent);
	}
	
	private void novoVeiculo(JSONObject data){
		String action = null;
		Intent intent = new Intent();
		Veiculo veiculo;
		
		veiculo = toVeiculo(data);
		addSerializableToIntent(intent, veiculo);
		veiculoDAO().save(veiculo);
		action = BroadCastReceiverDictionary.NOVO_VEICULO.getValue();
		
		intent.setAction(action);
		context.sendBroadcast(intent);
	}
	
	private void modificaVeiculo(JSONObject data){
		String action = null;
		Intent intent = new Intent();
		Veiculo veiculo;
		
		veiculo = toVeiculo(data);
		addSerializableToIntent(intent, veiculo);
		veiculoDAO().save(veiculo);
		action = BroadCastReceiverDictionary.MODIFICA_VEICULO.getValue();
		
		intent.setAction(action);
		context.sendBroadcast(intent);
	}
	
	private void excluiVeiculo(JSONObject data){
		String action = null;
		Intent intent = new Intent();
		Veiculo veiculo;
		
		veiculo = toVeiculo(data);
		addSerializableToIntent(intent, veiculo);
		veiculoDAO().delete(veiculo.getId());
		action = BroadCastReceiverDictionary.EXCLUI_VEICULO.getValue();
		
		intent.setAction(action);
		context.sendBroadcast(intent);
	}
	
	private void novoAgente(JSONObject data){
		String action = null;
		Intent intent = new Intent();
		Agente agente;
		
		agente = toAgente(data);
		addSerializableToIntent(intent, agente);
		agenteDAO().save(agente);
		action = BroadCastReceiverDictionary.NOVO_AGENTE.getValue();
		
		intent.setAction(action);
		context.sendBroadcast(intent);
	}
	
	private void modificaAgente(JSONObject data){
		String action = null;
		Intent intent = new Intent();
		Agente agente;
		
		agente = toAgente(data);
		addSerializableToIntent(intent, agente);
		agenteDAO().save(agente);
		action = BroadCastReceiverDictionary.MODIFICA_AGENTE.getValue();
		
		
		intent.setAction(action);
		context.sendBroadcast(intent);
	}
	
	private void modificaHistorico(JSONObject data){
		String action = null;
		Intent intent = new Intent();
		Historico historico;
		
		historico = toHistorico(data);
		addSerializableToIntent(intent, historico);
		if ( historico != null ) {
			historicoDAO().saveFromNode(historico);
		}
		
		action = BroadCastReceiverDictionary.MODIFICA_HISTORICO.getValue();
		
		intent.setAction(action);
		context.sendBroadcast(intent);
	}
	
	private void abrirCamera(){
		String action = null;
		Intent intent = new Intent();
		action = BroadCastReceiverDictionary.ABRIR_CAMERA.getValue();
		intent.setAction(action);
		context.sendBroadcast(intent);
	}
	
	private void fecharCamera(){
		String action = null;
		Intent intent = new Intent();
		action = BroadCastReceiverDictionary.FECHAR_CAMERA.getValue();
		intent.setAction(action);
		context.sendBroadcast(intent);
	}
	
	private void connectChat(){
		notifyObserversChatConnected();
	}
	
	private void listaOperadores(List<Operador> operadores){
		try {
			notifyObserversRetrieveChatOperators(operadores);
		} catch (Exception e) {
			LogUtil.e("Erro ao receber a lista dos operadores", e);
		}
	}
	
	private void receberMensagem(JSONObject data){
		String action = null;
		Intent intent = new Intent();

		Mensagem mensagem  = toMensagem(data);
		mensagem.setEnviado(true);
		mensagemDAO().save(mensagem);
		intent.putExtra(ConstUtil.TOKEN, mensagem.getFrom());
		action = BroadCastReceiverDictionary.RECEBER_MENSAGEM.getValue();
		
		intent.setAction(action);
		context.sendBroadcast(intent);
	}

	private Operador toOperador(String data) {
		return (Operador) GsonUtil.fromGson(Operador.class, data);
	}

	private Mensagem toMensagem(JSONObject data) {
		return (Mensagem) GsonUtil.fromGson(ROOT_JSON_OBJECT, Mensagem.class, data);
	}

	private void addSerializableToIntent(Intent intent, Serializable serializable) {
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, serializable);
		intent.putExtras(bundle);
	}

	private Historico toHistorico(JSONObject data) {
		Historico historico = (Historico) GsonUtil.fromGson(ROOT_JSON_OBJECT, Historico.class, data);
		if(historico != null)
			historico.setEnviado(true);
		return historico;
	}

	private Agente toAgente(JSONObject data) {
		return (Agente) GsonUtil.fromGson(ROOT_JSON_OBJECT, Agente.class, data);
	}

	private Veiculo toVeiculo(final JSONObject data) {
		return (Veiculo) GsonUtil.fromGson(ROOT_JSON_OBJECT, Veiculo.class, data);
	}

	private Ocorrencia toOcorrencia(final JSONObject data) {
		return (Ocorrencia) GsonUtil.fromGson(ROOT_JSON_OBJECT, Ocorrencia.class, data);
	}
	
	private OcorrenciaDAO ocorrenciaDAO() {
		if ( ocorrenciaDAO == null ) {
			ocorrenciaDAO = new OcorrenciaDAO(this.context);
		}
		
		return ocorrenciaDAO;
	}
	
	private VeiculoDAO veiculoDAO() {
		if ( veiculoDAO == null ) {
			veiculoDAO = new VeiculoDAO(this.context);
		}
		
		return veiculoDAO;
	}
	
	private AgenteDAO agenteDAO() {
		if ( agenteDAO == null ) {
			agenteDAO = new AgenteDAO(this.context);
		}
		
		return agenteDAO;
	}
	
	private HistoricoDAO historicoDAO() {
		if ( historicoDAO == null ) {
			historicoDAO = new HistoricoDAO(this.context);
		}
		
		return historicoDAO;
	}
	
	private MensagemDAO mensagemDAO(){
		if(mensagemDAO == null){
			mensagemDAO = new MensagemDAO(context);
		}
		return mensagemDAO;
	}

	public void addChatListener(ChatListener listener) {
		if ( !chatListeners.contains(listener) ) {
			chatListeners.add(listener);
		}
	}

	public void removeChatListener(ChatListener listener) {
		if ( chatListeners.contains(listener) ) {
			chatListeners.remove(listener);
		}
	}
	
	private void notifyObserversRetrieveChatOperators(List<Operador> operators) {
		for (ChatListener listener : chatListeners) {
			listener.onRetrieveOperators(operators);
		}
	}

	private void notifyObserversChatConnected() {
		for (ChatListener listener : chatListeners) {
			listener.onConnected();
		}
	}
	
}
