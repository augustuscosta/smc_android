package br.com.otgmobile.trackteam.service;

import java.net.URISyntaxException;
import java.util.List;

import org.json.JSONObject;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.database.MensagemDAO;
import br.com.otgmobile.trackteam.model.Conexao;
import br.com.otgmobile.trackteam.model.Mensagem;
import br.com.otgmobile.trackteam.model.Operador;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.BroadCastReceiverDictionary;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

public class SocketService extends Service {

	public static final String SOCKET_SERVICE_INTENT = "SOCKET_SERVICE_INTENT";
	
	private final SocketIOBinder binder = new SocketIOBinder();
	
	private Socket socketIO;
	private Handler mHandler;
	private SocketManagerDelegate socketDelegate;
	

	public static void startService(Context context) {
		context.startService(new Intent(SOCKET_SERVICE_INTENT));
	}
	
	public static void stopService(Context context) {
		context.stopService(new Intent(SOCKET_SERVICE_INTENT));
	}
	
	private SocketManagerDelegate socketDelegate() {
		if ( socketDelegate == null ) {
			socketDelegate = new SocketManagerDelegate(getApplicationContext());
		}
		
		return socketDelegate;
	}
	
	public interface IChatService {
		public void connect(Conexao conexao);
		public void getOperatorsChat();
		public void sendMessage(Mensagem mensagem);
		public void addListener(ChatListener listener);
		public void removeListener(ChatListener listener);
		public void sendMessagesNotSent();
	}
	
	public interface ChatListener {
		public void onConnected();
		public void onRetrieveOperators(List<Operador> operators);
	}
	
	final private IChatService mChatInterface = new IChatService() {

		private MensagemDAO mensagemDAO;

		@Override
		public void sendMessage(Mensagem mensagem) {
			mensagemDAO().save(mensagem);
			sendChatMessage(mensagem);
		}
		
		private MensagemDAO mensagemDAO() {
			if(mensagemDAO == null){
				mensagemDAO = new MensagemDAO(SocketService.this);
			}
			
			return mensagemDAO;
		}

		@Override
		public void getOperatorsChat() {
			getOperators();
			
		}
		
		@Override
		public void connect(Conexao conexao) {
			connectToChat(conexao);
		}

		@Override
		public void addListener(ChatListener listener) {
			socketDelegate().addChatListener(listener);
		}

		@Override
		public void removeListener(ChatListener listener) {
			socketDelegate().removeChatListener(listener);
		}

		@Override
		public void sendMessagesNotSent() {
			List<Mensagem> notSent = mensagemDAO().findMensagensNotSent();
			if(notSent == null || notSent.isEmpty()) return;
			for (Mensagem mensagem : notSent) {
				sendMessage(mensagem);
			}
		}

	};
	
	public class SocketIOBinder extends Binder {
		public IChatService getChatInterface() {
			return mChatInterface;
		}
	}
	
	@Override
	public IBinder onBind(Intent it) {
		return binder;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if(socketIO != null){
			atualizaSessao();
			Session.setSocketConectionStatus(this, SocketConectionStatus.CONNECTED.getValue());
			return super.onStartCommand(intent, flags, startId);
		}
			
		
		mHandler = new Handler();		
		try {
			IO.Options opts = new IO.Options();
			opts.forceNew = true;
			socketIO = IO.socket(Session.getSocketServer(getApplicationContext()),opts);
			socketDelegate().setSocket(socketIO);
		} catch (URISyntaxException e) {
			Log.e("SOCKET_ERROR", e.getMessage());
		}
		prepareSocketIO();
		mHandler.post(mScheduler);
		return super.onStartCommand(intent, flags, startId);
	}
	
	private void prepareSocketIO(){
	
		socketIO.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
    		@Override
    		public void call(Object... args) {
    			onConnect();
    		}

    	});
    	
    	socketIO.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
    		
    		@Override
    		public void call(Object... args) {
    			System.out.println("Caiu a conexão com o SocketIO"); 
				onDisconnect();
				connect();
    		}

    	});
    	
    	socketIO.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
    		
    		@Override
    		public void call(Object... args) {
    			System.out.println("Erro de conexão SocketIO"); 
    			onConnectFailure();
    			connect();
    		}

    	});
    	
    	socketIO.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
    		
    		@Override
    		public void call(Object... args) {
    			System.out.println("Erro de conexão SocketIO"); 
    			onConnectFailure();
    			connect();
    		}

    	});
    	
    	socketIO.on(Socket.EVENT_RECONNECT_ERROR, new Emitter.Listener() {
    		
    		@Override
    		public void call(Object... args) {
    			System.out.println("Reconect error SocketIO"); 
    			onConnectFailure();
    			connect();
    		}

    	});
    	
    	socketIO.on(Socket.EVENT_RECONNECT_FAILED, new Emitter.Listener() {
    		
    		@Override
    		public void call(Object... args) {
    			System.out.println("Reconect failed SocketIO"); 
    			onConnectFailure();
    			connect();
    		}

    	});
    	
	}
	
	public void onConnect() {
		mChatInterface.sendMessagesNotSent();
		sendBroadCastConectionMessage(getString(R.string.connected));
		Session.setSocketConectionStatus(SocketService.this,2);
		Log.i("SOCKET_CONNECTED", "Connected with IOSocket...");
		atualizaSessao();
	}
	
	private void atualizaSessao(){
		Log.i("SOCKET", "Atualizando sessão no IOSocket...");
		JSONObject obj;
		try {
			obj = new JSONObject().accumulate("token", Session.getToken(getApplicationContext()));
			socketIO.emit(EventType.ATUALIZA_SESSAO.getValue(), obj);
		} catch (Exception e) {
			LogUtil.e("Erro enviando token para o node", e);
		}
	}
	
	public void connect() {
		Log.i("SOCKET_CONNECTING", "Connecting with IOSocket...");
    	socketIO.connect();
    }
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		disconnectIOSocket();
		release();
	}
	
	private void disconnectIOSocket() {
		if ( socketIO != null ) {
			socketIO.disconnect();
		}
	}

	private void release() {
		mHandler.removeCallbacks(mScheduler);
		mHandler = null;
	}

	/**
	 * Scheduler to check if IOSocket is alive(isConnected) otherwise trying connect.
	 */
	private final Runnable mScheduler = new Runnable() {
		@Override
		public void run() {
			connect();
		}
	};
	
	private void connectToChat(final Conexao conexao) {
		emitEventToNode(EventType.CONECTA_CHAT, conexao.toJson());
	}
	
	private void sendChatMessage(final Mensagem mensagem) {
		emitEventToNode(EventType.ENVIAR_MENSAGEM, mensagem.toJson());
		Log.i("ENIVAR_MENSAGEM", "Enviando mensagem de chat");
	}
	
	private void getOperators() {
		Log.i("CHAT_OPERADORES", "Pedindo lista de operadores");
		emitEventToNode(EventType.LISTA_OPERADORES, null);
	}
	
	private boolean emitEventToNode(final EventType eventType, final JSONObject jsonObject) {
		
		try {
			socketIO.emit(eventType.getValue(), jsonObject);
			return true;
		}catch (Exception e) {
			LogUtil.e("Erro enviando token para o node", e);
		}
		
		return false;
	}
	
	
	private void onDisconnect() {
		Session.setSocketConectionStatus(SocketService.this, 0);
		sendBroadCastConectionMessage(getString(R.string.dsconnected));
		Log.i("SOCKET_DISCONNECTED", "Disconnected from IOSocket...");
	}
	
	private void onConnectFailure() {
		sendBroadCastConectionMessage(getString(R.string.connection_failure));
		Session.setSocketConectionStatus(SocketService.this, 0);
		Log.i("SOCKET_CONNECTION", "Connection failure with IOSocket...");
	}
	

	private void sendBroadCastConectionMessage(String status) {
		Intent intent = new Intent();
		intent.putExtra(ConstUtil.CONECTION_STATUS, status);
		intent.setAction(BroadCastReceiverDictionary.EXIBIR_CONEXAO.getValue());
		sendBroadcast(intent);
	}
	
}
