package br.com.otgmobile.trackteam.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationProvider;
import android.os.Binder;
import android.os.IBinder;
import android.os.Looper;
import br.com.otgmobile.trackteam.database.HistoricoDAO;
import br.com.otgmobile.trackteam.gps.DLGps;
import br.com.otgmobile.trackteam.gps.DLGpsObserver;
import br.com.otgmobile.trackteam.model.Historico;
import br.com.otgmobile.trackteam.util.LogUtil;

public class PositionService extends Service {

	public static final String POSITION_SERVICE_INTENT = "POSITION_SERVICE_INTENT";
	private final PositionBinder mBinder = new PositionBinder();
	
	private final List<IPositionServiceListener> listeners = new ArrayList<PositionService.IPositionServiceListener>();
	private Location mLocation;
	private HistoricoDAO historicoDAO;
	private boolean isExistGPSListener;
	
	
	public static void startService(Context context) {
		context.startService(new Intent(POSITION_SERVICE_INTENT));
	}
	
	public static void stopService(Context context) {
		context.stopService(new Intent(POSITION_SERVICE_INTENT));
	}
	
	public interface IPositionListenerService {
		public void addListener(IPositionServiceListener listener);
		public void removeListener(IPositionServiceListener listener);
	}
	
	public interface IPositionServiceListener {
		public void gotHistorico(Historico historico);
		public void connecting(String deviceName);
		public void connected(String deviceName);
		public void disconnected();
		public void erro(String message);
	}
	
	public class PositionBinder extends Binder {
		public IPositionListenerService getInterface() {
			return mInterface;
		}
	}
	
	private final IPositionListenerService mInterface = new IPositionListenerService() {
		
		@Override
		public synchronized void addListener(IPositionServiceListener listener) {
			if ( !listeners.contains(listener) ) {
				listeners.add(listener);
			}
		}

		@Override
		public synchronized void removeListener(IPositionServiceListener listener) {
			if ( listeners.contains(listener) ) {
				listeners.remove(listener);
			}
		}

	};

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		start();
		return super.onStartCommand(intent, flags, startId);
	}
	
	private void start() {
		startGPSListener();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		release();
	}
	
	private void release() {
		DLGps.removeGpsObserver(gpsObserver, getApplicationContext());
		isExistGPSListener = false;
		historicoDAO = null;
	}

	
	private synchronized void startGPSListener() {
		if ( isExistGPSListener ) return;
		
		LogUtil.i("Start GPS Thread.");
		final GPSThread gpsThread = new GPSThread();
		gpsThread.setName(GPSThread.class.getSimpleName());
		gpsThread.setPriority(Thread.MIN_PRIORITY);
		gpsThread.start();
	}


	private class GPSThread extends Thread {
		
		@Override
		public void run() {
			try {
				Looper.prepare();
				DLGps.addGpsObserver(gpsObserver, getApplicationContext());
				isExistGPSListener = true;
				Looper.loop();
			} finally {
				Looper.myLooper().quit();
				isExistGPSListener = false;
			}
		}
	}

	/**
	 * Observer GPS(Listener).
	 */
	final private DLGpsObserver gpsObserver = new DLGpsObserver() {
		
		@Override
		public void onStatusChanged(int status) {
			if ( LocationProvider.AVAILABLE != status ) { // GPS nao esta mais habilitado.
				mLocation = null;
			}
		}
		
		@Override
		public void onLocationChanged(Location location) {
			mLocation = location;
			
			if ( mLocation != null ) {
				saveHistorico(null);
			}
		}
	};
	
	private synchronized void saveHistorico(Historico historico) {
		if ( historico == null ) {
			historico = new Historico();
		}
		
		setLatLongInHistorico(historico);
		historico.setDataHora(Calendar.getInstance().getTimeInMillis());
		historico.setEnviado(false);
		historicoDAO().save(historico);
	}

	private void setLatLongInHistorico(Historico historico) {
		if ( mLocation == null ) {
			return;
		}
		
		historico.setLatitude(((Double) mLocation.getLatitude()).floatValue());
		historico.setLongitude(((Double) mLocation.getLongitude()).floatValue());
		LogUtil.i("Recebendo Lat - " + historico.getLatitude() + ", Long - " + historico.getLongitude());
	}
	
	private HistoricoDAO historicoDAO() {
		if ( historicoDAO == null ) {
			historicoDAO = new HistoricoDAO(getApplicationContext());
		}
		
		return historicoDAO;
	}
	
}
