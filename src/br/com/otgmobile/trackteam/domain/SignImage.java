package br.com.otgmobile.trackteam.domain;

import com.google.gson.annotations.SerializedName;



public class SignImage {

	@SerializedName("ait")
	private String ait;
	
	@SerializedName("image")
	private byte[] image;
	
	@SerializedName("date")
	private Long date;
	
	// 0 = autuado / 1 = fiscal / 2 = testemunha
	@SerializedName("type")
	private Integer type = 0;
	
	private Integer id;

	public String getAit() {
		return ait;
	}

	public void setAit(String ait) {
		this.ait = ait;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
