package br.com.otgmobile.trackteam.gps;

import android.location.Location;

public interface DLGpsObserver {
	
	void onLocationChanged(Location location);
	
	void onStatusChanged(int status);

}
