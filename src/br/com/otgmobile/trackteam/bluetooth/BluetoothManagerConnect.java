package br.com.otgmobile.trackteam.bluetooth;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import br.com.otgmobile.trackteam.bluetooth.BluetoothConnector.OnDiscoveryListener;
import br.com.otgmobile.trackteam.model.Device;
import br.com.otgmobile.trackteam.util.LogUtil;

/**
 * 
 * @author pierrediderot@gmail.com
 * @since 02/03/2012
 * @version $Revision:  $ <br>
 *          $Date:  $ <br> 
 *          $Author:  $
 */
public class BluetoothManagerConnect {

	private Context mContext;
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothConnector mConnector;
	private BluetoothListener bluetoothListener;

	public interface BluetoothListener {
		public void onFinishSearchDevices(List<Device> devices);
	}
	
	public BluetoothManagerConnect(Context mContext, BluetoothListener bluetoothListener) {
		this.mContext = mContext;
		this.bluetoothListener = bluetoothListener;
		createBluetoothAdapterInstance();
	}
	
	public BluetoothManagerConnect(Context context) {
		this.mContext = context;
		createBluetoothAdapterInstance();
	}
	
	private void createBluetoothAdapterInstance() {
		if ( mBluetoothAdapter == null ) {
			mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

			if ( mBluetoothAdapter == null ) {
				LogUtil.w("This API-Level not suport BluetoothAdapter");
			} else {
				enableBluetoothAdapter();
			}
		}
	}

	public void startBluetoothDiscovery() throws IOException {
		createBluetoothConnectorInstance();
		startSearchDevices();
	}

	private void startSearchDevices() throws IOException {
		mConnector.startDiscovery(mListener);
	}
	
	private void createBluetoothConnectorInstance() throws IOException {
		try {
			mConnector = BluetoothConnector.getConnector(mContext);
		} catch (IOException e) {
			LogUtil.e("Erro ao tentar instanciar BluetoothConnector", e);
			throw e;
		}
	}
	
	public void enableBluetoothAdapter() {
		// Se nao estiver habilitado ou nao estiver buscando por dispositivos. Habilite!
		if ( !mBluetoothAdapter.isEnabled() || !mBluetoothAdapter.isDiscovering()) {
			mBluetoothAdapter.enable();
		}
	}
	
	public boolean isEnable() {
		return mBluetoothAdapter != null && mBluetoothAdapter.isEnabled();
	}

	final private OnDiscoveryListener mListener = new OnDiscoveryListener() {

		private List<Device> devices;

		@Override
		public void onDiscoveryStarted() {
			if ( devices == null ) {
				devices = new ArrayList<Device>();
			}
			
			devices.clear();
		}

		@Override
		public void onDiscoveryFinished() {
			bluetoothListener.onFinishSearchDevices(devices);
		}

		@Override
		public void onDiscoveryError(String error) {
			LogUtil.e("Erro ao procurar devices via BluetoothAdapter\n" + error);
		}

		@Override
		public void onDeviceFound(String name, String address) {
			LogUtil.d("Device encontrado: Nome(" + name + "), Endereco(" + address + ")" );

			name = TextUtils.isEmpty(name) ? "No Name" : name;
			devices.add(Device.create(name, address));
		}
	};
	
	public void pairDevice(final String address) throws IOException {
		createBluetoothConnectorInstance();
		
		if ( mConnector == null ) {
			LogUtil.w("Nao pode realizar o pareamento bluetooth, pois nao existe BluetoothConnector");
			return;
		}
		
		mConnector.connect(address);
	}
	
	public synchronized void closeConnection() {
		if ( mConnector != null ) {
			try {
				mConnector.close();
			} catch (IOException e) {
				LogUtil.e("Erro ao tentar fechar connecxao bluetooth", e);
			} finally {
				mConnector = null;
			}
		}
	}
	
	public static void turnBluetoothVisible(Activity activity) {
		if ( activity != null && BluetoothAdapter.getDefaultAdapter().getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE ) {
    		final Intent it = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
    		it.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 120);
    		activity.startActivity(it);
    	}
	}
	
}
