package br.com.otgmobile.trackteam.customwidgets;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.NumberKeyListener;
import android.util.AttributeSet;
import android.widget.EditText;

public class PlacaEditText extends EditText {

	private static char[] CAPITAL_LETTERS_NUMBERS = { '0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
			'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

	private boolean isUpdating;

	private int positioning[] = { 0, 1, 2, 3, 5, 6, 7, 8 };

	public PlacaEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initialize();
	}

	public PlacaEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize();
	}

	public PlacaEditText(Context context) {
		super(context);
		initialize();
	}

	public String getCleanText() {
		String text = PlacaEditText.this.getText().toString();

		text.replaceAll("[^A-Z0-9]*", "");
		return text;
	}

	private void initialize() {

		final int maxNumberLength = 8;
		this.setKeyListener(keylistenerNumber);

		this.setText("   -    ");
		this.setSelection(1);

		this.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				String current = s.toString();

				if (isUpdating) {
					isUpdating = false;
					return;

				}

				String number = current.replaceAll("[^A-Z0-9]*", "");
				if (number.length() > 7)
					number = number.substring(0, 7);
				int length = number.length();

				String paddedLicense = padNumber(number, maxNumberLength);

				String part1 = paddedLicense.substring(0, 3);
				String part2 = paddedLicense.substring(3, 7);

				String placa = part1 + "-" + part2;

				isUpdating = true;
				PlacaEditText.this.setText(placa);

				PlacaEditText.this.setSelection(positioning[length]);

			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}
		});
	}

	protected String padNumber(String number, int maxLength) {
		String padded = new String(number);
		for (int i = 0; i < maxLength - number.length(); i++)
			padded += " ";
		return padded;
	}

	private final KeylistenerNumber keylistenerNumber = new KeylistenerNumber();

	private class KeylistenerNumber extends NumberKeyListener {

		public int getInputType() {
			return InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
					| InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS;
		}

		@Override
		protected char[] getAcceptedChars() {
			return CAPITAL_LETTERS_NUMBERS;

		}
	}
}
