package br.com.otgmobile.trackteam.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Sign;
import br.com.otgmobile.trackteam.row.SignRowHolderInfo;

public class SignAdapter extends BaseAdapter {

	List<Sign> list;
	Activity context;
	
	public SignAdapter(List<Sign> list,Activity context){
		this.list = list;
		this.context = context;
	}
	
	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list == null ? 0 :list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		SignRowHolderInfo	rowInfo;
		
		if(view == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.sign_row, null);
			rowInfo = new SignRowHolderInfo();
			rowInfo.signListImage = (ImageView) row.findViewById(R.id.sign_list_tumbnail);
			row.setTag(rowInfo);
			
		}else{
			rowInfo = (SignRowHolderInfo) view.getTag();
		}
		
		Sign obj = list.get(position);
		rowInfo.drawRow(obj, context);			
		
		return row;
	}


}
