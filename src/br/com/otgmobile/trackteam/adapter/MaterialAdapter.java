package br.com.otgmobile.trackteam.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Material;
import br.com.otgmobile.trackteam.row.MaterialRowHolderInfo;

public class MaterialAdapter extends BaseAdapter {
	
	private List<Material>  list;
	private Activity context;
	private List<Material> checkedList;
	private boolean toOcorrencia;
	
	public MaterialAdapter(List<Material>  list, final Activity context,final boolean toOcorrencia) {
		this.list 	 = list;
		this.context = context;
		this.toOcorrencia =toOcorrencia;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list == null ? 0 : list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		MaterialRowHolderInfo rowInfo;
		Material material = list.get(position);
		
		if (row == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.material_row, null);

			rowInfo = new MaterialRowHolderInfo();
			rowInfo.materialListCode 		 = (TextView) row.findViewById(R.id.material_list_code);
			rowInfo.materialListDescrription = (TextView) row.findViewById(R.id.material_list_description);
			rowInfo.materialListCheckBox = (CheckBox) row.findViewById(R.id.material_list_checkbox);
			if(toOcorrencia){
				rowInfo.materialListCheckBox.setOnClickListener(listener);
			}else{
				rowInfo.materialListCheckBox.setVisibility(View.GONE);				
			}
			
			row.setTag(rowInfo);
		} else {
			rowInfo = (MaterialRowHolderInfo) row.getTag();
			rowInfo.setChecked(false);
		}
		
		if ( isChecked(material) ) {
			rowInfo.setChecked(true);
		}
		
		row.setTag(R.layout.material, material);
		rowInfo.drawRow(material);
		return row;
	}
	
	/**
	 * Create a single instance of items checked.
	 */
	private void createSingleInstanceCheckedList() {
		if ( checkedList == null ) {
			checkedList = new ArrayList<Material>();
		}
	}
	
	private boolean isChecked(final Material material) {
		createSingleInstanceCheckedList();
		return checkedList.contains(material);
	}
	
	public void checkAll() {
		createSingleInstanceCheckedList();
		checkedList = new ArrayList<Material>(list);
		notifyDataSetChanged();
	}
	public void uncheckAll() {
		createSingleInstanceCheckedList();
		checkedList.clear();
		notifyDataSetChanged();
	}

	private void toogleCheckBox(View rootView) {
		MaterialRowHolderInfo rowInfo = (MaterialRowHolderInfo) rootView.getTag();
		final Material material = (Material) rootView.getTag(R.layout.material);
		createSingleInstanceCheckedList();
		
		if ( checkedList.contains(material) ) {
			checkedList.remove(material);
			rowInfo.setChecked(false);
		} else {
			checkedList.add(material);
			rowInfo.setChecked(true);
		}
	}
	
	public boolean hasCheckedItems(){
		createSingleInstanceCheckedList();
		return !checkedList.isEmpty();
	}
	
	public List<Material> getCheckedItems(){
		return checkedList;
	}
	
	public void setCheckedItems(final List<Material> materiais){
		checkedList = new ArrayList<Material>(materiais);
		notifyDataSetChanged();
	}
	
	private OnClickListener listener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			View rootView = (View) v.getParent().getParent().getParent();
			toogleCheckBox(rootView);
		}
	};
	
}
