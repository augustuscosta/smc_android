package br.com.otgmobile.trackteam.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.VeiculoEnvolvido;
import br.com.otgmobile.trackteam.row.VeiculoEnvolvidoCadRowHolderInfo;
import br.com.otgmobile.trackteam.util.HandleListButton;
import br.com.otgmobile.trackteam.util.SelectListItem;

public class VeiculoEnvolvidoCadAdapter extends BaseAdapter {
	
	private List<VeiculoEnvolvido> list;
	private Activity context;
	private HandleListButton handler; 
	
	public VeiculoEnvolvidoCadAdapter(List<VeiculoEnvolvido> list, Activity context, HandleListButton handler) {
		this.list = list;
		this.context = context;
		this.handler = handler;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list == null ? 0 : list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		VeiculoEnvolvidoCadRowHolderInfo rowInfo;
		
		if (view == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.veiculo_cad_row, null);
			
			rowInfo = new VeiculoEnvolvidoCadRowHolderInfo();
			rowInfo.involvedVehicleLicense = (TextView) row.findViewById(R.id.veiculo_envolvido_license);
			rowInfo.involvedVehicleBrand   = (TextView) row.findViewById(R.id.veiculo_envolvido_brand);
			rowInfo.involvedVehicleDeleteButton = (LinearLayout) row.findViewById(R.id.veiculo_delete_button);
			
			row.setTag(rowInfo);
		}else{
			rowInfo = (VeiculoEnvolvidoCadRowHolderInfo) row.getTag();
		}
		
		rowInfo.involvedVehicleDeleteButton.setOnClickListener(new SelectListItem(position, handler, row));
		VeiculoEnvolvido obj = list.get(position);
		rowInfo.drawRow(obj);
		return row;
	}

}
