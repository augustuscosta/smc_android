package br.com.otgmobile.trackteam.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Foto;
import br.com.otgmobile.trackteam.row.FotoRowHolderInfo;

public class FotoAdapter extends BaseAdapter{

	List<Foto> list;
	Activity context;
	
	public FotoAdapter(List<Foto> list,Activity context){
		this.list = list;
		this.context = context;
	}
	
	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list == null ? 0 :list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		View row = view;
		FotoRowHolderInfo  rowInfo;
		
		if(view == null){
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.foto_row, null);
			rowInfo = new FotoRowHolderInfo();
			rowInfo.fotoListImage = (ImageView) row.findViewById(R.id.foto_list_tumbnail);
			row.setTag(rowInfo);
			
		}else{
			rowInfo = (FotoRowHolderInfo) view.getTag();
		}
		
		Foto obj = list.get(position);
		rowInfo.drawRow(obj, context);			
		
		return row;
	}

}
