package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.Material;

public class MaterialDAO {
	// Database fields
	public static final String _ID = "_id";
	public static final String ID = "id";
	public static final String CODIGO = "codigo";
	public static final String DESCRICAO = "descricao";
	public static final String DISPONIVEL = "disponivel";

	private static final String[] COLUMNS =  { _ID,ID, CODIGO,DESCRICAO,DISPONIVEL};
	private static final String DATABASE_TABLE = "materiais";
	private SQLiteDatabase database;

	public MaterialDAO(Context context) {
		database = DBHelper.getDatabase(context);
	}


	public long save(Material obj){
		if(obj == null){
			return -1;
		}
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			obj.setDisponivel(find(obj.getId()).isDisponivel());
			inserted = update(obj);
		}

		return inserted;
	}

	private boolean isNew(Material obj) {
		if(find(obj.getId()) == null)
			return true;
		return false;
	}

	public long create(Material obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	public long update(Material obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, ID + " = ?",
				new String[] { obj.getId().toString() });
	}

	public boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, ID + "=" + id, null) > 0;
	}

	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE, null, null) > 0;
	}

	private Cursor findCursor(int codigo) {
		return database.query(DATABASE_TABLE, COLUMNS, ID + " = ?", new String[] { codigo + "" }, null,
				null, null);
	}

	public Material find(int id) {
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}
			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}

	public Cursor fetchAll() {
		return database.query(DATABASE_TABLE, COLUMNS, null, null, null, null, null);
	}
	
	private Cursor fetchAvailable() {
		return database.query(DATABASE_TABLE, COLUMNS, DISPONIVEL +" > "+ 0, null, null, null, null);
	}

	public List<Material> fetchAllParsed() {
		Cursor cursor = fetchAll();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	public List<Material> fetchAVailableParsed() {
		Cursor cursor = fetchAvailable();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}

	private List<Material> parse(Cursor cursor) {
		List<Material> toReturn = null;
		if (cursor != null) {
			toReturn = new ArrayList<Material>();
			Material obj;
			if (cursor.moveToFirst()) {
				while (!cursor.isAfterLast()) {
					obj = parseObject(cursor);
					toReturn.add(obj);
					cursor.moveToNext();
				}
			}
		}

		return toReturn;
	}

	private Material parseObject(Cursor cursor) {
		Material obj = null;
		obj = new Material();
		obj.set_id(cursor.getInt(0));
		obj.setId(cursor.getInt(1));
		obj.setCodigo(cursor.getString(2));
		obj.setDescricao(cursor.getString(3));
		obj.setDisponivel(cursor.getInt(4) > 0);
		return obj;
	}

	private ContentValues createContentValues(Material obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(CODIGO, obj.getCodigo());
		values.put(DESCRICAO, obj.getDescricao());
		values.put(DISPONIVEL, obj.isDisponivel());
		return values;
	}

	private ContentValues createUpdateContentValues(Material obj) {
		ContentValues values = new ContentValues();
		values.put(CODIGO, obj.getCodigo());
		values.put(DESCRICAO, obj.getDescricao());
		values.put(DISPONIVEL, obj.isDisponivel());
		return values;
	}
}
