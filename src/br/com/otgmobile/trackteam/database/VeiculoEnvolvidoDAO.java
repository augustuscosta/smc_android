package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import br.com.otgmobile.trackteam.model.VeiculoEnvolvido;

public class VeiculoEnvolvidoDAO implements BaseColumns {
	
	public static final String ID  			= "id";
	public static final String PLACA 		= "placa";
	public static final String MODELO		= "modelo";
	public static final String RENAVAN  	= "renavam";
	public static final String ANO			= "ano";
	public static final String MARCA		= "marca";
	public static final String CHASSI		= "chassi";
	
	
	private static String[] FIELDS = {_ID,ID,PLACA,MODELO,RENAVAN,ANO,MARCA,CHASSI};
	
	private static final String DATABASE_TABLE = "veiculo_envolvido";
	private SQLiteDatabase database;
	
	public VeiculoEnvolvidoDAO(Context context){
		database = DBHelper.getDatabase(context);
	}
	
	public long save(VeiculoEnvolvido obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = find(obj.getPlaca()).get_id(); 
			update(obj);
		}

		return inserted;
	}
	
	private boolean isNew(VeiculoEnvolvido obj) {
		if(find(obj.getPlaca()) == null)
			return true;
		return false;
	}

	private long create(VeiculoEnvolvido obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}
	

	private long update(VeiculoEnvolvido obj) {
		ContentValues values = createUpdateContentValues(obj); 
		return database.update(DATABASE_TABLE, values, PLACA+"=?", new String[]{obj.getPlaca()});
	}
	
	public boolean delete(String placa){
		return database.delete(DATABASE_TABLE, PLACA+"=" +placa, null)>0 ;
	}
	
	public boolean deleteAll(){
		return database.delete(DATABASE_TABLE, null, null)>0;
	}

	private Cursor findCursor(String codigo){
		return database.query(DATABASE_TABLE, FIELDS, PLACA+" = ?",new String[]{codigo+""}, null, null, null);
	}
	
	private Cursor findCursor(int codigo){
		return database.query(DATABASE_TABLE, FIELDS, _ID+" = ?",new String[]{codigo+""}, null, null, null);
	}
	
	public Cursor fetchAll() {
		return database.query(DATABASE_TABLE, FIELDS, null, null,
				null, null, null);
	}
	
	private Cursor searchCursor(String parameter){
		String wereClausule = PLACA + " like " + "'%" + parameter + "%'"
				+ " OR " + MODELO + " like " + "'%" + parameter + "%'" + " OR "+
				RENAVAN+" like " + "'%" + parameter + "%'";
		return database.query(DATABASE_TABLE,FIELDS, wereClausule,null, null, null, null);
	}
	
	public List<VeiculoEnvolvido> search(String parameter){
		Cursor cursor = searchCursor(parameter);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	public VeiculoEnvolvido find(String placa){
		Cursor cursor = findCursor(placa);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
		
	}
	
	public VeiculoEnvolvido find(int codigo){
		Cursor cursor = findCursor(codigo);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}
			
			return parseObject(cursor);
		} finally {
			cursor.close();
		}
		
	}
	
	public List<VeiculoEnvolvido> fetchAllParsed(){
		Cursor cursor = fetchAll();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
		
	}
	
	
	private List<VeiculoEnvolvido> parse(Cursor cursor) {
		List<VeiculoEnvolvido> toReturn = new ArrayList<VeiculoEnvolvido>();
		if(cursor !=null){
			if(cursor.moveToFirst()){
				VeiculoEnvolvido obj;
				while(!cursor.isAfterLast()){
					obj = parseObject(cursor);
					toReturn.add(obj);
					cursor.moveToNext();
				}
			}
		}
		return toReturn;
	}

	private VeiculoEnvolvido parseObject(Cursor cursor) {
		VeiculoEnvolvido obj = null;
		obj = new VeiculoEnvolvido();
		obj.set_id(cursor.getInt(0));
		obj.setId(DatabaseUtil.returnNullValueForInt(cursor,ID));
		obj.setPlaca(cursor.getString(2));
		obj.setModelo(cursor.getString(3));
		obj.setRenavam(cursor.getString(4));
		obj.setAno(cursor.getString(5));
		obj.setMarca(cursor.getString(6));
		obj.setChassi(cursor.getString(7));
		return obj;
	}
	
	private ContentValues createContentValues(VeiculoEnvolvido obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(PLACA, obj.getPlaca());
		values.put(MODELO, obj.getModelo());
		values.put(RENAVAN, obj.getRenavam());
		values.put(ANO, obj.getAno());
		values.put(MARCA, obj.getMarca());
		values.put(CHASSI, obj.getChassi());
		return values;
	}

	private ContentValues createUpdateContentValues(VeiculoEnvolvido obj) {
		ContentValues values = new ContentValues();
		values.put(PLACA, obj.getPlaca());
		values.put(MODELO, obj.getModelo());
		values.put(RENAVAN, obj.getRenavam());
		values.put(ANO, obj.getAno());
		values.put(MARCA, obj.getMarca());
		values.put(CHASSI, obj.getChassi());
		return values;
	}
}
