package br.com.otgmobile.trackteam.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import br.com.otgmobile.trackteam.R;

public class DBHelper extends SQLiteOpenHelper {
	
	public static final String DATABASE_NAME = "database.db";
	private static final int DATABASE_VERSION = 1;

	private static String[] createTables;
	private static String[] dropTables;
	private final Context context;
	private static DBHelper dbHelper;

	private DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
		getQueryArrays();
	}
	
	public static SQLiteDatabase getDatabase(Context context){
		if(dbHelper == null)
			dbHelper = new DBHelper(context);
		
		return dbHelper.getWritableDatabase();
	}

	private void getQueryArrays() {
		createTables = context.getResources().getStringArray(R.array.create_tables);
		dropTables = context.getResources().getStringArray(R.array.drop_tables);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		createTables(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		dropTables(db);
		createTables(db);
		
	}
	
	
	private void createTables(SQLiteDatabase db) {
		db.beginTransaction();
		try {
			for (String createTable : createTables) {
				db.execSQL(createTable);
			}
			db.setTransactionSuccessful();
		} catch (final SQLException e) {
			Log.e("DB_ERROR", "Error creating database. " + e);
		} finally {
			db.endTransaction();
		}
	}

	private void dropTables(SQLiteDatabase db) {
		db.beginTransaction();
		try {
			for (String dropTable : dropTables) {
				db.execSQL(dropTable);
			}
			db.setTransactionSuccessful();
		} catch (final SQLException e) {
			Log.e("DB_ERROR", "Error dropping tables. " + e);
		} finally {
			db.endTransaction();
		}
	}

}
