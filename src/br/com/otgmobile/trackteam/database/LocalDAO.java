package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.Local;


public class LocalDAO {
	// Database fields
	public static final String _ID = "_id";
	public static final String ID = "id";
	public static final String MUNICIPIO ="municipio";
	public static final String UF ="uf";
	
	
	private static final String DATABASE_TABLE = "local";
	private SQLiteDatabase database;
	
	public LocalDAO(Context context){
		database = DBHelper.getDatabase(context);
	}
	
	
	public long save(Local obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = update(obj);
		}

		return inserted;
	}

	private boolean isNew(Local obj) {
		if(find(obj.getId()) == null)
			return true;
		return false;
	}
	
	public long create(Local obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}
	
	public long update(Local obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, ID+ " = ?",new String[] { obj.getId().toString()});
	}
	
	public boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, ID + "=" + id, null) > 0;
	}
	
	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE,null,null) > 0;
	}
		
	private Cursor findCursor(int codigo){
		return database.query(DATABASE_TABLE, new String[] { _ID,ID,MUNICIPIO,UF}, ID+ " = ?", new String[] { codigo + "" }, null, null, null);
	}
	
	public Local find(int id){
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}
			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}
	
	
	public Cursor fetchAll() {
		return database.query(DATABASE_TABLE, new String[] { _ID,ID,MUNICIPIO,UF }, null, null,
				null, null, null);
	}
	
	public List<Local> fetchAllParsed(){
		Cursor cursor = fetchAll();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	private List<Local> parse(Cursor cursor){
		List<Local>  toReturn= null;
		if(cursor != null){
			toReturn = new ArrayList<Local>();
			Local obj;
			if(cursor.moveToFirst()){
				while(!cursor.isAfterLast()){
					obj = parseObject(cursor);
					toReturn.add(obj);
					cursor.moveToNext();
				}
			}
		}
		
		
		return toReturn; 
	}
	
	private Local parseObject(Cursor cursor){
		Local obj = null;
		obj = new Local();
		obj.set_id(cursor.getInt(0));
		obj.setId(cursor.getInt(1));
		obj.setMunicipio(cursor.getString(2));
		obj.setUf(cursor.getString(3));
		return obj;
	}
	
	private ContentValues createContentValues(Local obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(MUNICIPIO, obj.getMunicipio());
		values.put(UF, obj.getUf());
		return values;
	}
	
	private ContentValues createUpdateContentValues(Local obj) {
		ContentValues values = new ContentValues();
		values.put(MUNICIPIO, obj.getMunicipio());
		values.put(UF, obj.getUf());
		return values;
	}
}
