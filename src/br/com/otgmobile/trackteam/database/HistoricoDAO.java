package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.Agente;
import br.com.otgmobile.trackteam.model.Historico;
import br.com.otgmobile.trackteam.model.Veiculo;


public class HistoricoDAO {
	
	// Database fields
	public static final String _ID = "_id";
	public static final String ID = "id";
	public static final String DATAHORA ="data_hora";
	public static final String LATITUDE ="latitude";
	public static final String LONGITUDE ="longitude";
	public static final String TEMPERATURA ="temperatura";
	public static final String VELOCIDADE ="velocidade";
	public static final String RPM ="rpm";
	public static final String TENSAO_BATERIA ="tensao_bateria";
	public static final String ENVIADO = "enviado";
	public static final String VEICULOID ="veiculo_id";
	public static final String AGENTEID ="veiculo_id";
	public static final String[] COLUMNS = new String[] { _ID,ID,DATAHORA,LATITUDE,LONGITUDE,TEMPERATURA,VELOCIDADE,RPM,TENSAO_BATERIA,ENVIADO,VEICULOID,AGENTEID };
	
	
	private static final String DATABASE_TABLE = "historico";
	private SQLiteDatabase database;
	
	public HistoricoDAO(Context context){
		database = DBHelper.getDatabase(context);
	}
	
	public long save(Historico obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = update(obj);
		}

		return inserted;
	}
	
	public long saveFromNode(Historico obj){
		if(obj.getVeiculoID() == null && obj.getAgenteID() == null)
			return 0;
		
		if(obj.getVeiculoID() != null){
			Veiculo veiculo = new Veiculo();
			veiculo.setId(obj.getVeiculoID());
			if(findByVehicle(veiculo) != null)
				return updateByVehicle(obj);
		}else if(obj.getAgenteID() != null){
			Agente agente = new Agente();
			agente.setId(obj.getAgenteID());
			if(findByAgent(agente) != null)
				return updateByAgent(obj);
		}

		return create(obj);
	}

	

	private boolean isNew(Historico obj) {
		if(obj.get_id() == null || find(obj.get_id()) == null)
			return true;
		return false;
	}
	
	public long create(Historico obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}
	
	public long update(Historico obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, _ID + " = ?",new String[] { obj.getId().toString()});
	}
	
	private long updateByVehicle(Historico obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, VEICULOID + " = ?",new String[] { obj.getVeiculoID().toString()});
	}
	
	private long updateByAgent(Historico obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, AGENTEID + " = ?",new String[] { obj.getAgenteID().toString()});
	}
	
	public void updateSentStatus(final List<Integer> ids, final boolean status) {
		ContentValues cv = new ContentValues();
		cv.put(ENVIADO, status);
		final String whereClasule = _ID + " in (" + DatabaseUtil.concatValuesToUseInCondition(ids.toArray()) + ")";
		database.update(DATABASE_TABLE, cv, whereClasule, null);
	}
	
	public boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, _ID + "=" + id, null) > 0;
	}
	
	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE,null,null) > 0;
	}
		
	private Cursor findCursor(int codigo){
		return database.query(DATABASE_TABLE, COLUMNS, _ID+ " = ?", new String[] { codigo + "" }, null, null, null);
	}
	
	public Historico find(int id){
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}
			
			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}
	
	
	public Historico findByVehicle(Veiculo veiculo){
		if(veiculo == null || veiculo.getId() == null)
			return null;
		
		Cursor cursor = findCursor(veiculo);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}
			
			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}
	
	public Historico findByAgent(Agente agente){
		if(agente == null || agente.getId() == null)
			return null;
		
		Cursor cursor = findCursor(agente);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}
			
			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}
	
	public Historico retrieveLastSelfHistory(){
		Cursor cursor = retrieveLastSelfHistoryCursor();
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}
			
			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}
	
	private Cursor findCursor(Veiculo veiculo) {
		return database.query(DATABASE_TABLE, COLUMNS,
				VEICULOID + " = ?", new String[] { veiculo.getId().toString()}, null, null, DATAHORA + " DESC");
	}
	
	private Cursor findCursor(Agente agente) {
		return database.query(DATABASE_TABLE, COLUMNS,
				AGENTEID + " = ?", new String[] { agente.getId().toString()}, null, null, DATAHORA + " DESC");
	}

	public List<Historico> retrieveAllPending() {
		Cursor cursor = retrieveCursorAllPending();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	public Cursor retrieveCursorAllPending() {
		final String whereClasule = ENVIADO + " = 0 and " + ID + " is null";
		return database.query(DATABASE_TABLE, COLUMNS, whereClasule, null, null, null, null, "200");
	}
	
	public Cursor retrieveLastSelfHistoryCursor() {
		final String whereClasule = ID + " is null";
		return database.query(DATABASE_TABLE, COLUMNS, whereClasule, null, null, null, DATAHORA + " DESC");
	}
	
	public Cursor fetchAll() {
		return database.query(DATABASE_TABLE, COLUMNS, null, null, null, null, null);
	}
	
	public List<Historico> fetchAllParsed(){
		Cursor cursor = fetchAll();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	private List<Historico> parse(Cursor cursor){
		if ( cursor.moveToFirst() ) {
			List<Historico> toReturn = new ArrayList<Historico>();;
			while ( !cursor.isAfterLast() ) {
				toReturn.add(parseObject(cursor));
				cursor.moveToNext();
			}
			
			return toReturn;
		}

		return Collections.emptyList(); 
	}
	
	private Historico parseObject(Cursor cursor){
		Historico obj = new Historico();
		obj.set_id(cursor.getInt(0));
		obj.setId(DatabaseUtil.returnNullValueForInt(cursor, ID));
		obj.setDataHora(cursor.getLong(2));
		obj.setLatitude(cursor.getFloat(3));
		obj.setLongitude(cursor.getFloat(4));
		obj.setTemperatura(cursor.getString(5));
		obj.setVelocidade(cursor.getString(6));
		obj.setRpm(cursor.getString(7));
		obj.setTensaoBateria(cursor.getString(8));
		obj.setEnviado(cursor.getInt(9)>0);
		obj.setVeiculoID(DatabaseUtil.returnNullValueForInt(cursor, VEICULOID));
		obj.setAgenteID(DatabaseUtil.returnNullValueForInt(cursor, AGENTEID));
		return obj;
	}
	
	private ContentValues createContentValues(Historico obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(DATAHORA, obj.getDataHora());
		values.put(LATITUDE, obj.getLatitude());
		values.put(LONGITUDE, obj.getLongitude());
		values.put(TEMPERATURA, obj.getTemperatura());
		values.put(VELOCIDADE, obj.getVelocidade());
		values.put(RPM, obj.getRpm());
		values.put(TENSAO_BATERIA, obj.getTensaoBateria());
		values.put(ENVIADO, obj.isEnviado());
		values.put(VEICULOID, obj.getVeiculoID());
		values.put(AGENTEID, obj.getAgenteID());
		return values;
	}
	
	private ContentValues createUpdateContentValues(Historico obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(DATAHORA, obj.getDataHora());
		values.put(LATITUDE, obj.getLatitude());
		values.put(LONGITUDE, obj.getLongitude());
		values.put(TEMPERATURA, obj.getTemperatura());
		values.put(VELOCIDADE, obj.getVelocidade());
		values.put(RPM, obj.getRpm());
		values.put(TENSAO_BATERIA, obj.getTensaoBateria());
		values.put(ENVIADO, obj.isEnviado());
		values.put(VEICULOID, obj.getVeiculoID());
		values.put(AGENTEID, obj.getAgenteID());
		return values;
	}

	public void delete(List<Integer> sentIds, boolean b) {
		if(sentIds == null)
			return;
		final String whereClasule = _ID + " in (" + DatabaseUtil.concatValuesToUseInCondition(sentIds.toArray()) + ")";
		database.delete(DATABASE_TABLE, whereClasule, null);
	}

}
