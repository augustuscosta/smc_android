package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import br.com.otgmobile.trackteam.model.Sign;

public class SignDAO implements BaseColumns{

	// Database fields
	public static final String ID = "id";
	public static final String IMAGE = "image";
	public static final String OCORRENCIA_ID = "ocorrencia_id";
	public static final String OCORRENCIA_SERVER_ID = "ocorrencia_server_id";
	public static final String ENVIADO = "enviado";
	
	
	private static final String[] FIELDS = {_ID,ID,IMAGE,OCORRENCIA_ID,OCORRENCIA_SERVER_ID,ENVIADO};
	private static final String DATABASE_TABLE = "assinatura_ocorrencia";
	private SQLiteDatabase database;
	private Context context;

	public SignDAO(Context context) {
		this.context = context;
		database = DBHelper.getDatabase(context);
	}

	public long save(Sign obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = update(obj);
		}

		return inserted;
	}

	private boolean isNew(Sign obj) {
		if(obj.get_id() == null || find(obj.get_id()) == null)
			return true;
		return false;
	}

	public long create(Sign obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	public long update(Sign obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, _ID + " = " +obj.get_id(),null);
	}

	public boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, _ID + "=" + id, null) > 0;
	}
	
	public void deleteSignFile(Sign sign){
		context.deleteFile(sign.getImagem());
		delete(sign.get_id());
	}

	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE, null, null) > 0;
	}

	private Cursor findCursor(int codigo) {
		return database.query(DATABASE_TABLE,FIELDS, _ID + " = ?", new String[] { codigo + "" }, null,
				null, null);
	}
	
	private Cursor findCursorFromOcorrencia(int codigo) {
		return database.query(DATABASE_TABLE, FIELDS,OCORRENCIA_ID  + " = ?", new String[] { codigo + "" }, null,
				null, null);
	}
	
	private Cursor findCursorFromOcorrencianotSent(int codigo) {
		String whereCaluse = OCORRENCIA_ID +" = "+ codigo +" AND "+" (" + ENVIADO + " = 0 OR " + ENVIADO + " is null)";
		return database.query(DATABASE_TABLE, FIELDS,whereCaluse,null, null,null, null);
	}
	
	public void deleteSignFilesFromOcorrencia(int codigo){
		List<Sign> signs = findFromOcorrencia(codigo);
		if(signs!=null && !signs.isEmpty()){
			for(Sign sign:signs){
				deleteSignFile(sign);
			}
		}
	}

	public Sign find(int id) {
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}

	
	public List<Sign> findFromOcorrencia(int id) {
		Cursor cursor = findCursorFromOcorrencia(id);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	public List<Sign> findFromOcorrenciaNotSent(int id) {
		Cursor cursor = findCursorFromOcorrencianotSent(id);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	public Cursor fetchAll() {
		return database.query(DATABASE_TABLE, new String[] { _ID,ID, IMAGE,
				OCORRENCIA_ID }, null, null, null, null, null);
	}

	public List<Sign> fetchAllParsed() {
		Cursor cursor = fetchAll();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}

	private List<Sign> parse(Cursor cursor) {
		List<Sign> toReturn = null;
		if (cursor != null) {
			toReturn = new ArrayList<Sign>();
			Sign obj;
			if (cursor.moveToFirst()) {
				while (!cursor.isAfterLast()) {
					obj = parseObject(cursor);
					toReturn.add(obj);
					cursor.moveToNext();
				}
			}
		}

		return toReturn;
	}

	private Sign parseObject(Cursor cursor) {
		Sign obj = null;
		obj = new Sign();
		obj.set_id(cursor.getInt(0));
		obj.setId(cursor.getInt(1));
		obj.setImagem(cursor.getString(2));
		obj.setOcorrenciaId(cursor.getInt(3));
		obj.setOcorrenciaServerId(cursor.getInt(4));
		obj.setEnviado(cursor.getLong(5));
		return obj;
	}

	private ContentValues createContentValues(Sign obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(IMAGE, obj.getImagem());
		values.put(OCORRENCIA_ID, obj.getOcorrenciaId());
		values.put(OCORRENCIA_SERVER_ID, obj.getOcorrenciaServerId());
		values.put(ENVIADO, obj.getEnviado());
		return values;
	}

	private ContentValues createUpdateContentValues(Sign obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(IMAGE, obj.getImagem());
		values.put(OCORRENCIA_ID, obj.getOcorrenciaId());
		values.put(OCORRENCIA_SERVER_ID, obj.getOcorrenciaServerId());
		values.put(ENVIADO, obj.getEnviado());
		return values;
	}
}
