package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.Rota;

public class RotaDAO {

	// Database fields
	public static final String _ID = "_id";
	public static final String ID = "id";
	public static final String DESCRICAO ="descricao";
	public static final String HORAINICIO ="hora_inicio";
	public static final String HORAFIM ="hora_fim";
	public static final String VEICULOID ="veiculo_id";
	private static final String[] COLUMNS = new String[] { _ID,ID,DESCRICAO,HORAINICIO,HORAFIM,VEICULOID };

	
	private static final String DATABASE_TABLE = "rota";
	private SQLiteDatabase database;
	private GeoPontoDAO geoPontoDAO;
	private Context context;
	
	public RotaDAO(Context context){
		database = DBHelper.getDatabase(context);
		this.context = context;
	}
	
	
	public long save(Rota obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = update(obj);
		}
		
		if ( inserted > 0 ) {
			geoPontoDAO().deleteGeoPontoFromRota(obj.getId());
			geoPontoDAO().save(null, obj.getId(), obj.getGeoPonto());
		}

		return inserted;
	}

	private boolean isNew(Rota obj) {
		if(find(obj.getId()) == null)
			return true;
		return false;
	}
	
	public long create(Rota obj) {
		ContentValues	initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}
	
	public long update(Rota obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, ID + " = ?",new String[] { obj.getId().toString()});
	}
	
	public boolean delete(final int id) {
		geoPontoDAO().deleteGeoPontoFromRota(id);
		return database.delete(DATABASE_TABLE, ID + "=" + id, null) > 0;
	}
	
	public boolean deleteAll() {
		geoPontoDAO().deleteAllGeoPontoFromRota();
		return database.delete(DATABASE_TABLE,null,null) > 0;
	}
		
	public Cursor findCursor(final Integer codigo){
		// Se codigo(id) for igual a nulo clasula where nao ira existir retornando todos.
		String whereClasule = codigo == null ? null : ID + " = " + codigo; 
		return database.query(DATABASE_TABLE, COLUMNS, whereClasule, null, null, null, null);
	}
	
	public Rota find(int id){
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}
	
	public List<Rota> fetchAllParsed(){
		Cursor cursor = findCursor(null);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	private List<Rota> parse(Cursor cursor){
		List<Rota>  toReturn = new ArrayList<Rota>();
		if(cursor.moveToFirst()){
			while(!cursor.isAfterLast()){
				toReturn.add(parseObject(cursor));
				cursor.moveToNext();
			}
		}
		
		return toReturn; 
	}
	
	private Rota parseObject(Cursor cursor){
		Rota rota = new Rota();
		rota.set_id(cursor.getInt(0));
		rota.setId(cursor.getInt(1));
		rota.setDescricao(cursor.getString(2));
		rota.setHoraInicio(cursor.getLong(3));
		rota.setHoraFim(cursor.getLong(4));
		rota.setVeiculoID(cursor.getInt(5));
		rota.setGeoPonto(geoPontoDAO().findGeoPontoFromRota(rota.getId()));
		return rota;
	}
	
	private ContentValues createContentValues(Rota obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(DESCRICAO, obj.getDescricao());
		values.put(HORAINICIO, obj.getHoraInicio());
		values.put(HORAFIM, obj.getHoraFim());
		values.put(VEICULOID, obj.getVeiculoID());
		return values;
	}
	
	private ContentValues createUpdateContentValues(Rota obj) {
		ContentValues values = new ContentValues();
		values.put(DESCRICAO, obj.getDescricao());
		values.put(HORAINICIO, obj.getHoraInicio());
		values.put(HORAFIM, obj.getHoraFim());
		values.put(VEICULOID, obj.getVeiculoID());
		return values;
	}

	private GeoPontoDAO geoPontoDAO() {
		if ( geoPontoDAO == null ) {
			geoPontoDAO = new GeoPontoDAO(context);
		}
		
		return geoPontoDAO;
	}
	
}
