package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.GeoPonto;


public class GeoPontoDAO {

	// Database fields
	public static final String _ID = "_id";
	public static final String ID = "id";
	public static final String LATITUDE ="latitude";
	public static final String LONGITUDE ="longitude";
	public static final String ROTAID ="rota_id";
	public static final String CERCAID ="cerca_id";
	private static final String[] COLUMNS = new String[] { _ID,ID,LATITUDE,LONGITUDE,ROTAID,CERCAID };



	private static final String DATABASE_TABLE = "geoponto";
	private SQLiteDatabase database;

	public GeoPontoDAO(Context context){
		database = DBHelper.getDatabase(context);
	}

	public long save(GeoPonto obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = update(obj);
		}

		return inserted;
	}

	private boolean isNew(GeoPonto obj) {
		if(find(obj.getId()) == null)
			return true;
		return false;
	}

	/**
	 * Cria ou atualiza GeoPontos, para uma Cerca ou Rota.
	 * 
	 * OBS: Se quiser vincular GeoPonto a uma rota passe o rotaID e null para cercaID.
	 * 
	 * @param cercaID - vincula GeoPonto ao uma cerca.
	 * @param rotaID - vincula GeoPonto a uma rota
	 * @param geoPontos
	 */
	public void save(final Integer cercaID, final Integer rotaID, List<GeoPonto> geoPontos) {
		if ( geoPontos == null ) {
			return;
		}

		for (GeoPonto geoPonto : geoPontos) {
			geoPonto.setCercaID(cercaID);
			geoPonto.setRotaID(rotaID);
			save(geoPonto);
		}
	}

	public long create(GeoPonto obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	public long update(GeoPonto obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, ID+ " = ?",new String[] { obj.getId().toString()});
	}

	public boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, ID + "=" + id, null) > 0;
	}

	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE,null,null) > 0;
	}

	public void deleteGeoPontoFromCerca(final int cercaID) {
		String whereClause = CERCAID + " = " + cercaID;
		database.delete(DATABASE_TABLE, whereClause, null);
	}

	public void deleteAllGeoPontoFromCerca() {
		String whereClause = CERCAID + " is not null";
		database.delete(DATABASE_TABLE, whereClause, null);
	}

	public void deleteGeoPontoFromRota(final int rotaID) {
		String whereClause = ROTAID + " = " + rotaID;
		database.delete(DATABASE_TABLE, whereClause, null);
	}

	public void deleteAllGeoPontoFromRota() {
		String whereClause = ROTAID+ " is not null";
		database.delete(DATABASE_TABLE, whereClause, null);
	}

	public Cursor findCursor(final Integer codigo) {
		// Se codigo(id) for igual a nulo clasula where nao ira existir retornando todos.
		String whereClasule = codigo == null ? null : ID + " = " + codigo; 
		return database.query(DATABASE_TABLE, COLUMNS, whereClasule, null, null, null, null);
	}

	public GeoPonto find(int id) {
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}

	public List<GeoPonto> findGeoPontoFromCerca(final int cercaID) {
		Cursor cursor = database.query(DATABASE_TABLE, COLUMNS, CERCAID + " = " + cercaID, null, null, null, null);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}

	public List<GeoPonto> findGeoPontoFromRota(final int rotaID) {
		Cursor cursor = database.query(DATABASE_TABLE, COLUMNS, ROTAID + " = " + rotaID, null, null, null, null);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}

	public List<GeoPonto> fetchAllParsed() {
		Cursor cursor = findCursor(null);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}

	private List<GeoPonto> parse(Cursor cursor) {
		List<GeoPonto> toReturn = new ArrayList<GeoPonto>();
		if ( cursor.moveToFirst() ) {
			while(!cursor.isAfterLast()){
				toReturn.add(parseObject(cursor));
				cursor.moveToNext();
			}
		}

		return toReturn; 
	}

	private GeoPonto parseObject(Cursor cursor){
		GeoPonto obj = new GeoPonto();
		obj.set_id(cursor.getInt(0));
		obj.setId(cursor.getInt(1));
		obj.setLatitude(cursor.getFloat(2));
		obj.setLongitude(cursor.getFloat(3));
		obj.setRotaID(cursor.getInt(4));
		obj.setCercaID(cursor.getInt(5));
		return obj;
	}

	private ContentValues createContentValues(GeoPonto obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(LATITUDE, obj.getLatitude());
		values.put(LONGITUDE, obj.getLongitude());
		values.put(ROTAID, obj.getRotaID());
		values.put(CERCAID, obj.getCercaID());
		return values;
	}

	private ContentValues createUpdateContentValues(GeoPonto obj) {
		ContentValues values = new ContentValues();
		values.put(LATITUDE, obj.getLatitude());
		values.put(LONGITUDE, obj.getLongitude());
		values.put(ROTAID, obj.getRotaID());
		values.put(CERCAID, obj.getCercaID());
		return values;
	}

}
