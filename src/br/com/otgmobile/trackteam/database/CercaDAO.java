package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.Cerca;

public class CercaDAO {
	
	// Database fields
	public static final String _ID = "_id";
	public static final String ID = "id";
	public static final String CERCACOL = "cercacol";
	private static final String[] COLUMNS = new String[] { _ID, ID, CERCACOL };
	
	public static final String DATABASE_TABLE = "cerca";
	
	private SQLiteDatabase database;
	private Context context;
	private GeoPontoDAO geoPontoDAO;
	
	public CercaDAO(Context context){
		database = DBHelper.getDatabase(context);
		this.context = context;
	}
	
	public long save(Cerca obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = update(obj);
		}
		
		if ( inserted > 0 ) {
			geoPontoDAO().deleteGeoPontoFromCerca(obj.getId());
			geoPontoDAO().save(obj.getId(), null, obj.getGeoPonto());
		}

		return inserted;
	}

	private boolean isNew(Cerca obj) {
		if(find(obj.getId()) == null)
			return true;
		return false;
	}
	
	public long create(Cerca obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}
	
	public long update(Cerca obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, ID + " = ?",new String[] { obj.getId().toString()});
	}
	
	public boolean delete(final int id) {
		geoPontoDAO().deleteGeoPontoFromCerca(id);
		return database.delete(DATABASE_TABLE, ID + " = " + id, null) > 0;
	}
	
	public boolean deleteAll() {
		geoPontoDAO().deleteAllGeoPontoFromCerca();
		return database.delete(DATABASE_TABLE,null,null) > 0;
	}
		
	public Cursor findCursor(final Integer codigo) {
		// Se codigo(id) for igual a nulo clasula where nao ira existir retornando todos.
		String whereClasule = codigo == null ? null : ID + " = " + codigo; 
		return database.query(DATABASE_TABLE, COLUMNS, whereClasule, null, null, null, null);
	}

	public Cerca find(int id){
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}
		
	public List<Cerca> fetchAllParsed(){
		Cursor cursor = findCursor(null);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	private List<Cerca> parse(Cursor cursor){
		List<Cerca> toReturn = new ArrayList<Cerca>();
		if(cursor.moveToFirst()){
			while(!cursor.isAfterLast()){
				toReturn.add(parseObject(cursor));
				cursor.moveToNext();
			}
		}

		return toReturn; 
	}
	
	private Cerca parseObject(Cursor cursor){
		Cerca obj = new Cerca();
		obj.set_id(cursor.getInt(0));
		obj.setId(cursor.getInt(1));
		obj.setCercacol(cursor.getString(2));
		obj.setGeoPonto(geoPontoDAO().findGeoPontoFromCerca(obj.getId()));
		return obj;
	}
	
	private ContentValues createContentValues(Cerca obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(CERCACOL, obj.getCercacol());
		return values;
	}
	
	private ContentValues createUpdateContentValues(Cerca obj) {
		ContentValues values = new ContentValues();
		values.put(CERCACOL, obj.getCercacol());
		return values;
	}
	
	private GeoPontoDAO geoPontoDAO() {
		if ( geoPontoDAO == null ) {
			geoPontoDAO = new GeoPontoDAO(context);
		}
		
		return geoPontoDAO;
	}

}
