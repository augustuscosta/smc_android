package br.com.otgmobile.trackteam.database;



import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.Mensagem;

public class MensagemDAO {

	public static final String _ID = "_id";
	public static final String FROM = "tokenfrom";
	public static final String TO = "tokento";
	public static final String MENSAGEM = "mensagem";
	public static final String DATA_MENSAGEM = "data_mensagem";
	public static final String ENVIADO = "enviado";
	private static final String[] COLUMNS = new String[] { _ID, FROM,TO, MENSAGEM, DATA_MENSAGEM,ENVIADO };

	private static final String DATABASE_TABLE ="mensagens";
	private SQLiteDatabase database;

	public MensagemDAO(Context context) {
		database = DBHelper.getDatabase(context);		
	}

	public long save(Mensagem obj){
		long inserted = 0;
		if(notNew(obj)){
			inserted = update(obj);
		}else{
			inserted = create(obj);
		}
		
		return inserted;
	}
	
	private long update(Mensagem obj) {
		String whereClasule =  FROM+ " = " + "'"+obj.getFrom()+"'" 
				+" AND "+ TO +" = " + "'"+obj.getTo()+"' AND " + DATA_MENSAGEM + 
				" = " +obj.getDataMensagem(); 
		ContentValues values = createContentValues(obj);
		return database.update(DATABASE_TABLE, values, whereClasule, null);
	}

	private boolean notNew(final Mensagem obj) {
		if(findtoUpdate(obj) == null){
			return false;			
		}
		return true;
	}

	public long create(Mensagem obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	private Cursor findCursor(final String token) {
		String whereClasule =  FROM+ " = " + "'"+token+"'" +" OR "+ TO +" = " + "'"+token+"'"; 
		return database.query(DATABASE_TABLE, COLUMNS, whereClasule, null, null, null, _ID);
	}
	
	private Cursor findToUpdateCursor(final Mensagem obj) {
		String whereClasule =  FROM+ " = " + "'"+obj.getFrom()+"'" 
				+" AND "+ TO +" = " + "'"+obj.getTo()+"' AND " + DATA_MENSAGEM + 
				" = " +" "+obj.getDataMensagem(); 
		return database.query(DATABASE_TABLE, COLUMNS, whereClasule, null, null, null, _ID);
	}
	
	private Cursor findNotSentCursor() {
		String whereClasule =  ENVIADO +" < 1"; 
		return database.query(DATABASE_TABLE, COLUMNS, whereClasule, null, null, null, _ID);
	}

	private Cursor fetchAll() {
		return database.query(DATABASE_TABLE, COLUMNS, null, null, null, null, null);
	}

	public  boolean deleteMessagesFromOperator(final String token) {
		String whereClasule =  FROM+ " = " + "'"+token+"'" +" OR "+ TO +" = " + "'"+token+"'"; 
		return database.delete(DATABASE_TABLE, whereClasule, null) > 0;
	}

	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE, null, null) > 0;
	}
	

	public List<Mensagem> findMensagens(final String token){
		Cursor cursor = findCursor(token);
		List<Mensagem> toReturn = null;
		if(cursor.moveToFirst()){
			toReturn = new ArrayList<Mensagem>();
			while(!cursor.isAfterLast()){
				toReturn.add(parseMensagem(cursor));
				cursor.moveToNext();
			}
		}
		cursor.close();
		return toReturn;
	}
	
	public List<Mensagem> findMensagensNotSent(){
		Cursor cursor = findNotSentCursor();
		List<Mensagem> toReturn = null;
		if(cursor.moveToFirst()){
			toReturn = new ArrayList<Mensagem>();
			while(!cursor.isAfterLast()){
				toReturn.add(parseMensagem(cursor));
				cursor.moveToNext();
			}
		}
		cursor.close();
		return toReturn;
	}

	public List<Mensagem> fetchAllParsed(){
		Cursor cursor = fetchAll();
		List<Mensagem> toReturn = null;
		if(cursor.moveToFirst()){
			toReturn = new ArrayList<Mensagem>();
			while(!cursor.isAfterLast()){
				toReturn.add(parseMensagem(cursor));
				cursor.moveToNext();
			}
		}
		cursor.close();
		return toReturn;
	}
	
	public Mensagem findtoUpdate(Mensagem obj){
		Cursor cursor = findToUpdateCursor(obj);
		Mensagem toReturn = null;
		try{
			if(cursor.moveToFirst()){
				toReturn = parseMensagem(cursor);
			}
			
			return toReturn;
			
		}finally{
			cursor.close();
		}
	}

	private Mensagem parseMensagem(Cursor cursor) {
		Mensagem obj = new Mensagem();
		obj.setFrom(cursor.getString(1));
		obj.setTo(cursor.getString(2));
		obj.setMensagem(cursor.getString(3));
		obj.setDataMensagem(DatabaseUtil.returnNullValueForLong(cursor, DATA_MENSAGEM));
		obj.setEnviado(cursor.getInt(5) > 0);
		return obj;
	}

	private ContentValues createContentValues(Mensagem obj) {
		ContentValues values = new ContentValues();
		values.put(FROM, obj.getFrom());
		values.put(TO, obj.getTo());
		values.put(MENSAGEM,obj.getMensagem());
		values.put(DATA_MENSAGEM,obj.getDataMensagem());
		values.put(ENVIADO,obj.isEnviado());
		return values;
	}

}
