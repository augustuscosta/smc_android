package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.Comentario;

public class ComentarioDAO {
	
	//Database Fields
	
	private static final String _ID = "_id";
	private static final String ID = "id";
	private static final String OCORRENCIA_ID = "ocorrencia_id";
	private static final String DESCRICAO = "descricao";
	private static final String DATA = "data";
	private static final String[] COLUMNS ={_ID,ID,OCORRENCIA_ID,DESCRICAO,DATA};
	
	public static final String DATABASE_TABLE = "comentario";
	
	private SQLiteDatabase database;
	
	
	public ComentarioDAO(Context context){
		database = DBHelper.getDatabase(context);
	}
	
	public long saveComentario(Comentario obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			if(obj.get_id() == null){
				inserted = update(obj);	
			}
			else{
				inserted = updateInternal(obj);
			}
		}
		
		return inserted;
	}
	
	private boolean isNew(Comentario obj){
		if(obj.get_id() != null){
			return false;
		}
		if(obj.getId() == null){
			return true;
		}
		if(find(obj.getId()) != null){
			return false;
		}
		
		return true;
	}
	
	
	public long create(Comentario obj){
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}
	
	public long update(Comentario obj){
		ContentValues updateValues = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, updateValues, ID+"= ? ", new String[] { obj.getId().toString()});
	}
	
	public long updateInternal(Comentario obj){
		ContentValues updateValues = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, updateValues, _ID+"= ? ", new String[] { obj.get_id().toString()});
	}
	
	public boolean delete(final int id){
		return database.delete(DATABASE_TABLE, ID + " = " + id, null)>0;		
	}
	
	public boolean deleteInternal(final int id){
		return database.delete(DATABASE_TABLE, _ID + " = " + id, null)>0;		
	}
	
	public boolean deleteAll(){
		return database.delete(DATABASE_TABLE,null, null)>0;		
	}
	
	public boolean deleteFromOcorrencia(final int ocorrenciaId){
		return database.delete(DATABASE_TABLE, OCORRENCIA_ID + " = " + ocorrenciaId, null)>0;
	}
	
	public Cursor findCursor(final Integer codigo) {
		String whereClasule = codigo == null ? null : ID + " = " + codigo;
		return database.query(DATABASE_TABLE, COLUMNS, whereClasule, null, null, null, null);
	}
	
	public Cursor findCursorFromOcorrencia(final Integer codigo) {
		String whereClasule = codigo == null ? null : OCORRENCIA_ID + " = " + codigo;
		return database.query(DATABASE_TABLE, COLUMNS, whereClasule, null, null, null, null);
	}
	
	
	
	public Comentario find(final int codigo){
		Cursor cursor = findCursor(codigo);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}

	public List<Comentario> fetchAllParsedFromOcorrencia(final Integer ocorrenciaId){
		if(ocorrenciaId == null){
			return null;
		}
		Cursor cursor = findCursorFromOcorrencia(ocorrenciaId);
		if(cursor == null){
			return null;
		}
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	
	public List<Comentario> fetchAllParsed(){
		Cursor cursor = findCursor(null);
		if(cursor == null){
			return null;
		}
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	private List<Comentario> parse(Cursor cursor) {
		List<Comentario> toReturn = new ArrayList<Comentario>();
		if(cursor.moveToFirst()){
			while(!cursor.isAfterLast()){
				toReturn.add(parseObject(cursor));
				cursor.moveToNext();
			}
		}
		return toReturn;
	}

	private Comentario parseObject(Cursor cursor) {
		if(cursor == null){
			return null;
		}
		Comentario obj = new Comentario();
		obj.set_id(cursor.getInt(0));
		obj.setId(DatabaseUtil.returnNullValueForInt(cursor, ID));
		obj.setOcorrenciaId(DatabaseUtil.returnNullValueForInt(cursor, OCORRENCIA_ID));
		obj.setDescricao(cursor.getString(3));
		obj.setData(cursor.getLong(4));
		return obj;
		
	}

	private ContentValues createContentValues(Comentario obj){
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(OCORRENCIA_ID,obj.getOcorrenciaId());
		values.put(DESCRICAO, obj.getDescricao());
		values.put(DATA,obj.getData());
		return values;
	}
	
	private ContentValues createUpdateContentValues(Comentario obj){
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(OCORRENCIA_ID,obj.getOcorrenciaId());
		values.put(DESCRICAO, obj.getDescricao());
		values.put(DATA,obj.getData());
		return values;
	}
	
	

}
