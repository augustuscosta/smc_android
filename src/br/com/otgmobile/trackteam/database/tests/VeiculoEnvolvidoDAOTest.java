package br.com.otgmobile.trackteam.database.tests;

import java.util.List;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import br.com.otgmobile.trackteam.database.DBHelper;
import br.com.otgmobile.trackteam.database.VeiculoEnvolvidoDAO;
import br.com.otgmobile.trackteam.model.VeiculoEnvolvido;

public class VeiculoEnvolvidoDAOTest extends AndroidTestCase{
	@Override
	protected void setUp() throws Exception {
		DBHelper.getDatabase(getContext());
	}

	@Override
	protected void tearDown() throws Exception {
		
	}
	
	public void testVeiculoEnvolvidoDAO(){
		VeiculoEnvolvidoDAO dao = new VeiculoEnvolvidoDAO(getContext());
		dao.deleteAll();
		List<VeiculoEnvolvido> list = dao.fetchAllParsed();
		Assert.assertNotNull(list);
		Assert.assertTrue(list.isEmpty());
		VeiculoEnvolvido obj;
		for (int i = 0; i < 100; i++) {
			obj = new VeiculoEnvolvido();
			obj.setId(i);
			obj.setPlaca(Integer.toString(i));
			obj.setMarca("Marca" + Integer.toString(i));
			obj.setAno(Integer.toString(i));
			obj.setModelo("modelo" + Integer.toString(i));
			obj.setRenavam(Integer.toString(i));		
			dao.save(obj);
		}
		list = dao.fetchAllParsed();
		Assert.assertTrue(!list.isEmpty());
		obj = dao.find("1");
		Assert.assertNotNull(obj);
		
	}
}
