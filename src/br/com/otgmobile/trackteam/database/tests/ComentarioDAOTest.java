package br.com.otgmobile.trackteam.database.tests;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import br.com.otgmobile.trackteam.database.ComentarioDAO;
import br.com.otgmobile.trackteam.database.DBHelper;
import br.com.otgmobile.trackteam.model.Comentario;

public class ComentarioDAOTest extends AndroidTestCase {
	@Override
	protected void setUp() throws Exception {
		DBHelper.getDatabase(getContext());
	}

	@Override
	protected void tearDown() throws Exception {
		
	}
	
	public void testComentarioDAO(){
		ComentarioDAO dao = new ComentarioDAO(getContext());
		dao.deleteAll();
		List<Comentario> list= new ArrayList<Comentario>();
		Assert.assertNotNull(list);
		Assert.assertTrue(list.isEmpty());
		Comentario obj;
		for (int i = 0; i < 100; i++) {
			obj = new Comentario();
			obj.setId(i);
			obj.setOcorrenciaId(i);
			obj.setDescricao(Integer.toString(i));
			obj.setData(new Date().getTime());
			dao.saveComentario(obj);
		}
		list = dao.fetchAllParsed();
		Assert.assertTrue(!list.isEmpty());
		list = dao.fetchAllParsedFromOcorrencia(1);
		Assert.assertTrue(!list.isEmpty());
		obj = dao.find(1);
		Assert.assertNotNull(obj);
	}
}
