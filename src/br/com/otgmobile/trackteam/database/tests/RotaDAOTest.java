package br.com.otgmobile.trackteam.database.tests;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import br.com.otgmobile.trackteam.database.DBHelper;
import br.com.otgmobile.trackteam.database.RotaDAO;
import br.com.otgmobile.trackteam.model.GeoPonto;
import br.com.otgmobile.trackteam.model.Rota;

public class RotaDAOTest extends AndroidTestCase {

	@Override
	protected void setUp() throws Exception {
		DBHelper.getDatabase(getContext());
	}

	@Override
	protected void tearDown() throws Exception {
		
	}
	
	public void testRota() throws Throwable {
		RotaDAO dao = new RotaDAO(getContext());
		Rota obj = null;
		dao.deleteAll();
		List<Rota> list = dao.fetchAllParsed();
		Assert.assertNotNull(list);
		Assert.assertTrue(list.isEmpty());
		GeoPonto ponto;
		GeoPonto ponto1;
		GeoPonto ponto2;
		obj = new Rota();
		obj.setGeoPonto(new ArrayList<GeoPonto>());
		obj.setId(1);
		obj.setDescricao("descricao" + Integer.toString(1));
		obj.setHoraInicio((long) (1));
		obj.setHoraFim((long) (1));
		obj.setVeiculoID((1));
		ponto = new GeoPonto();
		ponto.setId(4);
		ponto.setLatitude(0.f);
		ponto.setLongitude(0.f);
		obj.getGeoPonto().add(ponto);

		ponto1 = new GeoPonto();
		ponto1.setId(5);
		ponto1.setLatitude(0.f);
		ponto1.setLongitude(0.f);
		obj.getGeoPonto().add(ponto1);

		ponto2 = new GeoPonto();
		ponto2.setId(6);
		ponto2.setLatitude(0.f);
		ponto2.setLongitude(0.f);
		obj.getGeoPonto().add(ponto2);
		dao.save(obj);
		obj = null;
		list = dao.fetchAllParsed();
		Assert.assertTrue(list.size() > 0);
		obj = dao.find(1);
		Assert.assertNotNull(obj);
		Assert.assertNotNull(obj.getGeoPonto());
		Assert.assertTrue(obj.getGeoPonto().size() == 3);
		for(Rota item : list){
			Assert.assertNotNull(item.getGeoPonto());
			Assert.assertTrue(item.getGeoPonto().size() == 3);
		}
		
		obj = dao.find(1);
		obj.getGeoPonto().remove(0);
		dao.save(obj);
		obj = dao.find(1);
		Assert.assertNotNull(obj.getGeoPonto());
		Assert.assertTrue(obj.getGeoPonto().size() == 2);
		
		dao.delete(1);
		obj = dao.find(1);
		Assert.assertNull(obj);
		dao.deleteAll();
	}
}
