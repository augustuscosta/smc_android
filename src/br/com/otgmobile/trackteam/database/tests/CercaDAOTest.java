package br.com.otgmobile.trackteam.database.tests;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import br.com.otgmobile.trackteam.database.CercaDAO;
import br.com.otgmobile.trackteam.database.DBHelper;
import br.com.otgmobile.trackteam.model.Cerca;
import br.com.otgmobile.trackteam.model.GeoPonto;

public class CercaDAOTest extends AndroidTestCase {

	@Override
	protected void setUp() throws Exception {
		DBHelper.getDatabase(getContext());
	}

	@Override
	protected void tearDown() throws Exception {

	}

	public void testCerca() throws Exception {
		CercaDAO cercaDao = new CercaDAO(getContext());
		Cerca cerca = null;
		cercaDao.deleteAll();
		List<Cerca> list = cercaDao.fetchAllParsed();
		GeoPonto ponto;
		GeoPonto ponto1;
		GeoPonto ponto2;
		cerca = new Cerca();
		cerca.setId(1);
		cerca.setCercacol(Integer.toString(1));

		cerca.setGeoPonto(new ArrayList<GeoPonto>());

		ponto = new GeoPonto();
		ponto.setId(1);
		ponto.setLatitude(0.f);
		ponto.setLongitude(0.f);
		cerca.getGeoPonto().add(ponto);

		ponto1 = new GeoPonto();
		ponto1.setId(2);
		ponto1.setLatitude(0.f);
		ponto1.setLongitude(0.f);
		cerca.getGeoPonto().add(ponto1);

		ponto2 = new GeoPonto();
		ponto2.setId(3);
		ponto2.setLatitude(0.f);
		ponto2.setLongitude(0.f);
		cerca.getGeoPonto().add(ponto2);

		long testeDb = cercaDao.save(cerca);
		Assert.assertTrue(testeDb > 0);
		cerca = null;
		list = cercaDao.fetchAllParsed();
		Assert.assertTrue(list.size() > 0);
		cerca = cercaDao.find(1);
		Assert.assertNotNull(cerca);
		Assert.assertNotNull(cerca.getGeoPonto());
		Assert.assertTrue(cerca.getGeoPonto().size() == 3);
		for (Cerca obj : list) {
			Assert.assertNotNull(obj.getGeoPonto());
			Assert.assertTrue(obj.getGeoPonto().size() == 3);
		}

		cerca = cercaDao.find(1);
		cerca.getGeoPonto().remove(0);
		cercaDao.save(cerca);
		cerca = cercaDao.find(1);
		Assert.assertNotNull(cerca.getGeoPonto());
		Assert.assertTrue(cerca.getGeoPonto().size() == 2);

		cercaDao.delete(1);
		cerca = cercaDao.find(1);
		Assert.assertNull(cerca);
		cercaDao.deleteAll();

	}
}
