package br.com.otgmobile.trackteam.database.tests;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import br.com.otgmobile.trackteam.database.DBHelper;
import br.com.otgmobile.trackteam.database.EnderecoDAO;
import br.com.otgmobile.trackteam.database.MaterialDAO;
import br.com.otgmobile.trackteam.database.NaturezaDAO;
import br.com.otgmobile.trackteam.database.NivelEmergenciaDAO;
import br.com.otgmobile.trackteam.database.OcorrenciaDAO;
import br.com.otgmobile.trackteam.database.SolicitanteDAO;
import br.com.otgmobile.trackteam.database.StatusDAO;
import br.com.otgmobile.trackteam.database.VitimaDAO;
import br.com.otgmobile.trackteam.model.Endereco;
import br.com.otgmobile.trackteam.model.Estado;
import br.com.otgmobile.trackteam.model.Natureza;
import br.com.otgmobile.trackteam.model.NivelEmergencia;
import br.com.otgmobile.trackteam.model.Ocorrencia;

public class OcorrenciaDAOTest extends AndroidTestCase {

	@Override
	protected void setUp() throws Exception {
		DBHelper.getDatabase(getContext());
	}

	@Override
	protected void tearDown() throws Exception {

	}

	public void testOcorrencia() throws Throwable {
		OcorrenciaDAO ocorrenciaDAO = new OcorrenciaDAO(getContext());

		// Mandatory
		EnderecoDAO enderecoDAO = new EnderecoDAO(getContext());
		NivelEmergenciaDAO nivelDAO = new NivelEmergenciaDAO(getContext());
		StatusDAO statusDAO = new StatusDAO(getContext());
		NaturezaDAO naturezaDAO = new NaturezaDAO(getContext());

		// Lists
		SolicitanteDAO solicitanteDAO = new SolicitanteDAO(getContext());
		VitimaDAO vitimaDAO = new VitimaDAO(getContext());
		MaterialDAO materialDAO = new MaterialDAO(getContext());

		// clean db
		materialDAO.deleteAll();
		vitimaDAO.deleteAll();
		solicitanteDAO.deleteAll();
		ocorrenciaDAO.deleteAll();
		enderecoDAO.deleteAll();
		nivelDAO.deleteAll();
		statusDAO.deleteAll();
		naturezaDAO.deleteAll();

		Endereco endereco = new Endereco();
		endereco.setBairro("bairro");
		endereco.setEndGeoref("endereço georeferenciado");
		endereco.setId(1);
		endereco.setLatitude(0.f);
		endereco.setLongitude(0.f);
		endereco.setLogradouro("Logradouro");
		endereco.setNumero("000");

		Ocorrencia ocorrencia = new Ocorrencia();
		ocorrencia.setId(1);
		ocorrencia.setResumo("resumo");
		ocorrencia.setDescricao("descricao");

		ocorrencia.setEndereco(endereco);
		ocorrencia.setEnderecoID(1);

		ocorrenciaDAO.save(ocorrencia);

		Assert.assertNotNull(enderecoDAO.find(1));
		Assert.assertNotNull(ocorrenciaDAO.find(1).getEndereco());
		Assert.assertTrue(ocorrenciaDAO.find(1).getEndereco().getId() == 1);
		Assert.assertTrue(ocorrenciaDAO.find(1).getEnderecoID() == 1);

		endereco = new Endereco();
		endereco.setBairro("bairro");
		endereco.setEndGeoref("endereço georeferenciado");
		endereco.setId(2);
		endereco.setLatitude(0.f);
		endereco.setLongitude(0.f);
		endereco.setLogradouro("Logradouro");
		endereco.setNumero("000");

		ocorrencia.setEndereco(endereco);
		ocorrencia.setEnderecoID(2);

		ocorrenciaDAO.save(ocorrencia);

		Assert.assertNotNull(enderecoDAO.find(2));
		Assert.assertNotNull(ocorrenciaDAO.find(1).getEndereco());
		Assert.assertTrue(ocorrenciaDAO.find(1).getEndereco().getId() == 2);
		Assert.assertTrue(ocorrenciaDAO.find(1).getEnderecoID() == 2);

		endereco = new Endereco();
		endereco.setBairro("bairro");
		endereco.setEndGeoref("endereço georeferenciado");
		endereco.setId(1);
		endereco.setLatitude(0.f);
		endereco.setLongitude(0.f);
		endereco.setLogradouro("Logradouro");
		endereco.setNumero("000");

		ocorrencia.setEndereco(endereco);
		ocorrencia.setEnderecoID(1);

		ocorrenciaDAO.save(ocorrencia);

		Assert.assertNotNull(enderecoDAO.find(2));
		Assert.assertNotNull(ocorrenciaDAO.find(1).getEndereco());
		Assert.assertTrue(ocorrenciaDAO.find(1).getEndereco().getId() == 1);
		Assert.assertTrue(ocorrenciaDAO.find(1).getEnderecoID() == 1);

		Estado estado = new Estado();
		estado.setId(1);
		estado.setValor("Estado");
		ocorrencia.setEstado(1);
		ocorrencia.setEstadoObj(estado);
		ocorrenciaDAO.save(ocorrencia);
		Assert.assertNotNull(statusDAO.find(1));
		Assert.assertNotNull(ocorrenciaDAO.find(1).getEstadoObj());

		Natureza natureza = new Natureza();
		natureza.setId(1);
		natureza.setValor("Natureza");
		ocorrencia.setNatureza(1);
		ocorrencia.setNaturezaObj(natureza);
		ocorrenciaDAO.save(ocorrencia);
		Assert.assertNotNull(naturezaDAO.find(1));
		Assert.assertNotNull(ocorrenciaDAO.find(1).getNaturezaObj());

		NivelEmergencia nivel = new NivelEmergencia();
		nivel.setId(1);
		nivel.setCor("4444");
		nivel.setDescricao("Descrição");
		ocorrencia.setNivelEmergencia(nivel);
		ocorrencia.setNivelEmergenciaID(1);
		ocorrenciaDAO.save(ocorrencia);
		Assert.assertNotNull(nivelDAO.find(1));
		Assert.assertNotNull(ocorrenciaDAO.find(1).getNivelEmergencia());

	}

}
