package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import br.com.otgmobile.trackteam.model.NivelEmergencia;


public class NivelEmergenciaDAO {

	// Database fields
	public static final String _ID = "_id";
	public static final String ID = "id";
	public static final String COR ="cor";
	public static final String DESCRICAO ="descricao";
	public static final String[] COLUMNS = new String[] { _ID,ID,COR,DESCRICAO};




	private static final String DATABASE_TABLE = "nivel_emergencia";
	private SQLiteDatabase database;

	public NivelEmergenciaDAO(Context context){
		database = DBHelper.getDatabase(context);
	}

	public long save(NivelEmergencia obj){
		long inserted = 0;
		if(isNew(obj)){
			inserted = create(obj);
		}else{
			inserted = update(obj);
		}
		
		return inserted;
	}

	private boolean isNew(NivelEmergencia obj) {
		if(find(obj.getId()) == null)
			return true;
		return false;
	}

	public long create(NivelEmergencia obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	public long update(NivelEmergencia obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, ID + " = ?",new String[] { obj.getId().toString()});
	}

	public boolean delete(Integer id) {
		return database.delete(DATABASE_TABLE, ID + "=" + id, null) > 0;
	}

	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE,null,null) > 0;
	}

	public Cursor findCursor(final Integer codigo){
		// Se codigo(id) for igual a nulo clasula where nao ira existir retornando todos.
		String whereClasule = codigo == null ? null : ID + " = " + codigo; 
		return database.query(DATABASE_TABLE, COLUMNS, whereClasule, null, null, null, null);
	}

	public NivelEmergencia find(int id){
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if ( cursor.getCount() == 0 ) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}

	public List<NivelEmergencia> fetchAllParsed(){
		Cursor cursor = findCursor(null);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}

	private List<NivelEmergencia> parse(Cursor cursor){
		List<NivelEmergencia> toReturn = new ArrayList<NivelEmergencia>();
		if( cursor.moveToFirst() ) {
			while(!cursor.isAfterLast()){
				toReturn.add(parseObject(cursor));
				cursor.moveToNext();
			}
		}

		return toReturn; 
	}

	private NivelEmergencia parseObject(Cursor cursor){
		NivelEmergencia obj = new NivelEmergencia();
		obj.set_id(cursor.getInt(0));
		obj.setId(cursor.getInt(1));
		obj.setCor(cursor.getString(2));
		obj.setDescricao(cursor.getString(3));
		return obj;
	}

	private ContentValues createContentValues(NivelEmergencia obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(COR, obj.getCor());
		values.put(DESCRICAO, obj.getDescricao());
		return values;
	}

	private ContentValues createUpdateContentValues(NivelEmergencia obj) {
		ContentValues values = new ContentValues();
		values.put(COR, obj.getCor());
		values.put(DESCRICAO, obj.getDescricao());
		return values;
	}

}
