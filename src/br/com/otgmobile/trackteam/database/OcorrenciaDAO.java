package br.com.otgmobile.trackteam.database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import br.com.otgmobile.trackteam.model.Comentario;
import br.com.otgmobile.trackteam.model.Enquete;
import br.com.otgmobile.trackteam.model.Material;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.OcorrenciaEnquete;
import br.com.otgmobile.trackteam.model.OcorrenciaMaterial;
import br.com.otgmobile.trackteam.model.OcorrenciaVeiculo;
import br.com.otgmobile.trackteam.model.Solicitante;
import br.com.otgmobile.trackteam.model.Veiculo;
import br.com.otgmobile.trackteam.model.VeiculoEnvolvido;
import br.com.otgmobile.trackteam.model.Vitima;

public class OcorrenciaDAO implements BaseColumns {

	// Database fields
	public static final String ID = "id";
	public static final String DESCRICAO = "descricao";
	public static final String RESUMO = "resumo";
	public static final String HORACHEGADA = "hora_chegada";
	public static final String HORASAIDA = "hora_saida";
	public static final String INICIO = "inicio";
	public static final String FIM = "fim";
	public static final String MODIFICADO = "modificado";
	public static final String INICIO_PROGRAMADO = "inicio_programado";
	public static final String FIM_PROGRAMADO = "fim_programado";
	public static final String CANCELADO = "cancelado";
	public static final String VALIDADA = "validada";
	public static final String RAZAOCANCELAMENTO = "razao_cancelamento";
	public static final String ENVIADO = "enviado";
	public static final String NIVELEMERGENCIAID = "nivelemergencia_id";
	public static final String ENDERECOID = "endereco_id";
	public static final String NATUREZAID = "natureza_id";
	public static final String USUARIOID = "usuario_id";
	public static final String STATUSID = "status_id";
	public static final String OCORRENCIAID = "ocorrencia_id";
	public static final String MATERIALID = "material_id";
	public static final String VEICULOID = "veiculo_id";
	public static final String ENQUETEID = "enquete_id";
	public static final String SOLICITANTEID = "solicitante_id";
	public static final String RECEBIMENTO =  "recebimento";
	public static final String CODIGO =  "codigo";
	public static final String[] COLUMNS_OCORRENCIA = new String[] { _ID, ID,
																RESUMO, DESCRICAO, 
																HORACHEGADA, HORASAIDA, 
																CANCELADO,RAZAOCANCELAMENTO, 
																ENVIADO, NIVELEMERGENCIAID, 
																ENDERECOID,	NATUREZAID, 
																USUARIOID, STATUSID, 
																INICIO, INICIO_PROGRAMADO, 
																FIM,FIM_PROGRAMADO, 
																MODIFICADO, RECEBIMENTO,
																VALIDADA, CODIGO };

	private static final String DATABASE_TABLE = "ocorrencia";
	private static final String DATABASE_RELATIONSHIP_MATERIAIS = "ocorrencia_materiais";
	private static final String DATABASE_RELATIONSHIP_SOLICITANTES = "ocorrencia_solicitantes";
	private static final String DATABASE_RELATIONSHIP_VEICULOS = "ocorrencia_veiculos";
	private static final String DATABASE_RELATIONSHIP_VEICULOS_ENVOLVIDOS = "ocorrencia_veiculos_envolvidos";
	private static final String DATABASE_RELATIONSHIP_ENQUETE = "ocorrencia_enquetes";
	
	private static final String[] COLUMN_ENQUETE = {_ID, OCORRENCIAID, ENQUETEID};
	private static final String[] COLUMN_VEICULOS = {_ID,OCORRENCIAID,VEICULOID};
	private static final String[] COLUMN_MATERIAIS = {_ID,OCORRENCIAID,MATERIALID};

	private SQLiteDatabase database;
	private Context context;
	private EnderecoDAO enderecoDAO;
	private NivelEmergenciaDAO nivelEmergenciaDAO;
	private NaturezaDAO naturezaDAO;
	private StatusDAO statusDAO;
	private ComentarioDAO comentarioDAO;
	private FotoDAO fotoDAO;
	private SignDAO signDAO;
	private FilmagemDAO filmagemDAO;
	private VeiculoEnvolvidoDAO veiculoEnvolvidoDAO;
	private EnqueteDAO enqueteDAO;
	private MaterialDAO materialDAO;
	private SolicitanteDAO solicitanteDAO;
	private VitimaDAO vitimaDAO;

	public OcorrenciaDAO(Context context) {
		database = DBHelper.getDatabase(context);
		this.context = context;
	}

	public long save(Ocorrencia obj) {

		obj.setEnderecoID((int) saveEndereco(obj));
		saveEstado(obj);
		saveNatureza(obj);
		saveNivelEmergencia(obj);
		

		long inserted = 0;
		if(obj.get_id() != null) {
			updateInternal(obj);
		}else{
			
			if (isNew(obj)) {
				inserted = create(obj);
				obj.set_id((int)inserted);
			}
			
			else {
				inserted = update(obj);
				obj.set_id(find(obj.getId()).get_id());
			}
		}
		

		if (inserted > 0) {
			updateVeiculoReference(obj);
			updateVeiculoEnvolvidoReference(obj);	
			updateEnqueteReference(obj);
			updateMaterialReference(obj);
			saveSolicitantes(obj);
			saveVitimas(obj);
			saveComentarios(obj);
		}

		return inserted;
	}


	private void saveVitimas(final Ocorrencia obj) {
		if(obj.getVitimas() == null || obj.getVitimas().isEmpty()){
			return;
		}
		
		for (Vitima vitima : obj.getVitimas()) {
			vitima.setOcorrenciaID(obj.get_id());
			vitimaDAO().save(vitima);
		}
	}

	private void saveSolicitantes(final Ocorrencia obj) {
		if(obj.getSolicitantes() == null || obj.getSolicitantes().isEmpty()){
			return;
		}
		
		for (Solicitante solicitante : obj.getSolicitantes()) {
			solicitante.setOcorrenciaId(obj.get_id());
			solicitanteDAO().save(solicitante);
			
		}
		
	}

	private void saveComentarios(final Ocorrencia obj) {
		if(obj.getComentarios()==null||obj.getComentarios().isEmpty()){
			return;
		}
		
		for (Comentario comentario : obj.getComentarios()) {
			comentarioDAO().saveComentario(comentario);
		}
	}

	private boolean isNew(final Ocorrencia obj) {
		if (obj.get_id() != null && find(obj.get_id()) != null) {
			return false;
		}
		if (obj.getId() != null && find(obj.getId()) != null) {
			return false;
		}

		return true;
	}

	private void updateVeiculoReference(final Ocorrencia ocorrencia) {
		if (ocorrencia.getId() == null)
			return;
		// deleteAllVeiculosEmAtendimento(ocorrencia);
		if (ocorrencia.getVeiculos() != null) {
			for (Veiculo veiculo : ocorrencia.getVeiculos()) {
				if (veiculo.get_id() != null) {
					createVeiculoReference(ocorrencia, veiculo);
				}
			}
		}
	}
	
	private void updateMaterialReference(final Ocorrencia obj) {
		if(obj.get_id() == null) return;
		deleteAllMateriaisVinculados(obj);
		if(obj.getMateriais() != null && !obj.getMateriais().isEmpty()){
			for (Material material :obj.getMateriais()) {
				materialDAO().save(material);
				createMaterialReference(obj, material.getId());
			}
		}
	}
	
	private void updateVeiculoEnvolvidoReference(final Ocorrencia ocorrencia) {
		if (ocorrencia.get_id() == null)
			return;
		// deleteAllVeiculosEmAtendimento(ocorrencia);
		List<OcorrenciaVeiculo> ocorrenciaVeiculos = getVeiculoEnvolvidoReference(ocorrencia.get_id());
		if (ocorrencia.getVeiculosEnvolvidos() != null) {
			for (VeiculoEnvolvido veiculo : ocorrencia.getVeiculosEnvolvidos()) {
				boolean exist = false;
				veiculo.set_id((int) veiculoEnvolvidoDAO().save(veiculo));
				if(ocorrenciaVeiculos != null && !ocorrenciaVeiculos.isEmpty() ){
					for ( OcorrenciaVeiculo ocorrenciaVeiculo : ocorrenciaVeiculos) {
						if(veiculo.get_id().equals(ocorrenciaVeiculo.getVeiculoId())){
							exist = true;
						}
					}
				}
				if(!exist){
					createVeiculoEnvolvidoReference(ocorrencia, veiculo);
				}
			}
		}
	}
	
	private void updateEnqueteReference(final Ocorrencia ocorrencia){
		if(ocorrencia.get_id() == null)
			return;
		List<OcorrenciaEnquete> ocorrenciaEnquetes = getEnqueteReference(ocorrencia.get_id());
		if(ocorrencia.getEnquetes() != null){
			for(Enquete enquete : ocorrencia.getEnquetes()){
				boolean exist = false;
				enqueteDAO().save(enquete);
				if(ocorrenciaEnquetes != null && !ocorrenciaEnquetes.isEmpty()){
					for( OcorrenciaEnquete ocorrenciaEnquete : ocorrenciaEnquetes){
						if(enquete.getId().equals(ocorrenciaEnquete.getEnqueteId())){
							exist = true;
						}
					}
				}
				if(!exist){
					createEnqueteReference(ocorrencia, enquete);
				}
			}
		}
	}

	private void saveNivelEmergencia(final Ocorrencia ocorrencia) {
		if (ocorrencia.getNivelEmergencia() == null) {
			return;
		}

		ocorrencia.setNivelEmergenciaID(ocorrencia.getNivelEmergencia().getId());
		nivelEmergenciaDAO().save(ocorrencia.getNivelEmergencia());
	}

	private void saveNatureza(final Ocorrencia ocorrencia) {
		if (ocorrencia.getNaturezaObj() == null) {
			return;
		}

		// TODO Um dia no futuro quando tivermos uma conversao dos dados que vem
		// do server poderemos retirar essa linha.
		ocorrencia.setNatureza(ocorrencia.getNaturezaObj().getId());
		naturezaDAO().save(ocorrencia.getNaturezaObj());
	}

	private void saveEstado(final Ocorrencia ocorrencia) {
		if (ocorrencia.getEstadoObj() == null) {
			return;
		}

		// TODO Um dia no futuro quando tivermos uma conversao dos dados que vem
		// do server poderemos retirar essa linha.
		ocorrencia.setEstado(ocorrencia.getEstadoObj().getId());
		statusDAO().save(ocorrencia.getEstadoObj());
	}

	private long saveEndereco(final Ocorrencia ocorrencia) {
		if (ocorrencia.getEndereco() == null) {
			return  -1;
		}

		// TODO Um dia no futuro quando tivermos uma conversao dos dados que vem
		// do server poderemos retirar essa linha.
		ocorrencia.setEnderecoID(ocorrencia.getEndereco().getId());
		return enderecoDAO().save(ocorrencia.getEndereco());
	}

	public long create(final Ocorrencia obj) {
		ContentValues initialValues = createContentValues(obj);
		return database.insert(DATABASE_TABLE, null, initialValues);
	}

	public long updateInternal(final Ocorrencia obj) {
		obj.setEnderecoID((int) saveEndereco(obj));
		saveEstado(obj);
		saveNatureza(obj);
		saveNivelEmergencia(obj);

		ContentValues values = createUpdateContentValues(obj);
		long inserted = database.update(DATABASE_TABLE, values, _ID + " = ?",
				new String[] { obj.get_id().toString() });
		if (inserted > 0) {
			updateVeiculoReference(obj);
			updateVeiculoEnvolvidoReference(obj);
			updateEnqueteReference(obj);
			updateMaterialReference(obj);
		}
		return inserted;
	}

	public long createMaterialReference(final Ocorrencia obj,final int code) {
		ContentValues referenceValues = materialReferenceValues(obj, code);
		return database.insert(DATABASE_RELATIONSHIP_MATERIAIS, null,
				referenceValues);
	}

	public long createVeiculoReference(final Ocorrencia obj,final  Veiculo veiculo) {
		ContentValues referenceValues = veiculoReferenceValues(obj, veiculo);
		return database.insert(DATABASE_RELATIONSHIP_VEICULOS, null,
				referenceValues);
	}
	
	public long createVeiculoEnvolvidoReference(final Ocorrencia obj, final VeiculoEnvolvido veiculo) {
		ContentValues referenceValues = veiculoEnvolvidoReferenceValues(obj, veiculo);
		return database.insert(DATABASE_RELATIONSHIP_VEICULOS_ENVOLVIDOS, null,
				referenceValues);
	}
	public long createEnqueteReference(final Ocorrencia obj, final Enquete enquete) {
		ContentValues referenceValues = enqueteReferenceValues(obj, enquete);
		return database.insert(DATABASE_RELATIONSHIP_ENQUETE, null,
				referenceValues);
	}

	public long createSolicitanteReference(final Ocorrencia obj, final int code) {
		ContentValues referenceValues = solicitanteReferenceValues(obj, code);
		return database.insert(DATABASE_RELATIONSHIP_SOLICITANTES, null,
				referenceValues);
	}

	public long update(final Ocorrencia obj) {
		ContentValues values = createUpdateContentValues(obj);
		return database.update(DATABASE_TABLE, values, ID + " = ?",
				new String[] { obj.getId().toString() });
	}

	public boolean delete(final Integer id) {
		return database.delete(DATABASE_TABLE, _ID + "=" + id, null) > 0;
	}

	public boolean deleteAll() {
		return database.delete(DATABASE_TABLE, null, null) > 0;
	}

	public boolean deleteAllVeiculosEmAtendimento(final Ocorrencia ocorrencia) {
		String where = OCORRENCIAID + " = " + ocorrencia.getId();
		return database.delete(DATABASE_RELATIONSHIP_VEICULOS, where, null) > 0;
	}
	
	public boolean deleteAllVeiculosEnvolvidos(final Ocorrencia ocorrencia) {
		String where = OCORRENCIAID + " = " + ocorrencia.get_id();
		return database.delete(DATABASE_RELATIONSHIP_VEICULOS_ENVOLVIDOS, where, null) > 0;
	}
	public boolean deleteAllEnquetes(final Ocorrencia ocorrencia) {
		String where = OCORRENCIAID + " = " + ocorrencia.get_id();
		return database.delete(DATABASE_RELATIONSHIP_ENQUETE, where, null) > 0;
	}
	
	public boolean deleteAllMateriaisVinculados(final Ocorrencia ocorrencia) {
		String where = OCORRENCIAID + " = " + ocorrencia.get_id();
		return database.delete(DATABASE_RELATIONSHIP_MATERIAIS, where, null) > 0;
	}

	public Cursor findCursor(final Integer codigo) {
		String whereClasule = codigo == null ? null : ID + " = " + codigo;
		return database.query(DATABASE_TABLE, COLUMNS_OCORRENCIA, whereClasule,
				null, null, null, null);
	}
	public Cursor findValidadaCursor() {
		String whereClasule =  VALIDADA +  " = 1"; 
		return database.query(DATABASE_TABLE, COLUMNS_OCORRENCIA, whereClasule,
				null, null, null, ID);
	}
	
	public Cursor findEmAtendimentoCursor() {
		String whereClasule = INICIO +" > 0 "+ " AND " + FIM + " = 0";
		return database.query(DATABASE_TABLE, COLUMNS_OCORRENCIA, whereClasule,
				null, null, null, null);
	}
	
	public Cursor findPorAtenderCursor() {
		String whereClasule = "( "+ FIM + " = 0 " +" OR "+ FIM+ " is null" +" )"+ " AND " + VALIDADA +  " > 0";
		return database.query(DATABASE_TABLE, COLUMNS_OCORRENCIA, whereClasule,
				null, null, null, null);
	}
	
	private  List<VeiculoEnvolvido> locateVeiculosEnvolvidosOnOcorrencia(final int ocorrencia_id){
		List<VeiculoEnvolvido> veiculosEnvolvidos = null;
		List<OcorrenciaVeiculo> ocorrenciaVeiculos = getVeiculoEnvolvidoReference(ocorrencia_id);
		if(ocorrenciaVeiculos != null && !ocorrenciaVeiculos.isEmpty()){
			veiculosEnvolvidos = new ArrayList<VeiculoEnvolvido>();
			for (OcorrenciaVeiculo ocorrenciaVeiculo : ocorrenciaVeiculos) {
				veiculosEnvolvidos.add(veiculoEnvolvidoDAO().find(ocorrenciaVeiculo.getVeiculoId()));
			}
		}
		
		return veiculosEnvolvidos;
	}
	
	private List<Enquete> locateEnquetesOnOcorrencia(final int ocorrencia_id){
		List<Enquete> enquetes = null;
		List<OcorrenciaEnquete> ocorrenciaEnquetes = getEnqueteReference(ocorrencia_id);
		if(ocorrenciaEnquetes != null && !ocorrenciaEnquetes.isEmpty()){
			enquetes = new ArrayList<Enquete>();
			for (OcorrenciaEnquete ocorrenciaEnquete : ocorrenciaEnquetes) {
				enquetes.add(enqueteDAO().find(ocorrenciaEnquete.getEnqueteId()));
			}
		}
		
		return enquetes;
	}
	
	private List <OcorrenciaEnquete> getEnqueteReference(final int ocorrencia_id){
		Cursor cursor = enqueteReferenceCursor(ocorrencia_id);
		List<OcorrenciaEnquete> ocorrenciaEnquetes = null;
		try{
			if(cursor.moveToFirst()){
				ocorrenciaEnquetes = new ArrayList<OcorrenciaEnquete>();
				while (!cursor.isAfterLast()) {
					ocorrenciaEnquetes.add(new OcorrenciaEnquete(cursor));
					cursor.moveToNext();
				}
			}
			return ocorrenciaEnquetes;
		}finally {
			cursor.close();
		}
	}
	
	private List<OcorrenciaVeiculo> getVeiculoEnvolvidoReference(final int ocorrencia_id) {
		Cursor cursor  = veiculoEnvolvidoReferenceCursor(ocorrencia_id);
		List<OcorrenciaVeiculo> ocorrenciaVeiculos = null;
		try{
			if (cursor.moveToFirst()) {
				ocorrenciaVeiculos = new ArrayList<OcorrenciaVeiculo>();
				while (!cursor.isAfterLast()) {
					ocorrenciaVeiculos.add(new OcorrenciaVeiculo(cursor));
					cursor.moveToNext();
				}
			}
			return ocorrenciaVeiculos;
			
		}finally{
			cursor.close();
		}
	}
	
	private  List<Material> locateMateriasOnOcorrencia(final int ocorrencia_id){
		List<Material> materiais = new ArrayList<Material>();
		List<OcorrenciaMaterial> ocorrenciaMateriais = getMaterialReference(ocorrencia_id);
		if(ocorrenciaMateriais != null && !ocorrenciaMateriais.isEmpty()){
			materiais = new ArrayList<Material>();
			for (OcorrenciaMaterial ocorrenciaMaterial : ocorrenciaMateriais) {
				materiais.add(materialDAO().find(ocorrenciaMaterial.getMaterialId()));
			}
		}
		
		
		return materiais;
	}
	
	private List<OcorrenciaMaterial> getMaterialReference(final Integer ocorrencia_id) {
		Cursor cursor  = materialReferenceCursor(ocorrencia_id);
		List<OcorrenciaMaterial> ocorrenciaMaterias = null;
		try{
			if (cursor.moveToFirst()) {
				ocorrenciaMaterias = new ArrayList<OcorrenciaMaterial>();
				while (!cursor.isAfterLast()) {
					ocorrenciaMaterias.add(new OcorrenciaMaterial(cursor));
					cursor.moveToNext();
				}
			}
			return ocorrenciaMaterias;
			
		}finally{
			cursor.close();
		}
	}
	
	private Cursor enqueteReferenceCursor(final int ocorrencia_id){
		String whereClausule = OCORRENCIAID + "=" + ocorrencia_id;
		return database.query(DATABASE_RELATIONSHIP_ENQUETE, COLUMN_ENQUETE, whereClausule, null, null, null, null);
	}
	
	private Cursor veiculoEnvolvidoReferenceCursor(final int ocorrencia_id){
		String whereClausule = OCORRENCIAID + " = " + ocorrencia_id;
		return database.query(DATABASE_RELATIONSHIP_VEICULOS_ENVOLVIDOS, COLUMN_VEICULOS, whereClausule, null, null, null, null);
	}
	
	private Cursor materialReferenceCursor(final int ocorrencia_id){
		String whereClausule = OCORRENCIAID + " = " + ocorrencia_id;
		return database.query(DATABASE_RELATIONSHIP_MATERIAIS, COLUMN_MATERIAIS, whereClausule, null, null, null, null);
	}
	
	
	

	public List<Ocorrencia> retrieveAllPending() {
		Cursor cursor = retrieveCursorAllPending();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}

	public Cursor retrieveCursorAllPending() {
		final String whereClasule = ENVIADO + " = 0";
		return database.query(DATABASE_TABLE, COLUMNS_OCORRENCIA, whereClasule,
				null, null, null, null,"100");
	}


	public void updateSentStatus(final List<Integer> ids, final boolean status) {
		ContentValues cv = new ContentValues();
		cv.put(ENVIADO, status);
		final String whereClasule = _ID + " in ("
				+ DatabaseUtil.concatValuesToUseInCondition(ids.toArray())
				+ ")";
		database.update(DATABASE_TABLE, cv, whereClasule, null);
	}

	public Ocorrencia find(final int id) {
		Cursor cursor = findCursor(id);
		try {
			cursor.moveToFirst();
			if (cursor.getCount() == 0) {
				return null;
			}

			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}
	
	public Ocorrencia findEmAtendimento() {
		Cursor cursor = findEmAtendimentoCursor();
		try {
			cursor.moveToFirst();
			if (cursor.getCount() == 0) {
				return null;
			}
			
			return parseObject(cursor);
		} finally {
			cursor.close();
		}
	}
	
	public List<Ocorrencia> fetchAllParsed() {
		Cursor cursor = findCursor(null);
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	public List<Ocorrencia> fetchAllValidadaParsed() {
		Cursor cursor = findValidadaCursor();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	public List<Ocorrencia> findPorAtender() {
		Cursor cursor = findPorAtenderCursor();
		try {
			return parse(cursor);
		} finally {
			cursor.close();
		}
	}
	
	
	private FotoDAO fotoDAO(){
		if(fotoDAO == null){
			fotoDAO = new FotoDAO(context);
		}
		return fotoDAO;
	}
	
	private SignDAO signDAO(){
		if(signDAO == null){
			signDAO = new SignDAO(context);
		}
		return signDAO;
	}
	
	private FilmagemDAO filmagemDAO(){
		if(filmagemDAO == null){
			filmagemDAO = new FilmagemDAO(context);
		}
		return filmagemDAO;
	}


	private List<Ocorrencia> parse(Cursor cursor) {
		List<Ocorrencia> toReturn = new ArrayList<Ocorrencia>();
		if (cursor.moveToFirst()) {
			while (!cursor.isAfterLast()) {
				toReturn.add(parseObject(cursor));
				cursor.moveToNext();
			}
		}

		return toReturn;
	}

	private Ocorrencia parseObject(Cursor cursor) {
		Ocorrencia ocorrencia = new Ocorrencia();
		ocorrencia.set_id(cursor.getInt(0));
		ocorrencia.setId(DatabaseUtil.returnNullValueForInt(cursor, ID));
		ocorrencia.setResumo(cursor.getString(2));
		ocorrencia.setDescricao(cursor.getString(3));
		ocorrencia.setHoraChegada(cursor.getLong(4));
		ocorrencia.setHoraSaida(cursor.getLong(5));
		ocorrencia.setCancelado(cursor.getInt(6) > 0);
		ocorrencia.setRazaoCancelamento(cursor.getString(7));
		ocorrencia.setEnviado(cursor.getInt(8) > 0);
		ocorrencia.setNivelEmergenciaID(cursor.getInt(9));
		ocorrencia.setEnderecoID(cursor.getInt(10));
		ocorrencia.setNatureza(cursor.getInt(11));
		ocorrencia.setUsuarioID(cursor.getInt(12));
		ocorrencia.setEstado(cursor.getInt(13));
		ocorrencia.setCodigo(cursor.getString(21));
		ocorrencia.setInicio(DatabaseUtil
				.returnNullValueForLong(cursor, INICIO));
		ocorrencia.setInicioProgramado(DatabaseUtil.returnNullValueForLong(
				cursor, INICIO_PROGRAMADO));
		ocorrencia.setFim(DatabaseUtil.returnNullValueForLong(cursor, FIM));
		ocorrencia.setFimProgramado(DatabaseUtil.returnNullValueForLong(cursor,
				FIM_PROGRAMADO));
		ocorrencia.setModificado(DatabaseUtil.returnNullValueForLong(cursor,
				MODIFICADO));
		ocorrencia.setRecebimento(DatabaseUtil.returnNullValueForLong(cursor,
				RECEBIMENTO));
		ocorrencia.setEndereco(enderecoDAO().findLocal(ocorrencia.getEnderecoID()));
		ocorrencia.setEstadoObj(statusDAO().find(ocorrencia.getEstado()));
		ocorrencia.setNaturezaObj(naturezaDAO().find(ocorrencia.getNatureza()));
		ocorrencia.setNivelEmergencia(nivelEmergenciaDAO().find(
				ocorrencia.getNivelEmergenciaID()));
		ocorrencia.setComentarios(comentarioDAO().fetchAllParsedFromOcorrencia(ocorrencia.getId()));
		ocorrencia.setFotos(fotoDAO().findFromOcorrencia(ocorrencia.get_id()));
		ocorrencia.setSigns(signDAO().findFromOcorrencia(ocorrencia.get_id()));
		ocorrencia.setFilmagens(filmagemDAO().findFromOcorrencia(ocorrencia.get_id()));
		ocorrencia.setEnquetes(locateEnquetesOnOcorrencia(cursor.getInt(0)));
		ocorrencia.setVeiculosEnvolvidos(locateVeiculosEnvolvidosOnOcorrencia(cursor.getInt(0)));
		ocorrencia.setMateriais(locateMateriasOnOcorrencia(cursor.getInt(0)));
		ocorrencia.setValidada(cursor.getInt(20) > 0);
		ocorrencia.setSolicitantes(solicitanteDAO().fetchAllFromOcorrenciaParsed(cursor.getInt(0)));
		ocorrencia.setVitimas(vitimaDAO().findFromOcorrencia(cursor.getInt(0)));
		return ocorrencia;
	}



	private ContentValues createContentValues(Ocorrencia obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(DESCRICAO, obj.getDescricao());
		values.put(RESUMO, obj.getResumo());
		values.put(HORACHEGADA, obj.getHoraChegada());
		values.put(HORASAIDA, obj.getHoraSaida());
		values.put(CANCELADO, obj.isCancelado());
		values.put(RAZAOCANCELAMENTO, obj.getRazaoCancelamento());
		values.put(ENVIADO, obj.isEnviado());
		values.put(NIVELEMERGENCIAID, obj.getNivelEmergenciaID());
		values.put(ENDERECOID, obj.getEnderecoID());
		values.put(NATUREZAID, obj.getNatureza());
		values.put(USUARIOID, obj.getUsuarioID());
		values.put(STATUSID, obj.getEstado());
		values.put(INICIO, obj.getInicio());
		values.put(INICIO_PROGRAMADO, obj.getInicioProgramado());
		values.put(FIM, obj.getFim());
		values.put(FIM_PROGRAMADO, obj.getFimProgramado());
		values.put(MODIFICADO, new Date().getTime());
		values.put(RECEBIMENTO, obj.getRecebimento());
		values.put(VALIDADA, obj.isValidada());
		values.put(CODIGO, obj.getCodigo());
		return values;
	}

	private ContentValues createUpdateContentValues(Ocorrencia obj) {
		ContentValues values = new ContentValues();
		values.put(ID, obj.getId());
		values.put(DESCRICAO, obj.getDescricao());
		values.put(RESUMO, obj.getResumo());
		values.put(HORACHEGADA, obj.getHoraChegada());
		values.put(HORASAIDA, obj.getHoraSaida());
		values.put(CANCELADO, obj.isCancelado());
		values.put(RAZAOCANCELAMENTO, obj.getRazaoCancelamento());
		values.put(ENVIADO, obj.isEnviado());
		values.put(NIVELEMERGENCIAID, obj.getNivelEmergenciaID());
		values.put(ENDERECOID, obj.getEnderecoID());
		values.put(NATUREZAID, obj.getNatureza());
		values.put(USUARIOID, obj.getUsuarioID());
		values.put(STATUSID, obj.getEstado());
		values.put(INICIO, obj.getInicio());
		values.put(INICIO_PROGRAMADO, obj.getInicioProgramado());
		values.put(FIM, obj.getFim());
		values.put(FIM_PROGRAMADO, obj.getFimProgramado());
		values.put(MODIFICADO, new Date().getTime());
		values.put(RECEBIMENTO, obj.getRecebimento());
		values.put(VALIDADA, obj.isValidada());
		return values;
	}

	private ContentValues materialReferenceValues(Ocorrencia obj, int code) {
		ContentValues values = new ContentValues();
		values.put(OCORRENCIAID, obj.get_id());
		values.put(MATERIALID, code);
		return values;
	}

	private ContentValues veiculoReferenceValues(Ocorrencia obj, Veiculo veiculo) {
		ContentValues values = new ContentValues();
		values.put(OCORRENCIAID, obj.getId());
		values.put(VEICULOID, veiculo.get_id());
		return values;
	}
	
	private ContentValues veiculoEnvolvidoReferenceValues(Ocorrencia obj, VeiculoEnvolvido veiculo) {
		ContentValues values = new ContentValues();
		values.put(OCORRENCIAID, obj.get_id());
		values.put(VEICULOID, veiculo.get_id());
		return values;
	}
	
	private ContentValues enqueteReferenceValues(Ocorrencia obj, Enquete enquete) {
		ContentValues values = new ContentValues();
		values.put(OCORRENCIAID, obj.get_id());
		values.put(ENQUETEID, enquete.getId());
		return values;
	}

	private ContentValues solicitanteReferenceValues(Ocorrencia obj, int code) {
		ContentValues values = new ContentValues();
		values.put(OCORRENCIAID, obj.get_id());
		values.put(SOLICITANTEID, code);
		return values;
	}

	private EnderecoDAO enderecoDAO() {
		if (enderecoDAO == null) {
			enderecoDAO = new EnderecoDAO(context);
		}

		return enderecoDAO;
	}
	
	public ComentarioDAO comentarioDAO(){
		if(comentarioDAO== null){
			comentarioDAO = new ComentarioDAO(context);
		}
		
		return comentarioDAO;
	}

	private NivelEmergenciaDAO nivelEmergenciaDAO() {
		if (nivelEmergenciaDAO == null) {
			nivelEmergenciaDAO = new NivelEmergenciaDAO(context);
		}

		return nivelEmergenciaDAO;
	}

	private NaturezaDAO naturezaDAO() {
		if (naturezaDAO == null) {
			naturezaDAO = new NaturezaDAO(context);
		}

		return naturezaDAO;
	}
	
	private VeiculoEnvolvidoDAO veiculoEnvolvidoDAO() {
		if(veiculoEnvolvidoDAO == null) {
			veiculoEnvolvidoDAO = new VeiculoEnvolvidoDAO(context);
		}
		
		return veiculoEnvolvidoDAO;
	}
	
	private EnqueteDAO enqueteDAO(){
		if(enqueteDAO == null){
			enqueteDAO = new EnqueteDAO(context);
		}
		return enqueteDAO;
	}

	private StatusDAO statusDAO() {
		if (statusDAO == null) {
			statusDAO = new StatusDAO(context);
		}

		return statusDAO;
	}
	
	private MaterialDAO materialDAO() {
		if(materialDAO == null){
			materialDAO = new MaterialDAO(context);
		}
		return materialDAO;
	}
	
	private SolicitanteDAO solicitanteDAO() {
		if(solicitanteDAO == null){
			solicitanteDAO = new SolicitanteDAO(context);
		}
		return solicitanteDAO;
	}


	private VitimaDAO vitimaDAO() {
		if(vitimaDAO == null){
			vitimaDAO = new VitimaDAO(context);
		}
		
		return vitimaDAO;
	}
	
	public boolean deleteAndAllObjects(Ocorrencia forDelete) {
		boolean a;
		fotoDAO().deleteFotoFilesFromOcorrencia(forDelete.get_id());
		signDAO().deleteSignFilesFromOcorrencia(forDelete.get_id());
		filmagemDAO().deleteFotoFilesFromOcorrencia(forDelete.get_id());
		deleteAllMateriaisVinculados(forDelete);
		deleteAllVeiculosEnvolvidos(forDelete);
		deleteAllEnquetes(forDelete);
		a = delete(forDelete.get_id());
		return a;
	}
}
