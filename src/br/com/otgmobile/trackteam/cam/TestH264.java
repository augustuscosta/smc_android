package br.com.otgmobile.trackteam.cam;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import android.media.MediaRecorder;
import android.media.MediaRecorder.OnInfoListener;
import android.util.Log;
import android.view.SurfaceHolder;
import br.com.otgmobile.trackteam.activity.CamActivity;
import br.com.otgmobile.trackteam.cam.libmp4.MP4Parser;
import br.com.otgmobile.trackteam.cam.libmp4.StsdBox;

/* 
 * The purpose of this class is to test H.264 support on the phone
 * and to find H.264 pps ans sps parameters. They are needed to 
 * correctly decode the stream.
 * 
 */

public class TestH264 {

	/* Launches the test */
	public static void RunTest(File cacheDir,SurfaceHolder holder, int resX, int resY, int fps, Callback cb) {

		if (test == null) test = new TestH264();
		
		test.cb = cb;
		test.cacheDir = cacheDir;
		test.resX = resX;
		test.resY = resY;
		test.fps = fps;
		test.holder = holder;

		test.start();
		
	}
	
	/* If test successful, onSuccess is called and return H264 settings for the phone  */
	public interface Callback {
		public void onStart();
		public void onError(String error);
		public void onSuccess(String result);
	}
	
	private final String TESTFILE = "com.fotosensores.veideocache.mp4";
	
	private static TestH264 test = null;
	private SurfaceHolder.Callback shcb;
	private SurfaceHolder holder;
	private MediaRecorder mr = new MediaRecorder();
	private Callback cb;
	private File cacheDir;
	private int resX, resY, fps;
	private OnInfoListener infoListener;
	private boolean recording = false;
	
	private TestH264() {
		
		infoListener = new OnInfoListener() {
			@Override
			public void onInfo(MediaRecorder mr, int what, int extra) {
				if (what==MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
					runTest(1);
				}
			}
		};
		
		shcb = new SurfaceHolder.Callback() {

    		@Override
			public void surfaceChanged(SurfaceHolder holder, int format,
					int width, int height) {
    			
			}

    		@Override
			public void surfaceCreated(SurfaceHolder holder) {
    			runTest(0);
			}

    		@Override
			public void surfaceDestroyed(SurfaceHolder holder) {

			}
    		
    	};
		
	}

	private void start() {

		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    	holder.addCallback(shcb);
	}
	
	private void runTest(int step) {
		
		switch (step) {
		
		case 0:
		
			cb.onStart();
			
			/* 1 - Set up MediaRecorder */
			mr.setVideoSource(MediaRecorder.VideoSource.CAMERA);
			mr.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
			mr.setVideoFrameRate(fps);
			mr.setVideoSize(resX, resY);
			mr.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
			mr.setPreviewDisplay(holder.getSurface());
			mr.setOnInfoListener(infoListener);
			mr.setMaxDuration(500);
	
			mr.setOutputFile(cacheDir.getPath()+'/'+TESTFILE);
			
			try {
				mr.prepare();
			} catch (IOException e) {
				error("Can't record video, H.264 not supported ?");
				return;
			}
			
			/* 2 - Record dummy video for 500 msecs */
			mr.start(); recording = true;
		
			break;
		
		case 1:

			recording = false;
			try {
				mr.stop(); 
			}
			catch (IllegalStateException e) {
				mr.reset();
				error("Test cancelled !");
				return;
			}
			
			/* 3 - Parse video with MP4Parser */
			File file = new File(cacheDir.getPath()+'/'+TESTFILE);
			RandomAccessFile raf = null;
			try {
				raf = new RandomAccessFile(file, "r");
			} catch (FileNotFoundException e1) {
				error("Can't load dummy video");
				return;
			}
			
			MP4Parser parser = null;
			try {
				parser = new MP4Parser(raf);
			} catch (IOException e2) {
				error(e2.getMessage());
				return;
			}
			
			/* 4 - Get stsd box (contains h.264 parameters) */
			StsdBox stsd = null;
			try {
				stsd = parser.getStsdBox();
			} catch (IOException e1) {
				error(e1.getMessage());
				return;
			}
			
			try {
				raf.close();
			} catch (IOException e) {
				error("Error :(");
				return;
			}

			if (!file.delete()) Log.e(CamActivity.LOG_TAG,"Temp file not erased");
			
			success(stsd.getProfileLevel()+":"+stsd.getB64PPS()+":"+stsd.getB64SPS()); 	
			
		}
		
	}
	
	private void success(String result) {
		clean();
		cb.onSuccess(result);
	}
	
	private void error(String error) {
		clean();
		cb.onError(error);
	}
	
	private void clean () {
		if (recording) mr.stop();
		recording = false;
		holder.removeCallback(shcb);
	}
	
}
