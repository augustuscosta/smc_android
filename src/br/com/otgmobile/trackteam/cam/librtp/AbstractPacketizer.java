package br.com.otgmobile.trackteam.cam.librtp;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.SocketException;


abstract public class AbstractPacketizer extends Thread implements Runnable{
	
	protected SmallRtpSocket rsock = null;
	protected InputStream fis = null;
	protected boolean running = false;
	
	protected byte[] buffer = new byte[16384*2];	
	
	protected final int rtphl = 12; // Rtp header length
	
	
	public AbstractPacketizer(InputStream fis, InetAddress dest, int port) throws SocketException {
		
		this.fis = fis;
		this.rsock = new SmallRtpSocket(dest, port, buffer);
	}
	
	public void startStreaming() {
		running = true;
		start();
	}

	public void stopStreaming() {
		try {
			fis.close();
		} catch (IOException e) {
			
		}
		running = false;
	}
	
	abstract public void run();
	
    // Useful for debug
    protected String printBuffer(int start,int end) {
            String str = "";
            for (int i=start;i<end;i++) str+=","+Integer.toHexString(buffer[i]&0xFF);
            return str;
    }
	
}
