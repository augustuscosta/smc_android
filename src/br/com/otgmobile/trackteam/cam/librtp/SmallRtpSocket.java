package br.com.otgmobile.trackteam.cam.librtp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Random;

import android.util.Log;



public class SmallRtpSocket {
	
	static final public String LOG_TAG = "CAM";

	private DatagramSocket usock;
	private DatagramPacket upack;
	
	private byte[] buffer;
	private int seq = 0;
	private boolean upts = false;
	
	public static final int headerLength = 12;
	
	public SmallRtpSocket(InetAddress dest, int dport, byte[] buffer) throws SocketException {
		
		this.buffer = buffer;
		
		/*							     Version(2)  Padding(0)					 					*/
		/*									 ^		  ^			Extension(0)						*/
		/*									 |		  |				^								*/
		/*									 | --------				|								*/
		/*									 | |---------------------								*/
		/*									 | ||  -----------------------> Source Identifier(0)	*/
		/*									 | ||  |												*/
		buffer[0] = (byte) Integer.parseInt("10000000",2);
		
		/* Payload Type */
		buffer[1] = (byte) 96;
		
		/* Byte 2,3        ->  Sequence Number                   */
		/* Byte 4,5,6,7    ->  Timestamp                         */
		
		/* Byte 8,9,10,11  ->  Sync Source Identifier            */
		setLong((new Random()).nextLong(),8,12);
		
		usock = new DatagramSocket();
		upack = new DatagramPacket(buffer,1,dest,dport);

	}

	public void close() {
		usock.close();
	}
	
	/* Send RTP packet over the network */
	public void send(int length) {
		
		updateSequence();
		upack.setLength(length);
		
		try {
			usock.send(upack);
		} catch (IOException e) {
			Log.e(LOG_TAG,"Send failed");
		}
		
		if (upts) {
			upts = false;
			buffer[1] -= 0x80;
		}
		
	}
	
	private void updateSequence() {
		setLong(++seq, 2, 4);
	}
	
	public void updateTimestamp(long timestamp) {
		setLong(timestamp, 4, 8);
	}
	
	public void markNextPacket() {
		upts = true;
		buffer[1] += 0x80; // Mark next packet
	}
	
	public boolean isMarked() {
		return upts;
	}
	
	// Call this only one time !
	public void markAllPackets() {
		buffer[1] += 0x80;
	}
	
	private void setLong(long n, int begin, int end) {
		for (end--; end >= begin; end--) {
			buffer[end] = (byte) (n % 256);
			n >>= 8;
		}
	}	
	
}
