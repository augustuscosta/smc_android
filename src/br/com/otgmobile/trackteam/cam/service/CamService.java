package br.com.otgmobile.trackteam.cam.service;

import java.io.IOException;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.view.SurfaceView;
import br.com.otgmobile.trackteam.cam.CamPreferences;
import br.com.otgmobile.trackteam.cam.CameraStreamer;
import br.com.otgmobile.trackteam.cam.TestH264;
import br.com.otgmobile.trackteam.cam.librtp.SessionDescriptor;

public class CamService extends Service {
	static final public String LOG_TAG = "CAM";
	public static final String CAM_SERVICE_INTENT = "CAM_SERVICE_INTENT";
	
	private PowerManager.WakeLock wl;
	private static CameraStreamer streamer = new CameraStreamer();
	
	private int resX, resY, fps, br;
	
	public static void startService(Context context) {
		context.startService(new Intent(CAM_SERVICE_INTENT));
	}
	
	public static void stopService(Context context) {
		context.stopService(new Intent(CAM_SERVICE_INTENT));
	}
	
	private void startCamStreamEngine(){
		resX = CamPreferences.getResX(getApplicationContext());
       	resY = CamPreferences.getResY(getApplicationContext());
       	fps = CamPreferences.getFps(getApplicationContext());
       	br = CamPreferences.getBr(getApplicationContext());
       	wakeScreen(); //TODO testar sem o wake
       	testDeviceSupport();
	}
	
	private void testDeviceSupport(){
		toggleStreaming();
		if(!CamPreferences.isSupported(getApplicationContext())){
			TestH264.RunTest(this.getCacheDir(),new SurfaceView(CamService.this).getHolder(), resX, resY, fps, new TestH264.Callback() {
				
				@Override
				public void onStart() {
					Log.i(LOG_TAG,"<b>Testing H.264 Support, please wait...</b>");
				}
				
				// Called if H.264 isn't supported with chosen quality settings
				public void onError(String error) {
					Log.e(LOG_TAG,error);
					Log.i(LOG_TAG,"Something went wrong, the app may not work properly :/");
				}
				
				// Called if H.264 is supported with chosen quality settings
				public void onSuccess(String result) {

					String[] params = result.split(":");
					Log.i(LOG_TAG,"Phone supported ! ");
				    // we have everything we need to generate a proper SDP file
				    SessionDescriptor sd = new SessionDescriptor();
				    sd.addH264Track(params[0], params[2], params[1]);
				    sd.addAMRNBTrack();
				    try {
						sd.saveToFile("/sdcard/spydroid.sdp");
						CamPreferences.setCamParams(sd.toString(), CamService.this);
						// Store H264 parameters
			    	    CamPreferences.setProfile(params[0], CamService.this);
			    	    CamPreferences.setPps(params[1], CamService.this);
			    	    CamPreferences.setSps(params[2], CamService.this);
					} catch (IOException e) {
						Log.i(LOG_TAG,"Could not save sdp file to /sdcard/spydroid.sdp");
					}
					toggleStreaming();
				}
			});
		}else{
			toggleStreaming();
		}
		
	}

	private void wakeScreen() {
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "CamWakeLock");
	}
	
	private void toggleStreaming() {
    	
    	if (streamer.isStreaming())
    		stopStreaming();
    	else
    		startStreaming();

    }
	
	private void startStreaming() {
    	
    	if (streamer.isStreaming()) return;
    	
		try {
			streamer.setup(CamPreferences.getServer(CamService.this),CamPreferences.getPort(CamService.this), resX, resY, fps, br, null);
		} catch (IOException e) {
			Log.e(LOG_TAG,e.getMessage());
			return;
		}

		streamer.start();
		wl.acquire();
    }
    
    private void stopStreaming() {
    	if (!streamer.isStreaming()) return;
    	
		streamer.stop();
		wl.release();
    }
	
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		startCamStreamEngine();
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
