package br.com.otgmobile.trackteam.cam;


import br.com.otgmobile.trackteam.activity.QualityListActivity;
import android.content.Context;
import android.content.SharedPreferences;

public class CamPreferences {
	
	public static final String CAM_PREFS 				 = "CAM_PREFS";
	public static final String RESX 		 			 = "resX";
	public static final String RESY 		 			 = "resY";
	public static final String FPS 		 			 	 = "fps";
	public static final String BR 		 			 	 = "br";
	public static final String SERVER 		 			 = "server_cam";
	public static final String PORT 		 			 = "server_port";
	public static final String SUPPORT 		 			 = "support";
	public static final String PROFILE 		 			 = "profile";
	public static final String PPS 		 			 	 = "pps";
	public static final String SPS 		 			 	 = "sps";
	public static final String CAM_PARAMS 		 		 = "cam_params";

	private static SharedPreferences settings;
	
	private static SharedPreferences getSharedPreferencesInstance(Context context){
		if(settings == null){
			settings = context.getSharedPreferences(CAM_PREFS, Context.MODE_PRIVATE);
		}
		return settings;
	}
	
	public static void setServer(String server,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(SERVER, server);
		editor.commit();
	}
	
	public static String getServer(Context context){
		return getSharedPreferencesInstance(context).getString(SERVER, "10.0.1.7");
	}
	
	public static void setCamParams(String server,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(CAM_PARAMS, server);
		editor.commit();
	}
	
	public static String getCamParams(Context context){
		return getSharedPreferencesInstance(context).getString(CAM_PARAMS, null);
	}
	
	public static void setProfile(String profile,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PROFILE, profile);
		editor.commit();
	}
	
	public static String getProfile(Context context){
		return getSharedPreferencesInstance(context).getString(PROFILE, null);
	}
	
	public static String getPps(Context context){
		return getSharedPreferencesInstance(context).getString(PPS, null);
	}
	
	public static void setPps(String pps,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(PPS, pps);
		editor.commit();
	}
	
	public static Integer getPort(Context context){
		return getSharedPreferencesInstance(context).getInt(PORT, 5004);
	}
	
	public static void setPort(Integer port,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(PORT, port);
		editor.commit();
	}
	
	public static String getSps(Context context){
		return getSharedPreferencesInstance(context).getString(SPS, null);
	}
	
	public static void setSps(String sps,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(SPS, sps);
		editor.commit();
	}
	
	public static void setResX(Integer resX,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(RESX, resX);
		editor.commit();
	}
	
	public static Integer getResX(Context context){
		return getSharedPreferencesInstance(context).getInt(RESX, 640);
	}

	public static void setResY(Integer resY,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(RESY, resY);
		editor.commit();
	}
	
	public static Integer getResY(Context context){
		return getSharedPreferencesInstance(context).getInt(RESY, 480);
	}
	
	public static void setFps(Integer fps,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(FPS, fps);
		editor.commit();
	}
	
	public static Integer getFps(Context context){
		return getSharedPreferencesInstance(context).getInt(FPS, 5);
	}
	
	public static void setBr(Integer br,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(BR, br);
		editor.commit();
	}
	
	public static Integer getBr(Context context){
		return getSharedPreferencesInstance(context).getInt(BR, QualityListActivity.DefaultBitRate);
	}
	
	public static void setSupport(Boolean supported,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(SUPPORT, supported);
		editor.commit();
	}
	
	public static Boolean isSupported(Context context){
		return getSharedPreferencesInstance(context).getBoolean(SUPPORT, false);
	}

}
