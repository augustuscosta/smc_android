package br.com.otgmobile.trackteam.cam;

import java.io.IOException;
import java.io.InputStream;

import android.media.MediaRecorder;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.util.Log;

/* 
 *  Just a MediaRecorder that writes in a local socket instead of a file
 *  so that you can modify data on-the-fly with getInputStream()
 * 
 */

public class MediaStreamer extends MediaRecorder{
	
	static final public String LOG_TAG = "CAM";

	private static int id = 0;
	
	private LocalServerSocket lss = null;
	private LocalSocket receiver, sender = null;
	
	public void prepare() throws IllegalStateException,IOException {
		
		receiver = new LocalSocket();
		try {
			lss = new LocalServerSocket("librtp-"+id);
			receiver.connect(new LocalSocketAddress("librtp-"+id));
			receiver.setReceiveBufferSize(500000);
			receiver.setSendBufferSize(500000);
			sender = lss.accept();
			sender.setReceiveBufferSize(500000);
			sender.setSendBufferSize(500000); 
			id++;
		} catch (IOException e1) {
			throw new IOException("Can't create local socket !");
		}
		
		setOutputFile(sender.getFileDescriptor());
		
		try {
			super.prepare();
		} catch (IllegalStateException e) {
			closeSockets();
			throw e;
		} catch (IOException e) {
			closeSockets();
			throw e;
		}
		
	}
	
	public InputStream getInputStream() {
		
		InputStream out = null;
		
		try {
			out = receiver.getInputStream();
		} catch (IOException e) {
		}

		return out;
		
	}

	
	public void stop() {
		closeSockets();
		super.stop();
	}
	
	private void closeSockets() {
		if (lss!=null) {
			try {
				lss.close();
				sender.close();
				receiver.close();
			}
			catch (IOException e) {
				Log.e(LOG_TAG,"Error while attempting to close local sockets");
			}
			lss = null; sender = null; receiver = null;
		}
	}
	
}
