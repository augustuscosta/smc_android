package br.com.otgmobile.trackteam.cam;

import java.io.IOException;
import java.net.InetAddress;

import android.hardware.Camera;
import android.media.MediaRecorder;
import android.util.Log;
import android.view.SurfaceHolder;
import br.com.otgmobile.trackteam.activity.CamActivity;
import br.com.otgmobile.trackteam.activity.QualityListActivity;
import br.com.otgmobile.trackteam.cam.librtp.AMRNBPacketizer;
import br.com.otgmobile.trackteam.cam.librtp.H264Packetizer2;

/*
 * 
 * Instantiates two MediaStreamer objects, one for audio streaming and the other for video streaming
 * then it uses the H264Packetizer and the AMRNBPacketizer to generate two RTP streams
 * 
 */

public class CameraStreamer {

	private MediaStreamer sound = new MediaStreamer(), video = new MediaStreamer();
	private AMRNBPacketizer sstream = null;
	private H264Packetizer2 vstream = null;
	private boolean streaming = false;
	
	public void setup(String ip,int port, int resX, int resY, int fps, int br, SurfaceHolder surfaceHolder) throws IOException {
	
		// AUDIO
		
		sound.reset();
		
		sound.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
		sound.setOutputFormat(MediaRecorder.OutputFormat.RAW_AMR);
		sound.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		sound.setAudioChannels(1);
		
		try {
			sound.prepare();
		} catch (IOException e) {
			throw new IOException("Can't stream sound: "+e.getMessage()); 
		}
		
		try {
			sstream = new AMRNBPacketizer(sound.getInputStream(), InetAddress.getByName(ip), port);
		} catch (IOException e) {
			Log.e(CamActivity.LOG_TAG,"Unknown host");
			throw new IOException("Can't resolve host :(");
		}
		
		// VIDEO
		
		video.reset();
		
		video.setVideoSource(MediaRecorder.VideoSource.CAMERA);
		video.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		video.setVideoFrameRate(fps);
		video.setVideoSize(resX, resY);
		if (br != QualityListActivity.DefaultBitRate) video.setVideoEncodingBitRate(br*1000);
		video.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
		
		if(surfaceHolder != null)
			video.setPreviewDisplay(surfaceHolder.getSurface());
		//video.setCamera(Camera.open());
		
		
		try {
			video.prepare();
		} catch (IOException e) {
			throw new IOException("Can't stream video: "+e.getMessage());
		}
		
		try {
			vstream = new H264Packetizer2(video.getInputStream(), InetAddress.getByName(ip), 5006);
		} catch (IOException e) {
			Log.e(CamActivity.LOG_TAG,"Unknown host");
			throw new IOException("Can't resolve host :(");
		}
		
	}
	
	public void start() {
	
		// Start sound streaming
		sound.start();
		sstream.startStreaming();
		
		// Start video streaming
		video.start();
		vstream.startStreaming();
		
		streaming = true;
		
	}
	
	public void stop() {
		
		// Stop sound streaming
		sstream.stopStreaming();
		sound.stop();
	
		// Stop video streaming
		vstream.stopStreaming();
		video.stop();
		
		streaming = false;
		
	}
	
	public boolean isStreaming() {
		return streaming;
	}
	
}
