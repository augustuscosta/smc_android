package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

public class Pergunta implements Serializable{

	private static final long serialVersionUID = 142637899430753944L;
	
	private Integer _id;
	private Integer id;
	private String pergunta;
	private String tipoEntrada;
	private Integer enqueteId;
	
	public Integer getEnqueteId() {
		return enqueteId;
	}
	public void setEnqueteId(Integer enqueteId) {
		this.enqueteId = enqueteId;
	}
	public Integer get_id() {
		return _id;
	}
	public void set_id(Integer _id) {
		this._id = _id;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPergunta() {
		return pergunta;
	}
	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}
	public String getTipoEntrada() {
		return tipoEntrada;
	}
	public void setTipoEntrada(String tipoEntrada) {
		this.tipoEntrada = tipoEntrada;
	}
}
