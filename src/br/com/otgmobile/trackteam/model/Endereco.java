package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.maps.GeoPoint;

public class Endereco implements Serializable {

	private static final long serialVersionUID = 6890255612334626457L;
	
	private Integer _id;
	
	private Integer id;
	
	private Float latitude;
	
	private Float longitude;
	
	private String endGeoref;
	
	private String bairro;
	
	private String logradouro;
	
	private String numero;
	
	public GeoPoint getGeopoint(){
		double geoLatitude = getLatitude() * 1E6;
		double geoLongitude = getLongitude() * 1E6;
		return new GeoPoint((int) geoLatitude, (int) geoLongitude);
	}
	
	public LatLng getLatLng(){
		return new LatLng(getLatitude(), getLongitude());
	}
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getEndGeoref() {
		return endGeoref;
	}

	public void setEndGeoref(String endGeoref) {
		this.endGeoref = endGeoref;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Integer get_id() {
		return _id;
	}

	public void set_id(Integer _id) {
		this._id = _id;
	}
}
