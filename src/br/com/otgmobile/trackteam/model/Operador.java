package br.com.otgmobile.trackteam.model;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.otgmobile.trackteam.util.LogUtil;

public class Operador {
	
	private String name;
	
	private String token;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	public JSONObject toJson() {
		try {
			final JSONObject json = new JSONObject();
			json.put("name", name);
			json.put("token", token);
			return json;
		} catch (JSONException e) {
			LogUtil.e("Erro ao tentar criar o JsonObject de Operador...", e);
		}
		
		return null;
	}

}
