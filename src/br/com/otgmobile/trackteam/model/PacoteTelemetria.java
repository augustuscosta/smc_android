package br.com.otgmobile.trackteam.model;

import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.StringUtil;

/**
 * 
 * @author pierrediderot@gmail.com
 * @since 06/03/2012
 * @version $Revision:  $ <br>
 *          $Date:  $ <br> 
 *          $Author:  $
 */
public class PacoteTelemetria {

	private static final char TEMPERATURA = 'T';
	private static final char BATERIA = 'B';
	private static final char RPM = 'R';
	private static final char VELOCIDADE = 'V';

	public static void montaPacote(String[] commands, Historico historico) {
		for(String command : commands){
			LogUtil.d("Recieving command from serial bluetooth: " + command);
			final char typeCommand = command.toUpperCase().charAt(1);
			final String value = command.substring(2, command.length() - 1);
			
			switch ( typeCommand ) {
			case VELOCIDADE:
				LogUtil.d("velocidade: " + value);
				historico.setVelocidade(value);
				break;

			case RPM:
				LogUtil.d("rpm: " + value);
				historico.setRpm(value);
				break;

			case BATERIA:
				LogUtil.d("bateria: " + value);
				historico.setTensaoBateria(value);
				break;

			case TEMPERATURA:
				LogUtil.d("temperatura: " + value);
				historico.setTemperatura(value);
				break;

			default:
				LogUtil.w("nao definido.");
				break;
			}
		}
	}
	
	public static boolean isValidCommand(String[] commands) {
		
		if(commands == null || commands.length != 4)
			return false;
		for(String command : commands){
			if ( !(StringUtil.isValid(command)
					&& (command.charAt(0) == '[' && command.charAt(command.length() - 1) == ']')
					&& command.trim().length() > 2 ) ) {
				return false;
			}
		}
		return true;
	}

}
