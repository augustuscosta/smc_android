package br.com.otgmobile.trackteam.model;

public enum ConfiguracaoVisualizaOcorrencia {
	
	VISUALIZA_ATENDIMENTO(0),
	VISUALIZA_EMERGENCIA(1);
	
	int value;

	ConfiguracaoVisualizaOcorrencia(int value) {
		this.value = value;
	}
	
	public static ConfiguracaoVisualizaOcorrencia fromValue(int value){
		for (ConfiguracaoVisualizaOcorrencia configuracaoVisualizaOcorrencia : ConfiguracaoVisualizaOcorrencia.values()) {
			if(value == configuracaoVisualizaOcorrencia.getValue()){
				return configuracaoVisualizaOcorrencia;
			}
		}
		
		return null;
	}

	public int getValue() {
		return value;
	}


	public void setValue(int value) {
		this.value = value;
	}

}
