package br.com.otgmobile.trackteam.model;

import android.database.Cursor;

public class OcorrenciaEnquete {

	public OcorrenciaEnquete(Cursor cursor){
		this.ocorrenciaId = cursor.getInt(1);
		this.enqueteId = cursor.getInt(2);
	}
	
	private Integer ocorrenciaId;
	private Integer enqueteId;
	
	public Integer getOcorrenciaId() {
		return ocorrenciaId;
	}
	public void setOcorrenciaId(Integer ocorrenciaId) {
		this.ocorrenciaId = ocorrenciaId;
	}
	public Integer getEnqueteId() {
		return enqueteId;
	}
	public void setEnqueteId(Integer enqueteId) {
		this.enqueteId = enqueteId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((ocorrenciaId == null) ? 0 : ocorrenciaId.hashCode());
		result = prime * result
				+ ((enqueteId == null) ? 0 : enqueteId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OcorrenciaEnquete other = (OcorrenciaEnquete) obj;
		if (ocorrenciaId == null) {
			if (other.ocorrenciaId != null)
				return false;
		} else if (!ocorrenciaId.equals(other.ocorrenciaId))
			return false;
		if (enqueteId == null) {
			if (other.enqueteId != null)
				return false;
		} else if (!enqueteId.equals(other.enqueteId))
			return false;
		return true;
	}
	
}
