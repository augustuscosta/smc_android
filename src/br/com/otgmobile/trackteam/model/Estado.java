package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

public class Estado implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3334278059952224496L;
	
	private Integer _id;
	
	private Integer id;
	
	private String valor;
	
	private String cor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Integer get_id() {
		return _id;
	}

	public void set_id(Integer _id) {
		this._id = _id;
	}
	
	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

}
