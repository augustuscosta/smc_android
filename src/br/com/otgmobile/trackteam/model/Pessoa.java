package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Pessoa implements Serializable{
	
	/**Created by Bruno Ramos Dias
	 * at On the Go mobile
	 * 04/07/2010
	 */
	
	private static final long serialVersionUID = 8804423809078052635L;
	
	private Integer _id;
	private Integer id;
	private String nome;
	private String cpf;
	private String rg;
	private String passaporte;
	private String descricao;
	
	@SerializedName(value = "restricao")
	private Restricao restricao;
	
	private Integer restricaoId;

	public Integer get_id() {
		return _id;
	}
	
	public void set_id(Integer _id) {
		this._id = _id;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getRg() {
		return rg;
	}
	
	public void setRg(String rg) {
		this.rg = rg;
	}
	
	public String getPassaporte() {
		return passaporte;
	}
	
	public void setPassaporte(String passaporte) {
		this.passaporte = passaporte;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	

	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Restricao getRestricao() {
		return restricao;
	}

	public void setRestricao(Restricao restricao) {
		this.restricao = restricao;
	}

	public Integer getRestricaoId() {
		return restricaoId;
	}

	public void setRestricaoId(Integer restricaoId) {
		this.restricaoId = restricaoId;
	}

}
