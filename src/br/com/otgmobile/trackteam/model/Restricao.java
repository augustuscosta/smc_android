package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

public class Restricao implements Serializable{

	
	/**Created by Bruno Ramos Dias
	 * at On the Go mobile
	 * 04/07/2010
	 */
	
	private static final long serialVersionUID = 7444252409883248201L;
	private Integer _id;
	private Integer id;
	private String nome;
	private String descricao;
	private String cor;
	private String tipo;
	
	
	public Integer get_id() {
		return _id;
	}
	public void set_id(Integer _id) {
		this._id = _id;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
