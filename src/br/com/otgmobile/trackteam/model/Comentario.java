package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

public class Comentario implements Serializable {
	
	/**
	 * Created by Bruno Ramos Dias
	 */
	
	private static final long serialVersionUID = 7653036573834041692L;

	private Integer _id;
	
	private Integer id;
	
	private	Integer ocorrenciaId;
	
	private	String descricao;
	
	private Long data;

	public Integer get_id() {
		return _id;
	}

	public void set_id(Integer _id) {
		this._id = _id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOcorrenciaId() {
		return ocorrenciaId;
	}

	public void setOcorrenciaId(Integer ocorrenciaId) {
		this.ocorrenciaId = ocorrenciaId;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getData() {
		return data;
	}

	public void setData(Long data) {
		this.data = data;
	}

}
