package br.com.otgmobile.trackteam.model;

import android.database.Cursor;

public class OcorrenciaMaterial {
	
	public OcorrenciaMaterial(Cursor cursor){
		this.ocorrenciaId = cursor.getInt(1);
		this.materialId = cursor.getInt(2);
	}
	
	private Integer ocorrenciaId;
	private Integer materialId;
	
	
	public Integer getOcorrenciaId() {
		return ocorrenciaId;
	}
	public void setOcorrenciaId(Integer ocorrenciaId) {
		this.ocorrenciaId = ocorrenciaId;
	}
	public Integer getMaterialId() {
		return materialId;
	}
	public void setMaterialId(Integer materialId) {
		this.materialId = materialId;
	}

}
