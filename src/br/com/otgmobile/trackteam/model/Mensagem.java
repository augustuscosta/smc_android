package br.com.otgmobile.trackteam.model;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.otgmobile.trackteam.util.LogUtil;

public class Mensagem {

	private String from;
	private String to;
	private String mensagem;
	private Long dataMensagem;
	private boolean enviado;

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Long getDataMensagem() {
		return dataMensagem;
	}

	public void setDataMensagem(Long dataMensagem) {
		this.dataMensagem = dataMensagem;
	}

	public boolean isEnviado() {
		return enviado;
	}

	public void setEnviado(boolean enviado) {
		this.enviado = enviado;
	}

	public JSONObject toJson() {
		try {
			final JSONObject json = new JSONObject();
			json.put("from", from);
			json.put("to", to);
			json.put("mensagem", mensagem);
			json.put("dataMensagem", dataMensagem);
			return json;
		} catch (JSONException e) {
			LogUtil.e("Erro ao tentar criar o JsonObject de Mensagem...", e);
		}

		return null;
	}

}
