package br.com.otgmobile.trackteam.model;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author pierrediderot@gmail.com
 * @since 26/02/2012 - Edited
 * @version $Revision:  $ <br>
 *          $Date:  $ <br> 
 *          $Author:  $
 */
public class Ocorrencia implements Serializable {

	private static final long serialVersionUID = -9064857419995100811L;

	private Integer _id;

	private Integer id;
	
	private String codigo;

	private String resumo;

	private String descricao;

	private Integer estado;

	private Integer natureza;

	private Long horaChegada;

	private Long horaSaida;

	private boolean cancelado;

	private boolean validada;

	private String razaoCancelamento;

	private boolean enviado = true;

	private Integer nivelEmergenciaID;

	private Integer enderecoID;

	private NivelEmergencia nivelEmergencia;

	private Endereco endereco;

	private List<Material> materiais;

	private Integer usuarioID;

	private List<Vitima> vitimas;

	private List<Solicitante> solicitantes;

	private List<Veiculo> veiculos;
	
	private List<VeiculoEnvolvido> veiculosEnvolvidos;
	
	private List<Foto> fotos;
	
	private List<Sign> signs;
	
	private List<Filmagem> filmagens;
	
	private List<Comentario> comentarios;
	
	private List<Enquete> enquetes;

	private Estado estadoObj;

	private Natureza naturezaObj;
	
	private Long recebimento;
	
	private Long inicio;
	
	private Long fim;
	
	private Long inicioProgramado;
	
	private Long fimProgramado;
	
	private Long modificado;
	
	
	public String getFormatedEndereco(){
		String toReturn = "";
		if(getEndereco() != null && getEndereco().getEndGeoref() != null){
			toReturn = getEndereco().getEndGeoref();
		}
		return toReturn;
	}
	
	public String getFormatedNivelEmergencia(){
		String toReturn = "";
		if(getNivelEmergencia() != null && getNivelEmergencia().getDescricao() != null){
			toReturn = getNivelEmergencia().getDescricao();
		}
		return toReturn;
	}
	
	public String getFormatedEstado(){
		String toReturn = "";
		if(getEstadoObj() != null && getEstadoObj().getValor() != null){
			toReturn = getEstadoObj().getValor();
		}
		return toReturn;
	}
	
	public String getFormatedNatureza(){
		String toReturn = "";
		if(getNaturezaObj() != null && getNaturezaObj().getValor() != null){
			toReturn = getNaturezaObj().getValor();
		}
		return toReturn;
	}
	
	

	public Long getModificado() {
		return modificado;
	}

	public void setModificado(Long modificado) {
		this.modificado = modificado;
	}

	public Long getInicioProgramado() {
		return inicioProgramado;
	}

	public void setInicioProgramado(Long inicioProgramado) {
		this.inicioProgramado = inicioProgramado;
	}

	public Long getFimProgramado() {
		return fimProgramado;
	}

	public void setFimProgramado(Long fimProgramado) {
		this.fimProgramado = fimProgramado;
	}

	public Long getRecebimento() {
		return recebimento;
	}

	public void setRecebimento(Long recebimento) {
		this.recebimento = recebimento;
	}

	public Long getInicio() {
		return inicio;
	}

	public void setInicio(Long inicio) {
		this.inicio = inicio;
	}

	public Long getFim() {
		return fim;
	}

	public void setFim(Long fim) {
		this.fim = fim;
	}

	public Natureza getNaturezaObj() {
		return naturezaObj;
	}

	public void setNaturezaObj(Natureza naturezaObj) {
		this.naturezaObj = naturezaObj;
	}

	public Estado getEstadoObj() {
		return estadoObj;
	}

	public void setEstadoObj(Estado estadoObj) {
		this.estadoObj = estadoObj;
	}

	public List<Material> getMateriais() {
		return materiais;
	}

	public List<Comentario> getComentarios() {
		return comentarios;
	}

	public void setComentarios(List<Comentario> comentarios) {
		this.comentarios = comentarios;
	}

	public void setMateriais(List<Material> materiais) {
		this.materiais = materiais;
	}

	public Integer getUsuarioID() {
		return usuarioID;
	}

	public void setUsuarioID(Integer usuarioID) {
		this.usuarioID = usuarioID;
	}

	public boolean isEnviado() {
		return enviado;
	}

	public void setEnviado(boolean enviado) {
		this.enviado = enviado;
	}

	public boolean isCancelado() {
		return cancelado;
	}

	public void setCancelado(boolean cancelado) {
		this.cancelado = cancelado;
	}

	public String getRazaoCancelamento() {
		return razaoCancelamento;
	}

	public void setRazaoCancelamento(String razaoCancelamento) {
		this.razaoCancelamento = razaoCancelamento;
	}

	public Integer getNivelEmergenciaID() {
		return nivelEmergenciaID;
	}

	public void setNivelEmergenciaID(Integer nivelEmergenciaID) {
		this.nivelEmergenciaID = nivelEmergenciaID;
	}

	public Integer getEnderecoID() {
		return enderecoID;
	}

	public void setEnderecoID(Integer enderecoID) {
		this.enderecoID = enderecoID;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getResumo() {
		return resumo;
	}

	public void setResumo(String resumo) {
		this.resumo = resumo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getHoraChegada() {
		return horaChegada;
	}

	public void setHoraChegada(Long horaChegada) {
		this.horaChegada = horaChegada;
	}

	public Long getHoraSaida() {
		return horaSaida;
	}
	
	public List<Foto> getFotos() {
		return fotos;
	}

	public void setFotos(List<Foto> fotos) {
		this.fotos = fotos;
	}
	
	public List<Sign> getSigns() {
		return signs;
	}
	
	public void setSigns(List<Sign> signs){
		this.signs = signs;
	}

	public List<Filmagem> getFilmagens() {
		return filmagens;
	}

	public void setFilmagens(List<Filmagem> filmagens) {
		this.filmagens = filmagens;
	}

	public void setHoraSaida(Long horaSaida) {
		this.horaSaida = horaSaida;
	}

	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}

	public NivelEmergencia getNivelEmergencia() {
		return nivelEmergencia;
	}

	public void setNivelEmergencia(NivelEmergencia nivelEmergencia) {
		this.nivelEmergencia = nivelEmergencia;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Vitima> getVitimas() {
		return vitimas;
	}

	public void setVitimas(List<Vitima> vitimas) {
		this.vitimas = vitimas;
	}

	public List<Solicitante> getSolicitantes() {
		return solicitantes;
	}

	public void setSolicitantes(List<Solicitante> solicitantes) {
		this.solicitantes = solicitantes;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Integer getNatureza() {
		return natureza;
	}

	public void setNatureza(Integer natureza) {
		this.natureza = natureza;
	}

	public Integer get_id() {
		return _id;
	}

	public void set_id(Integer _id) {
		this._id = _id;
	}

	public List<Enquete> getEnquetes() {
		return enquetes;
	}

	public void setEnquetes(List<Enquete> enquetes) {
		this.enquetes = enquetes;
	}
	
	public List<VeiculoEnvolvido> getVeiculosEnvolvidos() {
		return veiculosEnvolvidos;
	}

	public void setVeiculosEnvolvidos(List<VeiculoEnvolvido> veiculosEnvolvidos) {
		this.veiculosEnvolvidos = veiculosEnvolvidos;
	}

	public boolean isValidada() {
		return validada;
	}

	public void setValidada(boolean validada) {
		this.validada = validada;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ocorrencia other = (Ocorrencia) obj;
		if (_id == null) {
			if (other._id != null)
				return false;
		} else if (!_id.equals(other._id))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
