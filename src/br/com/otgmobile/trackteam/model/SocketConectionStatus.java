package br.com.otgmobile.trackteam.model;

public enum SocketConectionStatus {
	
	DISCONNECTED(0),
	CONNECTING(1),
	CONNECTED(2);
	
	private int value;
	
	private SocketConectionStatus(int value) {
		this.value = value;
	}

	public static SocketConectionStatus fromValue(int value){
		for (SocketConectionStatus socketConectionStatus : SocketConectionStatus.values()) {
			if(socketConectionStatus.getValue() == value) return socketConectionStatus;
		}
		
		return null;
	}

	public int getValue() {
		return value;
	}
}
