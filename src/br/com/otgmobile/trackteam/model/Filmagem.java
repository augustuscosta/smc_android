package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

public class Filmagem implements Serializable {

	
	private static final long serialVersionUID = -2342189021427021754L;
	
	private Integer _id;
	
	private Integer id;
	private String filmagem;
	private Integer ocorrenciaId;
	private Integer ocorrenciaServerId;
	private Long enviado;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getOcorrenciaId() {
		return ocorrenciaId;
	}
	public void setOcorrenciaId(Integer ocorrenciaId) {
		this.ocorrenciaId = ocorrenciaId;
	}
	
	public Integer get_id() {
		return _id;
	}
	public void set_id(Integer _id) {
		this._id = _id;
	}
	public String getFilmagem() {
		return filmagem;
	}
	public void setFilmagem(String filmagem) {
		this.filmagem = filmagem;
	}
	public Integer getOcorrenciaServerId() {
		return ocorrenciaServerId;
	}
	public void setOcorrenciaServerId(Integer ocorrenciaServerId) {
		this.ocorrenciaServerId = ocorrenciaServerId;
	}
	public Long getEnviado() {
		return enviado;
	}
	public void setEnviado(Long enviado) {
		this.enviado = enviado;
	}
}
