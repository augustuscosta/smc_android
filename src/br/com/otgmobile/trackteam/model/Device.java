package br.com.otgmobile.trackteam.model;

public class Device {
	private String name;
	private String address;

	private Device(String name, String address) {
		this.name = name;
		this.address = address;
	}

	public static Device create(String name, String address) {
		return new Device(name, address);
	}

	@Override
	public String toString() {
		return name + " - " + address;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

}
