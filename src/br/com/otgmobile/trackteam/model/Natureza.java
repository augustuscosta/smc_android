package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

public class Natureza implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8975145248761749732L;

	private Integer _id;
	
	private Integer id;
	
	private String valor;

	public Integer get_id() {
		return _id;
	}

	public void set_id(Integer _id) {
		this._id = _id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}
