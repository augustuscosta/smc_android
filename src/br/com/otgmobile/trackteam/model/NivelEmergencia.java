package br.com.otgmobile.trackteam.model;

import java.io.Serializable;

public class NivelEmergencia implements Serializable {

	private static final long serialVersionUID = -620479816879943932L;
	
	private Integer _id;
	
	private Integer id;
	
	private String cor;
	
	private String descricao;

	public Integer get_id() {
		return _id;
	}

	public void set_id(Integer _id) {
		this._id = _id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
