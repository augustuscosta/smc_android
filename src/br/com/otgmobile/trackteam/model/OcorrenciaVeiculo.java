package br.com.otgmobile.trackteam.model;

import android.database.Cursor;

public class OcorrenciaVeiculo {
	
	public OcorrenciaVeiculo(Cursor cursor){
		this.ocorrenciaId = cursor.getInt(1);
		this.veiculoId = cursor.getInt(2);
	}
	
	private Integer ocorrenciaId;
	private Integer veiculoId;
	
	
	public Integer getOcorrenciaId() {
		return ocorrenciaId;
	}
	public void setOcorrenciaId(Integer ocorrenciaId) {
		this.ocorrenciaId = ocorrenciaId;
	}
	public Integer getVeiculoId() {
		return veiculoId;
	}
	public void setVeiculoId(Integer veiculoId) {
		this.veiculoId = veiculoId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((ocorrenciaId == null) ? 0 : ocorrenciaId.hashCode());
		result = prime * result
				+ ((veiculoId == null) ? 0 : veiculoId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OcorrenciaVeiculo other = (OcorrenciaVeiculo) obj;
		if (ocorrenciaId == null) {
			if (other.ocorrenciaId != null)
				return false;
		} else if (!ocorrenciaId.equals(other.ocorrenciaId))
			return false;
		if (veiculoId == null) {
			if (other.veiculoId != null)
				return false;
		} else if (!veiculoId.equals(other.veiculoId))
			return false;
		return true;
	}
	
	
	

}
