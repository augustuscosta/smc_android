package br.com.otgmobile.trackteam.model;

import java.io.Serializable;
import java.util.List;

public class Cerca implements Serializable {
	
	private static final long serialVersionUID = 4434030770353138268L;

	private Integer _id;
	private Integer id;
	private String cercacol;
	private List<GeoPonto> geoPonto;
	
	
	public Integer get_id() {
		return _id;
	}

	public void set_id(Integer _id) {
		this._id = _id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCercacol() {
		return cercacol;
	}

	public void setCercacol(String cercacol) {
		this.cercacol = cercacol;
	}

	public List<GeoPonto> getGeoPonto() {
		return geoPonto;
	}

	public void setGeoPonto(List<GeoPonto> geoPonto) {
		this.geoPonto = geoPonto;
	}

}
