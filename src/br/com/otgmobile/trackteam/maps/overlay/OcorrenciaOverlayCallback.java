package br.com.otgmobile.trackteam.maps.overlay;

import br.com.otgmobile.trackteam.model.Ocorrencia;


public interface OcorrenciaOverlayCallback {
	void ocorrenciaSelected(Ocorrencia ocorrencia);
}
