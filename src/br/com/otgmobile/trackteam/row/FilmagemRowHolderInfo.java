package br.com.otgmobile.trackteam.row;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.widget.ImageView;
import br.com.otgmobile.trackteam.model.Filmagem;
import br.com.otgmobile.trackteam.util.AppHelper;

public class FilmagemRowHolderInfo {
	
	public ImageView filmagemListImage;
	public Activity context;

	public void drawRow(Filmagem obj, Context context) {
		Bitmap b = null;
		if (obj == null) {
			return;
		}

		if (obj.getFilmagem() != null) {
			
			b= ThumbnailUtils.createVideoThumbnail(returnUriDecoded(obj.getFilmagem()), MediaStore.Video.Thumbnails.MINI_KIND);
			if(b !=null){				
				filmagemListImage.setImageBitmap(b);
			}
		}
	}
	
	private String returnUriDecoded(String uri){
		return AppHelper.getInstance().decodeUri(uri, context);
		
	}
	
	
	

		
	}
