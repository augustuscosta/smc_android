package br.com.otgmobile.trackteam.row;

import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.trackteam.model.Vitima;

public class VitimaRowHolderInfo {
	
	public TextView vitimaListDescription;
	public TextView vitimaListAge;
	public LinearLayout vitimaListButton;
	
	public void drawRow(Vitima obj){
		if(obj == null){
			return;
		}
		if(obj.getDescricao()!=null){
			vitimaListDescription.setText(obj.getDescricao());
		}
		else{
			vitimaListDescription.setText("");
		}
		if(obj.getDescricao()!=null){
			vitimaListAge.setText(Integer.toString(obj.getIdade()));
		}
		else{
			vitimaListAge.setText("");
		}
		
	}

}
