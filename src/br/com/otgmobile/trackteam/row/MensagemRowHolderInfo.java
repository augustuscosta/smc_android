package br.com.otgmobile.trackteam.row;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Mensagem;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.Session;

public class MensagemRowHolderInfo {

	public LinearLayout messageRowContainer;
	public TextView messageOwner;
	public TextView messageContent;
	public TextView messageDate;
	public Button mensagemUnsentFlag;

	public void drawRow(Mensagem obj, Activity context) {
		if (obj == null) {
			return;
		}

		if(obj.getFrom()!= null) {
			if (obj.getFrom().equals(Session.getToken(context))){
				messageOwner.setText(context.getString(R.string.eu));
				messageRowContainer.setBackgroundColor(Color.GRAY);
			}else {
				messageOwner.setText(context.getString(R.string.operador)); 		
				messageRowContainer.setBackgroundColor(Color.DKGRAY);
			}
		}

		if (obj.getDataMensagem() != null && obj.getDataMensagem() > 0) {
			messageDate.setText("("+AppHelper.getInstance().formateDate(context, obj.getDataMensagem())+"):");
		}else{
			messageDate.setText("(sem data):");			
		}

		if (obj.getMensagem() != null) {
			messageContent.setText(obj.getMensagem());
		}
		
		if(obj.isEnviado()){
			mensagemUnsentFlag.setVisibility(View.GONE);
		}else{
			mensagemUnsentFlag.setVisibility(View.VISIBLE);
		}

	} 
}
