package br.com.otgmobile.trackteam.row;

import android.widget.LinearLayout;
import android.widget.TextView;
import br.com.otgmobile.trackteam.model.VeiculoEnvolvido;

public class VeiculoEnvolvidoCadRowHolderInfo {
	
	public TextView 	involvedVehicleLicense;
	public TextView 	involvedVehicleBrand;
	public LinearLayout involvedVehicleDeleteButton;
	
	public void drawRow(VeiculoEnvolvido obj){
		if(obj == null){
			return;
		}
		
		if(obj.getPlaca()!=null){
			involvedVehicleLicense.setText(obj.getPlaca());
		}
		else{
			involvedVehicleLicense.setText("");
		}
		
		if(obj.getMarca()!=null){
			involvedVehicleBrand.setText(obj.getMarca());
		}
		else{
			involvedVehicleBrand.setText("");
		}
	}
	

}
