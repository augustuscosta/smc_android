package br.com.otgmobile.trackteam.row;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import br.com.otgmobile.trackteam.model.Sign;
import br.com.otgmobile.trackteam.util.LogUtil;

public class SignRowHolderInfo {

	public ImageView signListImage;

	public void drawRow(Sign obj, Context context) {
		Bitmap b = null;
		if (obj == null) {
			return;
		}

		if (obj.getImagem() != null) {
			
			b = openFromFile(obj.getImagem(), context);
			if(b !=null){				
				signListImage.setImageBitmap(b);
			}
		}
	}

	private Bitmap openFromFile(String imagem, Context context) {
		FileInputStream fis = null;
		Bitmap bitmap = null;
		try {
			fis = context.openFileInput(imagem);
			bitmap = BitmapFactory.decodeStream(fis);
		} catch (FileNotFoundException e) {
			LogUtil.e("Erro ao tentar printLogo", e);

		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException io) {
					LogUtil.e("Erro ao tentar fechar o FileInputStream", io);
				}
			}
		}
		return bitmap;
	}
}