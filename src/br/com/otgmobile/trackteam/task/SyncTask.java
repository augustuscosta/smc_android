package br.com.otgmobile.trackteam.task;

import java.util.Date;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.WindowManager;
import br.com.otgmobile.trackteam.cloud.rest.AgentesCloud;
import br.com.otgmobile.trackteam.cloud.rest.ConfigurationCloud;
import br.com.otgmobile.trackteam.cloud.rest.LocalizaMaterialCloud;
import br.com.otgmobile.trackteam.cloud.rest.MaterialCloud;
import br.com.otgmobile.trackteam.cloud.rest.NaturezaCloud;
import br.com.otgmobile.trackteam.cloud.rest.NivelEmergenciaCloud;
import br.com.otgmobile.trackteam.cloud.rest.OcorrenciasCloud;
import br.com.otgmobile.trackteam.cloud.rest.RespostaCloud;
import br.com.otgmobile.trackteam.cloud.rest.StatusCloud;
import br.com.otgmobile.trackteam.cloud.rest.VeiculosCloud;
import br.com.otgmobile.trackteam.cloud.rest.VeiculosEnvolvidosCloud;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

public class SyncTask extends AsyncTask<Void, Void, Void> {
	Activity context;
	public ProgressDialog mDialog;
	boolean ok = true;
	public SyncCallBack syncCallBack;
	
	
	public SyncTask(Activity activity) {
		this.context = activity;
		mDialog = ProgressDialog.show(context, "",
				"Carregando...", true, true);
	}

	@Override
	protected void onPreExecute() {
		context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		context.setProgressBarIndeterminate(true);
	}

	@Override
	protected Void doInBackground(Void... arg0) {
		
		ConfigurationCloud configCloud = new ConfigurationCloud();
		VeiculosCloud veiculoCloud = new VeiculosCloud();
		VeiculosEnvolvidosCloud veiculosEnvolvidosCloud = new VeiculosEnvolvidosCloud();
		NivelEmergenciaCloud nivelDeEmergenciaCloud = new NivelEmergenciaCloud();
		NaturezaCloud naturezaCloud = new NaturezaCloud();
		StatusCloud statusCloud = new StatusCloud();
		OcorrenciasCloud ocorrenciaCloud = new OcorrenciasCloud();
		MaterialCloud materialCloud = new MaterialCloud();
		AgentesCloud agenteCloud = new AgentesCloud();
		RespostaCloud respostaCloud = new RespostaCloud();
		LocalizaMaterialCloud localizaMaterialCloud = new LocalizaMaterialCloud();
		try {
			LogUtil.i("Sync Configurações");
			configCloud.sync(context);
			LogUtil.i("Sync veiculo");
			veiculoCloud.sync(context);
			LogUtil.i("Sync cerca");
			veiculoCloud.syncCerca(context);
			LogUtil.i("Sync rota");
			veiculoCloud.syncRota(context);
			LogUtil.i("Sync veiculos envolvidos");
			veiculosEnvolvidosCloud.sync(context);
			LogUtil.i("Sync nivel de emergencia");
			nivelDeEmergenciaCloud.sync(context);
			LogUtil.i("Sync natureza");
			naturezaCloud.sync(context);
			LogUtil.i("Sync estado");
			statusCloud.sync(context);
			LogUtil.i("Enviando ocorrencias não sincronizadas");
			ocorrenciaCloud.sync(context);
			LogUtil.i("Sync ocorrencia");
			ocorrenciaCloud.sync(context);
			LogUtil.i("Enviando materiais em campo");
			localizaMaterialCloud.sendAllUnsent(context);
			LogUtil.i("Enviando respostas");
			respostaCloud.sendAllUnsent(context);
			LogUtil.i("Sync material");
			materialCloud.sync(context);
			LogUtil.i("Sync agente");
			agenteCloud.sync(context);
		} catch (Throwable e) {
			LogUtil.e("erro ao syncronizar",e);
			ok = false;
		}
		return null;
	}
	
	@Override
	public void onPostExecute(Void params) {
		context.setProgressBarIndeterminate(false);
		context.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		mDialog.dismiss();
		if(ok){
			Session.setLastSync(new Date(), context);
			
		}else{
			AppHelper.getInstance().presentError(context, "Erro", "Erro, verifique as configurações..");
		}
		if (syncCallBack != null){
			syncCallBack.finishTask(ok);
		}
	}
}
