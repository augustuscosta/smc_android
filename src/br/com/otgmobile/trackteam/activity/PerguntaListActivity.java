package br.com.otgmobile.trackteam.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.RespostaActivity;
import br.com.otgmobile.trackteam.model.Enquete;
import br.com.otgmobile.trackteam.model.Pergunta;
import br.com.otgmobile.trackteam.util.ConstUtil;

public class PerguntaListActivity extends GenericListActivity {
	
	 ListView listView ;
	 List<Pergunta> list;
	 Enquete enquete;
	 Integer ocorrenciaId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pergunta_list);
		handleIntent();
		setList();
    }
	
	private void handleIntent() {
		Intent intent = getIntent();
		if (intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)) {
			enquete = (Enquete) intent.getExtras().get(ConstUtil.SERIALIZABLE_KEY);
			if(enquete.getPerguntas() != null){
				list = new ArrayList<Pergunta>();
				for(Pergunta pergunta : enquete.getPerguntas()){
					list.add(pergunta);
				}
			}
		}
		if (intent.hasExtra(ConstUtil.ID_KEY)) {
			ocorrenciaId = (Integer) intent.getExtras().get(ConstUtil.ID_KEY);
		}
	}
	
	private void setList(){
		// Get ListView object from xml
        listView = getListView();
        
        // Defined Array values to show in ListView
        ArrayList<String> values;
        values = new ArrayList<String>();
        for(Pergunta pergunta:list){
        	values.add(pergunta.getPergunta());
        }
        

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
          android.R.layout.simple_list_item_1, android.R.id.text1, values);


        // Assign adapter to ListView
        listView.setAdapter(adapter); 
        
        // ListView Item Click Listener
        listView.setOnItemClickListener(new OnItemClickListener() {

              @Override
              public void onItemClick(AdapterView<?> parent, View view,
                 int position, long id) {
            	  Pergunta pergunta = list.get(position);
                  
                  Intent intent = new Intent(PerguntaListActivity.this,
          				RespostaActivity.class);
   	       		Bundle bundle = new Bundle();
   	       		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, pergunta);
   	       		bundle.putInt(ConstUtil.ID_KEY, ocorrenciaId);
   	       		intent.putExtras(bundle);
   	       		startActivity(intent);
              }

         }); 
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
} 	
