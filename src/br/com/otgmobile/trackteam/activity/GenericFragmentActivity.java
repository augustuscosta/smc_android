package br.com.otgmobile.trackteam.activity;

import java.util.Date;

import org.apache.http.HttpStatus;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.location.GpsStatus;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.cloud.rest.ImagemRequeridaCloud;
import br.com.otgmobile.trackteam.database.ConfigurationDAO;
import br.com.otgmobile.trackteam.database.ImagemRequeridaDAO;
import br.com.otgmobile.trackteam.database.OcorrenciaDAO;
import br.com.otgmobile.trackteam.gps.DLGpsObserver;
import br.com.otgmobile.trackteam.model.Historico;
import br.com.otgmobile.trackteam.model.ImagemRequerida;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.BroadCastReceiverDictionary;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.FileUtil;
import br.com.otgmobile.trackteam.util.ImageRequestType;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.NotificationUtil;
import br.com.otgmobile.trackteam.util.PrecisionUtil;
import br.com.otgmobile.trackteam.util.Session;

public class GenericFragmentActivity extends FragmentActivity implements DLGpsObserver{

	private String token;
	protected Camera generalCamera;
	private Long mlastFixTime;
	protected Button gpsStatusIcon;

	@Override
	protected void onResume() {
		super.onResume();
		mlastFixTime = Session.getLastFixTime(this);
		setGpsStatusIcon();
		registerReceiver(TelemetriaStatusReciever, new IntentFilter(BroadCastReceiverDictionary.EXIBIR_CONEXAO_TELEMETRIA.getValue()));
		registerReceiver(openCamReceiver, new IntentFilter(BroadCastReceiverDictionary.ABRIR_CAMERA.getValue()));
		registerReceiver(updateOccurrenceReceiver, new IntentFilter(BroadCastReceiverDictionary.MODIFICA_OCORRENCIA.getValue()));
		registerReceiver(deleteOcurrenceReceiver, new IntentFilter(BroadCastReceiverDictionary.EXCLUI_OCORRENCIA.getValue()));
		registerReceiver(newOccurrenceReceiver, new IntentFilter(BroadCastReceiverDictionary.NOVA_OCORRENCIA.getValue()));
		registerReceiver(closeCamReceiver, new IntentFilter(BroadCastReceiverDictionary.FECHAR_CAMERA.getValue()));
		registerReceiver(newHistoryReceiver, new IntentFilter(BroadCastReceiverDictionary.MODIFICA_HISTORICO.getValue()));
		registerReceiver(newChatMessageReceiver, new IntentFilter(BroadCastReceiverDictionary.RECEBER_MENSAGEM.getValue()));
		registerReceiver(conectionStatusReciever, new IntentFilter(BroadCastReceiverDictionary.EXIBIR_CONEXAO.getValue()));

	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(TelemetriaStatusReciever);
		unregisterReceiver(newOccurrenceReceiver);
		unregisterReceiver(updateOccurrenceReceiver);
		unregisterReceiver(openCamReceiver);
		unregisterReceiver(closeCamReceiver);
		unregisterReceiver(newHistoryReceiver);
		unregisterReceiver(deleteOcurrenceReceiver);
		unregisterReceiver(newChatMessageReceiver);
		unregisterReceiver(conectionStatusReciever);
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	final private BroadcastReceiver openCamReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			gotCamRequest();
		}
	};

	final private BroadcastReceiver closeCamReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			gotCloseCamRequest();
		}
	};

	final private BroadcastReceiver updateOccurrenceReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Ocorrencia ocorrencia = (Ocorrencia) intent.getExtras()
					.getSerializable(ConstUtil.SERIALIZABLE_KEY);
			gotUpdateOccurrence(ocorrencia);
		}
	};

	final private BroadcastReceiver deleteOcurrenceReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Ocorrencia ocorrencia = (Ocorrencia) intent.getExtras()
					.getSerializable(ConstUtil.SERIALIZABLE_KEY);
			gotDeleteOccurrence(ocorrencia);
		}
	};

	final private BroadcastReceiver newOccurrenceReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Ocorrencia ocorrencia = (Ocorrencia) intent.getExtras()
					.getSerializable(ConstUtil.SERIALIZABLE_KEY);
			gotNewOccurrence(ocorrencia);
		}
	};

	final private BroadcastReceiver newHistoryReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			Historico historico = (Historico) intent.getExtras()
					.getSerializable(ConstUtil.SERIALIZABLE_KEY);
			gotNewHistory(historico);
		}
	};

	final private BroadcastReceiver newChatMessageReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			token = intent.getStringExtra(ConstUtil.TOKEN);
			gotNewChatMessage();
		}
	};


	final private BroadcastReceiver conectionStatusReciever = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String status = (String) intent.getExtras().get(ConstUtil.CONECTION_STATUS);
			showToastWithConectionStatus(status);
		}
	};


	final private BroadcastReceiver TelemetriaStatusReciever = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String status = (String) intent.getExtras().get(ConstUtil.TELEMETRIA_CONECTION_STATUS);
			updateTelemetriaStatusByBroadcast(status);
		}

	};


	protected void gotNewChatMessage() {
		if(token.equals(Session.getToken(this))) return;

		new AlertDialog.Builder(this)
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setTitle(R.string.nova_mensagem)
		.setMessage(R.string.ver_mensagem)
		.setPositiveButton(R.string.yes,
				new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog,
					int which) {
				Intent intent = new Intent(
						GenericFragmentActivity.this,
						ChatActivity.class);
				intent.putExtra(ConstUtil.TOKEN, token);
				startActivity(intent);
			}

		}).setNegativeButton(R.string.no, null).show();
	}

	protected void updateBatteryStatus(int percent) {
		// TODO Auto-generated method stub

	}


	protected void gotNewOccurrence(Ocorrencia ocorrencia) {
		showNewOccurenceDialog(ocorrencia);
	}

	protected void gotUpdateOccurrence(Ocorrencia ocorrencia) {

	}

	protected void gotDeleteOccurrence(Ocorrencia ocorrencia) {

	}

	protected void gotCamRequest() {
		ConfigurationDAO dao = new  ConfigurationDAO(this);
		if(dao.getCamRequest() == ImageRequestType.VIDEO)
			startActivity(new Intent(this, CamActivity.class));
		else
			takePictureNoPreview();
	}

	public void takePictureNoPreview(){
		// open back facing camera by default
		if(generalCamera != null){
			return;
		}
		generalCamera = Camera.open();			
		if(generalCamera!=null){
			try{
				SurfaceView dummy=new SurfaceView(this);
				generalCamera.setPreviewDisplay(dummy.getHolder());    
				generalCamera.startPreview();
				generalCamera.takePicture(null, null, jpeg);
			}catch (Exception e) {
				LogUtil.e("Erro ao capturar a imagem requerida pelo servidor", e);
			}
		}else{
			LogUtil.e("Não foi possivel abrir a câmera");
			return;
		}
	}

	PictureCallback jpeg = new PictureCallback() {
		@Override
		public void onPictureTaken(byte[] data, final Camera camera) {
			FileUtil fileUtil = new FileUtil();
			ImagemRequeridaCloud cloud = new ImagemRequeridaCloud();
			try {
				try{
					cloud.uploadImagemRequerida(GenericFragmentActivity.this, data);
				}catch (Throwable e) {
					LogUtil.e("Erro ao enviar a imagem requerida para o servidor", e);
				}
				if(cloud.getResponseCode() != HttpStatus.SC_OK){
					ImagemRequeridaDAO dao = new ImagemRequeridaDAO(GenericFragmentActivity.this);
					ImagemRequerida imagem = new ImagemRequerida();
					imagem.setImagem("imagem_requerida"+AppHelper.getCurrentTime());
					fileUtil.writeBitmapToFile(data, imagem.getImagem(), GenericFragmentActivity.this);
					dao.save(imagem);
				}
			} catch (Throwable e) {
				LogUtil.e("Erro ao salvar a imagem requerida pelo servidor", e);
			}
			finally{
				if(generalCamera!=null){
					generalCamera.stopPreview();
					generalCamera.setPreviewCallback(null);
					generalCamera.release();
					generalCamera = null;
				}

			}
		}
	};


	protected void gotCloseCamRequest() {
		// TODO Auto-generated method stub
	}

	protected void gotNewHistory(Historico historico) {
		// TODO Auto-generated method stub
	}

	protected void showNewOccurenceDialog(final Ocorrencia ocorrencia) {

		if (ocorrencia == null)
			return;

		final Dialog dialog = new Dialog(this);

		OnClickListener confirmButtonListener = new View.OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
				novaOcorrencia(ocorrencia);
				NotificationUtil.removeNotification(GenericFragmentActivity.this);
			}

		};

		dialog.setContentView(R.layout.nova_ocorrencia_alert);
		final Button ok = (Button) dialog.findViewById(R.id.bt_ok);
		//final TextView address = (TextView) dialog.findViewById(R.id.address);
		final TextView nivelEmergencia = (TextView) dialog
				.findViewById(R.id.nivelEmergencia);

		//address.setText(ocorrencia.getFormatedEndereco());
		nivelEmergencia.setText(ocorrencia.getFormatedNivelEmergencia());
		ok.setOnClickListener(confirmButtonListener);
		dialog.show();
		NotificationUtil.createNewOcurenceNotification(ocorrencia, this);
	}

	private void novaOcorrencia(final Ocorrencia ocorrencia) {
		OcorrenciaDAO dao = new OcorrenciaDAO(GenericFragmentActivity.this);
		ocorrencia.setRecebimento(new Date().getTime());
		ocorrencia.setEnviado(false);
		dao.save(ocorrencia);
		Intent intent = new Intent(GenericFragmentActivity.this, OcorrenciaPreviewActivity.class);
		final Bundle bundle = new Bundle();
		Ocorrencia ocorrencia2 = dao.find(ocorrencia.getId());
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia2);
		intent.putExtras(bundle);
		startActivity(intent);
	}

	// KIOSKE MODE THINGS

	@Override
	public boolean onSearchRequested() {
		return false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (acceptKey(keyCode)) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		if (acceptKey(keyCode)) {
			return true;
		}

		return super.onKeyLongPress(keyCode, event);
	}

	public static boolean acceptKey(final int keyCode) {
		if ((KeyEvent.KEYCODE_HOME == keyCode || KeyEvent.KEYCODE_SEARCH == keyCode)) {

			LogUtil.d("Kiosk mode is enabled, block keyPress");
			return true;
		}

		return false;
	}

	// MENU SECURITY

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return showMenuSecurity();
	}

	protected boolean showMenuSecurity() {
		Intent intent = new Intent(GenericFragmentActivity.this,
				AutenticacaoActivity.class);
		startActivityForResult(intent, ConstUtil.SECURITY_REQUEST);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		if (requestCode == ConstUtil.SECURITY_REQUEST
				&& resultCode == RESULT_OK) {
			SettingsActivity.open(this);
		}
	}

	// Conection Listener


	protected void showToastWithConectionStatus(String status) {
		Toast.makeText(GenericFragmentActivity.this, status, Toast.LENGTH_SHORT).show();
	}

	protected void updateTelemetriaStatusByBroadcast(String status) {

	}

	@Override
	public void onLocationChanged(Location location) {
		mlastFixTime = AppHelper.getCurrentTime();
		Session.setLastFixTime(mlastFixTime, this);
		setGpsStatusIcon();
	}

	@Override
	public void onStatusChanged(int status) {
		setGpsStatusIcon();
	}



	protected Boolean setFixStatus(int status) {
		if(status == GpsStatus.GPS_EVENT_FIRST_FIX){
			mlastFixTime = AppHelper.getCurrentTime();
			Session.setLastFixTime(mlastFixTime, this);
			return true;
		}else if((mlastFixTime == 0|| mlastFixTime == null)){
			return false;
		}else if((AppHelper.getCurrentTime() -mlastFixTime) < PrecisionUtil.getMinTimeGps(this)){
			return true;
		}
		return false;
	}

	private void setGpsStatusIcon() {
		if(gpsStatusIcon != null){
			if(setFixStatus(GpsStatus.GPS_EVENT_SATELLITE_STATUS)){
				gpsStatusIcon.setVisibility(View.VISIBLE);
			}else{
				gpsStatusIcon.setVisibility(View.GONE);
			}
		}

	}

}
