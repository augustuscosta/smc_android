package br.com.otgmobile.trackteam.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.adapter.MaterialAdapter;
import br.com.otgmobile.trackteam.cloud.rest.MaterialCloud;
import br.com.otgmobile.trackteam.model.Material;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

public class MaterialRequestActivity extends GenericListActivity {

	private MaterialAdapter adapter;
	private List<Material> list;
	private List<Material> requestedItemsList;
	private static OnlineMaterialSearchTask onlineMaterialSearchTask;
	private static OnlineMaterialRequestTask onlineMaterialRequestTask;
	private MaterialCloud materialCloud;
	private EditText materialSearch;
	private Button socketStatus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_materiais_for_request);
		configureActivity();
	}

	private void configureActivity() {
		materialSearch = (EditText) findViewById(R.id.material_search);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}

	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}

	@Override
	public void onBackPressed() {
		if(requestedItemsList != null && !requestedItemsList.isEmpty()){
			onlineMaterialRequestTask = new OnlineMaterialRequestTask(this);
			onlineMaterialRequestTask.execute();			
		}else{
			finish();
		}
	};
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu material_request_menu) {
		MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.material_request_menu, material_request_menu);
        return true;
	};
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.material_request_material_add:
        	if(adapter == null || list == null){
    			return true;
    		}
        	else{
        		addMaterial();
        	}
            return true;
 
        case R.id.material_request_materials:
        	viewMaterials();
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	private void setAdapter() {
		adapter = new MaterialAdapter(list, this, true);
		setListAdapter(adapter);
	}

	private class OnlineMaterialSearchTask extends AsyncTask<Void, Void, List<Material>>{
		ProgressDialog mDialog;
		Context context;
		String  search;

		OnlineMaterialSearchTask(Context context,String  search){
			this.context = context;
			this.search = search;
		}

		List<Material> searchPessoa() throws Exception{
			return materialCloud().search(context, search);
		}

		@Override
		protected void onPreExecute() {
			getWindow()
			.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			mDialog = ProgressDialog.show(context, "",
					getString(R.string.processing), true, false);
			mDialog.setCancelable(false);
			setProgressBarIndeterminate(true);
		}

		@Override
		protected List<Material> doInBackground(Void... params) {
			try {
				return searchPessoa();
			} catch (Exception e) {
				LogUtil.e("erro ao pesquisar pessoa", e);
				return null;
			}
		}

		@Override
		protected void onPostExecute(List<Material> result) {				
			list = result;
			mDialog.cancel();
			setAdapter();
		}

	}

	private class OnlineMaterialRequestTask extends AsyncTask<Void, Void, Boolean>{
		ProgressDialog mDialog;
		Context context;

		OnlineMaterialRequestTask(Context context){
			this.context = context;
		}

		Boolean requestMaterials() throws Throwable{
			return materialCloud().sendCheckList(requestedItemsList, context);
		}

		@Override
		protected void onPreExecute() {
			getWindow()
			.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			mDialog = ProgressDialog.show(context, "",
					getString(R.string.processing), true, false);
			mDialog.setCancelable(false);
			setProgressBarIndeterminate(true);
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				return requestMaterials();
			} catch (Throwable e) {
				LogUtil.e("erro ao pesquisar material", e);
				return false;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {				
			mDialog.cancel();
			if(result){
				finish();
			}else{
				getFailureConFimationDialog();
			}
		}

	}

	private void getFailureConFimationDialog() {
		new AlertDialog.Builder(this)
		.setIcon(android.R.drawable.ic_dialog_alert)
		.setTitle(R.string.alert)
		.setMessage(R.string.material_request_failure)
		.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}

		})
		.setNegativeButton(R.string.no, null)
		.show();

	}


	private MaterialCloud materialCloud() {
		if(materialCloud == null) {
			materialCloud = new MaterialCloud();
		}

		return materialCloud;
	}

	//Handle Button Clicks

	public void addMaterial(){
		creatSingleInstanceRequestedItemsList();
		if(!adapter.getCheckedItems().isEmpty()){
			for (Material material : adapter.getCheckedItems()) {
				if(!requestedItemsList.contains(material)){
					requestedItemsList.add(material);
					
					new AlertDialog.Builder(this)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle(R.string.alert)
					.setMessage(R.string.add_material_feedback)
					.setPositiveButton(R.string.ok, null)
					.show();
				}
			}
		}
	}

	protected void creatSingleInstanceRequestedItemsList() {
		if(requestedItemsList == null){
			requestedItemsList = new ArrayList<Material>();
		}
	}

	public void viewMaterials(){
		if(requestedItemsList != null && !requestedItemsList.isEmpty()){
			viewSelectedMaterials();
			return;
		}
		AppHelper.getInstance().presentError(this, getString(R.string.error), getString(R.string.material_unavailable));

	}

	protected void viewSelectedMaterials() {
		Intent intent = new Intent(MaterialRequestActivity.this, MaterialList.class);
		Bundle bundle = new Bundle();
		Ocorrencia ocorrencia = new Ocorrencia();
		ocorrencia.setMateriais(requestedItemsList);
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		bundle.putBoolean(ConstUtil.FOR_MATERIAL_REQUEST, true);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.MATERIAL_REQUEST);
	}

	public void onClickSearch(View view){
		onlineMaterialSearchTask = new OnlineMaterialSearchTask(this, materialSearch.getText().toString());
		onlineMaterialSearchTask.execute();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		if (requestCode == ConstUtil.MATERIAL_REQUEST) {
			if (resultCode == RESULT_OK) {
				final Ocorrencia ocorrencia;
				ocorrencia = (Ocorrencia) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY);
				requestedItemsList = ocorrencia.getMateriais();
			}
		}

	}

	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}

	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}

	protected void setSocketStatus() {
		SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);

		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));

			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}
}
