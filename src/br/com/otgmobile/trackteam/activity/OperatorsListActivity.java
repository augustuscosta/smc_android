package br.com.otgmobile.trackteam.activity;

import java.util.List;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Operador;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.service.SocketService;
import br.com.otgmobile.trackteam.service.SocketService.ChatListener;
import br.com.otgmobile.trackteam.service.SocketService.IChatService;
import br.com.otgmobile.trackteam.service.SocketService.SocketIOBinder;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

public class OperatorsListActivity extends GenericListActivity {

	private IChatService mChatService;
	private ArrayAdapter<String> adapter;
	private List<Operador> operators;
	private Button socketStatus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.operadores_list);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
		
	}

	@Override
	protected void onResume() {
		super.onResume();
		bindService(new Intent(SocketService.SOCKET_SERVICE_INTENT),
				serviceConn, BIND_AUTO_CREATE);
		setSocketStatus();
		
	}

	@Override
	protected void onPause() {
		super.onPause();
		unbindService(serviceConn);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	};
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu operadores_menu) {
		MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.operadores_menu, operadores_menu);
        return true;
	};
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.operadores_menu_refresh:
        	refreshOperators();
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	private ServiceConnection serviceConn = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mChatService.removeListener(chatListener);
			mChatService = null;

		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder binder) {
			mChatService = ((SocketIOBinder) binder).getChatInterface();	
			mChatService.addListener(chatListener);
			mChatService.getOperatorsChat();				


		}
	};

	final private ChatListener chatListener = new ChatListener() {

		@Override
		public void onRetrieveOperators(final List<Operador> operators) {
			OperatorsListActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					setAdapter(operators);
				}
			});
		}

		

		@Override
		public void onConnected() {
			LogUtil.d("Connected with chat");
		}
	};
	
	public void refreshOperators(){
		if(mChatService != null){
			mChatService.getOperatorsChat();
		}
	}
	
	private void setAdapter(List<Operador> operators) {
		if(operators == null || operators.isEmpty()){
			return;
		}
		
		if ( adapter == null ) {
			adapter = new ArrayAdapter<String>(this, R.layout.simple_list_item_1, android.R.id.text1);
		} else {
			adapter.clear();
		}
		
		this.operators = operators;
		for (Operador operator : operators) {
			adapter.add(operator.getName() );
		}	
		setListAdapter(adapter);
		getListView().setVisibility(View.VISIBLE);
		
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Operador obj = operators.get(position);
		Intent intent = new Intent(OperatorsListActivity.this,ChatActivity.class);
		intent.putExtra(ConstUtil.TOKEN, obj.getToken());
		startActivity(intent);
		
	}
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}

}
