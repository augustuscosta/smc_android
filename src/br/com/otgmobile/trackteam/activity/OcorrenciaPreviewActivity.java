package br.com.otgmobile.trackteam.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.database.OcorrenciaDAO;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.NavigationUtil;
import br.com.otgmobile.trackteam.util.Session;

public class OcorrenciaPreviewActivity extends GenericActivity{
	private TextView idText;
	private TextView resumeText;
	private TextView descriptionText;
	private TextView addressText;
	private TextView naturezaText;
	private TextView estadoText;
	private TextView horaProgramaText;
	private TextView horaAtendimentoText;
	private TextView fimText;
	private TextView fimProgramadoText;
	private TextView nivelEmergenciaText;
	private Ocorrencia ocorrencia;
	private Button socketStatus;
	private Button bateriaStatus;
	private Button telemetriaStatus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ocorrencia_preview);
		configureActivity();
		handleIntent();
	}

	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu ocorrencias_preview_menu) {
		MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.ocorrencia_preview_menu, ocorrencias_preview_menu);
        return true;
	};
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.ocorrencias_preview_menu_caller_select:
        	getCallers();
            return true;
        case R.id.ocorrencias_preview_menu_material_select:
        	getMaterials();
            return true;
        case R.id.ocorrencias_preview_menu_navigate:
        	navigateToOcorrencia();
            return true;
        case R.id.ocorrencias_preview_menu_photos:
        	takePicture();
            return true;
        case R.id.ocorrencias_preview_menu_signs:
        	takeSign();
        	return true;
        /*case R.id.ocorrencias_preview_menu_video_gallery:
        	takeFootage();
            return true;*/
        case R.id.ocorrencias_preview_menu_comments_select:
        	seeAndAddComents();
            return true;
        case R.id.ocorrencias_preview_menu_poll:
        	seePolls();
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	private void configureActivity() {
		idText = (TextView) findViewById(R.id.id_ocorrencia);
		resumeText = (TextView) findViewById(R.id.resume_text);
		descriptionText = (TextView) findViewById(R.id.description_text);
		addressText = (TextView) findViewById(R.id.address_text);
		naturezaText = (TextView) findViewById(R.id.natureza_text);
		estadoText = (TextView) findViewById(R.id.status_text);
		nivelEmergenciaText = (TextView) findViewById(R.id.emergencia_text);
		horaProgramaText = (TextView) findViewById(R.id.hora_programada_text);
		horaAtendimentoText = (TextView) findViewById(R.id.hora_atendimento_text);
		fimText = (TextView) findViewById(R.id.hora_final_text);
		fimProgramadoText = (TextView) findViewById(R.id.hora_programada_fim_text);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		bateriaStatus = (Button) findViewById(R.id.bateriaStatusIcon);
		telemetriaStatus = (Button) findViewById(R.id.telemetriaStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}

	private void handleIntent() {
		Intent intent = getIntent();
		if (intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)) {
			ocorrencia = (Ocorrencia) intent.getExtras().get(
					ConstUtil.SERIALIZABLE_KEY);		
			fillViewWithData(ocorrencia);
		}

	}


	private void fillViewWithData(Ocorrencia obj) {
		if (obj == null) {
			return;
		}

		if(obj.getCodigo() != null){
			idText.setText(obj.getCodigo().toString());
		}
		if (obj.getResumo() != null) {
			resumeText.setText(obj.getResumo());
		}
		if (obj.getDescricao() != null) {
			descriptionText.setText(obj.getDescricao());
		}
		if (obj.getEndereco() != null
				&& obj.getEndereco().getEndGeoref() != null) {
			addressText.setText(obj.getEndereco().getEndGeoref());
		}
		if (obj.getNaturezaObj() != null
				&& obj.getNaturezaObj().getValor() != null) {
			naturezaText.setText(obj.getNaturezaObj().getValor());
		}
		if (obj.getEstadoObj() != null && obj.getEstadoObj().getValor() != null) {
			estadoText.setText(obj.getEstadoObj().getValor());
		}
		if (obj.getNivelEmergencia() != null
				&& obj.getNivelEmergencia().getDescricao() != null) {
			nivelEmergenciaText
			.setText(obj.getNivelEmergencia().getDescricao());
		}

		if (obj.getInicioProgramado() != null && obj.getInicioProgramado() > 0) {
			horaProgramaText.setText(AppHelper.getInstance().formateDate(this,obj.getInicioProgramado()));
		}

		if (obj.getInicio() != null && obj.getInicio() > 0) {
			horaAtendimentoText.setText(AppHelper.getInstance().formateDate(this,obj.getInicio()));
		}

		if(obj.getFimProgramado()!=null && obj.getFimProgramado() > 0){
			fimProgramadoText.setText(AppHelper.getInstance().formateDate(this,obj.getFimProgramado()));
		}

		if(obj.getFim()!=null && obj.getFim() > 0){
			fimText.setText(AppHelper.getInstance().formateDate(this,obj.getFim()));
		}

	}

	public void comentarioButtonOnClick(View view){
		seeAndAddComents();
	}

	public void materialSelectButtonOnClick(View view) {
		getMaterials();
	}

	private void getMaterials() {
		Intent intent = new Intent(OcorrenciaPreviewActivity.this, MaterialList.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.MATERIAL_REQUEST);
	} 

	@Override
	public void onBackPressed() {
		upadte();
		super.onBackPressed();
	}


	private void upadte() {
		ocorrencia.setEnviado(false);
		OcorrenciaDAO dao = new OcorrenciaDAO(this);
		dao.save(ocorrencia);	
	}

	public void filmButtonOnClick(View view) {
		takeFootage();
	}

	private void seeAndAddComents() {
		Intent intent = new Intent(OcorrenciaPreviewActivity.this, ComentarioListActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.COMMENT_REQUEST);		
	}
	
	private void seePolls(){
		Intent intent = new Intent(OcorrenciaPreviewActivity.this, EnqueteListActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.ENQUETE_REQUEST);
	}

	private void takePicture() {
		Intent intent = new Intent(OcorrenciaPreviewActivity.this, FotoListActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.FOTO_REQUEST);
	}

	private void takeFootage() {
		Intent intent = new Intent(OcorrenciaPreviewActivity.this,
				FilmagemListActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.FOOTAGE_REQUEST);
	}
	
	private void takeSign(){
		Intent intent = new Intent(OcorrenciaPreviewActivity.this, SignListActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.SIGN_REQUEST);
	}

	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		if (requestCode == ConstUtil.FOTO_REQUEST) {
			if (resultCode == RESULT_OK) {
				ocorrencia = (Ocorrencia) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY);
			}
		}

		if (requestCode == ConstUtil.FOOTAGE_REQUEST) {
			if (resultCode == RESULT_OK) {
				ocorrencia = (Ocorrencia) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY);
			}
		}

		if (requestCode == ConstUtil.COMMENT_REQUEST) {
			if (resultCode == RESULT_OK) {
				ocorrencia = (Ocorrencia) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY);
			}
		}

		if (requestCode == ConstUtil.MATERIAL_REQUEST) {
			if (resultCode == RESULT_OK) {
				ocorrencia = (Ocorrencia) intent.getExtras().get(ConstUtil.SERIALIZABLE_KEY);
			}
		}
		if (requestCode == ConstUtil.CALLER_REQUEST) {
			if (resultCode == RESULT_OK) {
				ocorrencia = (Ocorrencia) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY);
			}
		}
	}

	private void getCallers() {
		Intent intent = new Intent(OcorrenciaPreviewActivity.this, SolicitanteList.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		bundle.putBoolean(ConstUtil.BLOCK_DELETE_AND_CREATE, true);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.CALLER_REQUEST);
	}

	private void navigateToOcorrencia() {
		NavigationUtil.openNavigationFromStopMap(this, ocorrencia.getEndereco().getLatitude(), ocorrencia.getEndereco().getLongitude());
	}

	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}

	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}

	protected void setSocketStatus() {
		SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);

		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));

			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}

	@Override
	protected void updateBatteryStatus(int percent) {
		if(percent <= 30){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_vermelha));
		}else if(percent <= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_amarela));

		}else if(percent >= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_verde));			
		}

	}

	@Override
	protected void updateTelemetriaStatusByBroadcast(String status) {
		if(status.equals(getString(R.string.connected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluegreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.blueyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluered));
			return;
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		super.onLocationChanged(location);
	}

	@Override
	public void onStatusChanged(int status) {
		super.onStatusChanged(status);
	}

}
