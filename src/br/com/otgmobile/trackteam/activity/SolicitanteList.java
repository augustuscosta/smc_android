package br.com.otgmobile.trackteam.activity;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.adapter.SolicitanteAdapter;
import br.com.otgmobile.trackteam.database.SolicitanteDAO;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.model.Solicitante;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.HandleListButton;
import br.com.otgmobile.trackteam.util.Session;

public class SolicitanteList extends GenericListActivity implements HandleListButton{

	private List<Solicitante> list;
	private Ocorrencia ocorrencia;
	private boolean otherRuns = false;
	private SolicitanteAdapter adapter;
	private boolean blockDeleteAndCreate;
	private LinearLayout listSolicitanteContiner;
	private Button socketStatus;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_solicitantes);
		configureActivity();
		handleIntent();
		
	}
	
	@Override
	public void onBackPressed() {
		quitAndPassCallers();
	};
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (blockDeleteAndCreate) {
			return false;
		} else {
			return true;
		}
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu solicitantes_menu) {
		MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.solicitantes_menu, solicitantes_menu);
        return true;
	};
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.solicitantes_menu_new_caller:
        	registerNewCaller();
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	private void configureActivity() {

		  listSolicitanteContiner = (LinearLayout) findViewById(R.id.list_solicitantes_container);
		  socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		  gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}

	public void handleIntent(){
		Intent intent = getIntent();
		ocorrencia = (Ocorrencia)  intent.getExtras().get(ConstUtil.SERIALIZABLE_KEY);
		blockDeleteAndCreate = intent.getBooleanExtra(ConstUtil.BLOCK_DELETE_AND_CREATE, false);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		getSolicitantes();
		setSocketStatus();
	}
	
	@Override
	protected void onDestroy() {
		setListAdapter(null);
		AppHelper.unbindDrawables(listSolicitanteContiner);
		super.onDestroy();
	}
	
	private void getSolicitantes() {
		list = search();
		if((list!=null&& !list.isEmpty())){
			setAdapter();
		}
		else{
			if(!otherRuns && !blockDeleteAndCreate){
				otherRuns = true;
				registerNewCaller();
			}
		}
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Solicitante obj = list.get(position);
		Intent intent = new Intent(SolicitanteList.this, SolicitanteForm.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, obj);
		bundle.putBoolean(ConstUtil.BLOCK_DELETE_AND_CREATE, blockDeleteAndCreate);
		intent.putExtras(bundle);
		startActivity(intent);
	}
	
	private void quitAndPassCallers() {
		ocorrencia.setSolicitantes(list);
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		setResult(RESULT_OK, intent);
		finish();
	}

	
	
	private void setAdapter(){
		 adapter = new SolicitanteAdapter(list, this,this,blockDeleteAndCreate);
		setListAdapter(adapter);
	}
	
	private void registerNewCaller(){
		Solicitante obj= new Solicitante();
		obj.setOcorrenciaId(ocorrencia.get_id());
		Intent intent = new Intent(SolicitanteList.this,SolicitanteForm.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, obj);
		intent.putExtras(bundle);
		startActivity(intent);
	}
	
	List<Solicitante> search() {
		SolicitanteDAO dao = new SolicitanteDAO(this);
		List<Solicitante> toReturn = dao.fetchAllFromOcorrenciaParsed(ocorrencia.get_id());
		return toReturn;
	}

	@Override
	public void positionSelect(int position) {
		delete(list.get(position));
	}

	private void delete(Solicitante obj) {
		SolicitanteDAO dao = new SolicitanteDAO(this);
		dao.delete(obj.get_id());
		list = search();
		setAdapter();
	}

	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}
}
