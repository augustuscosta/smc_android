package br.com.otgmobile.trackteam.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.adapter.VeiculoEnvolvidoCadAdapter;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.model.VeiculoEnvolvido;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.HandleListButton;
import br.com.otgmobile.trackteam.util.Session;

public class VeiculoEnvolvidoCadList extends GenericListActivity implements HandleListButton{

	private List<VeiculoEnvolvido> list;
	private VeiculoEnvolvidoCadAdapter adapter;
	private Ocorrencia ocorrencia;
	private Button socketStatus;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_veiculos_envolvidos);
		configureActivity();
		handleIntent();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}
	
	@Override
	public void onBackPressed() {
		finishVeiculoList();
	};
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu vehicles_menu) {
		MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.vehicles_menu, vehicles_menu);
        return true;
	};
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.ocorrencias_menu_photos:
        	registerNewVeiculo();
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }


	private void configureActivity() {
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}

	private void handleIntent(){
		Intent intent = getIntent();
		if(intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)){
			ocorrencia = (Ocorrencia)  intent.getExtras().get(ConstUtil.SERIALIZABLE_KEY);
			list = ocorrencia.getVeiculosEnvolvidos();
			if(list == null || list.isEmpty()){
				registerNewVeiculo();
			}else{
				setAdapter();
			}
		}
		
	}	

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		VeiculoEnvolvido obj = list.get(position);
		Intent intent = new Intent(VeiculoEnvolvidoCadList.this, VeiculoFormActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, obj);
		bundle.putInt(ConstUtil.LIST_POSITION, position);
		intent.putExtras(bundle);
		startActivityForResult(intent,ConstUtil.VEHICLE_EDIT_REQUEST);
		super.onListItemClick(l, v, position, id);
	}

	private void finishVeiculoList(){
		quitAndPassVehicles();
	}

	private void quitAndPassVehicles() {
		ocorrencia.setVeiculosEnvolvidos(list);
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		setResult(RESULT_OK,intent);
		finish();	

	}

	private void setAdapter(){
		adapter = new VeiculoEnvolvidoCadAdapter(list, this,this);
		setListAdapter(adapter);
	}

	protected void registerNewVeiculo() {
		Intent intent = new Intent(VeiculoEnvolvidoCadList.this, VeiculoFormActivity.class);
		startActivityForResult(intent, ConstUtil.VEHICLE_CAD_REQUEST);
	}

	@Override
	public void positionSelect(int position) {
		list.remove(position);
		setAdapter();
		
	}
	
	// Handling the results from the Vehicles Form
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode,Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		if (requestCode == ConstUtil.VEHICLE_CAD_REQUEST) {
			if (resultCode == RESULT_OK) {
				if(list == null) list = new ArrayList<VeiculoEnvolvido>();
				list.add( (VeiculoEnvolvido) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY));
				setAdapter();
			}
		}
		
		if (requestCode == ConstUtil.VEHICLE_EDIT_REQUEST) {
			if (resultCode == RESULT_OK) {
				Integer position = intent.getIntExtra(ConstUtil.LIST_POSITION, list.size());
				list.set( position,(VeiculoEnvolvido) intent.getExtras().get(
						ConstUtil.SERIALIZABLE_KEY));
				setAdapter();
				
			}
		}
	}
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}
	
}
