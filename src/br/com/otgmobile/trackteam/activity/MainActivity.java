package br.com.otgmobile.trackteam.activity;


import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.cam.CamPreferences;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.Session;

public class MainActivity extends GenericActivity {

	private EditText servidor;
	private EditText node;
	private EditText videoIp;
	private EditText videoPorta;
	private Button socketStatus;
	private Button bateriaStatus;
	private Button telemetriaStatus;

	
	
	@Override
	protected void onCreate(Bundle bundler) {
		super.onCreate(bundler);
		setContentView(R.layout.main);
		configureActivity();
		
	}

	private void configureActivity() {
		servidor = (EditText)findViewById(R.id.servidor);
		node = (EditText)findViewById(R.id.node);
		videoIp = (EditText)findViewById(R.id.video_ip);
		videoPorta = (EditText)findViewById(R.id.video_port);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		bateriaStatus = (Button) findViewById(R.id.bateriaStatusIcon);
		telemetriaStatus = (Button) findViewById(R.id.telemetriaStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		servidor.setText(Session.getServer(this));
		node.setText(Session.getSocketServer(this));
		videoIp.setText(CamPreferences.getServer(this));
		videoPorta.setText(CamPreferences.getPort(this).toString());
		setSocketStatus();
	}
	

	
	private void configSessionPreferences(){
		Session.setServer(servidor.getText().toString(), this);
		Session.setSocketServer(node.getText().toString(), this);
		CamPreferences.setServer(videoIp.getText().toString(), this);
		CamPreferences.setPort(Integer.parseInt(videoPorta.getText().toString()), this);
	}
	
	public void returnButtonOnClick(View view){
		finish();
	}
	
	public void confirmButtonOnClick(View view){
		configSessionPreferences();
		finish();
	}
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}

	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}

	protected void setSocketStatus() {
		SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);

		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));

			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}


	@Override
	protected void updateBatteryStatus(int percent) {
		if(percent <= 30){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_vermelha));
		}else if(percent <= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_amarela));

		}else if(percent >= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_verde));			
		}

	}

	@Override
	protected void updateTelemetriaStatusByBroadcast(String status) {
		if(status.equals(getString(R.string.connected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluegreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.blueyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluered));
			return;
		}
	}
	
	@Override
    public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    	return true;
    };

	@Override
	public void onLocationChanged(Location location) {
		super.onLocationChanged(location);
		
	}

	@Override
	public void onStatusChanged(int status) {
		super.onStatusChanged(status);
	}
	
	
	
}
