package br.com.otgmobile.trackteam.activity;

import java.io.IOException;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.adapter.SignAdapter;
import br.com.otgmobile.trackteam.database.SignDAO;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.Sign;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.FileUtil;
import br.com.otgmobile.trackteam.util.Session;

public class SignListActivity extends GenericListActivity {

	private Button socketStatus;
	private Ocorrencia ocorrencia;
	private LinearLayout signListContainer;
	private List<Sign> list;
	private SignAdapter adapter;
	private int number = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_list);
		
		configureActivity();
		handleIntent();
		fetchData();
	}

	private void configureActivity(){
		signListContainer = (LinearLayout)  findViewById(R.id.list_fotos_container);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}
	
	private void fetchData() {
		if (ocorrencia != null) {
			SignDAO dao = new SignDAO(this);
			list = dao.findFromOcorrencia(ocorrencia.get_id());
			if (list == null || list.isEmpty()) {
				captureSign();
				return;
			}
			setAdapter(list);
		}
	}
	
	private void setAdapter(List<Sign> list) {
		adapter = new SignAdapter(list, this);
		setListAdapter(adapter);
	}
	
	private void handleIntent() {
		Intent intent = getIntent();
		if (intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)) {
			ocorrencia = (Ocorrencia) intent.getExtras().get(
					ConstUtil.SERIALIZABLE_KEY);

		}

	}
		
	private void captureSign() {
		Intent intent = new Intent(SignListActivity.this, SignActivity.class);
		//intent.putExtra(android.provider.MediaStore.EXTRA_VIDEO_QUALITY,1);
		startActivityForResult(intent, ConstUtil.SIGN_REQUEST);
	}
	
	private void finishSelection() {
		ocorrencia.setSigns(list);
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		if (requestCode == ConstUtil.SIGN_REQUEST) {
			if (resultCode == RESULT_OK) {
				ocorrencia.setEnviado(false);
				byte[] bitmapArray = (byte[]) intent.getExtras().get("sign");
				Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapArray , 0, bitmapArray.length);
				saveSignToOcorrencia(bitmap);
			}
		}
	}
	
	private void saveSignToOcorrencia(Bitmap bitmap) {
		//Bitmap signHighQuality;
		try {
			FileUtil fileUtil = new FileUtil();
			Sign sign = new Sign();
			sign.setImagem(calculateSignFileName());
			sign.setOcorrenciaId(ocorrencia.get_id());
			//signHighQuality = android.provider.MediaStore.Images.Media.getBitmap();
			fileUtil.writeBitmapToFile(fileUtil.convertBitmapToArray(bitmap), sign.getImagem(),this);
			saveSignPath(sign);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			refresh();
		}
	}
	
	private String calculateSignFileName() {
		if (list != null || !list.isEmpty() || number < list.size()) {
			number = list.size();
		} else {
			number++;
		}
		String signTag = ConstUtil.OCORRENCIATAG
				+ Integer.toString(ocorrencia.get_id()) 
				+ ConstUtil.SIGNTAG
				+ Integer.toString(number);
		return signTag;
	}
	
	private void saveSignPath(Sign obj) {
		SignDAO dao = new SignDAO(this);
		dao.create(obj);
	}

	private void refresh() {
		SignDAO dao = new SignDAO(this);
		list = dao.findFromOcorrencia(ocorrencia.get_id());
		if (list != null) {
			setAdapter(list);
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}
	
	@Override
	public void onBackPressed(){
		setListAdapter(null);
		AppHelper.unbindDrawables(findViewById(R.id.list_sign_container));
		System.gc();
		finishSelection();
		super.onBackPressed();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		setListAdapter(null);
		unbindDrawables(findViewById(R.id.list_sign_container));
	}
	
	private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
        view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
            unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
        ((ViewGroup) view).removeAllViews();
        }
    }
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sign_list, menu);
		return true;
	}

	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.new_sign:
        	captureSign();
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}
}
