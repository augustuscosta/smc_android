package br.com.otgmobile.trackteam.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.cloud.rest.RestClient;
import br.com.otgmobile.trackteam.cloud.rest.SessionCloud;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

import com.crittercism.app.Crittercism;

public class Login extends GenericActivity {
	
	private EditText userField;
	private EditText passField;
	private Button loginButton;
	private static LoginTask loginTask;
	private TextView deviceIdTextView;
	private TextView versionInfoText;
	String version = "";
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        LogUtil.initLog(this, null);
        Crittercism.init(getApplicationContext(), "51955ebbc463c27dd5000002");
        version = AppHelper.getInstance().getAppVersion(this);
        showSplashScreen();
    }
    

	@Override
    protected void onResume() {
		checkToken();
    	finishTask();
        configureActivity();
        clean();
    	super.onResume();
    }
    
    @Override
    protected void onPause() {
    	finishTask();
    	super.onPause();
    }
    
    private void checkToken() {
		if(Session.getToken(this) != null && Session.getToken(this).length() > 0){
			Intent intent = new Intent(Login.this, MapViewActivity.class);
			startActivity(intent);
		}
	}



	private void finishTask(){
		if(loginTask != null){
			loginTask.cancel(true);
			loginTask = null;
		}
	}
    
    private void configureActivity(){
        userField = (EditText) findViewById(R.id.username);
		passField = (EditText) findViewById(R.id.password);
		loginButton = (Button) findViewById(R.id.login_button);
		deviceIdTextView = (TextView) findViewById(R.id.device_id);
		deviceIdTextView.setText(Session.getDeviceId(this));
		versionInfoText = (TextView) findViewById(R.id.version_info_text);
		versionInfoText.setText(version);
		
    }
    
    public void loginButonClick(View view){
    	doLogin();
    }
    
    public void cleanButonClick(View view){
    	clean();
    }
    
    private void clean() {
		userField.setText("");
		passField.setText("");
	}

	private void doLogin(){
    	if (loginTask != null)
			return;

		String login   = userField.getText().toString();
		String senha   = passField.getText().toString();
		

		if (login.length() == 0 || senha.length() == 0) {
			AppHelper.getInstance().presentError(this, getResources().getString(R.string.ERROR_MISSINGINFO_TITLE),
					getResources().getString(R.string.ERROR_MISSINGINFO));
			return;
		}
		
		finishTask();
		loginTask = new LoginTask(this);
		loginTask.execute(login, senha);
    }
	
	public void securityButtonOnClick(View view){
		showMenuSecurity();
	}
    
    
    private class LoginTask extends AsyncTask<String, Void, RestClient> {
		ProgressDialog mDialog;
		Context context;
		String erroMessage = null;
		String user;
		String password;
		SessionCloud cloud;
		
		LoginTask(Context context) {
			loginButton.setEnabled(false);
			this.context = context;
			mDialog = ProgressDialog.show(context, "", getString(R.string.processing), true, false);
			mDialog.setCancelable(false);
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		@Override
		public RestClient doInBackground(String... params){
			user = params[0];
			password = params[1];
			return login();
		}

		RestClient login(){
			cloud = new SessionCloud();
			try {
				cloud.login(user, password, context);
			} catch (Exception e) {
				erroMessage = e.getLocalizedMessage();
				return cloud;
			}
			if(cloud.getResponseCode() != 200){
				erroMessage = cloud.getErrorMessage();
				return cloud;
			}
			return cloud;
		}

		@Override
		public void onPostExecute(RestClient cloud) {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			loginButton.setEnabled(true);
			loginTask = null;
			mDialog.dismiss();
			if (erroMessage != null) {
				if(cloud.getResponseCode() == 404){
					AppHelper.getInstance().presentError(context,getResources().getString(R.string.ERROR_CONNECTION),getResources().getString(R.string.WRONG_USER_OR_PASSWORD));
				}else{
					AppHelper.getInstance().presentError(context,getResources().getString(R.string.ERROR_CONNECTION),erroMessage);
				}
			} else {
				checkToken();
			}
				
		}
	}
    
    private void showSplashScreen() {
    	startActivity(new Intent(Login.this, SplashScreen.class));
	}

    @Override
    public boolean onPrepareOptionsMenu(android.view.Menu menu) {
    	return true;
    };

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onStatusChanged(int status) {
		// TODO Auto-generated method stub
		
	}
}