package br.com.otgmobile.trackteam.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.Session;

public class MaterialManagementActivity extends GenericActivity {
	
	private Button socketStatus;
	private Button bateriaStatus;
	private Button telemetriaStatus;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.material_operations);
		configureActivity();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	};
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
	
	private void configureActivity() {
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		bateriaStatus = (Button) findViewById(R.id.bateriaStatusIcon);
		telemetriaStatus = (Button) findViewById(R.id.telemetriaStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}


	public void OnClickViewMaterials(View view){
		seeAvailableMaterials();
	}
	
	public void OnClickRequestMaterials(View view){
		requestMaterialsToUse();
	}	
	
	public void OnClickLocateMaterials(View view){
		signalMaterialOnStreet();
	}
	
	private void signalMaterialOnStreet() {
		Intent intent = new Intent(MaterialManagementActivity.this,LocalizaMaterialActivity.class);
		startActivityForResult(intent, ConstUtil.MATERIAL_ON_STREET);		
	}

	private void seeAvailableMaterials(){
		Intent intent = new Intent(MaterialManagementActivity.this,MaterialList.class);
		startActivityForResult(intent, ConstUtil.MATERIAL_REQUEST);
	}

	private void requestMaterialsToUse() {
		Intent intent = new Intent(MaterialManagementActivity.this,MaterialRequestActivity.class);
		startActivityForResult(intent, ConstUtil.MATERIAL_CHECKLIST);
	}
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}

	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}

	protected void setSocketStatus() {
		SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);

		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));

			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}


	@Override
	protected void updateBatteryStatus(int percent) {
		if(percent <= 30){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_vermelha));
		}else if(percent <= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_amarela));

		}else if(percent >= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_verde));			
		}

	}

	@Override
	protected void updateTelemetriaStatusByBroadcast(String status) {
		if(status.equals(getString(R.string.connected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluegreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.blueyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluered));
			return;
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		super.onLocationChanged(location);
	}

	@Override
	public void onStatusChanged(int status) {
		super.onStatusChanged(status);
	}

	
}
