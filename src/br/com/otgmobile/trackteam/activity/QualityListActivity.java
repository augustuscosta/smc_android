package br.com.otgmobile.trackteam.activity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.cam.CamPreferences;

/*
 * Activity that is displayed when clicking on "Quality" in the option menu of the main activity
 */

public class QualityListActivity extends GenericListActivity {

	public static int DefaultBitRate = -1;
	private ListView listView;
	
	private int fps, br, resX, resY;
	
    /* User can choose a quality among those */
    private String[] resolutions = new String[] {
    		"640x480",
    		"320x240",
    		"176x144"  		
    };
    private String[] framerates = new String[] {
    		"20 fps",
    		"15 fps",
    		"10 fps",
    		"8 fps"
    };
    private String[] bitrates = new String[] {
    		"Default",
    		"1000 kb/s",
    		"500 kb/s",
    		"100 kb/s",
    		"50 kb/s",
    		"10 kb/s"
    };
    
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        
        setListAdapter(new CustomAdapter(this, new String[] {getString(R.string.resolution)
        		,getString(R.string.framerate),getString(R.string.bitrate)}
        , new String[][] {resolutions,framerates,bitrates}));
        
        listView = getListView();
        listView.setItemsCanFocus(false);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        
    }
    
    protected void onResume() {
    	super.onResume();
		resX = CamPreferences.getResX(this);
		resY = CamPreferences.getResY(this);
		fps = CamPreferences.getFps(this);
		br = CamPreferences.getBr(this);
		updateSelection();
    }
    
    public void onListItemClick(ListView l, View v, int position, long id) {
    	
    	Adapter a = listView.getAdapter();
    	Pattern p;
    	Matcher m;
    	
    	switch ((int )id) {
    	
    	// User changes resolution
    	case 0:
    		p = Pattern.compile("(\\d+)x(\\d+)");
    		m = p.matcher((String) a.getItem(position)); m.find();
    		resX = Integer.parseInt(m.group(1));
    		resY = Integer.parseInt(m.group(2));
    		break;
    		
    	// User changes framerate
    	case 1:
    		p = Pattern.compile("(\\d+)[^\\d]+");
    		m = p.matcher((String) a.getItem(position)); m.find();
    		fps = Integer.parseInt(m.group(1));
    		break;
    		
    	// User changes bitrate
    	case 2:
    		p = Pattern.compile("(\\d+)[^\\d]+");
    		m = p.matcher((String) a.getItem(position));
    		br = m.find()?Integer.parseInt(m.group(1)):DefaultBitRate;
    		break;
    	
    	}
    	
    	updateSelection();
 
    	CamPreferences.setResX(resX, this);
    	CamPreferences.setResY(resY, this);
    	CamPreferences.setFps(fps, this);
    	CamPreferences.setBr(br, this);
    	
    	finish();

    }
	
    private void updateSelection() {
    
    	listView.clearChoices();
    	
		listView.setItemChecked(getId(resX+"x"+resY),true);
		listView.setItemChecked(getId(fps+" fps"),true);
		listView.setItemChecked(getId(br==DefaultBitRate?"Default":br+" kb/s"),true);
    	
    }
    
	private int getId(String s) {
		
		Adapter a = listView.getAdapter();
		
		for (int i=0;i<a.getCount();i++) {
			if (s.equals((String) a.getItem(i))) return i;
		}
		
		return -1;
		
	}
    
    
    private class CustomAdapter extends BaseAdapter {

    	private LayoutInflater inflater = null;
    	
    	private String [] headers;
    	private String [][] items;
    	private int count = 0;
    	
    	CustomAdapter(Context context, String[] headers, String[][] items) {
    		
    		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    		this.headers = headers;
    		this.items = items;
    		
    		for (int i=0;i<items.length;i++) 
    			count+=items[i].length+1;
    		
    	}
    	
		@Override
		public int getCount() {
			return count;
		}

		@Override
		public Object getItem(int position) {
			int[] section = getSection(position);
			
			if (section[1] == 0) 
				return headers[section[0]];
			else 
				return items[section[0]][section[1]-1];

		}

		@Override
		public long getItemId(int position) {
			return (long) getSection(position)[0];
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			View view;
			TextView text;
			
			int[] section = getSection(position);
			
			if (section[1]==0) {
				view = inflater.inflate(R.layout.quality_header, parent, false);
			}
			else {
				view = inflater.inflate(R.layout.quality_item, parent, false);
			}
			
			text = (TextView) view;
			text.setText( (String) getItem(position));
			
			return view;
		}

		private int[] getSection(int position) {
			
			int p = 0, s = 0, sum = 0;
			
			for (int i=0;i<items.length;i++) {
				if (position>=sum && position<sum+items[i].length+1) {
					s = i;
					p = position-sum;
					break;
				}
				sum += items[i].length+1;
			}
			
			return new int[] {s, p};
			
		}
		
    }



}
