package br.com.otgmobile.trackteam.activity;

import java.util.List;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.adapter.VeiculoEnvolvidoAdapter;
import br.com.otgmobile.trackteam.cloud.rest.VeiculosEnvolvidosCloud;
import br.com.otgmobile.trackteam.database.VeiculoEnvolvidoDAO;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.model.VeiculoEnvolvido;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.LogUtil;
import br.com.otgmobile.trackteam.util.Session;

public class VeiculoEnvolvidoList extends GenericListActivity {

	private List<VeiculoEnvolvido> list;
	private VeiculoEnvolvidoAdapter adapter;
	private EditText searchPlaca;
	private OnlineSearchTask onlineSearchTask;
	private VeiculoEnvolvidoDAO veiculoEnvolvidoDAO;
	private VeiculosEnvolvidosCloud veiculosEnvolvidosCloud;
	private LinearLayout listVeiculoContainer;
	private Button socketStatus;
	private Button bateriaStatus;
	private Button telemetriaStatus;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_vehicles);
		configureActivity();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	};
	
	private void configureActivity() {
		searchPlaca = (EditText) findViewById(R.id.vehicle_search);			
		searchPlaca.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
		listVeiculoContainer = (LinearLayout) findViewById(R.id.list_vehicle_container);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		bateriaStatus = (Button) findViewById(R.id.bateriaStatusIcon);
		telemetriaStatus = (Button) findViewById(R.id.telemetriaStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		VeiculoEnvolvido obj = list.get(position);
		Intent intent = new Intent(VeiculoEnvolvidoList.this, VeiculoDetail.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, obj);
		intent.putExtras(bundle);
		startActivity(intent);
		super.onListItemClick(l, v, position, id);
	}

	private void finishVeiculoList(){
		finish();
	}

	private void setAdapter(){
		if(list == null || list.isEmpty()){
			Toast.makeText(VeiculoEnvolvidoList.this, getString(R.string.sem_registros), Toast.LENGTH_LONG).show();
		}
		adapter = new VeiculoEnvolvidoAdapter(list, this);
		setListAdapter(adapter);
	}

	public void searchButtonOnClick(View view) {
		runOnlineSearchTask();
	}

	public  void returnButtonOnClick(View view){
		finishVeiculoList();
	}
	
	private void runOnlineSearchTask(){
		onlineSearchTask = new OnlineSearchTask(this, searchPlaca.getText().toString());
		onlineSearchTask.execute();
	}
	
	private class OnlineSearchTask extends AsyncTask<Void,Void, List<VeiculoEnvolvido>> {
		ProgressDialog mDialog;
		Context context;
		String  search;

		public OnlineSearchTask(Context context, String search) {
			this.context = context;
			this.search  = search;
			mDialog = ProgressDialog.show(context, "",
					getString(R.string.processing), true, false);
			mDialog.setCancelable(false);
		}

		@Override
		protected void onPreExecute() {
			getWindow()
			.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			setProgressBarIndeterminate(true);
		}
		
		List<VeiculoEnvolvido> onlineSearch(String query) throws Exception{
			return veiculosEnvolvidosCloud().search(context,query);
		}
		
		@Override
		protected List<VeiculoEnvolvido> doInBackground(Void... params) {
			
			try {
				return onlineSearch(search);
			} catch (Exception e) {
				LogUtil.e("erro ao tentar buscar veiculos pesquisados", e);
				return null;
			}
		}

		@Override
		protected void onPostExecute(List<VeiculoEnvolvido> result) {
			if(result == null || result.isEmpty()){
				list = veiculoEnvolvidoDAO().search(search);
			}else{
				for (VeiculoEnvolvido veiculoEnvolvido : result) {
					veiculoEnvolvidoDAO().save(veiculoEnvolvido);
				}
				list = result;
			}
			mDialog.cancel();
			setAdapter();
			super.onPostExecute(result);
		}		
		
	}
	
	// Lazy instances
	
	private VeiculoEnvolvidoDAO veiculoEnvolvidoDAO() {
		if(veiculoEnvolvidoDAO == null){
			veiculoEnvolvidoDAO= new VeiculoEnvolvidoDAO(this);
		}
		
		return veiculoEnvolvidoDAO;
	}
	
	private VeiculosEnvolvidosCloud veiculosEnvolvidosCloud() {
		if(veiculosEnvolvidosCloud == null) {
			veiculosEnvolvidosCloud = new VeiculosEnvolvidosCloud();
		}
		return veiculosEnvolvidosCloud;
	}
	
	//Releasing memory on finish
	
	@Override
	protected void onDestroy() {
		setListAdapter(null);
		AppHelper.unbindDrawables(listVeiculoContainer);
		super.onDestroy();
	}
	
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}
	
	@Override
	protected void updateBatteryStatus(int percent) {
		if(percent <= 30){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_vermelha));
		}else if(percent <= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_amarela));
			
		}else if(percent >= 60){
			bateriaStatus.setText(Integer.toString(percent));
			bateriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bateria_verde));			
		}
		
	}
	
	@Override
	protected void updateTelemetriaStatusByBroadcast(String status) {
		if(status.equals(getString(R.string.connected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluegreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.blueyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			telemetriaStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluered));
			return;
		}
	}
}
