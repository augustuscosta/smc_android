package br.com.otgmobile.trackteam.activity;

import java.io.IOException;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.adapter.FotoAdapter;
import br.com.otgmobile.trackteam.database.FotoDAO;
import br.com.otgmobile.trackteam.model.Foto;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.FileUtil;
import br.com.otgmobile.trackteam.util.Session;

public class FotoListActivity extends GenericListActivity {
	private Ocorrencia ocorrencia;
	private List<Foto> list;
	private FotoAdapter adapter;
	private int number = 0;
	private LinearLayout fotoListContainer;
	private Button socketStatus;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.list_fotos);
		configureActivity();
		handleIntent();
		fetchData();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}

	
	@Override
	public void onBackPressed(){
		setListAdapter(null);
		AppHelper.unbindDrawables(findViewById(R.id.list_fotos_container));
		System.gc();
		finishSelection();
		super.onBackPressed();
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu photos_menu) {
		MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.photos_menu, photos_menu);
        return true;
	};
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
         
        switch (item.getItemId())
        {
        case R.id.photos_menu_take_photo:
        	captureFoto();
            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	private void configureActivity() {
		fotoListContainer = (LinearLayout)  findViewById(R.id.list_fotos_container);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}

	private void fetchData() {
		if (ocorrencia != null) {
			FotoDAO dao = new FotoDAO(this);
			list = dao.findFromOcorrencia(ocorrencia.get_id());
			if (list == null || list.isEmpty()) {
				captureFoto();
				return;
			}
			setAdapter(list);
		}
	}

	private void setAdapter(List<Foto> list) {
		adapter = new FotoAdapter(list, this);
		setListAdapter(adapter);
	}

	private void handleIntent() {
		Intent intent = getIntent();
		if (intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)) {
			ocorrencia = (Ocorrencia) intent.getExtras().get(
					ConstUtil.SERIALIZABLE_KEY);

		}

	}


	private void captureFoto() {
		Intent cameraIntent = new Intent(
				android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		cameraIntent.putExtra(android.provider.MediaStore.EXTRA_VIDEO_QUALITY,1);
		startActivityForResult(cameraIntent, ConstUtil.CAMERA_PIC_REQUEST);
	}

	private void finishSelection() {
		ocorrencia.setFotos(list);
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, ocorrencia);
		intent.putExtras(bundle);
		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		if (requestCode == ConstUtil.CAMERA_PIC_REQUEST) {
			if (resultCode == RESULT_OK) {
				ocorrencia.setEnviado(false);
				savePictureToOcorrencia((Bitmap) intent.getExtras().get("data"));
			}
		}
	}

	private void savePictureToOcorrencia(Bitmap bitmap) {
		try {
			FileUtil fileUtil = new FileUtil();
			Foto foto = new Foto();
			foto.setImagem(calculateFotoFileName());
			foto.setOcorrenciaId(ocorrencia.get_id());
			fileUtil.writeBitmapToFile(fileUtil.convertBitmapToArray(bitmap), foto.getImagem(),this);
			saveFotoPath(foto);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			refresh();
		}
	}

	private String calculateFotoFileName() {
		if (list != null || !list.isEmpty() || number < list.size()) {
			number = list.size();
		} else {
			number++;
		}
		String fotoTag = ConstUtil.OCORRENCIATAG
				+ Integer.toString(ocorrencia.get_id()) 
				+ ConstUtil.FOTOTAG
				+ Integer.toString(number);
		return fotoTag;
	}

	private void saveFotoPath(Foto obj) {
		FotoDAO dao = new FotoDAO(this);
		dao.create(obj);
	}

	private void refresh() {
		FotoDAO dao = new FotoDAO(this);
		list = dao.findFromOcorrencia(ocorrencia.get_id());
		if (list != null) {
			setAdapter(list);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		setListAdapter(null);
		unbindDrawables(findViewById(R.id.list_fotos_container));
	}
	
	 private void unbindDrawables(View view) {
	        if (view.getBackground() != null) {
	        view.getBackground().setCallback(null);
	        }
	        if (view instanceof ViewGroup) {
	            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
	            unbindDrawables(((ViewGroup) view).getChildAt(i));
	            }
	        ((ViewGroup) view).removeAllViews();
	        }
	    }
	
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}
	
	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}
		
		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}
		
		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
		
		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}
	
	protected void setSocketStatus() {
		 SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);
		
		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			
			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}

}
