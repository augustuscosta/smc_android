package br.com.otgmobile.trackteam.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.cloud.rest.GoogleGeolocationCloud;
import br.com.otgmobile.trackteam.gps.DLGps;
import br.com.otgmobile.trackteam.model.Endereco;
import br.com.otgmobile.trackteam.model.SocketConectionStatus;
import br.com.otgmobile.trackteam.util.AppHelper;
import br.com.otgmobile.trackteam.util.ConstUtil;
import br.com.otgmobile.trackteam.util.Session;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class SelectLocationMap extends GenericFragmentActivity implements OnMapLongClickListener{

	private GoogleMap map;
	private EditText addressTextView;
	private static GeocoderTask geocodeTask;
	private static ReverseGeocoderTask reverseGeocodeTask;
	private double latitude;
	private double longitude;
	private LinearLayout selectMapButtonContainer;
	private Button socketStatus;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_location_map);
		initMap();
		configureActivity();

		Location location = DLGps.getLastLocation(this);
		if(location != null)
			centerMap(getLatLngByLocation(location));
	}
	
	private void initMap(){
		SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        map = fm.getMap();
        map.setMyLocationEnabled(true);
        map.setOnMapLongClickListener(this);
	}

	private void configureActivity() {
		addressTextView = (EditText) findViewById(R.id.addressTextView);
		socketStatus = (Button) findViewById(R.id.socketStatusIcon);
		gpsStatusIcon = (Button) findViewById(R.id.gpsStatusIcon);
	}

	@Override
	protected void onPause() {
		finishTask();
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		setSocketStatus();
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};


	private void finishTask() {
		if (geocodeTask != null) {
			geocodeTask.cancel(true);
			geocodeTask = null;
		}
		if (reverseGeocodeTask != null) {
			reverseGeocodeTask.cancel(true);
			reverseGeocodeTask = null;
		}
	}

	public void searchAddressButtonOnClick(View view){
		String addressInput = addressTextView.getText().toString();
		if(!addressIsValid(addressInput)){
			AppHelper.getInstance().presentError(view.getContext(), getResources().getString(R.string.not_valid),
					getResources().getString(R.string.address_not_valid));
			return;
		}
		finishTask();
		reverseGeocodeTask = new ReverseGeocoderTask(this);
		reverseGeocodeTask.execute(addressInput);
	}

	public void returnButtonClick(View view){
		finish();
	}

	public void menuButtonClick(View view){
		togleMapMenu();
	}

	private void togleMapMenu() {
		if(selectMapButtonContainer.getVisibility() == View.GONE){
			selectMapButtonContainer.setVisibility(View.VISIBLE);
		}else{
			selectMapButtonContainer.setVisibility(View.GONE);
		}

	}

	private boolean addressIsValid(String addressInput){
		if(addressInput == null || addressInput.length() < 5){
			return false;
		}
		return true;
	}

	protected LatLng getLatLngByLocation(Location location) {
		return new LatLng(location.getLatitude(), location.getLongitude());
	}

	private void centerMap(LatLng latLng) {
		map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        map.animateCamera(CameraUpdateFactory.zoomTo(17));
	}




	 @Override
	 public void onMapLongClick(LatLng point) {
		 latitude = point.latitude;
		 longitude = point.longitude;
		 locationSelectedByUser();
	 }

	private void setSearchOverlay(LatLng point,String address){
		map.clear();
		MarkerOptions marker = new MarkerOptions()
        .position(point)
        .title(address)
        .snippet(getResources().getString(R.string.address))
        .icon(BitmapDescriptorFactory.fromResource(R.drawable.bt_position_red));
		map.addMarker(marker);
	}

	private void locationSelectedByUser() {
		finishTask();
		geocodeTask = new GeocoderTask(this);
		geocodeTask.execute();
	}

	private class GeocoderTask extends AsyncTask<String, Void, String> {
		GoogleGeolocationCloud cloud;
		Context context;
		ProgressDialog mDialog;
		String address;

		GeocoderTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "",
					getString(R.string.processing), true, false);
			mDialog.setCancelable(false);
			cloud = new GoogleGeolocationCloud();
		}

		@Override
		protected void onPreExecute() {
			setProgressBarIndeterminate(true);
		}

		@Override
		public String doInBackground(String... params) {
			return search();
		}

		String search() {
			try {
				String foundAddress = cloud.geocode(new LatLng(latitude, longitude));
				if(foundAddress != null && foundAddress.length() > 0){
					address = foundAddress;
					return null;

				}else{
					return context.getResources().getString(R.string.address_not_found);
				}
			}
			catch (Exception e) {
				return context.getResources().getString(R.string.search_not_possible);
			}

		}

		@Override
		public void onPostExecute(String erroMessage) {
			geocodeTask = null;
			setProgressBarIndeterminate(false);
			mDialog.dismiss();

			if(erroMessage != null){
				AppHelper.getInstance().presentError(context, getResources().getString(R.string.address_not_found),
						getResources().getString(R.string.error_location));
			}else if(address != null){
				Endereco endereco = new Endereco();
				endereco.setEndGeoref(address);
				endereco.setLatitude((float)latitude);
				endereco.setLongitude((float)longitude);
				Intent intent = new Intent();
				Bundle bundle = new Bundle();
				bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, endereco);
				intent.putExtras(bundle);
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}

	private class ReverseGeocoderTask extends AsyncTask<String, Void, String> {
		Context context;
		ProgressDialog mDialog;
		LatLng latLng;
		String searchAddress;
		GoogleGeolocationCloud cloud;

		ReverseGeocoderTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "",
					getString(R.string.processing), true, false);
			mDialog.setCancelable(false);
			cloud = new GoogleGeolocationCloud();
		}

		@Override
		protected void onPreExecute() {
			setProgressBarIndeterminate(true);
		}

		@Override
		public String doInBackground(String... params) {
			searchAddress = params[0];
			return search();
		}

		String search() {
			try {
				latLng = cloud.reverseGeocode(searchAddress, context);
				if(latLng == null){
					return context.getResources().getString(R.string.address_not_found);
				}
			}
			catch (Exception e) {
				return context.getResources().getString(R.string.search_not_possible);
			}
			return null;
		}

		@Override
		public void onPostExecute(String erroMessage) {
			reverseGeocodeTask = null;
			setProgressBarIndeterminate(false);
			mDialog.dismiss();

			if(erroMessage != null){
				AppHelper.getInstance().presentError(context, getResources().getString(R.string.error),erroMessage);
			}else if(latLng != null){
				centerMap(latLng);
				setSearchOverlay(latLng,searchAddress);
			}
		}
	}


	protected class Listener extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("FINISH_ACTIVITY")) {
				finish();
			}
		}
	}
	@Override
	protected void showToastWithConectionStatus(String status) {
		updateStatusByBroadCast(status);
		super.showToastWithConectionStatus(status);
	}

	private void updateStatusByBroadCast(final String status) {
		if(status.equals(getString(R.string.connected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			return;
		}

		if(status.equals(getString(R.string.connecting))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));
			return;
		}

		if(status.equals(getString(R.string.dsconnected))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_error))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}

		if(status.equals(getString(R.string.connection_failure))){
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			return;
		}
	}

	protected void setSocketStatus() {
		SocketConectionStatus socketConn = Session.getSocketConectionStatus(this);

		switch (socketConn.getValue()) {
		case 2:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wigreen));
			break;
		case 1:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wiyellow));

			break;
		case 0:
			socketStatus.setBackgroundDrawable(getResources().getDrawable(R.drawable.wired));
			break;

		default:
			break;
		}
	}

}
