package br.com.otgmobile.trackteam.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import br.com.otgmobile.trackteam.R;
import br.com.otgmobile.trackteam.model.Enquete;
import br.com.otgmobile.trackteam.model.Ocorrencia;
import br.com.otgmobile.trackteam.util.ConstUtil;

public class EnqueteListActivity extends GenericListActivity {
	
	 ListView listView ;
	 List<Enquete> list;
	 Ocorrencia ocorrencia;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_enquete_list);
		handleIntent();
		setList();
    }
	
	private void handleIntent() {
		Intent intent = getIntent();
		if (intent.hasExtra(ConstUtil.SERIALIZABLE_KEY)) {
			ocorrencia = (Ocorrencia) intent.getExtras().get(ConstUtil.SERIALIZABLE_KEY);
			if(ocorrencia.getEnquetes() != null){
				list = new ArrayList<Enquete>();
				for(Enquete enquete : ocorrencia.getEnquetes()){
					list.add(enquete);
				}
			}
		}
	}
	
	private void setList(){
		// Get ListView object from xml
        listView = getListView();
        
        // Defined Array values to show in ListView
        ArrayList<String> values;
        values = new ArrayList<String>();
        if(list != null){
	        for(Enquete enquete:list){
	        	values.add(enquete.getNome());
	        }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
          android.R.layout.simple_list_item_1, android.R.id.text1, values);


        // Assign adapter to ListView
        listView.setAdapter(adapter); 
        
        // ListView Item Click Listener
        listView.setOnItemClickListener(new OnItemClickListener() {

              @Override
              public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                
               // 
               Enquete enquete = list.get(position);
               
               Intent intent = new Intent(EnqueteListActivity.this,
       				PerguntaListActivity.class);
	       		Bundle bundle = new Bundle();
	       		bundle.putSerializable(ConstUtil.SERIALIZABLE_KEY, enquete);
	       		bundle.putInt(ConstUtil.ID_KEY, ocorrencia.getId());
	       		intent.putExtras(bundle);
	       		startActivity(intent);
              }

         }); 
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return true;
	};
} 	
